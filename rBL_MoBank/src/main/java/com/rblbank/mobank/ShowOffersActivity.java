package com.rblbank.mobank;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.rblbank.mobank.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.snapwork.ethreads.UserTask;
import com.snapwork.messages.ContactDetails;
import com.snapwork.messages.ReplyItem;
import com.snapwork.parser.ContactsParser;
import com.snapwork.util.ProxyUrlUtil;

//import android.util.Log;  CordovaPlu

public class ShowOffersActivity extends MapActivity implements OnClickListener,
		LocationListener, ReplyItem {

	ListView mOffersListView;
	/**
	 * user`s current location latitude String.
	 */
	private String CurrentLat;
	/**
	 * user`s current location longitude String.
	 */
	private String CurrentLon;
	/**
	 * Location latitude of the City Entered by user from Offers Home or
	 * Selected from Adv. Search City List.
	 */
	private String latEnterCity = null;
	/**
	 * Location longitude of the City Entered by user from Offers Home or
	 * Selected from Adv. Search City List.
	 */
	private String lonEnterCity = null;
	/**
	 * user`s current location latitude 'double'.
	 */
	private double Currlatitude/* =28.56843 */; // New Delhi
	/**
	 * user`s current location longitude 'double'.
	 */
	private double Currlongitude/* =77.218902 */;
	// private double Currlatitude=19.56843; //mumbai
	// private double Currlongitude=73.218902;
	MapView mMapView;
	LinearLayout offersListLayout;
	ReplyItem itm = this;
	Context ctx = this;
	private LocationManager lm;
	private List<ContactDetails> curLocationDetail;
	private List<Overlay> mapOffersOverlays;
	private GetLocation mGetLocation;

	private String Locality;
	/**
	 * String that contain either 'City' entered by user to search Offers arounf
	 * or 'Category' to search with in!
	 */
	private String cityOrCategory = "";
	/**
	 * current City Selected by User to get Offers Around from
	 * 'AdvancSearchActivity'.
	 */
	String citySelected = "";
	ImageView imgBtnShowList, imgBtnshowMap, imgBtnfilterOffers,
			imgBtnadvSearch, imgBtnback, imgBtnMenu;
	TextView textCategorySelected;
	/**
	 * Category that was clicked from 'filter'
	 */
	String clickedCategory;

	/**
	 * Text Field that is show when no 'Filter' are found!
	 */
	TextView noFilterFound;

	/**
	 * Text Field that show currently clicked 'Filter'. On click of this user
	 * navigate back to the categories filter.
	 */
	TextView filterCategoryText;

	/**
	 * Text View that is show only when NO OFFERS fond!
	 */
	TextView noffers;
	/**
	 * List of all category.
	 */
	private ArrayList<String> categoryArrayList = new ArrayList<String>();
	ListView categoryListView;

	/**
	 * Flag that indicate that whether or not an item of 'Category' is clicked.
	 * Its value(false->true) get updated when user click any of the available
	 * category.
	 * 
	 */
	boolean isCategoryItemClicked = false;
	/**
	 * Category item Position that was clicked.
	 */
	private int position_clicked = -1;

	/**
	 * List of all Cuinines.
	 */
	private ArrayList<String> cuisineArrayList = new ArrayList<String>();
	ListView cuisineListView;
	/**
	 * List of all Area.
	 */
	private ArrayList<String> areaArrayList = new ArrayList<String>();
	ListView areaListView;
	/**
	 * List of all 'area' that are used to 'Filter' all offers. gc...
	 */
	private ArrayList<String> filterOffersByThisAreaList = new ArrayList<String>();
	/**
	 * 'area' that are used to 'Filter' all offers. As, functionality changed,
	 * Now user can select only one area to filter Offers.
	 */
	private String filterOffersByThisArea = "";

	String cuisineStringTOFilterOffer = "", areaStringTOFilterOffer = "";

	/**
	 * List of all 'cuisine' that are used to 'Filter' all offers.
	 */
	private ArrayList<String> filterOffersByThisCuisineList = new ArrayList<String>();
	/**
	 * Lastly selected List of all 'cuisine' that are used to 'Filter' all
	 * offers.
	 */
	private ArrayList<String> last_filterOffersByThisCuisineList = new ArrayList<String>();

	/**
	 * Flag that indicate, whether or not user click on 'Filter' GO button to
	 * filter 'Offers'. Its value is 'False' always. but turned to 'true' when
	 * user have click on 'Go' and as soon as it enters in to 'GetOffersData'
	 * its value become set to 'False'.<br>
	 * 
	 * <b>Why i use this Flag_</b><br>
	 * since i am using the same logic(method) to get 'offers' whether user have
	 * getting these offers data by offers Item Click(carousel) or By entering
	 * City or By clicking Filte`s Go button and hence to identify from where i
	 * have came to here to get data of all 'Offers'!!! <br>
	 * and, in case of 'Filter' i have to pass some extra param(e.g., Cuisine &
	 * Area)
	 * 
	 */
	private boolean fromFilterGoFlag = false;

	/**
	 * Flag that indicate, whether or not user click/come from AdvSearch(B`coz
	 * it call onNewIntent when come from AdvSearch) on 'Search' GO button of
	 * 'AvdSearch' to get 'Offers'. Its value is 'False' always. but turned to
	 * 'true' when user have click on 'Search' Button of
	 * <b>'AdvancSearchActivity'</b> as soon as it enters in to 'GetOffersData'
	 * its value become set to 'False'.<br>
	 * 
	 * <b>Why i use this Flag_</b><br>
	 * since i am using the same logic(method) to get 'offers' whether user have
	 * getting these offers data by offers Item Click(carousel) or By entering
	 * City or By clicking Filte`s Go button and hence to identify from where i
	 * have came to here to get data of all 'Offers'!!! <br>
	 * and, in case of 'Filter' i have to pass some extra param(e.g., Cuisine &
	 * Area)
	 * 
	 */
	private boolean fromAdvSearchFlag = false;

	/**
	 * Bundle that contains all data selected by user on Advance Search
	 * Activity. It updated each time when user go to AdvSearch & come again on
	 * this Activity by Clicking 'Search' Button!
	 */
	private Bundle advSearcDatahBundle = null;

	/**
	 * Current category which was clicked by user from 'carousel'.
	 */
	private String currentCategory = "";
	/**
	 * Is user Click on any category or enter a city/area on Offer`s HOME.
	 * 'True' if clicks on a category 'False' otherwise.
	 */
	private boolean fromCurrentLocation;

	/**
	 * Message when no Filter is selected by user and he clicked on 'go' button.
	 */
	private String NO_FILTER_SELECTED = "Please select a filter!";
	// gc...
	// static final String[] FRUITS = new String[] {
	// "Restaurant 1","Restaurant 2", "Restaurant 3", "Restaurant 4",
	// "Restaurant 5"};
	private ProgressDialog mPdialog = null;

	Typeface fontFace;

	private Resources res;
	private String noNetwork = null;

	/**
	 * Massage when unable to conncet to the HDFC server.
	 */
	private String UNABLE_TO_REACH = null;

	/**
	 * Adapter that hold all 'Offers' data.
	 */
	OffersDataAdapter oDataAdapter;

	GetOffersData getOffersData;
	GetCityOffersData getCityOffersData;
	/**
	 * List of type `HashMap<String, String>` contains data of all 'OFFERS'.
	 */

	/**
	 * URL to get 'Offers' from.<br>
	 * <br>
	 * Live "http://app.mapmyindia.com/hdfcapi/poi.do"<br>
	 * UAT "http://122.180.94.253:8080/hdfcapi/poi.do/"
	 */
	private String PostUrl = null;

	/**
	 * Cuisine position ArrayList<Boolean> that hold the value for each cusine
	 * check box that whether the current cuisine at concern position is
	 * selected lastly or not.
	 */
	public ArrayList<Boolean> positionArray_c = new ArrayList<Boolean>();
	/**
	 * Lastly selected Cuisine position ArrayList<Boolean> that hold the value
	 * for each cusine check box that whether the current cuisine at concern
	 * position is selected lastly or not.
	 */
	public ArrayList<Boolean> last_positionArray_c = new ArrayList<Boolean>();

	/**
	 * Area position ArrayList<Boolean> that hold the value for each Area radio
	 * button that whether the current area at concern position is selected
	 * lastly or not.
	 */
	public ArrayList<Boolean> positionArray_a = new ArrayList<Boolean>();// gc...
																			// b`coz
																			// now
																			// refreshing
																			// A
																			// list
																			// everytime...
																			// as
																			// Paresh
																			// said
																			// to
																			// do...

	/**
	 * Flag that indicate me whether to update 'Cuisine' & 'Area' list or not.<br>
	 * <i> default is 'true' to indicate that user is coming from 'HDFC-HOME'
	 * and then update it to 'false' in 'doInBackground' of 'GetOffersData' as i
	 * am using it to build 'Cuisine' n 'Area' list to populate FILTER
	 * drawer.</i> <br>
	 * <br>
	 * <b> ---NOTE---</b><br>
	 * As the need of the situation is that_ i hvae to kept the 'Cuisine' &
	 * 'Area' list as it is throughout which i get in 1st hit untill user came
	 * back & come again or goes to AvSearch n then come here cy clicking 'go'.<br>
	 * <br>
	 * 
	 * update if 'true' n vice versa.
	 */
	boolean isFIrstTime = true;

	/**
	 * City that user entered either from HDFC-Offers Home or from ADv.Search.
	 * <i>Update its value when come from Home or from Adv.Search.
	 */
	private String enteredCity = "";

	/**
	 * Position of the current selected 'Area-Radio Button'.
	 */
	int selectedAreaRadioPosition = -1;
	/**
	 * Lastly selected Position of the 'Area-Radio Button'.
	 */
	int last_selectedAreaRadioPosition = -1;

	private List<HashMap<String, String>> offersDataList = new ArrayList<HashMap<String, String>>();

	// private List<OffersBean> offersDataList = new ArrayList<OffersBean>();
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Log.v("ShowOffersActivity",
		// "onCreate--- ShowOffersActivity Actvity!");
		// no more this
		// setContentView(R.layout.activity_main_1);
		setContentView(R.layout.show_offers_activity);

		res = getResources();
		noNetwork = res.getString(R.string.no_network);

		UNABLE_TO_REACH = res.getString(R.string.unable_to_connect);

		PostUrl = res.getString(R.string.OFFERS_LIVE_URL);

		fontFace = Typeface.createFromAsset(getBaseContext().getAssets(),
				"fonts/hlr___.ttf");

		// update redius in to Preferences...
		SharedPreferences sharedPreferences = getSharedPreferences("RADIUS",
				MODE_WORLD_READABLE);
		Editor editor = sharedPreferences.edit();
		editor.putInt("radius", 10000);
		editor.commit();

		// Show 'Offers' belongs to TextLevel...
		textCategorySelected = (TextView) findViewById(R.id.text_category_selected);
		textCategorySelected.setText("All");

		noffers = (TextView) findViewById(R.id.no_offers_found);
		noffers.setTypeface(fontFace);
		noffers.setText("Sorry there are no offers in your area currently. We are adding exciting offers everyday. Do come back and check later.");

		imgBtnback = (ImageView) findViewById(R.id.backbutton);
		imgBtnback.setOnClickListener(this);

		imgBtnfilterOffers = (ImageView) findViewById(R.id.filter);
		imgBtnfilterOffers.setOnClickListener(this);

		imgBtnShowList = (ImageView) findViewById(R.id.img_btn_show_list);
		imgBtnShowList.setOnClickListener(this);

		imgBtnshowMap = (ImageView) findViewById(R.id.img_btn_show_map);
		imgBtnshowMap.setOnClickListener(this);

		imgBtnadvSearch = (ImageView) findViewById(R.id.adv_search);
		imgBtnadvSearch.setOnClickListener(this);

		imgBtnMenu = (ImageView) findViewById(R.id.menu);
		imgBtnMenu.setOnClickListener(this);

		// Map View that show Offers on Map
		mMapView = (MapView) findViewById(R.id.mapview);
		mMapView.setBuiltInZoomControls(true);
		FrameLayout.LayoutParams zoomParams = new FrameLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ZoomControls controls = (ZoomControls) mMapView
				.getZoomButtonsController().getZoomControls();
		// controls.setGravity(Gravity.RIGHT);
		zoomParams.setMargins(0, 0, 0, 25);
		controls.setLayoutParams(zoomParams);

		offersListLayout = (LinearLayout) findViewById(R.id.linLayoutOfferList);

		// ListView View that show Offers in List
		mOffersListView = (ListView) findViewById(R.id.offers_list);

		// to set transparent divider in b/w list items!
		mOffersListView.setDividerHeight(10);
		mOffersListView.setDivider(getResources().getDrawable(
				android.R.color.transparent));

		oDataAdapter = new OffersDataAdapter(this, offersDataList);
		mOffersListView.setAdapter(oDataAdapter);

		// mListView.setAdapter(new ArrayAdapter<String>(this,
		// android.R.layout.simple_list_item_1, FRUITS));

		mOffersListView.setTextFilterEnabled(true);
		mOffersListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// Toast.makeText(getApplicationContext(),
				// FRUITS[position],Toast.LENGTH_SHORT).show();
				// oDataAdapter.getItem(position);
				// Log.v("Item Clicked",
				// oDataAdapter.getItem(position).toString()+",\n NAME:"+oDataAdapter.getItem(position).get("name"));
				// Log.v("Item Clicked",
				// oDataAdapter.getItem(position).toString()+",\n NAME:"+oDataAdapter.getItem(position).getName());//OffersBean

				Intent intent = new Intent(ShowOffersActivity.this,
						OffersDetailActivity.class);
				// Build bundle of offer details...
				Bundle bndl = new Bundle();
				bndl.putString("address",
						oDataAdapter.getItem(position).get("address"));
				bndl.putString("otype",
						oDataAdapter.getItem(position).get("otype"));
				bndl.putString("name",
						oDataAdapter.getItem(position).get("name"));
				bndl.putString("offers",
						oDataAdapter.getItem(position).get("offers"));
				bndl.putString("category",
						oDataAdapter.getItem(position).get("category"));
				bndl.putString("subcategory", oDataAdapter.getItem(position)
						.get("subcategory"));
				bndl.putString("validity",
						oDataAdapter.getItem(position).get("validity"));
				bndl.putString("o_latitude", oDataAdapter.getItem(position)
						.get("latitude"));
				bndl.putString("o_longitude", oDataAdapter.getItem(position)
						.get("longitude"));

				// User`s current Location.
				if (CurrentLon != null && CurrentLon != null) {
					bndl.putString("c_latitude", CurrentLat);
					bndl.putString("c_longitude", CurrentLon);
					bndl.putBoolean("from_current_location", true);
					bndl.putString("Locality", Locality);

				} else {
					bndl.putString("c_latitude", latEnterCity);
					bndl.putString("c_longitude", lonEnterCity);
					bndl.putBoolean("from_current_location", false);

				}
				// bndl.putString("c_latitude", CurrentLat);
				// bndl.putString("c_longitude", CurrentLon);
				intent.putExtras(bndl);// putExtra("offer_detail", bndl);

				startActivity(intent);
			}
		});

		if (ProxyUrlUtil.isNetworkAvailable(ShowOffersActivity.this)) {
			// do...

			/*
			 * Getting the LocationManager to obtain user`s Current Location.
			 */
			Location location = null;
			if (lm == null)
				lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			String provider = lm.getBestProvider(criteria, false);
			String netProvider = LocationManager.NETWORK_PROVIDER;
			String gpsProvider = LocationManager.GPS_PROVIDER;

			/*
			 * NEW LOACTION START...
			 */
			boolean gps_enabled = false, network_enabled = false;

			try {
				gps_enabled = lm
						.isProviderEnabled(LocationManager.GPS_PROVIDER);
				network_enabled = lm
						.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			} catch (Exception ex) {

			}

			if (!gps_enabled && !network_enabled) {
				enableLocationSettings();
				return;

			}

			if (provider.equalsIgnoreCase(netProvider)) {
				location = lm
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			} else if (provider.equalsIgnoreCase(gpsProvider)) {
				location = lm
						.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				if (location == null)
					;
				{
					location = lm
							.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
				}
			} else {
				location = lm
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}

			// Currently close as i m requesting the location of user in
			// 'GetOffers'...
			if (location != null) {
				onLocationChanged(location);
				// mGetLocation = (GetLocation) new GetLocation().execute();
			}
			lm.requestLocationUpdates(provider, 20000, 0, this);

			// ---------------LOCATION END---------------------

			// Getting adapter by passing xml data ArrayList
			// LazyAdapter adapter = new LazyAdapter(this, FRUITS);//demo adp.
			// mOffersListView.setAdapter(adapter);

			// Epoch Tech LAT-LONG 19.120517,73.014053
			String lat = String.valueOf(28.978603);
			String lon = String.valueOf(79.398413);
			// Current Lat Long
			// String lat=CurrentLat;//String.valueOf(28.978603);
			// String lon=CurrentLon;//String.valueOf(79.398413);

			/*
			 * get data passed from previous Activity! Since this bundle is
			 * coming from 'MainGridActivity' which is not present in WL hence
			 * assign the default values over here...
			 */
			Bundle bundle = getIntent().getExtras();
			// String category=bundle.getString("category");
			cityOrCategory = "";// bundle.getString("cityOrCategory");
			fromCurrentLocation = true;// bundle.getBoolean("fromCorrentLocation");

			if (cityOrCategory != null && !cityOrCategory.equalsIgnoreCase(""))// RTE...
																				// in
																				// 'S2,
																				// S3'
																				// it
																				// will
																				// recreate
																				// Activity
																				// from
																				// start.
																				// why?
				textCategorySelected.setText(cityOrCategory);

			// if (ProxyUrlUtil.isNetworkAvailable(ShowOffersActivity.this)) {
			// //do...
			if (fromCurrentLocation) {
				/*
				 * ENter here, means user have clicked a 'Category' Item.
				 * cityOrCategory always contain 'Category', here only.
				 */
				currentCategory = cityOrCategory;
				changeOffersListBachground();

				// Using Test Location...
				// getOffersData = (GetOffersData) new
				// GetOffersData().execute(CurrentLat, CurrentLon,
				// cityOrCategory!=null?cityOrCategory:"");

				// Using User`s Actual Current Location
				// Log.v("Oncreate:GetOffersData Call-",
				// "CurrentLat:"+CurrentLat+", CurrentLon:"+CurrentLon+", cityOrCategory:"+cityOrCategory);
				getOffersData = (GetOffersData) new GetOffersData().execute(
						CurrentLat, CurrentLon,
						cityOrCategory != null ? cityOrCategory : "");

			} else {

				// Set Default Image... If NO category...
				offersListLayout.setBackgroundDrawable(getResources()
						.getDrawable(R.drawable.bg_mix1));

				/**
				 * City that user entered either from HDFC-Offers Home or from
				 * ADv.Search...
				 */
				enteredCity = cityOrCategory;

				/*
				 * ENter here, means user have entered a 'City/Area' and click
				 * 'go' button.
				 */
				getCityOffersData = (GetCityOffersData) new GetCityOffersData()
						.execute(cityOrCategory);
			}
		} else {
			Toast.makeText(ShowOffersActivity.this, noNetwork, 1).show();

		}

	}

	/**
	 * Method to launch Settings to enable GPS/Network listener.
	 */
	private void enableLocationSettings() {
		// Intent settingsIntent = new
		// Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		// startActivity(settingsIntent);

		LayoutInflater infalter = LayoutInflater.from(ShowOffersActivity.this);
		View addressDialog = infalter.inflate(R.layout.addressdialog, null);

		final Myalert alert = new Myalert(ShowOffersActivity.this);
		alert.setInverseBackgroundForced(true);
		alert.setCancelable(false);
		alert.setView(addressDialog, 0, 0, 0, 0);
		alert.setIcon(android.R.drawable.btn_default);

		final ImageView close = (ImageView) addressDialog
				.findViewById(R.id.exit);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				alert.cancel();
				// ShowOffersActivity.this.finish();

			}
		});

		((TextView) addressDialog.findViewById(R.id.addtitle))
				.setText("Check Location services!");

		((TextView) addressDialog.findViewById(R.id.adddetails))
				.setText("Location Services are currently turned Off. Please enable the same");
		// show it
		alert.show();

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

	}

	/**
	 * Change the background Image of offers List according to the 'Category'
	 * selected.
	 */
	private void changeOffersListBachground() {
		// TODO Auto-generated method stub
		// TO set Background Image of Concern Item According to 'Category' it
		// belongs to...
		if (currentCategory.equalsIgnoreCase("Apparels")
				|| currentCategory.equalsIgnoreCase("Shopping")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_shopping));
		} else if (currentCategory.equalsIgnoreCase("Dining")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_restaurant));
		} else if (currentCategory.equalsIgnoreCase("Electronics")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_electronics));
		} else if (currentCategory.equalsIgnoreCase("Fuel")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_fuel));
		} else if (currentCategory.equalsIgnoreCase("Grocery")
				|| currentCategory.equalsIgnoreCase("Retail")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_retail));
		} else if (currentCategory.equalsIgnoreCase("Personal Care")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_personal_care));
		} else if (currentCategory.equalsIgnoreCase("Telecom")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_telecom));
		} else if (currentCategory.equalsIgnoreCase("Hotel")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_hotel));
		} else if (currentCategory.equalsIgnoreCase("Offer")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_adventure));
		} else if (currentCategory.equalsIgnoreCase("offers")) {
			// No Icon for 'offers'
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_adventure));
		} else if (currentCategory.equalsIgnoreCase("Restaurant")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_restaurant));
		} else if (currentCategory.equalsIgnoreCase("Telecommunication")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_telecom));
		} else if (currentCategory.equalsIgnoreCase("Tyre")) {
			// No Icon for 'Tyre'
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_adventure));
		} else if (currentCategory.equalsIgnoreCase("Entertainment")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_entertainment));
		} else if (currentCategory.equalsIgnoreCase("Movies")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_movies));
		} else if (currentCategory.equalsIgnoreCase("Travel")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_travel));
		} else if (currentCategory.equalsIgnoreCase("Caf�")
				|| currentCategory.equalsIgnoreCase("Cafe")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_cafe));
		} else if (currentCategory.equalsIgnoreCase("Adventure")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_adventure));
		} else if (currentCategory.equalsIgnoreCase("Sport")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_sport));
		} else if (currentCategory.equalsIgnoreCase("Others")) {
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_others));
		} else {
			// Set Default Image... If NO category...
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_mix1));
		}

	}

	@Override
	public void onNewIntent(Intent newIntent) {
		// TODO Auto-generated method stub
		super.onNewIntent(newIntent);
		// Toast.makeText(ShowOffersActivity.this,
		// "onNewIntent--- ShowOffersActivity Actvity!", 1).show();
		// Log.v("ShowOffersActivity",
		// "onNewIntent--- ShowOffersActivity Actvity!");
		advSearcDatahBundle = newIntent.getExtras();
		fromAdvSearchFlag = true;
		citySelected = advSearcDatahBundle.getString("city");
		// gc... need to remove 24.03.14
		String categorySelected = advSearcDatahBundle.getString("category");
		if (categorySelected != null && categorySelected.length() != 0) {
			currentCategory = categorySelected;
			changeOffersListBachground();
			textCategorySelected.setText(categorySelected);
		} else {
			// Set Default(Mix) Image... If NO category...
			offersListLayout.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.bg_mix1));
			textCategorySelected.setText(citySelected);
			// currentCategory = "";
			// changeOffersListBachground();
		}
		/**
		 * ---------------- +Important-NOTE: ---------------- As, After
		 * Discussion with Amit sr. If User select any 'City' from droap down
		 * then_
		 * 
		 * 1. You have to get Lat+Long of selected 'City' and then pass these
		 * Lat+Long to WebService to get all 'Offers'. In this case you not have
		 * to sent 'City' parameter to concern WS! -In this case, WS consider
		 * the Current 'City' which user select as his/her current location to
		 * get all offers... Where as TRUTH is Different user is not on to this
		 * Location! City Selected - Delhi Lat+Long of Delhi -
		 * x=28.6100&y=77.2300
		 * http://app.mapmyindia.com/hdfcapi/poi.do?x=28.6100
		 * &y=77.2300&radius=10
		 * &rtype=json/xml&type=offers&Area=&Category=&Cuisine
		 * =&Locality=&Program=&CardType=&Discount=&EMI=
		 * 
		 * 2. Pass user`s Current Location`s Lat+Long to concern WS including
		 * 'City' Parameter. This will give the Offers of/around the concern
		 * selected 'City' only! User`s Location(Mumbai) - 18.9750, 72.8258
		 * http:
		 * //app.mapmyindia.com/hdfcapi/poi.do?x=18.9750&y=72.8258&radius=10
		 * &rtype
		 * =json/xml&type=offers&City=Delhi&Area=&Category=&Cuisine=&Locality
		 * =&Program=&CardType=&Discount=&EMI=
		 * 
		 */
		if (ProxyUrlUtil.isNetworkAvailable(ShowOffersActivity.this)) {
			// do...
			if (citySelected.length() != 0) {
				// Enter here, means user have selected a City AND hence get
				// Lat+Long of this city & pass to get All Offers!
				// Log.v("ShowOffersActivity",
				// "citySelected--- ShowOffersActivity Actvity!"+citySelected);

				// Make it 'false' because user have select a CITY from
				// Adv.Search.
				fromCurrentLocation = false;
				// Using Reverse Geo coding...
				// getCityOffersData=(GetCityOffersData) new
				// GetCityOffersData().execute(citySelected);

				/*
				 * ---NOTE--- 25.03.14 Due to new requirments, NOw, once user
				 * click on 'search' button of 'AdvSearch' i got all the
				 * 'Offers' data... & location of the selected 'City' over their
				 * in 'AdvSearch' & i pass the 'offers-Data' &
				 * 'location-lat-long' from their to here.
				 * 
				 * N, Hence use the 'Offers-Data' & 'City Lat-Long' directly
				 * here with out going to 'GetCityOffersData' and change
				 * accordingly over their in 'GetOffersData`s' 'AdvSearch'
				 * flag...
				 */
				getOffersData = (GetOffersData) new GetOffersData().execute(
						advSearcDatahBundle.getString("latEnterCity"),
						advSearcDatahBundle.getString("lonEnterCity"), "");

			} else {
				// Here, Means use users current Lat+Long to get all Offeres!
				// Log.v("ShowOffersActivity",
				// "No citySelected--- ShowOffersActivity Actvity!");
				// String lat=String.valueOf(28.978603);
				// String lon=String.valueOf(79.398413);
				// getOffersData = (GetOffersData) new
				// GetOffersData().execute(lat, lon, "");

				// Make it 'true' because user have not select a CITY from
				// Adv.Search.
				if (!fromCurrentLocation) {
					/*
					 * Enter here, means 1. either user clicks on a category 1st
					 * and once get offers he/she click on AdvSearch and change
					 * the city & after getting new offers again click on
					 * AdvSearch and then does not select any CITY. And hence,
					 * now we have to get OFFERS using his/her CURRENT location!
					 */
					fromCurrentLocation = true;
				}

				// When no City selected by user then... it will get 'offers' of
				// all 'Category'
				getOffersData = (GetOffersData) new GetOffersData().execute(
						CurrentLat, CurrentLon, "");

			}
		} else {
			Toast.makeText(ShowOffersActivity.this, noNetwork, 1).show();

		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// Toast.makeText(ShowOffersActivity.this,
		// "onResume--- ShowOffersActivity Actvity!", 1).show();
		// Log.v("ShowOffersActivity",
		// "onResume--- ShowOffersActivity Actvity!");

		if (getOffersData != null) {
			// Log.v("Get-Offers",
			// "getOffersData is NOT 'NULL'. getOffersData-isDoinBAcCompleted:"+getOffersData.isDoinBAcCompleted);
			// Log.v("Get-Offers STATUS---",
			// "getOffersData Status :"+getOffersData.getStatus());
			if (getOffersData.isDoinBAcCompleted
					&& getOffersData.getStatus() == UserTask.Status.RUNNING) {
				// getOffersData.onPostExecute(getOffersData.OffersResponseData);

			} else {

			}

			/*
			 * if(getOffersData.getStatus() == UserTask.Status.PENDING){ // My
			 * AsyncTask has not started yet Log.v("Get-Offers",
			 * "getOffersData Status :"+getOffersData.getStatus()); }
			 * 
			 * if(getOffersData.getStatus() == UserTask.Status.RUNNING){ // My
			 * AsyncTask is currently doing work in doInBackground() }
			 * 
			 * if(getOffersData.getStatus() == UserTask.Status.FINISHED){ // My
			 * AsyncTask is done and onPostExecute was called }
			 */
		}

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (getOffersData != null) {
			getOffersData.cancel(true);
		}
		if (getCityOffersData != null) {
			getCityOffersData.cancel(true);
		}
		if (mGetLocation != null) {
			mGetLocation.cancel(true);
		}

		if (mPdialog != null) {
			if (mPdialog.isShowing()) {
				mPdialog.dismiss();
			}
		}

	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.filter:

			/*
			 * ---NOTE--- When NO filters Found--- Set Filters Drawer to
			 * unmounted from The Main Activity View! 07.02.2014...
			 */
			// if
			// (cuisineArrayList.size()!=0||areaArrayList.size()!=0||(cuisineArrayList.size()!=0
			// && areaArrayList.size()!=0)) {
			if (categoryArrayList.size() != 0) {

				// http://docs.xamarin.com/guides/android/user_interface/working_with_listviews_and_adapters/part_3_-_customizing_a_listview%27s_appearance/
				// http://stackoverflow.com/questions/17778257/selecting-a-specific-checkbox-in-a-long-listview-checks-multiple-checkboxes

				/*
				 * Below URL MAy help for Multiple CheckBox Selection...
				 * 30.12.13 http
				 * ://stackoverflow.com/questions/15710543/custom-style
				 * -on-dialog -checkbox
				 * http://stackoverflow.com/questions/13858974/custom-dialog
				 * -with-setmultichoiceitems
				 * http://www.yogeshblogspot.com/android-alertdialog
				 * -with-checkboxes/
				 */
				final Dialog callAlert = new Dialog(ShowOffersActivity.this,
						R.style.CutomDialog);
				// callAlert.requestWindowFeature(Window.FEATURE_NO_TITLE);
				callAlert.getWindow().setFlags(
						WindowManager.LayoutParams.FLAG_FULLSCREEN,
						WindowManager.LayoutParams.FLAG_FULLSCREEN);
				WindowManager.LayoutParams wmlp = callAlert.getWindow()
						.getAttributes();
				wmlp.gravity = Gravity.LEFT;// CENTER;
				wmlp.width = LayoutParams.WRAP_CONTENT;// ru
				wmlp.height = LayoutParams.MATCH_PARENT;// ru
				// callAlert.getWindow().setAttributes(wmlp);
				callAlert.setContentView(R.layout.filters_layout);
				// ru
				callAlert.getWindow().setLayout(LayoutParams.WRAP_CONTENT,
						LayoutParams.FILL_PARENT);

				ImageView close = (ImageView) callAlert
						.findViewById(R.id.btn_close);
				final ImageView btnGo = (ImageView) callAlert
						.findViewById(R.id.btn_go);

				// list view of all 'Categories'
				categoryListView = (ListView) callAlert
						.findViewById(R.id.category_filter_list);
				// Get ListView of 'Cuisine & Area'
				areaListView = (ListView) callAlert
						.findViewById(R.id.area_filter_list);
				cuisineListView = (ListView) callAlert
						.findViewById(R.id.cuisine_filter_list);

				// to set transparent divider in b/w list items!
				cuisineListView.setDividerHeight(10);
				cuisineListView.setDivider(getResources().getDrawable(
						android.R.color.transparent));
				areaListView.setDivider(getResources().getDrawable(
						android.R.color.transparent));
				categoryListView.setDivider(getResources().getDrawable(
						android.R.color.transparent));

				// Cusine and Area layout that contain 'Title' & their concern
				// 'List' of filter items.
				final LinearLayout byCuisineLayout = (LinearLayout) callAlert
						.findViewById(R.id.byCusine_layout);
				final LinearLayout byAreaLayout = (LinearLayout) callAlert
						.findViewById(R.id.byArea_layout);
				final LinearLayout categoryLayout = (LinearLayout) callAlert
						.findViewById(R.id.category_layout);
				final LinearLayout categoryBackLayout = (LinearLayout) callAlert
						.findViewById(R.id.back_to_category_layout);

				noFilterFound = (TextView) callAlert
						.findViewById(R.id.text_no_filter_found);
				filterCategoryText = (TextView) callAlert
						.findViewById(R.id.back_to_category_filter_text);

				ImageView bkCateArrow = (ImageView) callAlert
						.findViewById(R.id.bk_cate_arrow);
				bkCateArrow.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (categoryLayout.getVisibility() == View.GONE) {
							categoryLayout.setVisibility(View.VISIBLE);

							// hide 'byCuisineLayout' and 'byAreaLayout'
							byCuisineLayout.setVisibility(View.GONE);
							byAreaLayout.setVisibility(View.GONE);

							// hide 'filterCategoryText' and 'btnGo'
							// filterCategoryText.setVisibility(View.GONE);

							// hide category back layout...
							categoryBackLayout.setVisibility(View.GONE);

							btnGo.setVisibility(View.GONE);

						}
					}
				});
				// onCLick of 'CategoryBack' show category list...
				filterCategoryText.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (categoryLayout.getVisibility() == View.GONE) {
							categoryLayout.setVisibility(View.VISIBLE);

							// hide 'byCuisineLayout' and 'byAreaLayout'
							byCuisineLayout.setVisibility(View.GONE);
							byAreaLayout.setVisibility(View.GONE);

							// hide 'filterCategoryText' and 'btnGo'
							// filterCategoryText.setVisibility(View.GONE);

							// hide category back layout...
							categoryBackLayout.setVisibility(View.GONE);

							btnGo.setVisibility(View.GONE);

						}
					}
				});

				// Set Adapter for 'Cuisine & Area' ListView
				// areaListView.setAdapter(new
				// AreaDataAdapter(ShowOffersActivity.this,
				// R.layout.filterlist_row, areaArrayList));
				// cuisineListView.setAdapter(new
				// CuisineDataAdapter(ShowOffersActivity.this,
				// R.layout.filterlist_row, cuisineArrayList));

				// areaListView.setAdapter(new ArrayAdapter<String>(this,
				// android.R.layout.simple_list_item_1, FRUITS));
				// cuisineListView.setAdapter(new ArrayAdapter<String>(this,
				// android.R.layout.simple_list_item_1, FRUITS));

				if (categoryArrayList.size() != 0) {

					// categoryListView.setAdapter(new
					// AreaDataAdapter(ShowOffersActivity.this,
					// R.layout.filterlist_row, categoryArrayList));
					categoryListView.setAdapter(new CategoryDataAdapter(
							ShowOffersActivity.this, R.layout.category_row,
							categoryArrayList));

					categoryListView
							.setOnItemClickListener(new OnItemClickListener() {

								@Override
								public void onItemClick(AdapterView<?> aView,
										View v, int position, long id) {

									// hide 'category' list layout...
									categoryLayout.setVisibility(View.GONE);

									clickedCategory = aView.getItemAtPosition(
											position).toString();

									if (aView.getItemAtPosition(position)
											.toString()
											.equalsIgnoreCase("Dining")) {

										// For Cuisine Filters Item...
										if (cuisineArrayList.size() != 0) {
											byCuisineLayout
													.setVisibility(View.VISIBLE);
											cuisineListView
													.setAdapter(new CuisineDataAdapter(
															ShowOffersActivity.this,
															R.layout.filterlist_row,
															cuisineArrayList));

										}

										// For Area Filters Item...
										if (areaArrayList.size() != 0) {
											byAreaLayout
													.setVisibility(View.VISIBLE);
											areaListView
													.setAdapter(new AreaDataAdapter(
															ShowOffersActivity.this,
															R.layout.filterlist_row,
															areaArrayList));

										}

									} else {

										// For Area Filters Item...
										if (areaArrayList.size() != 0) {
											byAreaLayout
													.setVisibility(View.VISIBLE);
											areaListView
													.setAdapter(new AreaDataAdapter(
															ShowOffersActivity.this,
															R.layout.filterlist_row,
															areaArrayList));

										} else {

											/*---NOTE---
											 * Since no are found...
											 * In this condition u need to call WS of HDFC by passing the concern 'CATEGORY'
											 * to get all offers data...
											 */
											// remove drawer
											callAlert.dismiss();
											/*
											 * To change the background of the
											 * 'offers'...
											 */
											currentCategory = clickedCategory;
											changeOffersListBachground();

											// Change flag to 'true'
											fromFilterGoFlag = true;

											if (latEnterCity != null
													&& lonEnterCity != null) {
												// Using Enter/Selected City
												// Location...
												getOffersData = (GetOffersData) new GetOffersData()
														.execute(latEnterCity,
																lonEnterCity,
																clickedCategory);
												// getOffersData =
												// (GetOffersData) new
												// GetOffersData().execute(latEnterCity,
												// lonEnterCity, "");
											} else {
												// Using User`s Actual Current
												// Location...
												getOffersData = (GetOffersData) new GetOffersData()
														.execute(CurrentLat,
																CurrentLon,
																clickedCategory);
												// getOffersData =
												// (GetOffersData) new
												// GetOffersData().execute(CurrentLat,
												// CurrentLon, "");
											}

										}
									}
									filterCategoryText.setText(aView
											.getItemAtPosition(position)
											.toString());
									// onCLick of 'CategoryBack' show category
									// list...
									categoryBackLayout
											.setVisibility(View.VISIBLE);
									// make category back layoput visible...
									// filterCategoryText.setVisibility(View.VISIBLE);.
									btnGo.setVisibility(View.VISIBLE);

								}
							});

				} else {
					Toast.makeText(ShowOffersActivity.this, "No filter found!",
							0).show();

				}

				/*
				 * ---NOTE--- Below is for showing Message when NO filters
				 * available... But later as disscussed with Salil... we not
				 * need to show 'Filter' when no filters found...
				 */
				if (cuisineArrayList.size() == 0 && areaArrayList.size() == 0) {
					// Show No Filter Text...
					if (noFilterFound.getVisibility() == View.GONE) {
						noFilterFound.setVisibility(View.VISIBLE);
					}
				} else {
					// Hide No Filter if Visible...
					if (noFilterFound.getVisibility() == View.VISIBLE) {
						noFilterFound.setVisibility(View.GONE);
					}

				}

				/*
				 * Goes into the 'Category' list click...
				 */
				// //For Cuisine Filters Item...
				// if (cuisineArrayList.size()!=0) {
				//
				// if (byCuisineLayout.getVisibility()==View.GONE) {
				// //Toast.makeText(ShowOffersActivity.this, "Enter in RU_"+ru,
				// 0).show();
				// byCuisineLayout.setVisibility(View.VISIBLE);
				//
				// }
				//
				// cuisineListView.setAdapter(new
				// CuisineDataAdapter(ShowOffersActivity.this,
				// R.layout.filterlist_row, cuisineArrayList));
				// //gc...
				// //cuisineListView.setAdapter(new
				// CuisineDataAdapter(ShowOffersActivity.this,
				// R.layout.filterlist_row, categoryArrayList));
				//
				// } else {
				// //Toast.makeText(ShowOffersActivity.this, "Enter in RU_"+ru,
				// 0).show();
				// if (byCuisineLayout.getVisibility()==View.VISIBLE) {
				// byCuisineLayout.setVisibility(View.GONE);
				// //cuisineArrayList.add("INdian");
				//
				// }
				//
				// }
				//
				// //For Area Filters Item...
				// if (areaArrayList.size()!=0) {
				//
				// if (byAreaLayout.getVisibility()==View.GONE) {
				// //Toast.makeText(ShowOffersActivity.this, "Enter in RU_"+ru,
				// 0).show();
				// byAreaLayout.setVisibility(View.VISIBLE);
				//
				// }
				//
				// areaListView.setAdapter(new
				// AreaDataAdapter(ShowOffersActivity.this,
				// R.layout.filterlist_row, areaArrayList));
				//
				// } else {
				// //Toast.makeText(ShowOffersActivity.this, "Enter in RU_"+ru,
				// 0).show();
				// if (byAreaLayout.getVisibility()==View.VISIBLE) {
				// byAreaLayout.setVisibility(View.GONE);
				// //areaArrayList.add("TEST AREA");
				//
				// }
				//
				// }
				//
				// onclick of this 'btnGo' you have to get the 'Offers'
				// according to 'Filter' items checked!
				// btnGo.setOnClickListener(ShowOffersActivity.this);
				btnGo.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						/*
						 * Update 'last_positionArray_c' to 'positionArray_c'
						 */
						// last_positionArray_c=
						if (last_positionArray_c != null
								&& last_positionArray_c.size() != 0) {
							last_positionArray_c.clear();
						}
						last_positionArray_c.addAll(positionArray_c);

						// areaStringTOFilterOffer=filterOffersByThisAreaList.toString().substring(1,
						// filterOffersByThisAreaList.toString().length() -
						// 1).replace(", ", ",");
						areaStringTOFilterOffer = filterOffersByThisArea;

						/**
						 * Since, user have clicked on 'go' button hence update
						 * the lastly selected 'position'
						 */
						last_selectedAreaRadioPosition = selectedAreaRadioPosition;

						cuisineStringTOFilterOffer = filterOffersByThisCuisineList
								.toString()
								.substring(
										1,
										filterOffersByThisCuisineList
												.toString().length() - 1)
								.replace(", ", ",");

						/*
						 * Update 'last_filterOffersByThisCuisineList' to
						 * 'filterOffersByThisCuisineList'
						 */
						if (last_filterOffersByThisCuisineList != null
								&& last_filterOffersByThisCuisineList.size() != 0) {
							last_filterOffersByThisCuisineList.clear();
						}
						last_filterOffersByThisCuisineList
								.addAll(filterOffersByThisCuisineList);

						// clear 'cuisine' & 'area' list used to filter. be`coz
						// we not need old item when click on go & agin come
						// here!
						filterOffersByThisAreaList.clear();// gc...

						/*---NOTE--- 03.03.14
						 * As per new requirements_ 04.03.14 Tuesday.
						 * We need to remember the lastly selected 'cuisine' & 'area' that`s why we need not to cleare this 
						 * 'filterOffersByThisCuisineList' & 'filterOffersByThisArea' 
						 * 
						 * .....This is autometically updated if user select or diselect any 'Cuisine'.....
						 */
						// filterOffersByThisCuisineList.clear();
						/*
						 * as per new flow! user can select only one area to
						 * filter Offers. But if you want to refresh 'area' list
						 * then need to clear/reset lastly selected area
						 * string...
						 * 
						 * BUT ... As now we need to remember the last selected
						 * 'area' we need to keep the last selected area!
						 */
						// filterOffersByThisArea="";

						/*
						 * clear all 'cuisines' & 'area'... But not from here,
						 * clear it from 'GetOffersData' task if you find/got
						 * new data
						 */
						// areaArrayList.clear();
						// cuisineArrayList.clear();

						// Change flag to 'true'
						fromFilterGoFlag = true;

						// Rudrapur Location
						String lat = String.valueOf(28.978603);
						String lon = String.valueOf(79.398413);
						// Executr AsynTask to get 'Filtered' data.
						/*
						 * NOTE: Change 'lat-long' to dynamic it later... when
						 * API response is correct and as expacted!
						 */
						if (ProxyUrlUtil
								.isNetworkAvailable(ShowOffersActivity.this)) {
							// do...

							// Using Test Location...
							// getOffersData = (GetOffersData) new
							// GetOffersData().execute(lat, lon,
							// currentCategory);
							if (cuisineStringTOFilterOffer.length() != 0
									|| areaStringTOFilterOffer.length() != 0) {
								// remove drawer
								callAlert.dismiss();

								/*
								 * To change the background of the 'offers'...
								 */
								currentCategory = clickedCategory;
								changeOffersListBachground();

								if (latEnterCity != null
										&& lonEnterCity != null) {
									// Using Enter/Selected City Location...
									// getOffersData = (GetOffersData) new
									// GetOffersData().execute(latEnterCity,
									// lonEnterCity, currentCategory);
									getOffersData = (GetOffersData) new GetOffersData()
											.execute(latEnterCity,
													lonEnterCity,
													clickedCategory);
									// getOffersData = (GetOffersData) new
									// GetOffersData().execute(latEnterCity,
									// lonEnterCity, "");
								} else {
									textCategorySelected
											.setText(clickedCategory);

									// Using User`s Actual Current Location...
									// getOffersData = (GetOffersData) new
									// GetOffersData().execute(CurrentLat,
									// CurrentLon, currentCategory);
									getOffersData = (GetOffersData) new GetOffersData()
											.execute(CurrentLat, CurrentLon,
													clickedCategory);
									// getOffersData = (GetOffersData) new
									// GetOffersData().execute(CurrentLat,
									// CurrentLon, "");
								}
							} else {
								Toast.makeText(ShowOffersActivity.this,
										NO_FILTER_SELECTED, 1).show();

							}

						} else {
							// remove drawer when no N/W connection...
							callAlert.dismiss();
							Toast.makeText(ShowOffersActivity.this, noNetwork,
									1).show();

						}

					}
				});
				close.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						callAlert.dismiss();
						// filterOffersByThisCuisineList.clear();//gc...

						System.out.println("last_positionArray_c:"
								+ last_positionArray_c
								+ ", last_filterOffersByThisCuisineList:"
								+ last_filterOffersByThisCuisineList);
						if (positionArray_c != null
								&& positionArray_c.size() != 0) {
							positionArray_c.clear();
						}
						positionArray_c.addAll(last_positionArray_c);
						// positionArray_c=last_positionArray_c;

						/*
						 * Update 'last_positionArray_c' to 'positionArray_c'
						 */
						if (filterOffersByThisCuisineList != null
								&& filterOffersByThisCuisineList.size() != 0) {
							filterOffersByThisCuisineList.clear();
						}
						filterOffersByThisCuisineList
								.addAll(last_filterOffersByThisCuisineList);

						System.out.println("positionArray_c:" + positionArray_c
								+ ", filterOffersByThisCuisineList:"
								+ filterOffersByThisCuisineList);

						/**
						 * Since, user have clicked on 'close' button hence
						 * revert the 'selectedAreaRadioPosition' position to
						 * lastly selected 'position' N... The text string as
						 * well that contains the value of lastly selected
						 * 'Area'
						 */
						selectedAreaRadioPosition = last_selectedAreaRadioPosition;
						filterOffersByThisArea = areaStringTOFilterOffer;

						// for (HashMap.Entry<Integer, Integer> entry :
						// lastlySelectedCusinePositionList.entrySet()) {
						// System.out.println(entry.getKey() + "/" +
						// entry.getValue());
						// // positionArray_c.set(entry.getValue(), true);.
						//
						// for (int i = 0; i < positionArray_c.size(); i++) {.
						// if (((Integer)entry.getKey()).equalsIgnoreCase(i)) {
						// positionArray_c.set(entry.getValue(), true);
						// filterOffersByThisCuisineList.add(cuisineArrayList.get(entry.getValue()));
						// } else {
						// if (lastlySelectedCusinePositionList.containsKey(i))
						// {
						// //DO nothing as it is already in the last selected
						// cusine list...
						// } else {
						// positionArray_c.set(entry.getValue(), false);
						// }
						//
						// }
						// }
						//
						// //
						// filterOffersByThisCuisineList.add(cuisineArrayList.get(entry.getValue()));
						//
						// }

						// Iterator<Integer> iter =
						// lastlySelectedCusinePositionList.keySet().iterator();
						// while(iter.hasNext()) {
						// Integer key = (Integer)iter.next();
						// Integer val =
						// (Integer)lastlySelectedCusinePositionList.get(key);
						// System.out.println("key,val: " + key + "," + val);
						//
						// for (int i = 0; i < positionArray_c.size(); i++) {.
						// if (val.equalsIgnoreCase(i)) {
						// System.out.println("UUUU");
						// // if (positionArray_c.get(i)) {
						// positionArray_c.set(val, true);
						// // } else {
						// //
						// // }
						//
						// filterOffersByThisCuisineList.add(cuisineArrayList.get(val));
						//
						// } else {
						// if (lastlySelectedCusinePositionList.containsKey(i))
						// {
						// //DO nothing as it is already in the last selected
						// cusine list...
						// System.out.println("VVV");
						// } else {
						// positionArray_c.set(val, false);
						// System.out.println("WWW");
						// }
						//
						// }
						// }
						//
						// }

						// filterOffersByThisCuisineList.add(cuisineArrayList.get(entry.getValue()));

					}
				});

				callAlert.show();

			} else {
				Toast.makeText(ShowOffersActivity.this,
						"No filters available!", 1).show();

			}

			/*
			 * //gc... AlertDialog.Builder alert = new
			 * AlertDialog.Builder(this); alert.setTitle("Final Result");
			 * alert.setMessage("HEllo..."); alert.setPositiveButton("OK", new
			 * DialogInterface.OnClickListener() { public void
			 * onClick(DialogInterface dialog, int which) { // TODO
			 * Auto-generated method stub } });
			 * 
			 * // dlgAlert.create(); AlertDialog dialog_card = alert.create();
			 * // dlgAlert.requestWindowFeature(Window.FEATURE_NO_TITLE); //
			 * WindowManager.LayoutParams WMLP =
			 * dialog_card.getWindow().setGravity(Gravity.TOP);
			 * dialog_card.show();
			 */
			break;
		case R.id.btn_go:
			// Not using this...
			Toast.makeText(ShowOffersActivity.this, "Filter 'go' clicked!", 0)
					.show();
			// Change flag to 'true'
			fromFilterGoFlag = true;
			// Executr AsynTask to get 'Filtered' data.
			getOffersData = (GetOffersData) new GetOffersData().execute(
					CurrentLat, CurrentLon, "");

			/*
			 * When Click 'Filter`s Go' button. Now simply you have to get the
			 * Text value of all checked(Checkbox) items. and pass them to WS to
			 * get all 'Offers'!
			 */

			break;
		case R.id.adv_search:
			startActivity(new Intent(ShowOffersActivity.this,
					AdvancSearchActivity.class));

			break;
		case R.id.img_btn_show_list:
			// textCategorySelected.setVisibility(textCategorySelected.getVisibility()==View.VISIBLE?View.GONE:View.VISIBLE);
			/*
			 * imgBtnshowMap.setImageDrawable(getResources().getDrawable(
			 * R.drawable.show_mapview_btn_normal));
			 * imgBtnShowList.setImageDrawable(getResources().getDrawable(
			 * R.drawable.show_listview_btn_selected));
			 */
			imgBtnshowMap.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.show_mapview_btn_normal));
			imgBtnShowList.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.show_listview_btn_selected));
			if (mMapView.getVisibility() == View.VISIBLE) {
				mMapView.setVisibility(View.GONE);
				offersListLayout.setVisibility(View.VISIBLE);
				// mOffersListView.setVisibility(View.VISIBLE);
				// textCategorySelected.setVisibility(View.VISIBLE);
			}
			if (offersDataList.size() == 0) {
				if (noffers.getVisibility() == View.GONE) {
					// set Text Level NO-OFFERS to be VISIBLE!
					noffers.setText("No Offers Found!");
					noffers.setVisibility(View.VISIBLE);
				}
			}
			// Toast.makeText(ctx,
			// "List View is Visible:"+offersListLayout.isShown()+", offersDataList Size:"+offersDataList.size(),
			// 1).show();

			break;
		case R.id.img_btn_show_map:
			// textCategorySelected.setVisibility(textCategorySelected.getVisibility()==View.VISIBLE?View.GONE:View.VISIBLE);
			/*
			 * imgBtnShowList.setImageDrawable(getResources().getDrawable(
			 * R.drawable.show_listview_btn_normal));
			 * imgBtnshowMap.setImageDrawable(getResources().getDrawable(
			 * R.drawable.show_mapview_btn_selected));
			 */
			imgBtnShowList.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.show_listview_btn_normal));
			imgBtnshowMap.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.show_mapview_btn_selected));
			if (mOffersListView.getVisibility() == View.VISIBLE) {
				mMapView.setVisibility(View.VISIBLE);
				offersListLayout.setVisibility(View.GONE);
				// mOffersListView.setVisibility(View.GONE);
				// textCategorySelected.setVisibility(View.GONE);

			}
			if (noffers.getVisibility() == View.VISIBLE) {
				// set Text Level NO-OFFERS to be GONE!
				noffers.setVisibility(View.GONE);
			}

			break;
		case R.id.backbutton:
			// On Back Button Press
			ShowOffersActivity.this.finish();
			// Intent intent=new Intent(ShowOffersActivity.this,
			// OffersActivity.class);
			// startActivity(intent);
			break;
		case R.id.menu:
			Intent intent = new Intent(ShowOffersActivity.this, RBL_iBank.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			// intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			// Bundle bundle = new Bundle();
			// bundle.putString("test", "smdashboardlist");
			// intent.putExtras(bundle);
			finish();
			startActivity(intent);
		default:
			break;
		}
	}

	/**
	 * My custom ALert Dialog!
	 * 
	 * @author Rupesh
	 * 
	 */
	class Myalert extends AlertDialog {

		protected Myalert(Context context, int theme) {
			super(context, theme);
		}

		protected Myalert(Context context) {
			super(context);
		}

	}

	static class OffersViewHolder {
		TextView offtext, offdis, offdist;
		ImageView offIcont;
	}

	/**
	 * Adapter that hold data of all 'Offers'.
	 * 
	 * @author Rupesh
	 * 
	 */
	public class OffersDataAdapter extends
			ArrayAdapter<HashMap<String, String>> {
		private List<HashMap<String, String>> pla;
		private Activity context;

		public OffersDataAdapter(Activity context,
				List<HashMap<String, String>> pla) {
			super(ShowOffersActivity.this, R.layout.list_row, pla);
			this.context = context;
			this.pla = pla;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			OffersViewHolder holder;
			final int clickPos = position;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.list_row, parent, false);
				holder = new OffersViewHolder();
				holder.offtext = (TextView) row
						.findViewById(R.id.text_offer_list_item_title);
				holder.offtext.setTypeface(fontFace);
				holder.offdis = (TextView) row
						.findViewById(R.id.text_offer_list_item_disc);
				holder.offdis.setTypeface(fontFace);
				holder.offIcont = (ImageView) row.findViewById(R.id.list_image);
				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (OffersViewHolder) row.getTag();
			}
			// ofer=pla.get(position).get("name");//gc...
			holder.offtext.setText(pla.get(position).get("name"));
			holder.offdis.setText(pla.get(position).get("offers"));

			// TO set Image of Concern Item According to 'Category' it belongs
			// to...
			String offerCategory = pla.get(position).get("category");

			if (offerCategory.equalsIgnoreCase("Apparels")
					|| offerCategory.equalsIgnoreCase("Shopping")) {
				holder.offIcont
						.setImageResource(R.drawable.apparels_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Dining")) {
				holder.offIcont
						.setImageResource(R.drawable.resturant_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Electronics")) {
				holder.offIcont
						.setImageResource(R.drawable.electronics_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Fuel")) {
				holder.offIcont.setImageResource(R.drawable.fuel_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Grocery")
					|| offerCategory.equalsIgnoreCase("Retail")) {
				holder.offIcont
						.setImageResource(R.drawable.grocery_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Personal Care")) {
				holder.offIcont
						.setImageResource(R.drawable.personal_care_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Telecom")) {
				holder.offIcont
						.setImageResource(R.drawable.telecom_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Hotel")) {
				// No icon for HOTEL...
				holder.offIcont.setImageResource(R.drawable.hotel_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Offer")) {
				holder.offIcont.setImageResource(R.drawable.offer);
			} else if (offerCategory.equalsIgnoreCase("offers")) {
				// No Icon for 'offers'
				holder.offIcont.setImageResource(R.drawable.offer);
			} else if (offerCategory.equalsIgnoreCase("Restaurant")) {
				holder.offIcont
						.setImageResource(R.drawable.resturant_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Telecommunication")) {
				holder.offIcont
						.setImageResource(R.drawable.telecom_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Tyre")) {
				// No Icon for 'Tyre'
				holder.offIcont.setImageResource(R.drawable.offer);
			} else if (offerCategory.equalsIgnoreCase("Entertainment")) {
				holder.offIcont
						.setImageResource(R.drawable.entertainment_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Movies")) {
				holder.offIcont
						.setImageResource(R.drawable.movies_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Travel")) {
				holder.offIcont
						.setImageResource(R.drawable.travel_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Caf�")
					|| offerCategory.equalsIgnoreCase("Cafe")) {
				holder.offIcont.setImageResource(R.drawable.cafe_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Adventure")) {
				holder.offIcont
						.setImageResource(R.drawable.adventure_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Sport")) {
				holder.offIcont.setImageResource(R.drawable.sport_listing_icon);
			} else if (offerCategory.equalsIgnoreCase("Others")) {
				holder.offIcont.setImageResource(R.drawable.other_listing_icon);
			} else {
				// Set Default Image... If NO category...
				holder.offIcont.setImageResource(R.drawable.other_listing_icon);
			}

			return row;

		}

	}

	static class FilterViewHolder {
		CheckBox filterCheckBox;
		RadioButton radioButton;

	}

	// ??????????????????????????????????????????????????????........................

	static class CategoryFilterViewHolder {
		TextView categoryTextView;

	}

	/**
	 * Adapter that hold data of all 'Cuisine Data'.
	 * 
	 * @author Rupesh
	 * 
	 */
	public class CategoryDataAdapter extends ArrayAdapter<String> {
		private Context ctx;
		private List<String> myList;

		public CategoryDataAdapter(Context context,
				List<String> categoryArrayList) {
			super(context, R.layout.category_row, categoryArrayList);
			this.ctx = context;
			this.myList = categoryArrayList;

		}

		public CategoryDataAdapter(ShowOffersActivity showOffersActivity,
				int filterlistRow, ArrayList<String> categoryArrayList) {
			super(showOffersActivity, filterlistRow, categoryArrayList);
			this.ctx = showOffersActivity;
			this.myList = categoryArrayList;

		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			/*
			 * Follow same as that of 'VideoActivity' Rupesh...
			 */
			View row = convertView;
			try {

				CategoryFilterViewHolder holder;
				if (row == null) {
					LayoutInflater inflater = ShowOffersActivity.this
							.getLayoutInflater();
					row = inflater
							.inflate(R.layout.category_row, parent, false);

					holder = new CategoryFilterViewHolder();
					holder.categoryTextView = (TextView) row
							.findViewById(R.id.categoryTextView);
					row.setTag(holder);
				} else {
					holder = (CategoryFilterViewHolder) row.getTag();
				}

				// set Text for the item of the CRS list.
				holder.categoryTextView
						.setText(myList.get(position).toString());

				// change colr of the text if item is clicked...
				if (isCategoryItemClicked) {
					if (position_clicked == position) {
						holder.categoryTextView
								.setTextColor(ctx.getResources().getColor(
										R.color.selected_category_text_color));
					} else {
						holder.categoryTextView.setTextColor(ctx.getResources()
								.getColor(R.color.lightBlack));
					}
				}

				// do the action needed when click on item...
				// holder.categoryTextView.setOnClickListener(new
				// OnClickListener() {
				//
				// @Override
				// public void onClick(View v) {
				// // TODO Auto-generated method stub
				//
				// isCategoryItemClicked=true;
				// position_clicked=position;
				// categoryAdapter.notifyDataSetChanged();
				//
				// // Toast.makeText(CSRActivity.this,
				// "File DL-URL:"+myList.get(position).getFiledownloadurl(),
				// 0).show();
				//
				// }
				// });

			} catch (Exception e) {
				e.printStackTrace();
			}

			return row;

		}
	}

	// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX....................................

	/**
	 * Adapter that hold data of all 'Cuisine Data'.
	 * 
	 * @author Rupesh
	 * 
	 */
	public class CuisineDataAdapter extends ArrayAdapter<String> {

		private List<String> cuisineList;
		private Activity context;

		/**
		 * Cuisine position ArrayList<Boolean> that hold the value for each
		 * cusine check box that whether the current cuisine at concern position
		 * is selected lastly or not.
		 */
		// public ArrayList<Boolean> positionArray_c;

		public CuisineDataAdapter(Context context, int textViewResourceId,
				List<String> cuisineList) {
			super(context, textViewResourceId, cuisineList);
			// TODO Auto-generated constructor stub
			// this.context=context;
			this.cuisineList = cuisineList;

			if (positionArray_c == null || positionArray_c.size() == 0) {
				System.out.println("UUUUVVV");
				/*
				 * Enter here means 'cuisines' list is 'null' & hence initialize
				 * it with the cuisine list & their respective value(t or f)
				 */
				positionArray_c = new ArrayList<Boolean>(cuisineList.size());
				for (int i = 0; i < cuisineList.size(); i++) {
					positionArray_c.add(false);
				}

			}

		}

		public CuisineDataAdapter(Activity context, List<String> cuisineList) {
			super(ShowOffersActivity.this, R.layout.filterlist_row, cuisineList);
			this.context = context;
			this.cuisineList = cuisineList;

			// Not remembering the lastly selected values...
			/*
			 * positionArray_c = new ArrayList<Boolean>(cuisineList.size());
			 * for(int i =0;i<cuisineList.size();i++){
			 * positionArray_c.add(false); }
			 */
			// remember lastly selected values...
			if (positionArray_c == null || positionArray_c.size() == 0) {
				/*
				 * Enter here means 'cuisines' list is 'null' & hence initialize
				 * it with the cuisine list & their respective value(t or f)
				 */
				positionArray_c = new ArrayList<Boolean>(cuisineList.size());
				for (int i = 0; i < cuisineList.size(); i++) {
					positionArray_c.add(false);
				}

			}
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = convertView;
			FilterViewHolder holder;
			// final int clickPos=position;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.filterlist_row, parent, false);
				holder = new FilterViewHolder();
				holder.filterCheckBox = (CheckBox) row
						.findViewById(R.id.filter_checkbox);
				holder.filterCheckBox.setTypeface(fontFace);

				// if (HDFCApp.apiLevel<=android.os.Build.VERSION_CODES.FROYO) {
				// To maintaining gap in between CheckBox and CB-TEXT... Add
				// Extra 5dp to Margin Left.
				final float scale = res.getDisplayMetrics().density;
				holder.filterCheckBox.setPadding(
						holder.filterCheckBox.getPaddingLeft()
								+ (int) (10.0f * scale + 0.5f),
						holder.filterCheckBox.getPaddingTop(),
						holder.filterCheckBox.getPaddingRight(),
						holder.filterCheckBox.getPaddingBottom());
				// } else {
				// //Do nothing...
				//
				// }

				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (FilterViewHolder) row.getTag();

				/*
				 * When a listview recycles views , it recycles its present
				 * state as well as listeners attached to it. if the checkbox
				 * was checked and has a onCheckedChangeListener set, both will
				 * remain a part of recycled view based on position. So it is
				 * our responsibility to reset all states and remove previous
				 * listeners. The listener was removed as below:-
				 */
				holder.filterCheckBox.setOnCheckedChangeListener(null);

			}
			holder.filterCheckBox.setText(cuisineList.get(position));
			holder.filterCheckBox.setFocusable(false);
			holder.filterCheckBox.setChecked(positionArray_c.get(position));
			holder.filterCheckBox.setText(cuisineList.get(position));

			holder.filterCheckBox
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub

							if (isChecked) {
								// Toast.makeText(ShowOffersActivity.this,
								// "Checked Item:"+cuisineList.get(clickPos),
								// 0).show();
								// change state of concern item in
								// 'positionArray_c' array to 'True'
								positionArray_c.set(position, true);
								// add the checked item in to cuisine list which
								// used to filter offers.
								filterOffersByThisCuisineList.add(cuisineList
										.get(position));

							} else {
								// Toast.makeText(ShowOffersActivity.this,
								// "UnChecked Item:"+cuisineList.get(clickPos),
								// 0).show();

								// change state of concern item in
								// 'positionArray_c' array to 'True'
								positionArray_c.set(position, false);
								// remove the unchecked item in to cuisine list
								// which used to filter offers.
								filterOffersByThisCuisineList
										.remove(cuisineList.get(position));

							}
						}
					});
			return row;
		}

	}

	// Not in use...
	public class MyCustomAdapter extends ArrayAdapter<String> {

		private List<String> cuisineList;
		private LayoutInflater mInflater;
		private PackageManager pm;
		ArrayList<Boolean> positionArray;
		private Context ctx;
		int[] visiblePosArray;
		private volatile int positionCheck;

		public MyCustomAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
			// TODO Auto-generated constructor stub
			this.ctx = context;
			this.cuisineList = objects;

			positionArray = new ArrayList<Boolean>(objects.size());
			for (int i = 0; i < objects.size(); i++) {
				positionArray.add(false);
			}

		}

		public MyCustomAdapter(Context context, List<String> myList) {
			super(context, NO_SELECTION);
			cuisineList = myList;
			ctx = context;
			mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			pm = context.getPackageManager();

			positionArray = new ArrayList<Boolean>(myList.size());
			for (int i = 0; i < myList.size(); i++) {
				positionArray.add(false);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return cuisineList.size();
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {

			View row = convertView;
			FilterViewHolder holder = null;
			final int clickPos = position;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.filterlist_row, parent, false);
				holder = new FilterViewHolder();
				holder.filterCheckBox = (CheckBox) row
						.findViewById(R.id.filter_checkbox);
				holder.filterCheckBox.setTypeface(fontFace);

				row.setTag(holder);
			} else {
				// holder = (View) row;
				// holder = (FilterViewHolder) row.getTag();
				holder = (FilterViewHolder) convertView.getTag();

				/*
				 * When a listview recycles views , it recycles its present
				 * state as well as listeners attached to it. if the checkbox
				 * was checked and has a onCheckedChangeListener set, both will
				 * remain a part of recycled view based on position. So it is
				 * our responsibility to reset all states and remove previous
				 * listeners. The listener was removed as below:-
				 */
				holder.filterCheckBox.setOnCheckedChangeListener(null);

			}

			holder.filterCheckBox.setFocusable(false);
			holder.filterCheckBox.setChecked(positionArray.get(position));
			holder.filterCheckBox.setText(cuisineList.get(position));

			holder.filterCheckBox
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								// Toast.makeText(ShowOffersActivity.this,
								// "Checked Item:"+cuisineList.get(clickPos),
								// 0).show();

								// change state of concern item in
								// 'positionArray' array to 'True'
								positionArray.set(position, true);
								// add the checked item in to cuisine list which
								// used to filter offers.
								filterOffersByThisCuisineList.add(cuisineList
										.get(position));

							} else {
								// Toast.makeText(ShowOffersActivity.this,
								// "UnChecked Item:"+cuisineList.get(clickPos),
								// 0).show();

								// change state of concern item in
								// 'positionArray' array to 'True'
								positionArray.set(position, false);
								// remove the unchecked item in to cuisine list
								// which used to filter offers.
								filterOffersByThisCuisineList
										.remove(cuisineList.get(position));

							}
						}
					});

			return row;

		}

	}

	// END...

	/**
	 * Adapter that hold data of all 'Area Data'. Old one, In this User can
	 * select Multiple Areas to filter Offers! Not Using This...
	 * 
	 * @author Rupesh
	 * 
	 */
	public class AreaDataAdapterOLD extends ArrayAdapter<String> {
		private List<String> areaList;
		private Activity context;
		ArrayList<Boolean> positionArray;

		public AreaDataAdapterOLD(Context context, int textViewResourceId,
				List<String> offersAreaList) {
			super(context, textViewResourceId, offersAreaList);
			// TODO Auto-generated constructor stub
			// this.context=context;
			this.areaList = offersAreaList;

			positionArray = new ArrayList<Boolean>(offersAreaList.size());
			for (int i = 0; i < offersAreaList.size(); i++) {
				positionArray.add(false);
			}

		}

		public AreaDataAdapterOLD(Activity context, List<String> offersAreaList) {
			super(ShowOffersActivity.this, R.layout.filterlist_row,
					offersAreaList);
			this.context = context;
			this.areaList = offersAreaList;

			positionArray = new ArrayList<Boolean>(offersAreaList.size());
			for (int i = 0; i < offersAreaList.size(); i++) {
				positionArray.add(false);
			}

		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = convertView;
			FilterViewHolder holder;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.filterlist_row, parent, false);
				holder = new FilterViewHolder();
				holder.filterCheckBox = (CheckBox) row
						.findViewById(R.id.filter_checkbox);
				holder.filterCheckBox.setTypeface(fontFace);
				// if (HDFCApp.apiLevel<=android.os.Build.VERSION_CODES.FROYO) {
				if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.FROYO) {
					// To maintaining gap in between CheckBox and CB-TEXT... Add
					// Extra 5dp to Margin Left.
					final float scale = res.getDisplayMetrics().density;
					holder.filterCheckBox.setPadding(
							holder.filterCheckBox.getPaddingLeft()
									+ (int) (10.0f * scale + 0.5f),
							holder.filterCheckBox.getPaddingTop(),
							holder.filterCheckBox.getPaddingRight(),
							holder.filterCheckBox.getPaddingBottom());
				} else {
					// Do nothing...

				}

				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (FilterViewHolder) row.getTag();

				/*
				 * When a listview recycles views , it recycles its present
				 * state as well as listeners attached to it. if the checkbox
				 * was checked and has a onCheckedChangeListener set, both will
				 * remain a part of recycled view based on position. So it is
				 * our responsibility to reset all states and remove previous
				 * listeners. The listener was removed as below:-
				 */
				holder.filterCheckBox.setOnCheckedChangeListener(null);

			}
			// Log.v("Item In Area List-", ""+areaList.get(position));
			holder.filterCheckBox.setText(areaList.get(position));
			holder.filterCheckBox.setFocusable(false);
			holder.filterCheckBox.setChecked(positionArray.get(position));
			holder.filterCheckBox.setText(areaList.get(position));

			holder.filterCheckBox
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								// Toast.makeText(ShowOffersActivity.this,
								// "Checked Item:"+areaList.get(clickPos),
								// 0).show();

								// change state of concern item in
								// 'positionArray' array to 'True'
								positionArray.set(position, true);
								// add the checked item in to area list which
								// used to filter offers.
								filterOffersByThisAreaList.add(areaList
										.get(position));

							} else {
								// Toast.makeText(ShowOffersActivity.this,
								// "UnChecked Item:"+areaList.get(clickPos),
								// 0).show();

								// change state of concern item in
								// 'positionArray' array to 'True'
								positionArray.set(position, false);
								// remove the unchecked item in to area list
								// which used to filter offers.
								filterOffersByThisAreaList.remove(areaList
										.get(position));

							}
						}
					});
			return row;
		}

	}

	// NEW Ares ADP---
	public class AreaDataAdapter extends ArrayAdapter<String> implements
			Checkable {
		private List<String> areaList;
		private Activity context;
		public ArrayList<Boolean> positionArray_a;

		public AreaDataAdapter(Context context, int textViewResourceId,
				List<String> offersAreaList) {
			super(context, textViewResourceId, offersAreaList);
			// TODO Auto-generated constructor stub
			// this.context=context;
			this.areaList = offersAreaList;

			/*---NOTE--- 04.03.14 Tuesday.
			 * Close--- Be`coz we need not to update 'area list' untill click on Adv.Search and or come again from
			 *categoriesHome. 
			 */
			positionArray_a = new ArrayList<Boolean>(offersAreaList.size());
			for (int i = 0; i < offersAreaList.size(); i++) {
				positionArray_a.add(false);
			}

			// if (positionArray_a==null /*&& positionArray_c.size()==0*/) {
			// /*
			// * Enter here means 'area' list is 'null' & hence
			// * initialize it with the area list & their respective value(t or
			// f)
			// */
			// positionArray_a = new ArrayList<Boolean>(offersAreaList.size());
			// for(int i =0;i<offersAreaList.size();i++){
			// positionArray_a.add(false);
			// }
			//
			// }

		}

		public AreaDataAdapter(Activity context, List<String> offersAreaList) {
			super(ShowOffersActivity.this, R.layout.filterlist_row,
					offersAreaList);
			this.context = context;
			this.areaList = offersAreaList;

			// ...
			positionArray_a = new ArrayList<Boolean>(offersAreaList.size());
			for (int i = 0; i < offersAreaList.size(); i++) {
				positionArray_a.add(false);
			}

			// close as now m refreshing 'area' list everytime...
			// if (positionArray_a==null /*&& positionArray_c.size()==0*/) {
			// /*
			// * Enter here means 'area' list is 'null' & hence
			// * initialize it with the area list & their respective value(t or
			// f)
			// */
			// positionArray_a = new ArrayList<Boolean>(offersAreaList.size());
			// for(int i =0;i<offersAreaList.size();i++){
			// positionArray_a.add(false);
			// }
			//
			// }

		}

		/**
		 * Position of the current selected 'Area-Radio Button'.
		 */
		// int selectedAreaRadioPosition=0;
		boolean anyRadioChecked = true;

		// NEW....................
		// ADv...
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View row = convertView;
			FilterViewHolder holder;
			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.filterlist_radio_row, parent,
						false);
				holder = new FilterViewHolder();
				holder.radioButton = (RadioButton) row
						.findViewById(R.id.rButton);
				holder.radioButton.setTypeface(fontFace);
				// if (HDFCApp.apiLevel<=android.os.Build.VERSION_CODES.FROYO) {
				// To maintaining gap in between CheckBox and CB-TEXT... Add
				// Extra 5dp to Margin Left.
				final float scale = res.getDisplayMetrics().density;
				holder.radioButton.setPadding(
						holder.radioButton.getPaddingLeft()
								+ (int) (10.0f * scale + 0.5f),
						holder.radioButton.getPaddingTop(),
						holder.radioButton.getPaddingRight(),
						holder.radioButton.getPaddingBottom());
				// } else {
				// //Do nothing...
				//
				// }

				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (FilterViewHolder) row.getTag();

				/*
				 * When a listview recycles views , it recycles its present
				 * state as well as listeners attached to it. if the checkbox
				 * was checked and has a onCheckedChangeListener set, both will
				 * remain a part of recycled view based on position. So it is
				 * our responsibility to reset all states and remove previous
				 * listeners. The listener was removed as below:-
				 */
				holder.radioButton.setOnCheckedChangeListener(null);

			}
			// Log.v("Item In Area List-", ""+areaList.get(position));

			holder.radioButton.setText(areaList.get(position));
			holder.radioButton.setFocusable(false);
			// Log.e("RadioCheck-VAlue",
			// position+"-is Radio checked or not:"+positionArray_a.get(position));
			if (position == selectedAreaRadioPosition && anyRadioChecked)
				holder.radioButton.setChecked(true);
			else
				holder.radioButton.setChecked(false);

			// holder.radioButton.setChecked(positionArray_a.get(position));
			// notifyDataSetChanged();
			holder.radioButton.setText(areaList.get(position));

			holder.radioButton
					.setOnCheckedChangeListener(new OnCheckedChangeListener() {

						@Override
						public void onCheckedChanged(CompoundButton buttonView,
								boolean isChecked) {
							// TODO Auto-generated method stub
							if (isChecked) {
								// Toast.makeText(ShowOffersActivity.this,
								// "Checked Item:"+areaList.get(clickPos),
								// 0).show();
								// areaStringTOFilterOffer =
								// ((RadioButton)buttonView).getText().toString();
								filterOffersByThisArea = ((RadioButton) buttonView)
										.getText().toString();
								anyRadioChecked = true;
								selectedAreaRadioPosition = position;

								// change state of concern item in
								// 'positionArray_a' array to 'True'
								// positionArray_a.set(position, true);
								// buttonView.invalidate();
								notifyDataSetChanged();
								// add the checked item in to area list which
								// used to filter offers.
								// filterOffersByThisAreaList.add(areaList.get(position));

							} else {
								// Toast.makeText(ShowOffersActivity.this,
								// "UnChecked Item:"+areaList.get(clickPos),
								// 0).show();
								positionArray_a.set(position, false);

							}
						}
					});
			return row;
		}

		@Override
		public boolean isChecked() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void setChecked(boolean checked) {
			// TODO Auto-generated method stub

		}

		@Override
		public void toggle() {
			// TODO Auto-generated method stub

		}

	}

	// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

	/**
	 * It will clear all list(Offres list, Map-Overlay List, Cuisine List and
	 * Area List) data.
	 */
	private void clearAllOffersList() {
		// TODO Auto-generated method stub
		if (mMapView.getOverlays() != null) {
			// Clear old Overlays...
			mMapView.getOverlays().clear();
			// May have to be remove... Not sure... TO locate User`s cURRENT
			// Location PIN only!
			// mGetLocation = (GetLocation) new GetLocation().execute();
		}

		/*
		 * If new data comes means then only you have to clear all older
		 * 'cuisines' & 'area' Bcoz, now we build new lists of C & A. OR... If
		 * 'na data found' then clear C n A list...
		 * 
		 * 04.03.14--- BUt due to new need... that we need to sustain 'cuisine'
		 * list(update only when come again or come from AdvSearch.) We are not
		 * going to clear C list...
		 * 
		 * 05.03.14--- Now we are updating the 'C' n 'A' only 1st time(when user
		 * come from HDFC-Offers-HOME or from Adv.Search) and tdue to this need
		 * not to clear 'C' n 'A' list...
		 */
		// areaArrayList.clear();
		// cuisineArrayList.clear();

		if (offersDataList != null) {
			// Clear Offers List...
			offersDataList.clear();
		}
	}

	/**
	 * Return all Offerers data.
	 * 
	 * @author Rupesh
	 * 
	 */
	private class GetOffersData extends UserTask<String, Void, String> {
		boolean isDoinBAcCompleted;// gc...
		String OffersResponseData;// gc...

		public void onPreExecute() {
			try {
				if (mPdialog == null)
					mPdialog = new ProgressDialog(ShowOffersActivity.this);

				// if(offersFlag)
				// {
				mPdialog.setMessage("Getting Offers...");
				// }else{
				// mPdialog.setMessage("Getting Map View...");
				// }

				mPdialog.setIndeterminate(false);
				mPdialog.setCancelable(false);
				mPdialog.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onPostExecute(String data) {
			isDoinBAcCompleted = false;

			String lat = null, lon = null;
			if (mPdialog != null) {
				/*
				 * if(mPdialog.isShowing()) mPdialog.dismiss();
				 */
				try {
					mPdialog.dismiss();
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
			/*
			 * Imp. Note- Here you are gatting below res. when no data founds_
			 * so keep this in mind! { "output": "No Records Found" }
			 */
			if (data != null && data.contains("output")) {
				try {

					if (mMapView.getVisibility() == View.GONE) {
						if (noffers.getVisibility() == View.GONE) {
							// set Text Level NO-OFFERS to be VISIBLE!
							// noffers.setText("No Offers Around "+
							// citySelected+"!");
							noffers.setText("No Offers Found!");
							noffers.setVisibility(View.VISIBLE);
						}
					}

					String msg = new JSONObject(data).getString("output");
					LayoutInflater infalter = LayoutInflater
							.from(ShowOffersActivity.this);
					View addressDialog = infalter.inflate(
							R.layout.addressdialog, null);

					// final AlertDialog.Builder alert=new
					// AlertDialog.Builder(ShowOffersActivity.this);
					final Myalert alert = new Myalert(ShowOffersActivity.this);
					alert.setInverseBackgroundForced(true);
					alert.setView(addressDialog, 0, 0, 0, 0);
					alert.setIcon(android.R.drawable.btn_default);
					alert.show();

					final ImageView close = (ImageView) addressDialog
							.findViewById(R.id.exit);
					close.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View arg0) {
							alert.cancel();

						}
					});

					((TextView) addressDialog.findViewById(R.id.addtitle))
							.setText("No Offers!");

					// ((TextView)addressDialog.findViewById(R.id.adddetails)).setText(msg);
					((TextView) addressDialog.findViewById(R.id.adddetails))
							.setText("No offers found");

					// alertDialog.getWindow().setBackgroundDrawable(new
					// ColorDrawable(android.graphics.Color.TRANSPARENT));
					// show it
					alert.show();

					if (msg.equalsIgnoreCase("No Records Found")) {

						clearAllOffersList();

						String cityName = "", stateName = "";
						if (curLocationDetail != null) {
							for (int i = 0; i < curLocationDetail.size(); i++) {
								cityName = curLocationDetail.get(i).getName();
								stateName = curLocationDetail.get(i).getSname();
								Locality = curLocationDetail.get(i)
										.getLocality();
								// Log.d("state n city=>",""+cityName+"==="+stateName+"==="+Locality);
							}
						} else {
							Locality = "My Location";
						}

						GeoPoint point = new GeoPoint(
								(int) (Currlatitude * 1E6),
								(int) (Currlongitude * 1E6));
						// SHows custom popup on overlayclick
						mapOffersOverlays = mMapView.getOverlays();
						Drawable atm_icon = getResources().getDrawable(
								R.drawable.arrow);
						CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
								atm_icon, ctx, ShowOffersActivity.this);
						// Log.d("cityName=>",""+cityName);
						OverlayItem overlayitem = new OverlayItem(point,
								"Current Location", Locality + ", " + cityName
										+ "\n" + stateName);
						itemizedOverlay.addOverlay(overlayitem);
						// mapOverlays.clear();
						mapOffersOverlays.add(itemizedOverlay);
						MapController mapController = mMapView.getController();
						/*
						 * ---NOTE--- 06.03.14 Thursday Conferm from Amit/Paresh
						 * that if no offers found then in this case do we need
						 * to navigate user to his/her current location or
						 * not!!!
						 */
						// mapController.animateTo(point);
						mapController.setZoom(12);

					} else {
						// Do Nothing...

					}

					return;
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				// gc... /*
				// * If new data comes means then only you have to clear all
				// older 'cuisines' & 'area'
				// * Bcoz, now we build new lists of C & A.
				// */
				// areaArrayList.clear();
				// cuisineArrayList.clear();

				/*
				 * if (noffers.getVisibility()==View.VISIBLE) { //set Text Level
				 * NO-OFFERS to be GONE! noffers.setVisibility(View.GONE); }
				 */
				if (data != null) {
					if (data.equalsIgnoreCase("timeout")) {//
						Toast.makeText(ShowOffersActivity.this,
								UNABLE_TO_REACH, 1).show();
						return;
					} else if (data.equalsIgnoreCase("nonetwork")) {
						Toast.makeText(ShowOffersActivity.this, noNetwork, 1)
								.show();
						return;
					}

				}

			}

			// Open Below if smthing went wrong...
			/*
			 * if(data==null || data.length()<20) { if
			 * (noffers.getVisibility()==View.GONE) { //noffers.setText(
			 * "Sorry there are no offers in your area currently. We are adding exciting offers everyday. Do come back and check later."
			 * ); Toast.makeText(ShowOffersActivity.this, noNetwork, 1).show();
			 * 
			 * } clearAllOffersList();
			 * 
			 * return;
			 * 
			 * }
			 */

			if (fromAdvSearchFlag) {
				// Change flag to 'false'
				fromAdvSearchFlag = false;

				// Change enter city Lat N Long...
				latEnterCity = advSearcDatahBundle.getString("latEnterCity");
				lonEnterCity = advSearcDatahBundle.getString("lonEnterCity");

				// Navigate map to ebtered 'City'
				Double latitude = Double.parseDouble(latEnterCity);
				Double longitude = Double.parseDouble(lonEnterCity);
				// Log.d("latitude & longitude=",latitude+"==	"+longitude);

				mapOffersOverlays = mMapView.getOverlays();// gc...
				GeoPoint point = new GeoPoint((int) (latitude * 1E6),
						(int) (longitude * 1E6));
				MapController mapController = mMapView.getController();
				mapController.animateTo(point);
				mapController.setZoom(15);

			}

			if (offersDataList != null) {
				offersDataList.clear();
			}
			try {
				JSONObject jObj = null;
				JSONArray list = null;

				JSONArray cusineJArr = null;
				JSONArray areaJArr = null;

				JSONArray categoryJArr = null;

				jObj = new JSONObject(data);

				JSONObject jFiltersObj = jObj.getJSONObject("filters");
				cusineJArr = jFiltersObj.getJSONArray("cuisine");
				areaJArr = jFiltersObj.getJSONArray("area");
				categoryJArr = jFiltersObj.getJSONArray("category");

				/*
				 * ---NOTE--- 03.03.2014 Monday TO set 'cuisine' & 'area'
				 * filters list as it is until user click on Adv.Search. and
				 * click on 'Search' button or goes back & come back again.
				 */
				// if(cuisineArrayList.size()==0) {
				// /*---NOTE---
				// * enter here means this the 1st time when user come here...
				// or
				// * Come from Adv.Search(since i plan to clear 'Cuisine' &
				// 'Area' list when user click 'Search' button
				// * in Adv.Search in 'doInBackground')!!!
				// */
				// if (cusineJArr.length()!=0 &&
				// !cusineJArr.getString(0).equalsIgnoreCase("-")) {
				// System.out.println("cusineJArr LengthIN:"+cusineJArr.length());
				// for (int i = 0; i < cusineJArr.length(); i++) {
				// System.out.println(i+"th cusineJArr Item:"+cusineJArr.getString(i));
				// if (!cusineJArr.getString(i).equalsIgnoreCase("-") &&
				// !(cusineJArr.getString(i).equalsIgnoreCase("")||cusineJArr.getString(i).equalsIgnoreCase(" ")))
				// {
				// //cusineJArr.getString(i);
				// cuisineArrayList.add(cusineJArr.getString(i));
				// }
				//
				// }
				// }
				//
				// } else {
				// //Do nothing...
				//
				// }
				//
				// /*
				// * FOR 'Area'...
				// * Later after disscussion with Paresh... updating 'area' list
				// everytime. 04.03.14
				// */
				// /*if(areaArrayList.size()==0) {
				// System.out.println("areaJArr Length:"+areaJArr.length());
				// if (areaJArr.length()!=0 &&
				// !areaJArr.getString(0).equalsIgnoreCase("-")) {
				// System.out.println("areaJArr LengthIN:"+areaJArr.length());
				// for (int i = 0; i < areaJArr.length(); i++) {
				// System.out.println(i+"th areaJArr Item:"+areaJArr.getString(i));
				// if (!areaJArr.getString(i).equalsIgnoreCase("-") &&
				// !(areaJArr.getString(i).equalsIgnoreCase("")||areaJArr.getString(i).equalsIgnoreCase(" ")))
				// {
				// //cusineJArr.getString(i);
				// areaArrayList.add(areaJArr.getString(i));
				// }
				//
				// }
				// }
				//
				// } else {
				// //Do nothing...
				//
				// }*/
				// END...
				// .............................................................................

				/*---NOTE---
				 * To update 'Cuisine' and 'Area' only at first time... when
				 * user come from 'HDFC-Offers-HOME' or from 'AdvSearch'
				 */
				if (isFIrstTime) {
					// Update to 'false' as now onward not to update 'C' n 'A'
					isFIrstTime = false;

					// Category...
					if (categoryArrayList != null) {
						categoryArrayList.clear();
					}
					System.out.println("categoryJArr Length:"
							+ categoryJArr.length());
					if (categoryJArr.length() != 0
							&& !categoryJArr.getString(0).equalsIgnoreCase("-")) {
						System.out.println("categoryJArr LengthIN:"
								+ categoryJArr.length());
						for (int i = 0; i < categoryJArr.length(); i++) {
							System.out.println(i + "th categoryJArr Item:"
									+ categoryJArr.getString(i));
							if (!categoryJArr.getString(i)
									.equalsIgnoreCase("-")
									&& !(categoryJArr.getString(i)
											.equalsIgnoreCase("") || categoryJArr
											.getString(i).equalsIgnoreCase(" "))) {
								// cusineJArr.getString(i);
								categoryArrayList
										.add(categoryJArr.getString(i));
							}

						}
						Collections.sort(categoryArrayList,
								new Comparator<String>() {
									@Override
									public int compare(String o1, String o2) {
										return o1.compareToIgnoreCase(o2);
									}
								});
						// Collections.sort(categoryArrayList);
						System.out.println("categoryArrayList size:"
								+ categoryArrayList.size()
								+ ", categoryArrayList:" + categoryArrayList);

					}

					// Cuisine...
					if (cuisineArrayList != null) {
						cuisineArrayList.clear();
					}
					System.out.println("cusineJArr Length:"
							+ cusineJArr.length());
					if (cusineJArr.length() != 0
							&& !cusineJArr.getString(0).equalsIgnoreCase("-")) {
						System.out.println("cusineJArr LengthIN:"
								+ cusineJArr.length());
						for (int i = 0; i < cusineJArr.length(); i++) {
							System.out.println(i + "th cusineJArr Item:"
									+ cusineJArr.getString(i));
							if (!cusineJArr.getString(i).equalsIgnoreCase("-")
									&& !(cusineJArr.getString(i)
											.equalsIgnoreCase("") || cusineJArr
											.getString(i).equalsIgnoreCase(" "))) {
								// cusineJArr.getString(i);
								cuisineArrayList.add(cusineJArr.getString(i));
							}

						}
						Collections.sort(cuisineArrayList,
								new Comparator<String>() {
									@Override
									public int compare(String o1, String o2) {
										return o1.compareToIgnoreCase(o2);
									}
								});
						// Collections.sort(cuisineArrayList);
						System.out.println("cuisineArrayList size:"
								+ cuisineArrayList.size()
								+ ", cuisineArrayList:" + cuisineArrayList);
					}

					// Area...
					if (areaArrayList != null) {
						areaArrayList.clear();
					}
					System.out.println("areaJArr Length:" + areaJArr.length());
					if (areaJArr.length() != 0
							&& !areaJArr.getString(0).equalsIgnoreCase("-")) {
						System.out.println("areaJArr LengthIN:"
								+ areaJArr.length());
						for (int i = 0; i < areaJArr.length(); i++) {
							System.out.println(i + "th areaJArr Item:"
									+ areaJArr.getString(i));

							if (!areaJArr.getString(i).equalsIgnoreCase("-")
									&& !(areaJArr.getString(i)
											.equalsIgnoreCase("") || areaJArr
											.getString(i).equalsIgnoreCase(" "))) {
								// cusineJArr.getString(i);
								areaArrayList.add(areaJArr.getString(i));
							}

						}

						Collections.sort(areaArrayList,
								new Comparator<String>() {
									@Override
									public int compare(String o1, String o2) {
										return o1.compareToIgnoreCase(o2);
									}
								});
						// Collections.sort(areaArrayList,
						// String.CASE_INSENSITIVE_ORDER);

						/*
						 * to add 'none' to AreaList so that user can dissecet
						 * area item... BUt only when there is atleast one item
						 * in 'Area' list!
						 */
						if (areaArrayList.size() != 0) {
							areaArrayList.add(0, "All");

						}
						System.out.println("areaArrayList size:"
								+ areaArrayList.size() + ", areaArrayList:"
								+ areaArrayList);

					}
				}

				list = jObj.getJSONArray("list");
				for (int i = 0; i < list.length(); i++) {
					try {
						String row = list.optString(i, "default");
						JSONObject robject = (JSONObject) new JSONTokener(row)
								.nextValue();

						String address = robject.getString("address");
						String otype = robject.getString("otype");
						String name = robject.getString("name");
						String offers = robject.getString("offers");
						String category = robject.getString("category");
						String city = robject.getString("city");

						// HDFC remove this 'item' without intemating and due to
						// that problem occoured on ANdroid APp... That`s not
						// good! 29.1.14 Wed.
						// String subcategory =
						// robject.getString("subcategory");

						String validity = robject.getString("validity");
						String latitude = robject.getString("latitude");
						String longitude = robject.getString("longitude");

						HashMap<String, String> offersMap = new HashMap<String, String>();
						offersMap.put("address", address);
						offersMap.put("otype", otype);
						offersMap.put("name", name);
						offersMap.put("offers", offers);
						offersMap.put("category", category);
						offersMap.put("city", city);
						// offersMap.put("subcategory", subcategory);
						offersMap.put("validity", validity);
						offersMap.put("latitude", latitude);
						offersMap.put("longitude", longitude);
						offersDataList.add(offersMap);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if (offersDataList.size() != 0) {
					if (noffers.getVisibility() == View.VISIBLE) {
						noffers.setVisibility(View.GONE);
					}

				} else {
					if (noffers.getVisibility() == View.GONE) {
						noffers.setVisibility(View.VISIBLE);
					}
				}

				oDataAdapter.notifyDataSetChanged();
			} catch (Exception e) {
				e.printStackTrace();
				// Log.e("JSON Parser", "Error parsing data " + e.toString());
			}

			if (mMapView.getOverlays() != null) {
				// Clear old Overlays...
				mMapView.getOverlays().clear();
				// May have to be remove... Not sure...
				if (ProxyUrlUtil.isNetworkAvailable(ShowOffersActivity.this)) {
					// do...
					mGetLocation = (GetLocation) new GetLocation().execute();
				} else {
					Toast.makeText(ShowOffersActivity.this, noNetwork, 1)
							.show();

				}

			}
			/**
			 * ----------------- - Important NOTE: ----------------- If i have
			 * to add Overlay ICON according to CATEGORIES then_ 1. Simpley
			 * check -> "category": "Apparels" & then DO Accordingly as per the
			 * NEED! :) { "num": "4", "X": "28.978603", "Y": "79.398413",
			 * "radius": "10", "filters": { "cuisine": [ "-" ], "area": [
			 * "Civil Line", "Durga Mandir Gali", "Main Market" ], "locality": [
			 * "Rudrapur" ], "category": [ "Apparels" ], "offer_program": [
			 * "Smartbuy_Discount offer" ], "cardType": [ "Debit Card",
			 * "Credit Card" ] }, "list": [ { "type": "offers", "address":
			 * "D1 D2 4 Civil Lines Agrasen Chowk", "otype":
			 * "Credit Card/ Debit Card", "name": "Talk Of The Town", "offers":
			 * "10% discount on Bill value", "phone": "", ---> "category":
			 * "Apparels", "subcategory": "", "validity": "2014-3-31",
			 * "pincode": "263153", "latitude": 28.978603, "longitude":
			 * 79.398415, "distance": 0, "offer_id": "Uttaranchal_1", "online":
			 * "NO", "offline": "YES", "url": "www.smartbuy.com/cardoffers",
			 * "state": "Uttarakhand", "city": "Rudrapur", "locality":
			 * "Rudrapur", "area": "Civil Line", "emi": "NO", "emi_details":
			 * "N", "cuisine": "-", "offer_program": "Smartbuy_Discount offer",
			 * "open_hours":
			 * "1:10:30:20:00,2:10:30:20:00,3:10:30:20:00,4:10:30:20:00,5:10:30:20:00,6:10:30:20:00,7:10:30:20:00"
			 * , "national": "NO", "regional": "YES", "classification":
			 * "Apparels", "tnc": "*T&C Appy", "comments": "" }, {},{},{},{} ] }
			 */
			for (int i = 0; i < offersDataList.size(); i++) {
				double latitudeE6 = Double.parseDouble(offersDataList.get(i)
						.get("latitude"));
				double longitudeE6 = Double.parseDouble(offersDataList.get(i)
						.get("longitude"));
				// if(offersDataList.get(i).get("category").equalsIgnoreCaseIgnoreCase("offers"))
				// {
				GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
						(int) (longitudeE6 * 1E6));
				mapOffersOverlays = mMapView.getOverlays();

				// TO set Image of Concern Item According to 'Category' it
				// belongs to...
				String offerCategory = offersDataList.get(i).get("category");
				Drawable offers = getResources().getDrawable(
						R.drawable.other_map_icon);
				if (offerCategory.equalsIgnoreCase("Apparels")
						|| offerCategory.equalsIgnoreCase("Shopping")) {
					offers = getResources().getDrawable(
							R.drawable.apparels_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Dining")) {
					offers = getResources().getDrawable(
							R.drawable.rest_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Electronics")) {
					offers = getResources().getDrawable(
							R.drawable.electronics_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Fuel")) {
					offers = getResources().getDrawable(
							R.drawable.fuel_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Grocery")
						|| offerCategory.equalsIgnoreCase("Retail")) {
					offers = getResources().getDrawable(
							R.drawable.grocery_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Personal Care")) {
					offers = getResources().getDrawable(
							R.drawable.personal_care_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Telecom")) {
					offers = getResources().getDrawable(
							R.drawable.telecom_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Hotel")) {
					// No icon for HOTEL...
					offers = getResources().getDrawable(
							R.drawable.hotel_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Offer")) {
					offers = getResources().getDrawable(R.drawable.offers_icon);
				} else if (offerCategory.equalsIgnoreCase("offers")) {
					// No Icon for 'offers'
					offers = getResources().getDrawable(R.drawable.offers_icon);
				} else if (offerCategory.equalsIgnoreCase("Restaurant")) {
					offers = getResources().getDrawable(
							R.drawable.rest_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Telecommunication")) {
					offers = getResources().getDrawable(
							R.drawable.telecom_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Tyre")) {
					// No Icon for 'Tyre'
					offers = getResources().getDrawable(R.drawable.offers_icon);
				} else if (offerCategory.equalsIgnoreCase("Entertainment")) {
					offers = getResources().getDrawable(
							R.drawable.entertainment_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Movies")) {
					offers = getResources().getDrawable(
							R.drawable.movies_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Travel")) {
					offers = getResources().getDrawable(
							R.drawable.travel_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Caf�")
						|| offerCategory.equalsIgnoreCase("Cafe")) {
					offers = getResources().getDrawable(
							R.drawable.cafe_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Adventure")) {
					offers = getResources().getDrawable(
							R.drawable.adventure_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Sport")) {
					offers = getResources().getDrawable(
							R.drawable.sport_map_icon);
				} else if (offerCategory.equalsIgnoreCase("Others")) {
					offers = getResources().getDrawable(
							R.drawable.other_map_icon);
				} else {
					// Set Default Image... If NO category...
					offers = getResources().getDrawable(
							R.drawable.other_map_icon);
				}

				CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
						offers, ctx, itm);
				OverlayItem overlayitem = new OverlayItem(point, offersDataList
						.get(i).get("name").trim()
						+ ".\n" + offersDataList.get(i).get("offers"),
						offersDataList.get(i).get("address"));
				itemizedOverlay.addOverlay(overlayitem);
				mapOffersOverlays.add(itemizedOverlay);
				MapController mapController = mMapView.getController();
				// mapController.animateTo(point);
				mapController.setZoom(12);
				// }
			}
		}

		public String doInBackground(String... params) {
			String response = null;
			String x = params[0];
			String y = params[1];
			String category = params[2];

			// Log.d("address=>",""+x+"==="+y+"=="+selectedRadius);
			try {
				// filterOffersByThisCuisineList.toString().substring(1,
				// filterOffersByThisCuisineList.toString().length() -
				// 1).replace(", ", ",");
				// http://app.mapmyindia.com/hdfcapi/poi.do?x=28.5229&y=77.2094&radius=2&type=offers
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("x", x));
				nameValuePairs.add(new BasicNameValuePair("y", y));
				nameValuePairs.add(new BasicNameValuePair("rtype", "json/xml"));
				nameValuePairs.add(new BasicNameValuePair("type", "offers"));
				// nameValuePairs.add(new
				// BasicNameValuePair("Category",category));//not add here!
				// separate 4 each...
				if (!fromAdvSearchFlag && !fromFilterGoFlag) {
					nameValuePairs
							.add(new BasicNameValuePair("radius", "" + 10));
					nameValuePairs.add(new BasicNameValuePair("Category",
							category));
					nameValuePairs.add(new BasicNameValuePair("City",
							enteredCity));
					// Log.v("doInBackground:GetOffersData Call-",
					// "CurrentLat:"+x+", CurrentLon:"+y+"radius:"+10+", rtype:json/xml, type:offers, cityOrCategory:"+category);

				} else {
					if (fromFilterGoFlag) {
						// Change flag to 'false'
						fromFilterGoFlag = false;// StringBuilder.

						/*
						 * Distance bar... Use this when we want the same redius
						 * as that of AdvSearch...
						 */
						SharedPreferences sharedPreferences = getSharedPreferences(
								"RADIUS", Context.MODE_WORLD_READABLE);
						final int radius = sharedPreferences.getInt("radius",
								10000);

						/*
						 * ---NOTE--- As of now, after 1st time from AdvSearch,
						 * if user come from 'Filter' ten it will get more data
						 * as compaired to previous just b`coz of that i am
						 * taking 'radius' to 10...
						 */
						// nameValuePairs.add(new BasicNameValuePair("radius",
						// ""+10));
						nameValuePairs.add(new BasicNameValuePair("radius", ""
								+ radius / 1000));
						nameValuePairs.add(new BasicNameValuePair("Category",
								category));
						if (category.equalsIgnoreCase("Dining")) {
							nameValuePairs.add(new BasicNameValuePair(
									"Cuisine", cuisineStringTOFilterOffer));

						}

						/*
						 * if area is 'All' then get all offers of the current
						 * category...
						 */
						nameValuePairs
								.add(new BasicNameValuePair("Area",
										areaStringTOFilterOffer
												.equalsIgnoreCase("All") ? ""
												: areaStringTOFilterOffer));
						if (offersDataList.size() != 0) {
							// String city =
							// offersDataList.size()!=0?offersDataList.get(0).get("city"):"";

							/*---NOTE--- 05.03.14
							 * This in case when user click on Ctegory & get offers & then filter Offers...
							 * Then in this cse i need City... which i get from the 1st offre item data.
							 * ---Case when no Data found--- 
							 * I clear offers data & hence here need to check whether the 'offersDataList'
							 * is not empty... if empty then keep the old city...  
							 */
							enteredCity = offersDataList.size() != 0 ? offersDataList
									.get(0).get("city") : "";

						}
						// offersDataList.size()!=0?offersDataList.get(0).get("city"):""));
						// nameValuePairs.add(new BasicNameValuePair("City",
						// enteredCity));
						nameValuePairs.add(new BasicNameValuePair("City", ""));

						/*
						 * Log.v("doInBackground:fromFilterGoFlag Call-",
						 * "City:"
						 * +enteredCity+", CurrentLat:"+x+", CurrentLon:"+
						 * y+"radius:"
						 * +10+", rtype:json/xml, type:offers, cityOrCategory:"
						 * +category+ ",\n Cuisine:"
						 * +cuisineStringTOFilterOffer+ ",\n Area:"+
						 * areaStringTOFilterOffer);
						 */

					}

					if (fromAdvSearchFlag) {
						enteredCity = advSearcDatahBundle.getString("city");

						/*
						 * Change flag to 'false'... ---NOTE--- 25.03.14 Now i
						 * am changing this flag value in 'onPostExe.' & navi.
						 * map to Entered 'City' Location over their...
						 */
						// fromAdvSearchFlag=false;

						// set 'area' filter radio again to default...
						selectedAreaRadioPosition = -1;

						/**
						 * update to 'true' be`coz when user come from
						 * Adv.Search you get new 'Cuisine' n 'Area' and you
						 * need to update C n A same as that of when you come
						 * 1st time by clicking 'Offers'.
						 */
						isFIrstTime = true;

						/*---NOTE---
						 * Need only in case when you are not using 'isFIrstTime'
						 * 
						 * Clear 'Cuisine' & 'Area' list... 03.03.2014 Monday.
						 * Since need to update the 'Cuisine' and 'area' list... 1st clear it.
						 */
						if (cuisineArrayList != null) {
							cuisineArrayList.clear();
						}
						if (areaArrayList != null) {
							areaArrayList.clear();
						}
						if (categoryArrayList != null) {
							categoryArrayList.clear();
						}
						// clear lastly selected C list... when come fm
						// Adv.Search...
						filterOffersByThisCuisineList.clear();

						/*
						 * ---NOTE--- 04.03.14 clear 'positionArray_c' position
						 * list... since now you have to refresh 'Cusine' list
						 * data and build 'positionArray_c' list data accrding
						 * to this new data...
						 */
						positionArray_c.clear();

						// city area category subCategory program cardType
						// discount emi radius
						// City Area Category Cuisine Locality Program CardType
						// Discount EMI
						// nameValuePairs.add(new BasicNameValuePair("radius",
						// ""+advSearcDatahBundle.getString("radius")));
						// nameValuePairs.add(new BasicNameValuePair("City",
						// ""+advSearcDatahBundle.getString("city")));
						// nameValuePairs.add(new BasicNameValuePair("radius",
						// "2100"));
						// nameValuePairs.add(new BasicNameValuePair("City",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("Area",
						// advSearcDatahBundle.getString("area")));
						// nameValuePairs.add(new BasicNameValuePair("Category",
						// ""+advSearcDatahBundle.getString("Category")));
						// nameValuePairs.add(new BasicNameValuePair("Cuisine",
						// ""+advSearcDatahBundle.getString("subCategory")));
						// nameValuePairs.add(new BasicNameValuePair("Program",
						// ""+advSearcDatahBundle.getString("program")));
						// nameValuePairs.add(new BasicNameValuePair("CardType",
						// ""+advSearcDatahBundle.getString("cardType")));
						// nameValuePairs.add(new BasicNameValuePair("Discount",
						// ""+advSearcDatahBundle.getString("discount")));
						// nameValuePairs.add(new BasicNameValuePair("EMI",
						// ""+advSearcDatahBundle.getString("emi")));

						// nameValuePairs.add(new BasicNameValuePair("radius",
						// "10"));
						nameValuePairs.add(new BasicNameValuePair("radius",
								advSearcDatahBundle.getString("radius")));

						/*---NOTE---
						 * We are sending 'City' only in Adv.Search b`coz if we don`t then 'MapMyIndia'
						 * API will return me all India`s data of concern 'Category'...
						 */
						nameValuePairs.add(new BasicNameValuePair("City",
								advSearcDatahBundle.getString("city")));
						// nameValuePairs.add(new BasicNameValuePair("City",
						// ""));
						nameValuePairs.add(new BasicNameValuePair("Area",
								advSearcDatahBundle.getString("area")));
						// Log.e("Test ---",
						// "Category:"+advSearcDatahBundle.getString("category"));

						// gc... need to remove 24.03.14
						// nameValuePairs.add(new BasicNameValuePair("Category",
						// advSearcDatahBundle.getString("category")));
						// nameValuePairs.add(new BasicNameValuePair("Cuisine",
						// advSearcDatahBundle.getString("subCategory")));
						// nameValuePairs.add(new BasicNameValuePair("Program",
						// advSearcDatahBundle.getString("program")));
						// nameValuePairs.add(new BasicNameValuePair("CardType",
						// advSearcDatahBundle.getString("cardType")));
						// nameValuePairs.add(new BasicNameValuePair("Discount",
						// advSearcDatahBundle.getString("discount")));
						nameValuePairs.add(new BasicNameValuePair("EMI",
								advSearcDatahBundle.getString("emi")));

						// nameValuePairs.add(new BasicNameValuePair("radius",
						// ""+advSearcDatahBundle.getString("radius")));
						// nameValuePairs.add(new BasicNameValuePair("radius",
						// "2100"));
						// nameValuePairs.add(new BasicNameValuePair("City",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("Area",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("Category",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("Cuisine",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("Program",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("CardType",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("Discount",
						// ""));
						// nameValuePairs.add(new BasicNameValuePair("EMI",
						// ""));

						return advSearcDatahBundle.getString("response");

					}
				}

				System.out.println("GetOffersData.doInBackground() URL:"
						+ PostUrl + ", Lat:" + x + ", Long:" + y);

				// response =
				// pU.getPostXML(PostUrl,ShowOffersActivity.this,nameValuePairs);
				response = ProxyUrlUtil.getOffers(PostUrl, nameValuePairs);
				// response =
				// ProxyUrlUtil.getOffers("http://app.mapmyindia.com/hdfcapi/poi.do",nameValuePairs);

				isDoinBAcCompleted = true;
				OffersResponseData = response;

			} catch (Exception e) {
				e.printStackTrace();
				if (ProxyUrlUtil.isNetworkAvailable(ShowOffersActivity.this)) {
					return "timeout";

				} else {
					return "nonetwork";

				}

			}

			return response;
		}
	}

	/**
	 * Get the Current Location of User and Navigate him/her to his current
	 * Location.
	 * 
	 * @author Rupesh
	 * 
	 */
	private class GetLocation extends UserTask<String, Void, String> {
		// ProgressDialog mPdialog;
		public void onPreExecute() {
			try {
				if (mPdialog == null)
					mPdialog = new ProgressDialog(ShowOffersActivity.this);

				// mPdialog.setMessage("Getting Map View...");
				mPdialog.setMessage("Please wait...");
				mPdialog.setIndeterminate(true);
				mPdialog.setCancelable(false);
				mPdialog.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onPostExecute(String data) {
			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}

			// Log.d("contactDetail.size()=>",""+curLocationDetail.size());
			String cityName = "", stateName = "";
			if (curLocationDetail != null) {
				for (int i = 0; i < curLocationDetail.size(); i++) {
					cityName = curLocationDetail.get(i).getName();
					stateName = curLocationDetail.get(i).getSname();
					Locality = curLocationDetail.get(i).getLocality();
					// Log.e("state n city=>"," getName="+curLocationDetail.get(i).getName()+", getSname="+curLocationDetail.get(i).getSname()+", Locality="+Locality);
				}
			} else {
				Locality = "My Location";
			}

			GeoPoint point = new GeoPoint((int) (Currlatitude * 1E6),
					(int) (Currlongitude * 1E6));
			// SHows custom popup on overlayclick
			mapOffersOverlays = mMapView.getOverlays();
			Drawable atm_icon = getResources().getDrawable(R.drawable.arrow);
			CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
					atm_icon, ctx, ShowOffersActivity.this);
			// Log.d("cityName=>",""+cityName);
			// OverlayItem overlayitem = new OverlayItem(point,
			// "Current Location", Locality+", "+cityName+"\n"+stateName);
			OverlayItem overlayitem = new OverlayItem(point,
					"Current Location", Locality);
			itemizedOverlay.addOverlay(overlayitem);
			// mapOverlays.clear();
			mapOffersOverlays.add(itemizedOverlay);
			MapController mapController = mMapView.getController();

			if (fromCurrentLocation) {
				/*
				 * -Note- If user clicks on a category to get the offers then
				 * only navigate user to his/her current location. and if Enter
				 * a city or select a city from Adv.Search then no no need to
				 * navigate to user`s current location (in this case user
				 * already get navigate to entered or selected city).
				 */
				mapController.animateTo(point);
			}

			mapController.setZoom(12);
		}

		@Override
		public String doInBackground(String... params) {

			try {
				// Log.d("lat n lon=>",""+CurrentLon+"="+CurrentLat);
				SAXParserFactory mySAXParserFactory = SAXParserFactory
						.newInstance();
				SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
				XMLReader myXMLReader = mySAXParser.getXMLReader();
				String url = "http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x="
						+ CurrentLon + "&y=" + CurrentLat;
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=77.2094&y=28.5229";
				// Log.d("url string=>",""+url);
				URL myUrl = new URL(url);

				curLocationDetail = new ArrayList<ContactDetails>();
				ContactsParser parse = new ContactsParser(curLocationDetail);
				myXMLReader.setContentHandler(parse);
				myXMLReader.parse(new InputSource(myUrl.openStream()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			return "Locality";
		}
	}

	// .......Start of 'GetSearchedData'
	/**
	 * Sole purpose of this Class is to get Location of City/Area entered(From
	 * Offers Home) os Selected(from Adv-Search). Once it get Location
	 * Successfully, it will pass it to 'GetOffersData' to get all 'Offers'
	 * nearBy or Around the concern City/Area.
	 * 
	 * @author Rupesh
	 * 
	 */
	// gc... shifted in to 'AdvSearch'
	private class GetCityOffersData extends UserTask<String, Void, String> {
		String type;// , lat, lon;
		ProgressDialog mPdialog;

		public void onPreExecute() {
			if (mPdialog == null)
				mPdialog = new ProgressDialog(ShowOffersActivity.this);

			mPdialog.setMessage("Please wait....");
			mPdialog.setIndeterminate(true);
			mPdialog.setCancelable(true);
			mPdialog.show();

		}

		public void onPostExecute(String data) {

			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}

			if (data == null || data.length() < 20) {
				return;
			}
			// try {
			/*
			 * This how the response 'data' looks like_ { "search": "map",
			 * "result": { "type": "exact/best", "address":
			 * "Vashi, Navi Mumbai, Maharashtra", "pos": { "lat": 19.0768,
			 * "lon": 73.001 }, "lev": 9 }, "alternates": [ { "address":
			 * "Kashi Vashi,Chembur,Mumbai,Maharashtra", "lev": 0
			 * },{},{},{},{},...
			 * 
			 * ] }
			 */
			JSONObject jObj = null;
			// JSONObject jObjj = null;
			JSONArray alternates = null;
			try {
				// Log.v("Entered-City Info", "All Info:"+data);
				jObj = new JSONObject(data);

				alternates = jObj.getJSONArray("alternates");

				JSONObject resultObj = jObj.getJSONObject("result");
				type = resultObj.getString("type");
				String address = resultObj.getString("address");
				String level = resultObj.getString("lev");

				JSONObject position = resultObj.getJSONObject("pos");
				latEnterCity = position.getString("lat");
				lonEnterCity = position.getString("lon");
				// latEnterCity=lat;
				// lonEnterCity=lon;

			} catch (JSONException e) {
				e.printStackTrace();
			}

			// Log.d("type=>",""+type);
			if (type.equalsIgnoreCase("exact") || type.equalsIgnoreCase("best")) {
				Double latitude = Double.parseDouble(latEnterCity);
				Double longitude = Double.parseDouble(lonEnterCity);
				// Log.d("latitude & longitude=",latitude+"==	"+longitude);

				mapOffersOverlays = mMapView.getOverlays();// gc...
				GeoPoint point = new GeoPoint((int) (latitude * 1E6),
						(int) (longitude * 1E6));
				MapController mapController = mMapView.getController();
				mapController.animateTo(point);
				mapController.setZoom(15);

				// In this case get all categories data...
				getOffersData = (GetOffersData) new GetOffersData().execute(
						latEnterCity, lonEnterCity, "");

			} else {

				Toast.makeText(ShowOffersActivity.this,
						"Please, Try again later!", 1).show();
				/*
				 * Enter here when the location lat-long 'type' of 'entered'
				 * city is not exact! Not needed here...
				 */
				try {

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public String doInBackground(String... params) {
			String response = null;
			String address = params[0];
			// Log.d("doInBackground=>","GetCityOffersData - Entered city:"+address);
			try {
				URL PostUrl = new URL(
						"http://apis.mapmyindia.com/v2.0/geocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json");
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);
				nameValuePairs.add(new BasicNameValuePair("addr", address));

				ProxyUrlUtil pU = new ProxyUrlUtil();

				response = pU.getPostXML(PostUrl, ShowOffersActivity.this,
						nameValuePairs);
				// Log.d("Responmse=>",""+response);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			return response;
		}

	}

	// ...........END....

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		Currlatitude = location.getLatitude();
		Currlongitude = location.getLongitude();

		CurrentLat = String.valueOf(Currlatitude);
		CurrentLon = String.valueOf(Currlongitude);
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setItem(OverlayItem item) {
		// TODO Auto-generated method stub

	}

}
