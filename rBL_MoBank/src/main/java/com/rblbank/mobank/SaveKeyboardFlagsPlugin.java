package com.rblbank.mobank;

import android.content.Intent;
import android.content.SharedPreferences;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class is used to save the device id received from native code.
 *
 * @author ReshmiR
 */
public class SaveKeyboardFlagsPlugin extends CordovaPlugin
{


    @Override
    public boolean execute(String action, JSONArray rawArgs,
                           CallbackContext callbackContext) throws JSONException
    {

//        Toast.makeText(RBL_iBank.getInstance().getContext(),"test",Toast.LENGTH_SHORT).show();
        SharedPreferences.Editor edit = RBL_iBank.getInstance()
                .getActivity().getSharedPreferences("com.hexadots.keyboardPayments.RBLMoBank", 0).edit();

        if (action != null && action.equals("DeviceID"))
        {
            String deviceID = rawArgs.getString(0);
            edit.putString("AppDeviceIdFlag", deviceID);
           /* edit.putString("AppActivatedFlag", "N");
            edit.putString("AppUserLockedFlag", "N");*/
            edit.commit();
        } else if (action != null && action.equals("UserRegistered"))
        {
            String UserRegFlag = rawArgs.getString(0);
            edit.putString("AppActivatedFlag", UserRegFlag);
            edit.putString("AppUserLockedFlag", "N");
            edit.commit();
        } else if (action != null && action.equals("UserLocked"))
        {
            String LockedFlag = rawArgs.getString(0);
            edit.putString("AppUserLockedFlag", LockedFlag);
            edit.commit();
        }

        else if (action != null && action.equals("ATMView"))
        {
            Intent intent = new Intent(this.cordova.getActivity(),
                    AtmBrancheLockerActivityV2.class);
            this.cordova.getActivity().startActivity(intent);

        }

        return false;

    }
}
