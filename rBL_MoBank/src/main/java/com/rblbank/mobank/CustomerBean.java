package com.rblbank.mobank;

public class CustomerBean {
	private String custId;
	private String custPWD;
	private Boolean custLogFlg;
	private String module;
	private String deviceId;

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustPWD() {
		return custPWD;
	}

	public void setCustPWD(String custPWD) {
		this.custPWD = custPWD;
	}

	public Boolean getCustLogFlg() {
		return custLogFlg;
	}

	public void setCustLogFlg(Boolean custLogFlg) {
		this.custLogFlg = custLogFlg;
	}
	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}
	
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
}