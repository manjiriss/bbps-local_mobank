package com.rblbank.mobank;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author awysocki
 */

public class isDeviceRooted extends CordovaPlugin {
	public static final String MOBANK = "rblMobank";

	@Override
	public boolean execute(String action, JSONArray data,
			final CallbackContext callbackContext) throws JSONException {

		/*if (MOBANK.equals(action)) {
			JSONObject obj = new JSONObject();
			obj.put("custId", com.rblbank.mobank.RBL_iBank.custBean.getCustId());
			obj.put("custPWD",
					com.rblbank.mobank.RBL_iBank.custBean.getCustPWD());
			obj.put("custLogFlg",
					com.rblbank.mobank.RBL_iBank.custBean.getCustLogFlg());
			obj.put("module", com.rblbank.mobank.RBL_iBank.custBean.getModule());
			obj.put("deviceId", com.rblbank.mobank.RBL_iBank.custBean.getDeviceId());
			// System.out.println("Response Needed" + data.getString(0));
			callbackContext.success(obj);
			return true;

		}*/
		//JSONObject obj = new JSONObject();
		if(isDeviceRooted1() == true)
		{
		
			callbackContext.success();
			return true;
		}
		else 
		{
			return false;
		}
			
	}
	
	public static boolean isDeviceRooted2() {
        return checkRootMethod1() || checkRootMethod2() || checkRootMethod3() || checkRootMethod4();
		//return  checkRootMethod4();
    }

    private static boolean checkRootMethod1() {
        String buildTags = android.os.Build.TAGS;
        return buildTags != null && buildTags.contains("test-keys");
    }

    private static boolean checkRootMethod2() {
        String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (String path : paths) {
            if (new File(path).exists()) return true;
        }
        return false;
    }

    private static boolean checkRootMethod3() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(new String[] { "/system/xbin/which", "su" });
            BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
            if (in.readLine() != null) return true;
            return false;
        } catch (Throwable t) {
            return false;
        } finally {
            if (process != null) process.destroy();
        }
    }
    
    public static boolean findBinary(String binaryName) {
        boolean found = false;
        if (!found) {
            String[] places = { "/sbin/", "/system/bin/", "/system/xbin/",
                    "/data/local/xbin/", "/data/local/bin/",
                    "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/" };
            for (String where : places) {
                if (new File(where + binaryName).exists()) {
                    found = true;

                    break;
                }
            }
        }
        return found;
    }
    
    private static boolean checkRootMethod4() {
    	return findBinary("su");
    }
    
    public static boolean isDeviceRooted1(){
        if (isDeviceRooted2() == true) {
            Process process = null;
            try {
                process = Runtime.getRuntime().exec(new String[]{"su", "-c", "id"});
                BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String output = in.readLine();
                if (output != null && output.toLowerCase().contains("uid=0"))
                    return true;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (process != null)
                    process.destroy();
            }
        }

        return false;
    }
}