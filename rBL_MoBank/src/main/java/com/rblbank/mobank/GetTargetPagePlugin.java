package com.rblbank.mobank;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
/**
 * This class is used to get the action page from native code and pass it to main.js class.
 * @author ReshmiR
 *
 */
public class GetTargetPagePlugin extends CordovaPlugin {

	public static final String PAGE = "PAGE";

	@Override
	public boolean execute(String action, String rawArgs,
			CallbackContext callbackContext) throws JSONException {

		String page = "";
		if (action.equals(PAGE)) {
			
			//Read value of page from preference
			  SharedPreferences selectPreferences = PreferenceManager.getDefaultSharedPreferences(RBL_iBank.context);
			  page =  selectPreferences.getString("page", "");

			
			JSONObject obj = new JSONObject();
			obj.put("page", page);
			
			   SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(RBL_iBank.context);
		        SharedPreferences.Editor prefsEditor = preferences.edit();
		        prefsEditor.putString("page", "");
		        prefsEditor.commit();

			
			callbackContext.success(obj);
		}
		return false;

	}
}
