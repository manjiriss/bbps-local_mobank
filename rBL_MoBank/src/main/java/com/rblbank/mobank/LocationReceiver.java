
package com.rblbank.mobank;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.focus.android.sdk.common.FAInfoWrapper;
import com.focus.android.sdk.common.internal.utils.StringUtils;
import com.focus.android.sdk.common.location.FALocation;
import com.focus.android.sdk.services.impl.LocationUtils;

public class LocationReceiver extends BroadcastReceiver {

    private static final String TAG = "FOCUSPLUGINRECEIVER";

    @Override
    public void onReceive(Context context, Intent intent) {
        FAInfoWrapper infoWrapper = LocationUtils.getLocationObject(context, intent.getStringExtra(StringUtils.EXTRA_REQUEST_ID));
        if (infoWrapper != null) {
            FALocation locationObject = infoWrapper.getLocationObject();

            if ( locationObject.isLocationAvailable() ) {
                Log.e(TAG, infoWrapper.getReadTime() + "," + locationObject.getLocationName());
//                if (!TextUtils.isEmpty(locationObject.getClientLocationIdentifier()) && !SectionType.OUTSIDE.equals(locationObject.getSectionType())) {
//                    displayNotification(context, locationObject.getClientLocationIdentifier());
//                }
            } else {
                Log.e(TAG, infoWrapper.getReadTime() + "," + (locationObject.getGpsData() == null ? "No GPS" : locationObject.getGpsData()));
            }

        } else {
            String failureMessage = intent.getStringExtra(StringUtils.EXTRA_LOCATION_FAIL_RESPONSE);
            Log.e(TAG, (failureMessage == null ? "Failed NA" : failureMessage));
        }
    }

    /*private void displayNotification(Context context, String notificationMessage) {
        Log.e("GetFocus", "Show notification");
        NotificationCompat.Builder nb= new NotificationCompat.Builder(context);
        nb.setSmallIcon(R.drawable.icon);
        nb.setContentTitle(context.getResources().getString(R.string.app_name));
        nb.setContentText(notificationMessage);

        Intent resultIntent = new Intent(context, RBL_iBank.class);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent resultPendingIntent = null;

        //if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
        *//*
        TaskStackBuilder TSB = TaskStackBuilder.create(context);
            TSB.addParentStack(RBL_iBank.class);
            // Adds the Intent that starts the Activity to the top of the stack
            TSB.addNextIntent(resultIntent);
            resultPendingIntent =
                    TSB.getPendingIntent(
                            0,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );
*//*
        //} else {
            resultPendingIntent = PendingIntent.getActivity(context, (int) new Date().getTime(), resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        //}
        nb.setContentIntent(resultPendingIntent);
        nb.setAutoCancel(true);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify((int) Math.round(Math.random()*100), nb.build());

    }*/
}
