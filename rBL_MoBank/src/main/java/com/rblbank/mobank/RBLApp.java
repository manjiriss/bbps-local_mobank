package com.rblbank.mobank;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.worklight.wlclient.api.WLClient;
import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.WLResponseListener;

public class RBLApp extends Application {

	public WLClient mWlClient;


	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	/**
	 * Flag to check whether WLClient get connected successfully or not. 'true'
	 * if connected else 'false'.
	 */
	public boolean isWlClientConnected = false;

	public RBLApp() {
		super();
		Log.e("In RBLApp-", "Getting 'WLClient.createInstance'");
		/*
		 * mWlClient=WLClient.createInstance(this); connectWLClient(this);
		 */
	}

	/**
	 * To connect WLClient.
	 */
	public void connectWLClient(Context mContext) {
		// TODO Auto-generated method stub

		if (!isWlClientConnected) {
			// connect WlClint
			mWlClient = WLClient.createInstance(mContext);
			mWlClient.connect(new WLResponseListener() {

				@Override
				public void onSuccess(WLResponse arg0) {
					// TODO Auto-generated method stub
					Log.e("  WLConnection onSuccess :=>: ",
							arg0.getResponseText());
					isWlClientConnected = true;
				}

				@Override
				public void onFailure(WLFailResponse arg0) {
					// TODO Auto-generated method stub
					Log.e(" error WLConnection onFailure :: ",
							arg0.getResponseText());
					isWlClientConnected = false;
				}
			});
		}

	}

}
