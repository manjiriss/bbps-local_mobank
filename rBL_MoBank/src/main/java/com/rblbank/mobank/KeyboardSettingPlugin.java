package com.rblbank.mobank;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONException;
import org.json.JSONObject;

import hexadots.in.rblmobank.services.FloatingViewService;
import hexadots.in.rblmobank.utils.Constants;

public class KeyboardSettingPlugin extends CordovaPlugin {

    public static final String KeyboardSetting = "KeyboardSetting";
    public static final String CheckStatusChatpay = "CheckStatusChatpay";
    public static final String TogglebuttonChatpayicon = "TogglebuttonChatpayicon";

    @Override
    public boolean execute(String action, String rawArgs,
                           CallbackContext callbackContext) throws JSONException {

        if (action.equals(KeyboardSetting)) {

            RBL_iBank.context.startActivityForResult(new Intent(android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS), 0);
            //callbackContext.success("response from plugin");
        } else if (action.equals(CheckStatusChatpay)) {
            SharedPreferences sharedpreferences = RBL_iBank.context.getSharedPreferences(Constants.KEY_PREF_NAME, Context.MODE_PRIVATE);
            Boolean bIsON = sharedpreferences.getBoolean(Constants.KEY_PREF_START, false);

            JSONObject obj = new JSONObject();
            obj.put("statuschatpay", bIsON);

            callbackContext.success(obj);

        } else if (action.equals(TogglebuttonChatpayicon)) {
            SharedPreferences sharedpreferences = RBL_iBank.context.getSharedPreferences(Constants.KEY_PREF_NAME, Context.MODE_PRIVATE);
            Boolean bIsON = sharedpreferences.getBoolean(Constants.KEY_PREF_START, false);
            if (bIsON) {
                //Hide the floating keyboard icon
                sharedpreferences.edit().putBoolean(Constants.KEY_PREF_START, false).commit();
                stopFloatingService();
            } else {
                //Show the floating keyboard icon
                sharedpreferences.edit().putBoolean(Constants.KEY_PREF_START, true).commit();
                startFloatingService();
            }
//            JSONObject obj = new JSONObject();
//            obj.put("statuschatpay", "aa");
//
//            callbackContext.success(obj);
        }

        return false;

    }

    private void startFloatingService()
    {
        RBL_iBank.context.startService(new Intent(RBL_iBank.context, FloatingViewService.class));
    }

    private void stopFloatingService() {
        RBL_iBank.context.stopService(new Intent(RBL_iBank.context, FloatingViewService.class));
    }
}
