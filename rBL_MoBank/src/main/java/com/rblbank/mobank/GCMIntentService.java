package com.rblbank.mobank;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

@SuppressLint("NewApi")
public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "NotificationText";
	private static final String TokenID = "";
	private static String notificationId = "";

	// private String notificationIdHP = "";

	private SharedPreferences sharedpreferences;

	private Editor editor;

	public GCMIntentService() {
		super("GCMIntentService");
	}

	@Override
	public void onRegistered(Context context, String regId) {

		// Log.v(TAG, "onRegistered: "+ regId);

		JSONObject json;

		try {
			json = new JSONObject().put("event", "registered");
			json.put("regid", regId);

			// Log.v(TAG, "onRegistered: " + json.toString());

			// Send this JSON data to the JavaScript application above EVENT
			// should be set to the msg type
			// In this case this is the registration ID
			PushPlugin.sendJavascript(json);

		} catch (JSONException e) {
			// No message to the user is sent, JSON failed
			Log.e(TAG, "onRegistered: JSON exception");
		}
	}

	@Override
	public void onUnregistered(Context context, String regId) {
		// Log.d(TAG, "onUnregistered - regId: " + regId);
	}

	@Override
	protected void onMessage(Context context, Intent intent) {
		// Log.d(TAG, "onMessage - context: " + context);

		sharedpreferences = getSharedPreferences("RBL_MO_BANK",
				Context.MODE_PRIVATE);
		editor = sharedpreferences.edit();

		// SharedPreferences preferences =
		// PreferenceManager.getDefaultSharedPreferences(RBL_iBank.context);
		// SharedPreferences.Editor prefsEditor = preferences.edit();
		// prefsEditor.putString("page", "accounts");
		// prefsEditor.commit();

		// Extract the payload from the message
		Bundle extras = intent.getExtras();
		boolean isDuplicate = false;

		//Log.d(TAG, "" + extras);

		//Log.d("NotificationText", "payload - " + extras.getString("payload"));
		if (extras != null) {
			try {
				if (extras.getString("payload") != null
						&& extras.getString("payload").length() != 0) {

					if (sharedpreferences
							.getString("notificationIdHP", "")
							.trim()
							.equalsIgnoreCase(
									extras.getString("payload").trim())) {
						// Log.d(TAG, "Duplicate");
						isDuplicate = true;
					} else {
						// Log.d(TAG, "Not Duplicate");
						isDuplicate = false;

						// notificationIdHP = "null";
						editor.putString("notificationIdHP",
								extras.getString("payload").trim()).commit();
						new Handler().postDelayed(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub

								editor.putString("notificationIdHP", "")
										.commit();
							}
						}, 1000 * 5);
					}

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// if we are in the foreground, just surface the payload, else post
			// it
			// to the statusbar
			if (!isDuplicate) {
				/*
				 * if (PushPlugin.isInForeground()) {
				 * extras.putBoolean("foreground", true);
				 * PushPlugin.sendExtras(extras); // Log.d(TAG, "onMessage 1");
				 * } else
				 */{
					extras.putBoolean("foreground", false);

					// Send a notification if there is a message
					if (extras.getString("payload") != null
							&& extras.getString("payload").length() != 0) {

						createNotification(context, extras);
						PushPlugin.sendExtras(extras);
						// Log.d(TAG, "onMessage 3");
					}
					// Log.d(TAG, "onMessage 2");
				}
			} else {
				// notification is not shown when we've received duplicate
				// notificationPoolId
			}
		}
	}

	public void createNotification(Context context, Bundle extras) {
		Intent notificationIntent = null;

		long when = System.currentTimeMillis();
		int notification_id = (int) when;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		String clickable = "";
		// TODO clickable
		// Log.e("createNotification", "called");
		String strDeeplinking = extras.getString("deeplinking");
		strDeeplinking = strDeeplinking.replaceAll("\\\\", "");

		try {
			if (strDeeplinking != null && !strDeeplinking.equals("null")) {
				JSONObject jsonObject = new JSONObject(strDeeplinking);

				String deeplinkingType = jsonObject.get("DeeplinkingType")
						.toString();
				if (deeplinkingType != null) {
					String action = "", page = "";

					clickable = jsonObject.get("Clickable").toString();

					if (deeplinkingType.equalsIgnoreCase("url")) {
						// if URL deeplinking

						action = jsonObject.get("Action").toString();
					} else if (deeplinkingType.equalsIgnoreCase("page")) {
						// if PAGE deeplinking
						page = jsonObject.get("Action").toString();
						// Page deeplinking
						if (page != null && !page.equals("")) {

							SharedPreferences preferences = PreferenceManager
									.getDefaultSharedPreferences(context);
							Editor prefsEditor = preferences
									.edit();
							prefsEditor.putString("page", page.trim()
									.toLowerCase());
							prefsEditor.commit();
						}
					}

					if (clickable != null) {

						if (clickable.equalsIgnoreCase("YES")) {
							if (action != null && !action.equals("")
									&& !action.equals("null")) {
								notificationIntent = new Intent(
										Intent.ACTION_VIEW, Uri.parse(action));
							}
							// System.out.println("Clicked on clickable TEXT");
							// TODO Clicked on clickable TEXT

						} else {
							// if clickable is "NO" -> nothing should happen on
							// click of notification
							// mNotificationManager.cancel(notification_id);
							// builder.setContentIntent(PendingIntent.getActivity(this,
							// 0, new Intent(), 0));
							// System.out.println("Clicked on non clickable TEXT");
							// TODO Clicked on non clickable TEXT
							if (action != null) {

							}
						}
					}

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (extras.getString("image").equalsIgnoreCase("null")) {

			String appName = getAppName(this);

			PendingIntent contentIntent = setPendingIntent(notificationIntent,
					extras);

			// Use NotificationCompat.Builder to set up our notification.
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					context);
			builder.setDefaults(Notification.DEFAULT_SOUND);

			// icon appears in device notification bar and right hand corner of
			// notification
			builder.setSmallIcon(context.getApplicationInfo().icon);

			// Content title, which appears in large type at the top of the
			// notification
			builder.setContentTitle(appName);

			// Content text, which appears in smaller text below the title
			String message = extras.getString("payload");
			if (message != null) {
				builder.setContentText(message);
			} else {
				builder.setContentText("<missing message content>");
			}

			builder.setStyle(new NotificationCompat.BigTextStyle()
					.bigText(message));

			builder.setDefaults(Notification.DEFAULT_VIBRATE);

			if (clickable.equalsIgnoreCase("NO")) {
				// Do nothing
				// mNotificationManager.cancel(notification_id);
				builder.setContentIntent(PendingIntent.getActivity(this, 0,
						new Intent(), 0));
				// System.out.println("Clicked on non clickable IMAGE");
				// TODO Clicked on non clickable IMAGE
			} else {
				// Set the intent that will fire when the user taps the
				// notification.
				builder.setContentIntent(contentIntent);
				// System.out.println("Clicked on clickable IMAGE");
				// TODO Clicked on clickable IMAGE
			}
			// set auto cancel
			builder.setAutoCancel(true);

			// Large icon appears on the left of the notification
			builder.setLargeIcon(BitmapFactory.decodeResource(
					context.getResources(), R.drawable.icon));
			int notId = 0;

			// Log.e(TAG, "createnotification called:" +
			// "showing notification");
			mNotificationManager.notify((String) appName, notification_id,
					builder.build());

		} else {
			// System.out.println("in generate picture");
			String message = extras.getString("payload");
			String imageURL = extras.getString("image");
			String appName = getAppName(this);

			PendingIntent contentIntent = null;

			if (clickable.equalsIgnoreCase("NO")) {
				mNotificationManager.cancel(notification_id);
				// Do nothing
				// builder.setContentIntent(PendingIntent.getActivity(this, 0,
				// new Intent(), 0));
				contentIntent = PendingIntent.getActivity(this, 0,
						new Intent(), 0);
				// System.out.println("Clicked on non clickable ELSE IMAGE ");
				// TODO Clicked on non clickable ELSE IMAGE
			} else {
				// System.out.println("Clicked on clickable ELSE IMAGE ");
				// TODO Clicked on clickable ELSE IMAGE
				if (notificationIntent == null) {
					PackageManager pm = context.getPackageManager();

					notificationIntent = pm
							.getLaunchIntentForPackage(getApplicationContext()
									.getPackageName());

					notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
							| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
							| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);

					// notificationIntent.putExtra("page", "testing");

					contentIntent = PendingIntent.getActivity(context, 100,
							notificationIntent, PendingIntent.FLAG_ONE_SHOT);

				} else {
					contentIntent = PendingIntent.getActivity(this, 0,
							notificationIntent, 0);

				}
			}
			new generatePictureStyleNotification(this, contentIntent, appName,
					message, imageURL).execute();

		}
	}

	private PendingIntent setPendingIntent(Intent notificationIntent,
										   Bundle extras) {
		PendingIntent contentIntent;

		// if notificationIntent is null then initialize the intent and open the
		// application with the required payload data.
		if (notificationIntent == null) {
			// For opening the application , on click of notification

			PackageManager pm = getPackageManager();
			notificationIntent = pm
					.getLaunchIntentForPackage(getApplicationContext()
							.getPackageName());

			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
					| Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
			notificationIntent.putExtra("pushBundle", extras);
			// notificationIntent.putExtra("page", "testing");

			contentIntent = PendingIntent.getActivity(this, 0,
					notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		} else {
			// For launching browser, on click of notification
			contentIntent = PendingIntent.getActivity(this, 0,
					notificationIntent, 0);
		}
		return contentIntent;
	}

	private static String getAppName(Context context) {
		CharSequence appName = context.getPackageManager().getApplicationLabel(
				context.getApplicationInfo());

		return (String) appName;
	}

	@Override
	public void onError(Context context, String errorId) {
		Log.e(TAG, "onError - errorId: " + errorId);
	}

}
