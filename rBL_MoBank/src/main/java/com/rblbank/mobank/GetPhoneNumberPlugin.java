package com.rblbank.mobank;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.telephony.TelephonyManager;
/**
 * This class is used to get the phone number from native code and pass it to main.js class.
 * @author ReshmiR
 *
 */
public class GetPhoneNumberPlugin extends CordovaPlugin {

	public static final String PHONE_NUMBER = "phone_number";

	@Override
	public boolean execute(String action, String rawArgs,
			CallbackContext callbackContext) throws JSONException {

		if (action.equals(PHONE_NUMBER)) {
			TelephonyManager tMgr = (TelephonyManager) RBL_iBank.getInstance()
					.getActivity().getSystemService(Context.TELEPHONY_SERVICE);
			String phoneNumber = tMgr.getLine1Number();
			if(phoneNumber == null || phoneNumber.equals("")){
				phoneNumber = "null";
			}
			
			JSONObject obj = new JSONObject();
			obj.put("phoneNumber", phoneNumber);
			
			callbackContext.success(obj);
		}
		return false;

	}
}
