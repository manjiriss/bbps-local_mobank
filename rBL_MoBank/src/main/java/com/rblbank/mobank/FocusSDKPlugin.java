package com.rblbank.mobank;

import android.content.Context;
import android.util.Log;

import com.focus.android.sdk.FocusSdk;
import com.focus.android.sdk.common.messaging.StatusCodeObjectWrapper;
import com.focus.android.sdk.common.user.Gender;
import com.focus.android.sdk.common.user.UserForm;
import com.focus.android.sdk.common.user.UserInfo;
import com.focus.android.sdk.services.communications.transport.callback.IDisableBackgroundTrackingRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IDisableUserDeviceRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IDisableUserRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IEnableBackgroundTrackingRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IEnableUserDeviceRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IEnableUserRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IFetchUserResponseListener;
import com.focus.android.sdk.services.communications.transport.callback.IInitializeRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IRegisterRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IUnregisterRequestListener;
import com.focus.android.sdk.services.communications.transport.callback.IUpdateUserRequestListener;
import com.focus.android.sdk.services.exceptions.FocusSdkInitializationException;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FocusSDKPlugin extends CordovaPlugin{
	
	private static final String TAG = "FOCUSPLUGIN";
	
	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView){
		super.initialize(cordova, webView);
	}
	
	@Override
	public boolean execute(String action, JSONArray args, final CallbackContext context) throws JSONException{
		Log.e("focus","execute " + action );
	switch(action){
            case "init":
            case "onDeviceReady":
            	initFocusSDK(context);
		return true;
            case "sdkState":
            	getState(cordova.getActivity().getApplicationContext(), context);
		return true;
            case "userServiceLevel":
            	getUserServiceLevel(context);
		return true;
            case "clientServiceLevel":
            	getClientServiceLevel(context);
		return true;
            case "registerUserId":
		registerUserId(args, context);
		return true;
	    case "fetchUserInfo":
	    	fetchUserInfo(context);
	    	return true;
	    case "updateUser":
	    	updateUser(args, context);
	    	return true;
	    case "registerUserMailId":
	    	registerUserMail(args, context);
	    	return true;
	    case "registerUserName":
	    	registerUserName(args, context);
	    	return true;	
            case "unRegisterUser":
            	unRegisterUser(context);
		return true;
            case "predictLocation":
            	predictLocation(context);
				Log.e("focus","predict triggered");
		return true;
            case "predictLocationParams":
          	predictLocationStr(args, context);
		return true;
	    case "predictLocationParamsMap":
	    	predictLocationMap(args, context);
            case "enableUser":
            	enableUser(context);
		return true;
            case "enableUserDevice":
            	enableUserDevice(context);
		return true;
            case "enableTracking":
            	enableTracking(context);
		return true;
            case "disableUser":
            	disableUser(context);
		return true;
            case "disableUserDevice":
            	disableUserDevice(context);
		return true;
            case "disableTracking":
            	disableTracking(context);
		return true;
            
            default:
            	return true;

        }
		
	}
	
	
	private void sendSuccessCallbackToJS(final CallbackContext context, String message){
		PluginResult result = new PluginResult(PluginResult.Status.OK, message);
		result.setKeepCallback(true);
		context.sendPluginResult(result);
	}

	private void sendFailureCallbackToJS(final CallbackContext context, String message){
		PluginResult result = new PluginResult(PluginResult.Status.ERROR, message);
		result.setKeepCallback(true);
		context.sendPluginResult(result);
	}
	
	private void initFocusSDK(final CallbackContext context){
		cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	FocusSdk.initialize(cordova.getActivity().getApplicationContext(), new IInitializeRequestListener() {
            		@Override
            		public void onInitializeCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
                		if( statusCodeObjectWrapper != null && statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
                    			//try {
                    			//initialized = true;
                        		sendSuccessCallbackToJS(context, "Initialized Successful");
                		}else{
                    			//Unsuccessful init
                    			sendFailureCallbackToJS(context, "failed - " + (statusCodeObjectWrapper != null ? statusCodeObjectWrapper.getStatusCode().getMessage() : "NULL"));
                		}
            		}
        	});    
                }
            });
		
        }
        
        private void getUserServiceLevel(final CallbackContext context){
        cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    String level = FocusSdk.getInstance().getUserState().toString();
		    sendSuccessCallbackToJS(context, "User ServiceLevel - " + level);
		} catch (FocusSdkInitializationException e) {
		    //e.printStackTrace();
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		
		}
	});
    }

    private void getClientServiceLevel(final CallbackContext context){
        cordova.getThreadPool().execute(new Runnable() {
                public void run() {
			try {
			    String level = FocusSdk.getInstance().getClientState().toString();
			    sendSuccessCallbackToJS(context, "Client ServiceLevel - " + level);
			} catch (FocusSdkInitializationException e) {
			    //e.printStackTrace();
			    sendFailureCallbackToJS(context, e.getMessage());
			}
		}
	});
    }

    private void registerUserId(final JSONArray args, final CallbackContext context){
    	
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try{
		    String uid = (args.length() == 1 ? args.getString(0) : null);
		    if( uid == null || uid.length() == 0 ){
			//Pass callback with error message saying null userid
			sendFailureCallbackToJS(context, "User Identifier should not be empty");
			return;
		    }
		    FocusSdk.getInstance().registerUser(new UserForm(uid), new IRegisterRequestListener() {
		        @Override
		        public void onRegisterCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
		            sendSuccessCallbackToJS(context,"user Registered successfully!");
		            }else{
		            	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
		            } 
		        }
		    });
		}catch (FocusSdkInitializationException | JSONException e){
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }
    
    private void registerUserMail(final JSONArray args, final CallbackContext context){
    	
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try{

		    if( args.length() != 2 ){
		    	sendFailureCallbackToJS(context, "Missing userId, email parameters for registration");
		    	return;
		    }
		    String uid = args.getString(0);
		    String email = args.getString(1);
		    if( uid == null || uid.length() == 0 || email == null || email.length() == 0 ){
			//Pass callback with error message saying null userid
			sendFailureCallbackToJS(context, "Error! UserId, email can't take null values");
			return;
		    }
		    FocusSdk.getInstance().registerUser(new UserForm(uid, email), new IRegisterRequestListener() {
		        @Override
		        public void onRegisterCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
		            	sendSuccessCallbackToJS(context, "User Registered successfully!");
		            }else{
		            	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
		            } 
		        }
		    });
		}catch (FocusSdkInitializationException | JSONException e){
		    //e.printStackTrace();
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }
    
    private void registerUserName(final JSONArray args, final CallbackContext context){
    	
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try{
		    UserForm userform = parseUserName(args);
		    if( userform == null ){
			//Pass callback with error message saying null userid
			sendFailureCallbackToJS(context, "Error parsing user details, possibly incorrect values");
			return;
		    }
		    FocusSdk.getInstance().registerUser(userform, new IRegisterRequestListener() {
		        @Override
		        public void onRegisterCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
		            	sendSuccessCallbackToJS(context, "User Registered successfully!");
		            }else{
		            	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
		            } 
		        }
		    });
		}catch (FocusSdkInitializationException | JSONException e){
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }
    
    private void updateUser(final JSONArray args, final CallbackContext context){
    	
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try{
		    UserForm userform = parseUserName(args);
		    if( userform == null ){
			//Pass callback with error message saying null userid
			sendFailureCallbackToJS(context, "Error parsing user details, possibly incorrect values");
			return;
		    }
		    FocusSdk.getInstance().updateUserInfo(userform, new  IUpdateUserRequestListener() {
		        @Override
		        public void onUpdateUserCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
		            	sendSuccessCallbackToJS(context, "User Registered successfully!");
		            }else{
		            	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
		            } 
		        }
		    });
		}catch (FocusSdkInitializationException | JSONException e){
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }
    
    private void fetchUserInfo(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
			try{
				FocusSdk.getInstance().fetchUserDetails(new IFetchUserResponseListener() {
				    @Override
				    public void onFetchUserResponseCallback(StatusCodeObjectWrapper<UserInfo> statusCodeObjectWrapper) {
					    if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
					    	StringBuilder builder = new StringBuilder();
					    	if( statusCodeObjectWrapper.getWrappedObject() != null ){
							UserInfo userInfo = statusCodeObjectWrapper.getWrappedObject();
						
							builder.append("UserId : " + userInfo.getUserId());
							builder.append("EMail : " + userInfo.getEmail());
							builder.append("FirstName : " + userInfo.getFirstName());
							builder.append("LastName : " + userInfo.getLastName());
							builder.append("MiddleName : " + userInfo.getMiddleName());
							builder.append("Gender : "+ (userInfo.getGender() == null ? "" : userInfo.getGender().toString()));
							builder.append("Mobile : " + userInfo.getMobile());
							builder.append("UserServiceLevel : " + ( userInfo.getUserServiceLevel() == null ? "" : userInfo.getUserServiceLevel().toString()));
							builder.append("BirthDay : " + userInfo.getBirthday());
							builder.append("DisplayName : " + userInfo.getDisplayName());
						
						}					 
					    	sendSuccessCallbackToJS(context, "Fetch details done - " + builder.toString());
					    }else{
					    	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
					    }					
				    }
				});
			
			}catch (FocusSdkInitializationException e) {
		    		sendFailureCallbackToJS(context, e.getMessage());
			}               	
                }
        });    	
    }

    private void unRegisterUser(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().unregisterUser(new IUnregisterRequestListener() {
		        @Override
		        public void onUnregisterCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
		            	sendSuccessCallbackToJS(context, "User un-Registered successfully");
		            }else{
		            	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
		            } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }

    private void getState(Context context, final CallbackContext callback){
	String state = FocusSdk.getSetupState(context).toString();
	sendSuccessCallbackToJS(callback, state);
    }
    
    
    private void enableUser(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().enableUser(new IEnableUserRequestListener() {
		        @Override
		        public void onEnableUserCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
				    sendSuccessCallbackToJS(context, "User Enabled successfully");
			      }else{
			      	    sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
			      } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
        }
    	});
    }

    private void disableUser(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().disableUser(new IDisableUserRequestListener() {
		        @Override
		        public void onDisableUserCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
				    	sendSuccessCallbackToJS(context, "User Disabled successfully");
				    }else{
				    	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
				    } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
        }
        });
    }

    private void enableUserDevice(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().enableUserDevice(new IEnableUserDeviceRequestListener() {
		        @Override
		        public void onEnableUserDeviceCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
				    	sendSuccessCallbackToJS(context, "UserDevice Enabled successfully");
				    }else{
				    	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
				    } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }

    private void disableUserDevice(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().disableUserDevice(new IDisableUserDeviceRequestListener() {
		        @Override
		        public void onDisableUserDeviceCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
				    	sendSuccessCallbackToJS(context, "UserDevice disabled successfully");
				    }else{
				    	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
				    } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }

    private void enableTracking(final CallbackContext context){
	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().enableBackgroundTracking(new IEnableBackgroundTrackingRequestListener() {
		        @Override
		        public void onEnableBackgroundTrackingCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
				    	sendSuccessCallbackToJS(context, "Background Tracking enabled successfully");
				    }else{
				    	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
				    } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }

    private void disableTracking(final CallbackContext context){
        cordova.getThreadPool().execute(new Runnable() {
                public void run() {
		try {
		    FocusSdk.getInstance().disableBackgroundTracking(new IDisableBackgroundTrackingRequestListener() {
		        @Override
		        public void onDisableBackgroundTrackingCallback(StatusCodeObjectWrapper statusCodeObjectWrapper) {
		            if( statusCodeObjectWrapper.getStatusCode().isSuccessful() ){
				    	sendSuccessCallbackToJS(context, "Background Tracking disabled successfully");
				    }else{
				    	sendFailureCallbackToJS(context, statusCodeObjectWrapper.getStatusCode().getMessage());
				    } 
		        }
		    });
		} catch (FocusSdkInitializationException e) {
		    sendFailureCallbackToJS(context, e.getMessage());
		}
		}
	});
    }
    
    private void predictLocation(final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
                	try{	
                		FocusSdk.getInstance().predictLocation();
						sendSuccessCallbackToJS(context,"location fetched");
                	}catch(FocusSdkInitializationException e){
                		sendFailureCallbackToJS(context, e.getMessage());
                	}
           	}
        });
    }
    
    private void predictLocationStr(final JSONArray args, final CallbackContext context){
    	
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
           		try{
           			String paramString = (args.length() == 1 ? args.getString(0) : null);
				if( paramString == null || paramString.length() == 0 ){
					//Pass callback with error message saying null userid
					sendFailureCallbackToJS(context, "Error parsing json array, possibly provided an Empty Array");
					return;
				}	
                		FocusSdk.getInstance().predictLocation(paramString);
                		sendSuccessCallbackToJS(context, "Sent for Prediction STR");
                	}catch(FocusSdkInitializationException | JSONException e){
                		sendFailureCallbackToJS(context, e.getMessage());
                	} 
                }
        });
    }
    
    private void predictLocationMap(final JSONArray paramsArray, final CallbackContext context){
    	cordova.getThreadPool().execute(new Runnable() {
                public void run() {
           		try{	
           			Map<String, String> map = convertToMap(paramsArray);
           			if( map == null || map.size() == 0 ){
           				sendFailureCallbackToJS(context, "Error parsing json array");
           			}else{
		        		FocusSdk.getInstance().predictLocation(map);
		        		sendSuccessCallbackToJS(context, "Sent for Prediction MAP");
                		}
                	}catch(FocusSdkInitializationException | JSONException e){
                		sendFailureCallbackToJS(context, e.getMessage());
                	} 
                }
        });
    }
    
    private static Map<String, String> convertToMap(JSONArray arr) throws JSONException{
        Map<String, String> paramsMap;
        if( arr == null || arr.length() == 0 || arr.isNull(0) ){
            return null;
        }else{
            try{
                JSONObject jsonParams = arr.getJSONObject(0);
                paramsMap = toMap(jsonParams);
                return paramsMap;

            }catch (JSONException e){
                return null;
            }
        }

    }

    private static Map<String, String> toMap(JSONObject jsonObject) throws JSONException {
        Map<String, String> map = new HashMap<>();
        Iterator keys = jsonObject.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            map.put(key, jsonObject.getString(key));
        }
        return map;
    }
    
    private static UserForm parseUserForm(JSONArray arr) throws JSONException {
        if( arr == null || arr.length() == 0 || arr.isNull(0) ){
            return null;
        }else{
            UserForm userForm = null;
            if( arr.length() > 1){
                userForm = new UserForm(arr.getString(0), arr.getString(1));

                if( arr.length() >= 5 ){
                    userForm.setDisplayName(arr.getString(2));
                    if( !arr.isNull(3) && arr.getInt(3) < 2 ){
                        userForm.setGender(arr.getInt(3) == 0 ? Gender.MALE : Gender.FEMALE);
                    }
                    //userForm.setMobile(arr.getString(4));
                    /*try {
			
                        userForm.setBirthday(new Date(arr.getString(5)));
                    } catch (FocusValidationException e) {
                        e.printStackTrace();
                    }*/
                }else if( arr.length() >= 4 ){
                    userForm.setDisplayName(arr.getString(2));
                    if( !arr.isNull(3) && arr.getInt(3) < 2 ){
                        userForm.setGender(arr.getInt(3) == 0 ? Gender.MALE : Gender.FEMALE);
                    }
                    //userForm.setMobile(arr.getString(4));
                }else if( arr.length() >= 3 ){
                    userForm.setDisplayName(arr.getString(2));
                    //userForm.setGender(Gender.valueOf(arr.getString(3)));
                }/*else if( arr.length() >= 2 ){
                    userForm.setDisplayName(arr.getString(2));
                }*/
            }else if( arr.length() == 0 ){
                userForm = new UserForm(arr.getString(0));
            }

            return userForm;
        }
    }
    
    private static UserForm parseUserName(JSONArray arr) throws JSONException{
    	if( arr == null || arr.length() == 0 || arr.length() != 3 || arr.isNull(0) || arr.isNull(1) || arr.isNull(2) ){
            return null;
        }else {
		return new UserForm( arr.getString(0), arr.getString(1), arr.getString(2) );
        }
    }
}
