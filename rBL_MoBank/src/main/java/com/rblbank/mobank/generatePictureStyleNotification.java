package com.rblbank.mobank;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;

@SuppressLint("NewApi")
public class generatePictureStyleNotification extends
		AsyncTask<String, Void, Bitmap> {

	private Context mContext;
	private String title, message, imageUrl;
	private int countTrials = 0;
	PendingIntent contentIntent;
	private Bitmap myBitmap = null;

	public generatePictureStyleNotification(Context context,
											PendingIntent contentIntent, String title, String message,
											String imageUrl) {
		super();
		this.mContext = context;
		this.contentIntent = contentIntent;
		this.title = title;
		this.message = message;
		this.imageUrl = imageUrl;
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		myBitmap = null;

		// System.out.println("start now");
		while (myBitmap == null) {
			// new Handler().postDelayed(new Runnable() {
			//
			// @Override
			// public void run() {
			// myBitmap = downloadImage();
			// if (countTrials > 4) {
			// System.out.println("go for normal now");
			// return;
			// } else {
			// System.out.println("try again - " + countTrials);
			// countTrials++;
			// }
			// }
			// }, 1000);


			try {
				myBitmap = downloadImage();
				if (countTrials > 4) {
					//System.out.println("go for normal now");
					break;
				} else {
					//System.out.println("try again - " + countTrials);
					countTrials++;
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}



			// myBitmap = downloadImage();
			// if (countTrials > 4) {
			// // System.out.println("go for normal now");
			// break;
			// } else {
			// // System.out.println("try again - "+countTrials);
			// countTrials++;
			// }
		}
		return myBitmap;
	}

	private Bitmap downloadImage() {
		try {
			URL url = new URL(this.imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream in = connection.getInputStream();
			return BitmapFactory.decodeStream(in);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		super.onPostExecute(result);

		if (result == null) {

//			System.out.println("Normal notification in itteration - "
//					+ countTrials);
			long when = System.currentTimeMillis();
			int notification_id = (int) when;
			NotificationManager mNotificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			String appName = this.title;

			// Use NotificationCompat.Builder to set up our notification.
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					mContext);
			builder.setDefaults(Notification.DEFAULT_SOUND);

			// icon appears in device notification bar and right hand corner
			// of
			// notification
			builder.setSmallIcon(mContext.getApplicationInfo().icon);

			// Content title, which appears in large type at the top of the
			// notification
			builder.setContentTitle(this.title);

			// Content text, which appears in smaller text below the title

			builder.setContentText(this.message);

			builder.setStyle(new NotificationCompat.BigTextStyle()
					.bigText(message));

			builder.setDefaults(Notification.DEFAULT_VIBRATE);

			// Set the intent that will fire when the user taps the
			// notification.
			if (contentIntent != null)
				builder.setContentIntent(contentIntent);

			// set auto cancel
			builder.setAutoCancel(true);

			// Large icon appears on the left of the notification
			builder.setLargeIcon(BitmapFactory.decodeResource(
					mContext.getResources(), R.drawable.icon));
			int notId = 0;

			// Log.e(TAG, "createnotification called:" +
			// "showing notification");
			mNotificationManager.notify((String) appName, notification_id,
					builder.build());

		} else {
//			System.out.println("Big picture in itteration - " + countTrials);

			long when = System.currentTimeMillis();
			int notification_id = (int) when;
			NotificationManager mNotificationManager = (NotificationManager) mContext
					.getSystemService(Context.NOTIFICATION_SERVICE);
			String appName = this.title;

			/*
			 * PackageManager pm = mContext.getPackageManager(); Intent
			 * notificationIntent = pm.getLaunchIntentForPackage(mContext
			 * .getApplicationContext().getPackageName());
			 *
			 * // notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP |
			 * // Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 * notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
			 * Intent.FLAG_ACTIVITY_REORDER_TO_FRONT |
			 * Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT); //
			 * notificationIntent.putExtra("pushBundle", extras);
			 *
			 * PendingIntent contentIntent = PendingIntent.getActivity(mContext,
			 * 100, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
			 */

			// Use NotificationCompat.Builder to set up our notification.
			NotificationCompat.Builder builder = new NotificationCompat.Builder(
					mContext);
			builder.setDefaults(Notification.DEFAULT_SOUND);

			// icon appears in device notification bar and right hand corner
			// of
			// notification
			builder.setSmallIcon(mContext.getApplicationInfo().icon);

			// Content title, which appears in large type at the top of the
			// notification
			builder.setContentTitle(this.title);

			// Content text, which appears in smaller text below the title

			builder.setContentText(this.message);

			// TODO ok till here

			NotificationCompat.BigPictureStyle s = new NotificationCompat.BigPictureStyle()
					.bigPicture(result);
			s.setSummaryText(message);
			builder.setStyle(s);

			// builder.setStyle(new
			// NotificationCompat.BigTextStyle().bigText(message));

			builder.setDefaults(Notification.DEFAULT_VIBRATE);

			// Set the intent that will fire when the user taps the
			// notification.
			if (contentIntent != null)
				builder.setContentIntent(contentIntent);

			// set auto cancel
			builder.setAutoCancel(true);

			// Large icon appears on the left of the notification
			builder.setLargeIcon(BitmapFactory.decodeResource(
					mContext.getResources(), R.drawable.icon));

			Notification notification = builder.build();
			// Will display the notification in the notification bar
			mNotificationManager.notify(notification_id, notification);
			// mNotificationManager.notify((String) appName,
			// notification_id,
			// builder.build());
		}
	}
}

/*
 * public class GCMIntentService extends
 * com.worklight.androidgap.push.GCMIntentService{ //Nothing to do here... }
 */
