package com.rblbank.mobank;

import android.content.Context;
import android.content.Intent;

import com.mobank.upi.view.activity.LoginActivity;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UPIPlugin extends CordovaPlugin {
    public static final String InitializeSDK = "initializeSDK";
//	public static final String ListBankAccount = "listBankAccount";


    private static String userId;
    private static String password;
//	private static String accountNumber;

    private Context getApplicationContext() {
        return this.cordova.getActivity().getApplicationContext();
    }


    @Override
    public boolean execute(String action, JSONArray data, final CallbackContext callbackContext)
            throws JSONException {

        boolean result = false;

        if (InitializeSDK.equals(action)) {

            Intent intent = new Intent(this.cordova.getActivity(),
                    LoginActivity.class);
            this.cordova.getActivity().startActivity(intent);

            JSONObject inObj = data.getJSONObject(0);
            userId = (String) inObj.get("userId");
            password = (String) inObj.get("password");

            JSONObject outObj = new JSONObject();
//			outObj.put("userId", userId);
//			outObj.put("password", password);
//			callbackContext.success(outObj);

            outObj.put("userId", userId);
            outObj.put("password", password);
            callbackContext.success(outObj);

            result = true;
        }

//		else if(ListBankAccount.equals(action)){
//
//			JSONObject inObj =  data.getJSONObject(0);
//			accountNumber = (String) inObj.get("accountNumber");
//			
//			JSONObject outObj = new JSONObject();
//			outObj.put("accountNumber", accountNumber);
//			callbackContext.success(outObj);
//			
//			result = true;
//		}
        return result;
    }
}
