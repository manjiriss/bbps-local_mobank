package com.rblbank.mobank;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.rblbank.mobank.helpers.ConnectionHelper;
import com.worklight.androidgap.WLDroidGap;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;

import hexadots.in.rblmobank.keyboardbanking.MyShakeMotion;
import hexadots.in.rblmobank.services.FloatingViewService;
import hexadots.in.rblmobank.utils.Constants;


public class RBL_iBank extends WLDroidGap {
    public static CustomerBean custBean = new CustomerBean();
    /**
     * RBLApplication instance.
     */
    RBLApp rblApp;
    public static RBL_iBank context;

    /**
     * flag that informs if app is updated or not as per the playstore
     * if its true by default, it wont even check for playstore
     * version so keep it true while giving it to client for testing
     * and false while publishing to playstore
     */
    public boolean isUpdated = false;
    public boolean isDownloadPopupShowing = false;
    private String currentVersion;
    private String latestVersion;

    private static final int OVERLAY_PERMISSION_REQ_CODE = 1000;
    public static RBL_iBank getInstance() {
        return context;
    }

    private SharedPreferences sharedpreferences;
    private static final int REQUEST_SMS = 1;

    private static String[] PERMISSIONS_RBL = {
            android.Manifest.permission.RECEIVE_SMS,
            android.Manifest.permission.READ_SMS
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);// index.html



        context = this;
        String flg;
        Intent intent = getIntent();
        if (intent.hasExtra("custLogFlg")) {
            flg = decrypt(intent.getStringExtra("custLogFlg"));
            custBean.setCustLogFlg(Boolean.parseBoolean(flg));
        } else {
            custBean.setCustLogFlg(false);
        }
        if (intent.hasExtra("custId")) {
            String cid = decrypt(intent.getStringExtra("custId"));
            custBean.setCustId(cid);
            //System.out.println(cid);
        } else {
            custBean.setCustId(null);
        }
        if (intent.hasExtra("custPWD")) {
            String cpwd = decrypt(intent.getStringExtra("custPWD"));
            custBean.setCustPWD(cpwd);
            //System.out.println(cpwd);
        } else {
            custBean.setCustPWD(null);
        }
        if (intent.hasExtra("module")) {
            String module = decrypt(intent.getStringExtra("module"));
            custBean.setModule(module);
            //System.out.println(module);
        } else {
            custBean.setModule(null);
        }
        if (intent.hasExtra("deviceId")) {
            String deviceId = decrypt(intent.getStringExtra("deviceId"));
            custBean.setDeviceId(deviceId);
            //System.out.println(deviceId);
        } else {
            custBean.setDeviceId(null);
        }

        getCurrentVersion();
        initKeyboard();
    }


    /*This method is used to initialize and start the RBL keyboard functionality*/
    private void initKeyboard()
    {
        verifySMSPermissions();
        askForOverLay();
        startService();

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void verifySMSPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Check if we have write permission
            int permission = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.RECEIVE_SMS);
            int readSMSPermissions = ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_SMS);
            if (permission != PackageManager.PERMISSION_GRANTED || readSMSPermissions != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(PERMISSIONS_RBL, REQUEST_SMS);
            } else {
                Log.v(TAG, "--- permissions already granted ----");
            }
        }
    }



    /* RBL keyboard code below*/
    @TargetApi(Build.VERSION_CODES.M)
    public void askForOverLay()
    {// Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {

            if (!Settings.canDrawOverlays(this))
            {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            } else
            {
                initToggle();
            }
        } else
        {
            initToggle();
        }

    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        try
        {

            if (requestCode == OVERLAY_PERMISSION_REQ_CODE)
            {
                if (!Settings.canDrawOverlays(this))
                {
                    // SYSTEM_ALERT_WINDOW permission not granted...
                    //HERE we need to hide the floating switch because user didn't give the permission.
                } else
                {
                    startFloatingService();
                }
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void initToggle()
    {
        sharedpreferences = this.getSharedPreferences(Constants.KEY_PREF_NAME, Context.MODE_PRIVATE);
//        tglBoot.setChecked(sharedpreferences.getBoolean(Constants.KEY_PREF_BOOT, false));
//        tglStart.setChecked(sharedpreferences.getBoolean(Constants.KEY_PREF_START, false));
        if (sharedpreferences.getBoolean(Constants.KEY_PREF_START, false))
        {
            startFloatingService();
        }
    }

    private void startFloatingService()
    {
        startService(new Intent(context, FloatingViewService.class));
    }

    // Method to start the service
    public void startService()
    {
        startService(new Intent(context, MyShakeMotion.class));
    }

    /**
     * onWLInitCompleted is called when the Worklight runtime framework
     * initialization is complete
     */
    @Override
    public void onWLInitCompleted(Bundle savedInstanceState) {
        super.loadUrl(getWebMainFilePath());
        // Add custom initialization code after this line

		/*
         * Get Application instance...
		 */
        rblApp = (RBLApp) getApplication();
        // rblApp.connectWLClient(getActivity());

    }

    public static String decrypt(String str) {
        try {
            String str1 = xor(str);
            return new String(Base64.decode(str1.getBytes(), Base64.DEFAULT));
        } catch (Exception e) {
//                            Log.e(Constants.TAG , "decrypt Exception : "+e);
        }
        return "";
    }

    private static String xor(String input) {
        input = input.trim();
        String result = "";
        for (int index = 0; index < input.length(); index++) {
            int a = input.charAt(index);
            int b = a ^ 197346;
            result = result + Character.toString((char) b);
        }
        return result;
    }


    //check for update

    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
//It retrieves the latest version by scraping the content of current version from play store at runtime
                Document doc = Jsoup.connect("https://play.google.com/store/apps/details?id=com.rblbank.mobank").get();
                latestVersion = doc.getElementsByAttributeValue("itemprop", "softwareVersion").first().text();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if (latestVersion != null) {
                    if (currentVersion.equalsIgnoreCase(latestVersion)) {
                        isUpdated = true;
//                        loginFragment.handler.sendEmptyMessage(DataConstants.TOUCH_ID);
                    } else {
                        isUpdated = false;
                        if (isAppOnForeground()) { //This would help to check the context of whether activity is running or not, otherwise you'd get bind error sometimes
                            showUpdateDialog();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            super.onPostExecute(jsonObject);
        }
    }

    /**
     * Checks whether the app is in foreground or background
     *
     * @return true if app is foreground or false if app background
     */
    private boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return false;
        }
        String packageName = getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Retrieving customer id which was stored in encrypted format using Base64 encryption in Shared Preferences
     * <br/>
     * ((MainActivity) getActivity()).showMessage(message)
     *
     * @param message message to be shown on toast
     */
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    public void getCurrentVersion() {
        try {
            PackageManager pm = this.getPackageManager();
            PackageInfo pInfo = null;

            pInfo = pm.getPackageInfo(this.getPackageName(), 0);

            currentVersion = pInfo.versionName;

            if (new ConnectionHelper().isConnected(this)/* && !BuildConfig.DEBUG*/) {
                if (!isUpdated) {
                    new GetLatestVersion().execute();
                }
            } else {
                showMessage("Please ensure that you have network connectivity and try again!");
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showUpdateDialog() {
        try {
            isDownloadPopupShowing = true;
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.app_name));
            builder.setMessage("You're using an older version of the App, please download the latest version now.");
            builder.setPositiveButton("Download", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.rblbank.mobank")));
                    dialog.dismiss();
                    isDownloadPopupShowing = false;
                    finish();
                }
            });
            builder.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    isDownloadPopupShowing = true;
                    showRequestforUpdateDialog();
                }
            });

            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finish();
                }
            });

//        builder.setCancelable(false);
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showRequestforUpdateDialog() {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.app_name));
            builder.setMessage("The older version of the App may not function correctly. We request you to download the latest version before proceeding.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
//                    if (loginFragment != null) {
//                        if (loginFragment.isResumed()) {
//                            if (loginFragment.handler != null) {
//                                loginFragment.handler.sendEmptyMessage(DataConstants.TOUCH_ID);
//                            }
//                        }
//                    }
                }
            });

            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finish();
                }
            });

//        builder.setCancelable(false);
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}