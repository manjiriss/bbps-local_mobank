package com.rblbank.mobank;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.MyLocationOverlay;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.snapwork.constants.AppConstants;
import com.snapwork.ethreads.UserTask;
import com.snapwork.messages.ContactDetails;
import com.snapwork.messages.Place;
import com.snapwork.messages.ReplyItem;
import com.snapwork.parser.ContactsParser;
import com.snapwork.parser.PlaceDetailsParser;
import com.snapwork.util.ProxyUrlUtil;
import com.snapwork.widget.ActionItem;
import com.worklight.wlclient.api.WLClient;
import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLProcedureInvocationData;
import com.worklight.wlclient.api.WLRequestOptions;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.WLResponseListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/*
 * Chenge 'bg.png' (image behing down arrow)
 */
public class AtmBrancheLockerActivityD extends MapActivity implements
		OnClickListener, LocationListener, ReplyItem {

	private LocationManager lm;
	private List<Overlay> mapOverlays;
	private ListView listview;
	// private View sup;
	private double latitudeE6/* =28.5229 */;
	private double longitudeE6/* =77.2094 */;
	private String latitude/* ="28.5229" */; // New Delhi
	private String longitude/* ="77.2094" */;

	ReplyItem itm = this;
	Context ctx = this;
	String responce;
	private String CurrentLat;
	private String CurrentLon;
	// New Delhi 28.56843, 77.218902 Mumbai 19.56843, 73.218902
	private double Currlatitude;
	private double Currlongitude;
	boolean isGPSEnabled = false;
	boolean isNetworkEnabled = false;
	PlaceDetailsParser mPlaceHandler;
	Place placeList = null;
	private boolean EMULATOR = false;
	private TextView area, num, adddetail, ifsccode, weekon, weekend, weekoff,
			searcharea, close, facility, tlocker, tgold, tsilver, tatm;
	private EditText editarea;
	private MapView mapview;
	private ImageView sDown, edit, go, map, showlist, back, branch, atm,
			direction, sbranch, satm, soffres, sdirection, icon, atmfac,
			locfac, goldfac, silverfac, btnDirection;
	private ImageView direct, navhome, navbranch, navatm, navlock, dirshowlist,
			showText, ChangeStart, dropdown;
	private RelativeLayout showSlider, relative, directionrel;
	private LinearLayout inrel;
	private GetSearchData mGetSearchData;
	private ProgressDialog mPdialog = null;
	/**
	 * Data list of all ATMs, Branches and Lockers.
	 */
	private List<HashMap<String, String>> PlaceData = new ArrayList<HashMap<String, String>>();
	/**
	 * ATMs data list.
	 */
	private List<HashMap<String, String>> atmData = new ArrayList<HashMap<String, String>>();
	/**
	 * Branches data list.
	 */
	private List<HashMap<String, String>> branchData = new ArrayList<HashMap<String, String>>();
	/**
	 * Lockers data list.
	 */
	private List<HashMap<String, String>> lockerData = new ArrayList<HashMap<String, String>>();

	private List<HashMap<String, String>> searchData = new ArrayList<HashMap<String, String>>();
	private List<HashMap<String, String>> DirectionData = new ArrayList<HashMap<String, String>>();
	private List<HashMap<String, String>> offersData = new ArrayList<HashMap<String, String>>();
	private List<HashMap<String, String>> suggestData = new ArrayList<HashMap<String, String>>();
	private String areaName, cityName, stateName, Locality, fromX, fromY, toX,
			toY, curCity, CurDest, provider, areaCity, radius, setCurLat,
			setCurLon, ClickLat, ClickLon, Destination;
	private String atmDest, brDest, offTitle;
	private boolean flag, showlistFlag, branchFlag, atmFlag, lockerFlag,
			directionFlag, toggleFlag = true, click = true, animateToFlag,
			showlistFlagg;
	PopupWindow popup;
	boolean gpsFlag; // for GpsLocation
	boolean manFlag; // for ManualLocation
	boolean setting;
	private boolean branchOverFlag, atmOverFlag, offerOverFlag, tempFlag,
			offersFilterFlag, msgFlag;
	private List<ContactDetails> curLocationDetail;
	boolean allFlag;
	SharedPreferences settingPref;
	double selectedRadius;
	int sRadius;
	Typeface fontFace;
	// private GetLocation mGetLocation;

	private com.snapwork.widget.QuickAction mQuickAction;

	/**
	 * To handle visibility of 'ShowDirection' image button
	 */
	boolean hideBtnDirection = false;

	protected void onStart() {
		// initCheckBox();
		super.onStart();
		String resFrom = getResources().getString(R.string.res_from);
	}

	public void onPause() {
		super.onPause();
		// Log.i("onPause","onPause");
		try {
			if (mGetSearchData != null) {
				mGetSearchData.cancel(true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	boolean suggestlistFlag;
	int restCount = 0, spaCount = 0;

	/**
	 * RBLApplication instance.
	 */
	RBLApp rblApp;

	public void onCreate(Bundle savedInstancestate) {
		super.onCreate(savedInstancestate);
		setContentView(R.layout.showmapview);

		/*
		 * Get Application instance...
		 */
		rblApp = (RBLApp) getApplication();

		// Log.d("MapviewACitivyt","MapviewACitivyt");
		final ActionItem popupgo = new ActionItem();
		popupgo.setOnClickListener(cancelListener);

		fontFace = Typeface.createFromAsset(getBaseContext().getAssets(),
				"fonts/hlr___.ttf");

		mapview = (MapView) findViewById(R.id.mapview);
		mapview.setBuiltInZoomControls(true);

		Location location = null;
		lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String provider = lm.getBestProvider(criteria, false);
		String netProvider = LocationManager.NETWORK_PROVIDER;
		String gpsProvider = LocationManager.GPS_PROVIDER;
		if (provider.equalsIgnoreCase(netProvider)) {
			location = lm
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		} else if (provider.equalsIgnoreCase(gpsProvider)) {
			location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
			if (location == null)
				;
			{
				location = lm
						.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
			}
		} else {
			location = lm
					.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		if (location != null) {
			onLocationChanged(location);
			// mGetLocation = (GetLocation) new GetLocation().execute();
			// CallWLAdapter(""+(int)(Currlatitude*1E6),""+(int)(Currlongitude*1E6));
		}
		lm.requestLocationUpdates(provider, 20000, 0, this);

		listview = (ListView) findViewById(R.id.slist);

		sDown = (ImageView) findViewById(R.id.sdown);
		showSlider = (RelativeLayout) findViewById(R.id.slider);
		searcharea = (TextView) findViewById(R.id.searcharea);
		showlist = (ImageView) findViewById(R.id.btnList);
		map = (ImageView) findViewById(R.id.mapbtn);
		// map.setBackgroundResource(R.drawable.map_clicked);
		map.setBackgroundResource(R.drawable.map_clicked);
		back = (ImageView) findViewById(R.id.backbutton);

		/*
		 * Legend images
		 */
		branch = (ImageView) findViewById(R.id.branch);
		atm = (ImageView) findViewById(R.id.atm);
		direction = (ImageView) findViewById(R.id.dir);

		branch.setOnClickListener(this);
		atm.setOnClickListener(this);

		/*
		 * Bottom Navigation buttons
		 */
		navhome = (ImageView) findViewById(R.id.navhome);
		// navhome.setImageResource(R.drawable.home_new_hover);
		navhome.setBackgroundResource(R.drawable.home_new_hover);
		navbranch = (ImageView) findViewById(R.id.navbranch);
		navatm = (ImageView) findViewById(R.id.navatm);
		navlock = (ImageView) findViewById(R.id.navlock);

		navhome.setOnClickListener(this);
		navbranch.setOnClickListener(this);
		navatm.setOnClickListener(this);
		navlock.setOnClickListener(this);

		RelativeLayout settings = (RelativeLayout) findViewById(R.id.rel3);
		settings.setVisibility(View.VISIBLE);

		// Address Test Details View layout
		relative = (RelativeLayout) findViewById(R.id.relative);

		directionrel = (RelativeLayout) findViewById(R.id.directionrel);

		settingPref = getSharedPreferences("settings", 0);
		selectedRadius = settingPref.getInt("radius", 100);
		selectedRadius = (double) Math.round(selectedRadius / 1000);
		sRadius = (int) selectedRadius;
		// Log.e("selectedRadius555555",""+sRadius);
		if (sRadius == 0) {
			sRadius = 5;
		}

		/*
		 * ---NOTE--- Check below code. As of now, we are using 'WorkLite' n
		 * hence we may need not to do it.
		 */
		if (getIntent().hasExtra("gpsFlag")
				&& getIntent().getExtras().getBoolean("gpsFlag")) {
			// Log.i("My Location", "My Location : gpsFlag");
			searcharea.setText("Near Me");
		} else if (getIntent().hasExtra("area") || getIntent().hasExtra("lat")) {
			Bundle b = getIntent().getExtras();
			gpsFlag = b.getBoolean("gpsFlag");
			manFlag = b.getBoolean("manFlag");
			setting = b.getBoolean("setting");
			areaCity = b.getString("area");
			radius = b.getString("radius");
			setCurLat = b.getString("lat");
			setCurLon = b.getString("lon");
			// Log.d("gpsFlag manFlag setting area",""+gpsFlag+"=="+manFlag+"="+setting+"="+areaCity+"="+radius
			// +setCurLat+"="+setCurLon);

			CurrentLat = setCurLat;
			CurrentLon = setCurLon;

		} else {
			// searcharea.setText("My Location");
			searcharea.setText("Near Me");
		}

		if (manFlag) {
			// Log.i("manFlag", "manFlag");
			if (PlaceData != null) {
				PlaceData.clear();
				atmData.clear();
				branchData.clear();
				lockerData.clear();
				// DirectionData.clear();
			}

			mGetSearchData = (GetSearchData) new GetSearchData()
					.execute(areaCity);
		}
		if (gpsFlag) {
			searcharea.setVisibility(View.VISIBLE);
			searcharea.setText("Near Me");
			// Log.i("gpsFlag", "gpsFlag");
			if (EMULATOR) {
				// mGetOffersData = (GetOffersData) new
				// GetOffersData().execute(latitude,longitude);
			} else {
				// mGetAllList = (GetAllList) new
				// GetAllList().execute(CurrentLat,CurrentLon);
				// CallWLAdapter(CurrentLat,CurrentLon);
			}
		}

		back.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AtmBrancheLockerActivityD.this,
						RBL_iBank.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

				/*
				 * TO send the ID back to the WL main Activity
				 */
				// String phoneNumber =pageID;
				// Intent phoneNumberInfo = new Intent();
				// phoneNumberInfo.putExtra("phoneNumber", phoneNumber);
				// setResult(RESULT_OK, phoneNumberInfo);
				// AtmBrancheLockerActivity.this.finish();

				startActivity(intent);
				// Toast.makeText(LocateActivity.this, "Toast back Locate",
				// Toast.LENGTH_SHORT).show();
			}
		});

		edit = (ImageView) findViewById(R.id.edit);
		go = (ImageView) findViewById(R.id.go);
		if (areaCity != null) {
			searcharea.setText(areaCity);
		}
		editarea = (EditText) findViewById(R.id.editarea);

		edit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				editarea.setVisibility(View.VISIBLE);
				// editarea.setFocusable(true);
				searcharea.setText("");
				editarea.setText("");
				edit.setVisibility(View.GONE);
				searcharea.setVisibility(View.GONE);
				go.setVisibility(View.VISIBLE);
			}
		});

		go.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showlistFlag = true;
				branchFlag = false;
				atmFlag = false;
				lockerFlag = false;
				animateToFlag = false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				if (editarea.getText().toString().length() < 2) {
					final AlertDialog alertDialog = new AlertDialog.Builder(
							AtmBrancheLockerActivityD.this).create();
					alertDialog.setTitle("Alert");
					alertDialog.setMessage("Please Enter Location");
					alertDialog.setButton("OK",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
										int which) {
									alertDialog.dismiss();
								}
							});

					// Showing Alert Message
					alertDialog.show();
					editarea.clearFocus();
					// rupesh
					// editarea.setVisibility(View.GONE);
					// searcharea.setVisibility(View.VISIBLE);
					// go.setVisibility(View.GONE);
					// edit.setVisibility(View.VISIBLE);
					return;
				}

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				// imm.hideSoftInputFromWindow(go.getWindowToken(),
				// InputMethodManager.HIDE_IMPLICIT_ONLY);
				imm.hideSoftInputFromWindow(go.getWindowToken(), 0);
				// getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				relative.setVisibility(View.GONE);
				mapview.setVisibility(View.VISIBLE);
				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);
				editarea.setVisibility(View.GONE);
				searcharea.setVisibility(View.VISIBLE);
				areaName = editarea.getText().toString();
				searcharea.setText(areaName);
				go.setVisibility(View.GONE);
				edit.setVisibility(View.VISIBLE);

				navhome.setBackgroundResource(R.drawable.home_new_hover);
				navbranch.setBackgroundResource(R.drawable.branch_new);
				navatm.setBackgroundResource(R.drawable.atm_new);

				map.setVisibility(View.VISIBLE);
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);
				/*
				 * GetDirection Image Button
				 */
				btnDirection = (ImageView) findViewById(R.id.btnDirection);
				btnDirection.setVisibility(View.GONE);
				/*
				 * GetDirection Image in List Item
				 */
				dirshowlist = (ImageView) findViewById(R.id.showbtn);
				dirshowlist.setVisibility(View.GONE);
				showText = (ImageView) findViewById(R.id.btnText);
				showText.setVisibility(View.GONE);
				ChangeStart = (ImageView) findViewById(R.id.btnChange);
				ChangeStart.setVisibility(View.GONE);

				if (PlaceData != null) {
					PlaceData.clear();
					atmData.clear();
					branchData.clear();
					lockerData.clear();
					mapview.getOverlays().clear();
					// DirectionData.clear();
				}
				if (suggestData != null) {
					suggestData.clear();
				}
				if (areaName.length() > 1) {
					suggestlistFlag = true;
					mGetSearchData = (GetSearchData) new GetSearchData()
							.execute(areaName);
				} else {
					final AlertDialog alertDialog = new AlertDialog.Builder(
							AtmBrancheLockerActivityD.this).create();
					alertDialog.setTitle("Alert");
					alertDialog.setMessage("Please Enter Location");
					alertDialog.setButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									alertDialog.dismiss();
								}
							});
					alertDialog.show();
				}

				PlaceDetailAdapter mAdapter = new PlaceDetailAdapter(
						AtmBrancheLockerActivityD.this, PlaceData);
				listview.setAdapter(mAdapter);
				// listview.removeAllViews();
				mAdapter.notifyDataSetChanged();
			}
		});

		/*
		 * if(CurrentLat!=null && CurrentLon!=null) { //mGetAllList =
		 * (GetAllList) new GetAllList().execute(latitude,longitude);
		 * mGetAllList = (GetAllList) new
		 * GetAllList().execute(CurrentLat,CurrentLon); }
		 */
		map.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					TextView noffers = (TextView) findViewById(R.id.noffers);
					noffers.setVisibility(View.GONE);
					ImageView alert = (ImageView) findViewById(R.id.alert);
					alert.setVisibility(View.GONE);
					mapview.setVisibility(View.VISIBLE);
					showlist.setVisibility(View.VISIBLE);
					btnDirection = (ImageView) findViewById(R.id.btnDirection);
					btnDirection.setVisibility(View.GONE);
					listview.setVisibility(View.GONE);
					inrel = (LinearLayout) findViewById(R.id.inrel);
					inrel.setVisibility(View.GONE);
					RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
					innrel.setVisibility(View.GONE);
					// offersrel.setVisibility(View.GONE);
					directionrel.setVisibility(View.GONE);
					// Log.e("map.isSelected()",""+map.isSelected()+""+map.isPressed()+"==="+flag);
					// Log.d("map predd=>",""+map.isEnabled()+"=="+map.isFocused()+"=="+map.isFocusable());
					if (flag) {
						inrel.setVisibility(View.GONE);
						relative.setVisibility(View.GONE);
						// btnDirection.setVisibility(View.GONE);
					}
					map.setBackgroundResource(R.drawable.map_clicked);
					showlist.setBackgroundResource(R.drawable.show_list);
					Intent intent = new Intent(AtmBrancheLockerActivityD.this,
							AtmBrancheLockerActivityD.class);
					// Toast.makeText(AtmBrancheLockerActivity.this,
					// "AtmBrancheLockerActivity", 0).show();
					startActivity(intent);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		// Log.e("CurrentLat",""+CurrentLat+"CurrentLon==="+CurrentLon+"=setting="+setting
		// +"==>"+searcharea.getText().toString());

		if (!setting) {
			// Log.i("!setting", "!setting");
			if (EMULATOR) {
				// Log.d("latitude=longitude",""+latitude+"==="+longitude);
				// mGetAllList = (GetAllList) new
				// GetAllList().execute(latitude,longitude);
				CallWLAdapter(latitude, longitude);
			} else {
				CallWLAdapter(CurrentLat, CurrentLon);

				// mGetAllList = (GetAllList) new
				// GetAllList().execute(CurrentLat,CurrentLon);
			}

		}

		showlist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showlistFlag = true;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				map.setBackgroundResource(R.drawable.mapbk);
				showlist.setBackgroundResource(R.drawable.show_list_clicked);
				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.VISIBLE);
				mapview.setVisibility(View.GONE);

				if (flag) {
					inrel.setVisibility(View.GONE);
					relative.setVisibility(View.GONE);
				}
				if (branchFlag) {
					showlistFlag = false;
					tempFlag = false;
					BranchDataAdapter mAdapter = new BranchDataAdapter(
							AtmBrancheLockerActivityD.this, branchData);
					listview.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				} else if (atmFlag) {
					showlistFlag = false;
					tempFlag = false;
					ATMDataAdapter mAdapter = new ATMDataAdapter(
							AtmBrancheLockerActivityD.this, atmData);
					listview.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				} else if (lockerFlag) {
					showlistFlag = false;
					tempFlag = false;
					// change the adapter later. or build generic adapter.
					ATMDataAdapter mAdapter = new ATMDataAdapter(
							AtmBrancheLockerActivityD.this, lockerData);
					listview.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				} else if (showlistFlag) {
					tempFlag = false;
					// Log.d("placedata before adapter call=>",""+PlaceData.size()+"=====showlistFlag="+showlistFlag);
					PlaceDetailAdapter mAdapter = new PlaceDetailAdapter(
							AtmBrancheLockerActivityD.this, PlaceData);
					// OffersDataAdapter mAdapter=new
					// OffersDataAdapter(AtmBrancheLockerActivity.this,offersData);
					listview.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				}

				listview.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> l, View view,
							int position, long arg3) {
						// Log.d("tempFlag=>",""+tempFlag);
						if (tempFlag) {
							return;
						}
						flag = true;
						TextView phone, ifsc;
						View view1;
						final RelativeLayout dirrel;
						ImageView btnText = (ImageView) findViewById(R.id.btnText);
						dirrel = (RelativeLayout) findViewById(R.id.dirrel);
						final int clickPos = position;
						// Log.d("offersFlag$$$$$$$$$$",""+offersFlag);
						try {
							inrel = (LinearLayout) findViewById(R.id.inrel);
							inrel.setVisibility(View.VISIBLE);
							RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
							innrel.setVisibility(View.VISIBLE);
							// offersrel.setVisibility(View.GONE);
							directionrel.setVisibility(View.GONE);
							relative.setVisibility(View.VISIBLE);
							atmfac = (ImageView) findViewById(R.id.atmfac);
							locfac = (ImageView) findViewById(R.id.locfac);
							goldfac = (ImageView) findViewById(R.id.goldfac);
							silverfac = (ImageView) findViewById(R.id.silverfac);
							area = (TextView) findViewById(R.id.area);
							area.setTypeface(fontFace);
							num = (TextView) findViewById(R.id.num);
							num.setTypeface(fontFace);
							adddetail = (TextView) findViewById(R.id.adddetail);
							adddetail.setTypeface(fontFace);
							ifsccode = (TextView) findViewById(R.id.ifsccode);
							ifsccode.setTypeface(fontFace);
							weekon = (TextView) findViewById(R.id.weekon);
							weekon.setTypeface(fontFace);
							weekend = (TextView) findViewById(R.id.weekend);
							weekend.setTypeface(fontFace);
							phone = (TextView) findViewById(R.id.phone);
							phone.setTypeface(fontFace);
							ifsc = (TextView) findViewById(R.id.ifsc);
							ifsc.setTypeface(fontFace);
							close = (TextView) findViewById(R.id.close);
							close.setTypeface(fontFace);
							facility = (TextView) findViewById(R.id.tvfacility);
							facility.setTypeface(fontFace);
							tlocker = (TextView) findViewById(R.id.tlocker);
							tlocker.setTypeface(fontFace);
							tgold = (TextView) findViewById(R.id.tgold);
							tgold.setTypeface(fontFace);
							tsilver = (TextView) findViewById(R.id.tsilver);
							tsilver.setTypeface(fontFace);
							tatm = (TextView) findViewById(R.id.tatm);
							tatm.setTypeface(fontFace);
							btnDirection = (ImageView) findViewById(R.id.btnDirection);

							/*
							 * if(!hideBtnDirection)
							 * btnDirection.setVisibility(View.VISIBLE);
							 * showlist.setVisibility(View.GONE);
							 */// ru...

							// Log.e("showlistFlag outside listview",""+showlistFlag);
							if (showlistFlag) {
								if ("A".equalsIgnoreCase(PlaceData
										.get(position).get("type"))) {
									// Toast.makeText(AtmBrancheLockerActivity.this,position+"=>A",
									// Toast.LENGTH_SHORT).show();
									// area.setText(PlaceData.get(position).get("city"));
									branchOverFlag = false;
									atmOverFlag = true;
									lockerFlag = false;
									offerOverFlag = false;// gc...
									area = (TextView) findViewById(R.id.area);
									area.setTypeface(fontFace);
									area.setVisibility(View.VISIBLE);
									area.setText(PlaceData.get(position).get(
											"landmark"));
									adddetail.setText(PlaceData.get(position)
											.get("address"));
									inrel = (LinearLayout) findViewById(R.id.inrel);
									inrel.setVisibility(View.VISIBLE);
									RelativeLayout irel = (RelativeLayout) findViewById(R.id.innrel);
									irel.setVisibility(View.VISIBLE);
									icon = (ImageView) findViewById(R.id.iconn);
									icon.setVisibility(View.GONE);
									icon.invalidate();
									icon.setImageResource(0);
									// icon.setImageResource(R.drawable.atm_icon);

									ImageView atmicon = (ImageView) findViewById(R.id.atmicon);
									atmicon.setVisibility(View.VISIBLE);

									num.setVisibility(View.GONE);
									ifsccode.setVisibility(View.GONE);
									weekon.setVisibility(View.GONE);
									weekend.setVisibility(View.GONE);
									phone.setVisibility(View.GONE);
									ifsc.setVisibility(View.GONE);

									close.setVisibility(View.GONE);
									facility.setVisibility(View.GONE);
									tgold.setVisibility(View.GONE);
									tatm.setVisibility(View.GONE);
									tlocker.setVisibility(View.GONE);
									tsilver.setVisibility(View.GONE);
									atmfac.setVisibility(View.GONE);
									locfac.setVisibility(View.GONE);
									goldfac.setVisibility(View.GONE);
									silverfac.setVisibility(View.GONE);

									atmDest = PlaceData.get(clickPos).get(
											"address");
									// Log.e("address in offer click :",""+atmDest
									// );
								} else if ("B".equalsIgnoreCase(PlaceData.get(
										position).get("type"))
										|| "L".equalsIgnoreCase(PlaceData.get(
												position).get("type"))) {
									branchOverFlag = true;
									atmOverFlag = false;
									lockerFlag = false;
									offerOverFlag = false;// gc...
									num.setVisibility(View.VISIBLE);
									ifsccode.setVisibility(View.VISIBLE);
									weekon.setVisibility(View.VISIBLE);
									weekend.setVisibility(View.VISIBLE);
									phone.setVisibility(View.VISIBLE);
									ifsc.setVisibility(View.VISIBLE);

									close.setVisibility(View.VISIBLE);
									facility.setVisibility(View.VISIBLE);
									// area.setText(PlaceData.get(position).get("city"));
									area = (TextView) findViewById(R.id.area);
									area.setTypeface(fontFace);
									area.setVisibility(View.VISIBLE);
									area.setText(PlaceData.get(position).get(
											"landmark"));
									num.setText(PlaceData.get(position).get(
											"phone"));
									adddetail.setText(PlaceData.get(position)
											.get("address"));
									ifsccode.setText(PlaceData.get(position)
											.get("ifccode"));

									// hide if concern data is 'null'
									num.setVisibility(!PlaceData.get(position)
											.get("phone")
											.equalsIgnoreCase("null") ? View.VISIBLE
											: View.GONE);
									ifsccode.setVisibility(!PlaceData
											.get(position).get("ifccode")
											.equalsIgnoreCase("null") ? View.VISIBLE
											: View.GONE);

									inrel = (LinearLayout) findViewById(R.id.inrel);
									inrel.setVisibility(View.VISIBLE);
									RelativeLayout irel = (RelativeLayout) findViewById(R.id.innrel);
									irel.setVisibility(View.VISIBLE);
									icon = (ImageView) findViewById(R.id.iconn);
									icon.setVisibility(View.VISIBLE);
									icon.setImageResource(R.drawable.branch_icon);

									ImageView atmicon = (ImageView) findViewById(R.id.atmicon);
									atmicon.setVisibility(View.GONE);

//									weekon.setText(PlaceData.get(position).get(
//											"weekdayInfo"));
//									weekend.setText(PlaceData.get(position)
//											.get("weekendinfo"));

									weekon.setText(Html.fromHtml(PlaceData.get(position).get("weekdayInfo")));
									weekend.setText(Html.fromHtml(PlaceData.get(position).get("weekendinfo")));

									brDest = PlaceData.get(clickPos).get(
											"address");
									// Log.e("address in offer click :",""+brDest
									// );
								}
							} else if (branchFlag || lockerFlag) {
								/**
								 * Data list of 'Branch' or 'Locker'
								 */
								List<HashMap<String, String>> tempDataList = new ArrayList<HashMap<String, String>>();
								if (branchFlag) {
									branchOverFlag = true;
									atmOverFlag = false;
									lockerFlag = false;

									tempDataList = branchData;
									// Toast.makeText(AtmBrancheLockerActivity.this,
									// "In  Branch...FLAG", 1).show();

								} else if (lockerFlag) {
									branchOverFlag = false;
									atmOverFlag = false;
									lockerFlag = true;

									tempDataList = lockerData;
									// Toast.makeText(AtmBrancheLockerActivity.this,
									// "In Locker ...FLAG", 1).show();

								}

								// Log.d("branchFlag inside listview",""+branchFlag);
								num.setVisibility(View.VISIBLE);
								ifsccode.setVisibility(View.VISIBLE);
								weekon.setVisibility(View.VISIBLE);
								weekend.setVisibility(View.VISIBLE);
								phone.setVisibility(View.VISIBLE);
								ifsc.setVisibility(View.VISIBLE);

								close.setVisibility(View.VISIBLE);
								facility.setVisibility(View.VISIBLE);
								try {
									// Log.d("branchData.get(position).getatmInfo)",""+branchData.get(position).get("atmInfo"));
									// Log.d("pla.get(position).get(atminfo).equalsIgnoreCase(Y)",""+pla.get(position).get("atmInfo"));
									if ("Y".equalsIgnoreCase(tempDataList.get(
											position).get("atmInfo"))) {
										tatm.setVisibility(View.VISIBLE);
										atmfac.setVisibility(View.VISIBLE);
									} else {
										tatm.setVisibility(View.GONE);
										atmfac.setVisibility(View.GONE);
									}

									if ("Y".equalsIgnoreCase(tempDataList.get(
											position).get("lockerInfo"))) {
										tlocker.setVisibility(View.VISIBLE);
										locfac.setVisibility(View.VISIBLE);
									} else {
										tlocker.setVisibility(View.GONE);
										locfac.setVisibility(View.GONE);
									}

									// gc... in RBL
									if ("Y".equalsIgnoreCase(branchData.get(
											position).get("flggold"))) {
										tgold.setVisibility(View.VISIBLE);
										goldfac.setVisibility(View.VISIBLE);
									} else {
										tgold.setVisibility(View.GONE);
										goldfac.setVisibility(View.GONE);
									}

									if ("Y".equalsIgnoreCase(branchData.get(
											position).get("flgsilver"))) {
										tsilver.setVisibility(View.VISIBLE);
										silverfac.setVisibility(View.VISIBLE);
									} else {
										tsilver.setVisibility(View.GONE);
										silverfac.setVisibility(View.GONE);
									}

								} catch (Exception e) {
									e.printStackTrace();
								}

								area = (TextView) findViewById(R.id.area);
								area.setTypeface(fontFace);
								area.setVisibility(View.VISIBLE);
								area.setText(tempDataList.get(position).get(
										"landmark"));
								num.setText(tempDataList.get(position).get(
										"phone"));
								adddetail.setText(tempDataList.get(position)
										.get("address"));
								ifsccode.setText(tempDataList.get(position)
										.get("ifccode"));

								// hide if concern data is 'null'
								num.setVisibility(!tempDataList.get(position)
										.get("phone").equalsIgnoreCase("null") ? View.VISIBLE
										: View.GONE);
								ifsccode.setVisibility(!tempDataList
										.get(position).get("ifccode")
										.equalsIgnoreCase("null") ? View.VISIBLE
										: View.GONE);

								icon = (ImageView) findViewById(R.id.iconn);
								icon.setVisibility(View.VISIBLE);
								icon.setImageResource(R.drawable.branch_icon);

								ImageView atmicon = (ImageView) findViewById(R.id.atmicon);
								atmicon.setVisibility(View.GONE);

//								weekon.setText(tempDataList.get(position).get(
//										"weekdayInfo"));
//								weekend.setText(tempDataList.get(position).get(
//										"weekendinfo"));

                                weekon.setText(Html.fromHtml(tempDataList.get(position).get("weekdayInfo")));
                                weekend.setText(Html.fromHtml(tempDataList.get(position).get("weekendinfo")));

								brDest = tempDataList.get(clickPos).get(
										"address");
								// Log.e("address in offer click :",""+brDest );
							} else if (atmFlag) {
								branchOverFlag = false;
								atmOverFlag = true;
								offerOverFlag = false;
								// Log.d("atmFlag inside listview",""+atmFlag);
								num.setVisibility(View.GONE);
								ifsccode.setVisibility(View.GONE);
								weekon.setVisibility(View.GONE);
								weekend.setVisibility(View.GONE);
								phone.setVisibility(View.GONE);
								ifsc.setVisibility(View.GONE);

								close.setVisibility(View.GONE);
								facility.setVisibility(View.GONE);
								tgold.setVisibility(View.GONE);
								tatm.setVisibility(View.GONE);
								tlocker.setVisibility(View.GONE);
								tsilver.setVisibility(View.GONE);
								atmfac.setVisibility(View.GONE);
								locfac.setVisibility(View.GONE);
								goldfac.setVisibility(View.GONE);
								silverfac.setVisibility(View.GONE);

								area = (TextView) findViewById(R.id.area);
								area.setTypeface(fontFace);
								area.setVisibility(View.VISIBLE);
								area.setText(atmData.get(position).get(
										"landmark"));
								adddetail.setText(atmData.get(position).get(
										"address"));
								icon = (ImageView) findViewById(R.id.iconn);
								icon.setVisibility(View.VISIBLE);
								icon.setImageResource(R.drawable.atm_icon);

								atmDest = atmData.get(clickPos).get("address");
								// Log.e("address in offer click :",""+atmDest
								// );

							}

						} catch (Exception e) {
							e.printStackTrace();
						}

						ClickLat = PlaceData.get(position).get("latitude");
						ClickLon = PlaceData.get(position).get("longitude");

						final String toString = ClickLon + "," + ClickLat;

						listview.setVisibility(View.GONE);
						setShowlistTextviewChangestartUnselected();
						/*
						 * }catch(Exception e) { e.printStackTrace(); } }
						 */
					}

				});

			}
		});

	}

	/**
	 * Set 'ShowList', 'TextView' and 'ChangeStart' to unselected.
	 */
	public void setShowlistTextviewChangestartUnselected() {
		// TODO Auto-generated method stub
		try {
			dirshowlist.setBackgroundResource(R.drawable.show_list_branch);
			showText.setBackgroundResource(R.drawable.text_view);
			ChangeStart.setBackgroundResource(R.drawable.change_start);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	protected WindowManager windowManager;
	protected PopupWindow window;

	protected OnClickListener cancelListener = new OnClickListener() {
		public void onClick(View v) {
			try {
				if (mQuickAction != null)
					mQuickAction.dismiss();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	};

	public void Settings(View target) {
		Bundle b = new Bundle();
		b.putString("lastlocation", searcharea.getText().toString());
		Intent intent = new Intent(AtmBrancheLockerActivityD.this,
				SettingsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		intent.putExtras(b);
		startActivity(intent);
	}

	public void setOnDismissListener(PopupWindow.OnDismissListener listener) {
		window.setOnDismissListener(listener);
	}

	public void dismiss() {
		window.dismiss();
	}

	static class ViewHolder {
		TextView title, dist;
		ImageView icon, direct;
	}

	public class PlaceDetailAdapter extends
			ArrayAdapter<HashMap<String, String>> {
		private List<HashMap<String, String>> pla;
		private Activity context;

		public PlaceDetailAdapter(Activity context,
				List<HashMap<String, String>> pla) {
			super(AtmBrancheLockerActivityD.this, R.layout.showlist, pla);
			this.context = context;
			this.pla = pla;

		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			ViewHolder holder;
			final int clickPos = position;

			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.showlist, parent, false);
				holder = new ViewHolder();
				holder.title = (TextView) row.findViewById(R.id.text);
				holder.title.setTypeface(fontFace);
				holder.dist = (TextView) row.findViewById(R.id.dist);
				holder.dist.setTypeface(fontFace);
				holder.icon = (ImageView) row.findViewById(R.id.icon);
				holder.direct = (ImageView) row.findViewById(R.id.direct);

				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (ViewHolder) row.getTag();
			}
			// Log.d("pla size in placeDataAdapter=>",""+pla.size()+"==="+clickPos);

			if ("A".equalsIgnoreCase(pla.get(position).get("type"))) {
				branchOverFlag = false;
				atmOverFlag = true;
				offerOverFlag = false;
				lockerFlag = false;
				// Log.e("value of i in atm=",""+position);
				holder.title.setText(pla.get(position).get("locality"));
				// holder.icon.setBackgroundResource(R.drawable.atm_icon);//gc...
				holder.icon.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.atm_icon));
				holder.dist.setText("");
				holder.direct.setBackgroundResource(R.drawable.direction);
			} else if ("B".equalsIgnoreCase(pla.get(position).get("type"))) {
				branchOverFlag = true;
				atmOverFlag = false;
				offerOverFlag = false;
				lockerFlag = false;
				// Log.e("value of i in B=",""+position);
				holder.title.setText(pla.get(position).get("locality"));
				holder.icon.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.branch_icon));
				holder.dist.setText("");
				holder.direct.setBackgroundResource(R.drawable.direction);

			} else if ("L".equalsIgnoreCase(pla.get(position).get("type"))) {
				branchOverFlag = false;
				atmOverFlag = false;
				offerOverFlag = false;
				lockerFlag = true;
				// Log.e("value of i in B=",""+position);
				holder.title.setText(pla.get(position).get("locality"));
				holder.icon.setBackgroundDrawable(getResources().getDrawable(
						R.drawable.branch_icon));
				holder.dist.setText("");
				holder.direct.setBackgroundResource(R.drawable.direction);

			}

			return row;
		}

	}

	static class ATMViewHolder {
		TextView atmtext, atmdist;
		ImageView atmicon, atmdirect;
	}

	public class ATMDataAdapter extends ArrayAdapter<HashMap<String, String>> {
		private List<HashMap<String, String>> pla;
		private Activity context;

		public ATMDataAdapter(Activity context,
				List<HashMap<String, String>> pla) {
			super(AtmBrancheLockerActivityD.this, R.layout.atmlist, pla);
			this.context = context;
			this.pla = pla;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			ATMViewHolder holder;
			final int clickPos = position;

			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.atmlist, parent, false);
				holder = new ATMViewHolder();
				holder.atmtext = (TextView) row.findViewById(R.id.atmtext);
				holder.atmtext.setTypeface(fontFace);
				holder.atmdist = (TextView) row.findViewById(R.id.atmdist);
				holder.atmdist.setTypeface(fontFace);
				holder.atmicon = (ImageView) row.findViewById(R.id.atmicon);
				holder.atmdirect = (ImageView) row.findViewById(R.id.atmdirect);

				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (ATMViewHolder) row.getTag();
			}
			// Log.d("pla size i nATMDataAdapter=>",""+pla.size());
			if ("A".equalsIgnoreCase(pla.get(position).get("type"))) {
				holder.atmtext.setText(pla.get(position).get("locality"));
				holder.atmicon.setImageResource(R.drawable.atm_icon);
				holder.atmdist.setText("");
			} else if ("L".equalsIgnoreCase(pla.get(position).get("type"))) {
				holder.atmtext.setText(pla.get(position).get("locality"));
				holder.atmicon.setImageResource(R.drawable.atm_icon);
				holder.atmdist.setText("");
			}
			// holder.direct.setBackgroundResource(R.drawable.direction_legend);

			return row;
		}

	}

	static class BranchViewHolder {
		TextView brtext, brdist;
		ImageView bricon, brdirect, bratm, brlock, brgold, brsilver;
	}

	public class BranchDataAdapter extends
			ArrayAdapter<HashMap<String, String>> {
		private List<HashMap<String, String>> pla;
		private Activity context;

		public BranchDataAdapter(Activity context,
				List<HashMap<String, String>> pla) {
			super(AtmBrancheLockerActivityD.this, R.layout.branchlist, pla);
			this.context = context;
			this.pla = pla;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			BranchViewHolder holder;
			final int clickPos = position;

			if (row == null) {
				LayoutInflater inflater = getLayoutInflater();

				row = inflater.inflate(R.layout.branchlist, parent, false);
				holder = new BranchViewHolder();
				holder.brtext = (TextView) row.findViewById(R.id.brtext);
				holder.brtext.setTypeface(fontFace);
				// holder.brdist=(TextView)row.findViewById(R.id.brdist);
				// holder.brdist.setTypeface(fontFace);
				holder.bricon = (ImageView) row.findViewById(R.id.bricon);

				// as closed in XML row...
				/*
				 * holder.bratm=(ImageView)row.findViewById(R.id.bratm);
				 * holder.brlock=(ImageView)row.findViewById(R.id.brlock);
				 * holder.brgold=(ImageView)row.findViewById(R.id.brglod);
				 * holder.brsilver=(ImageView)row.findViewById(R.id.brsilver);
				 */
				holder.brdirect = (ImageView) row.findViewById(R.id.brdirect);
				row.setTag(holder);
			} else {
				// holder = (View) row;
				holder = (BranchViewHolder) row.getTag();
			}

			try {
				// Log.d("branchData.get(position).getatmInfo)",""+branchData.get(position).get("atmInfo"));
				// Log.d("pla.get(position).get(atminfo).equalsIgnoreCase(Y)",""+pla.get(position).get("atmInfo"));
				if (pla.get(position).get("atmInfo").equalsIgnoreCase("Y")) {
					holder.bratm.setVisibility(View.VISIBLE);
				} else {
					holder.bratm.setVisibility(View.GONE);
				}

				if (pla.get(position).get("lockerInfo").equalsIgnoreCase("Y")) {
					holder.brlock.setVisibility(View.VISIBLE);
				} else {
					holder.brlock.setVisibility(View.GONE);
				}

				if (pla.get(position).get("flggold").equalsIgnoreCase("Y")) {
					holder.brgold.setVisibility(View.VISIBLE);
				} else {
					holder.brgold.setVisibility(View.GONE);
				}

				if (pla.get(position).get("flgsilver").equalsIgnoreCase("Y")) {
					holder.brsilver.setVisibility(View.VISIBLE);
				} else {
					holder.brsilver.setVisibility(View.GONE);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			// holder.brtext.setText(pla.get(position).get("landmark"));
			holder.brtext.setText(pla.get(position).get("locality"));
			holder.bricon.setImageResource(R.drawable.branch_icon);
			// holder.brdist.setText("");

			return row;
		}

	}

	// remove in RBL. gc...
	public void showSlider(View show) {
		sDown.setVisibility(View.GONE);
		showSlider.setVisibility(View.VISIBLE);
	}

	public void hideSlider(View hide) {
		// Toast.makeText(LocateActivity.this, "Toast ghide",
		// Toast.LENGTH_SHORT).show();
		sDown.setVisibility(View.VISIBLE);
		showSlider.setVisibility(View.GONE);
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	String CurrentPosition;
	// private class GetAllListtest extends UserTask<String, Void, String>
	{/*
	 * public void onPreExecute() { try{ if(mPdialog==null) mPdialog = new
	 * ProgressDialog(AtmBrancheLockerActivity.this);
	 * 
	 * mPdialog.setMessage("Getting Map View...");
	 * mPdialog.setIndeterminate(true); mPdialog.setCancelable(true);
	 * mPdialog.show();
	 * 
	 * 
	 * }catch(Exception e){e.printStackTrace();} }
	 * 
	 * public void onPostExecute(String data){
	 * 
	 * if(mPdialog!=null) { if(mPdialog.isShowing()) mPdialog.dismiss(); }
	 * 
	 * if(data==null || data.length()<20) {
	 * Toast.makeText(AtmBrancheLockerActivity.this,
	 * "No Details Found. Please check your Network Connection."
	 * ,Toast.LENGTH_SHORT).show(); return; }
	 * 
	 * if(PlaceData!=null) { PlaceData.clear(); atmData.clear();
	 * branchData.clear(); lockerData.clear(); }
	 * 
	 * try { JSONObject jObj = null; JSONArray list = null; try { jObj = new
	 * JSONObject(data);
	 * 
	 * list=jObj.getJSONArray("list"); } catch (JSONException e) { //
	 * Log.e("JSON Parser", "Error parsing data " + e.toString()); }
	 * 
	 * 
	 * for(int i=0; i<list.length();i++) { try { String row = list.optString(i,
	 * "default"); JSONObject robject = (JSONObject) new
	 * JSONTokener(row).nextValue(); String City = "";
	 * 
	 * City = robject.getString("city"); String address =
	 * robject.getString("address"); String landmark =
	 * robject.getString("landmark"); CurrentPosition=landmark; String locality
	 * = robject.getString("locality"); String phone =
	 * robject.getString("phone"); String fax = robject.getString("fax"); String
	 * atmInfo = robject.getString("atmInfo"); String lockerInfo =
	 * robject.getString("lockerInfo"); String weekdayInfo =
	 * robject.getString("weekdayInfo"); String weekendinfo =
	 * robject.getString("weekendinfo"); String weeklyoffinfo =
	 * robject.getString("weeklyoffinfo"); String latitude =
	 * robject.getString("latitude"); String longitude =
	 * robject.getString("longitude"); String flggold =
	 * robject.getString("flggold"); String flgsilver =
	 * robject.getString("flgsilver"); String ifsccode =
	 * robject.getString("ifccode"); String type=robject.getString("type");
	 * 
	 * //Log.d("json string CIty=>",""+City+"=="+type+"=="+latitude+"=="+longitude
	 * );
	 * 
	 * HashMap<String,String> placesMap=new HashMap<String, String>();
	 * 
	 * placesMap.put("city", City); placesMap.put("address", address);
	 * placesMap.put("landmark", landmark); placesMap.put("locality", locality);
	 * placesMap.put("phone", phone); placesMap.put("fax", fax);
	 * placesMap.put("atmInfo", atmInfo); placesMap.put("lockerInfo",
	 * lockerInfo); placesMap.put("weekdayInfo", weekdayInfo);
	 * placesMap.put("weekendinfo", weekendinfo); placesMap.put("weeklyoffinfo",
	 * weeklyoffinfo); placesMap.put("latitude", latitude);
	 * placesMap.put("longitude", longitude); placesMap.put("flggold", flggold);
	 * placesMap.put("flgsilver", flgsilver); placesMap.put("ifccode",
	 * ifsccode); placesMap.put("type",type);
	 * 
	 * //placeDetail.add(cplaceDetails.copy()); //
	 * Log.d("Placedata size in Getalllist=>",""+PlaceData.size());
	 * PlaceData.add(placesMap); tempArray=PlaceData; } catch (JSONException e)
	 * { e.printStackTrace(); } }
	 * 
	 * 
	 * Setting list data for ATMs, Branches and Lockers.
	 * 
	 * for(int i=0;i<PlaceData.size();i++) {
	 * if("A".equalsIgnoreCase(PlaceData.get(i).get("type"))) {
	 * HashMap<String,String> placesMap=new HashMap<String, String>();
	 * 
	 * placesMap.put("city", PlaceData.get(i).get("city"));
	 * placesMap.put("address", PlaceData.get(i).get("address"));
	 * placesMap.put("landmark", PlaceData.get(i).get("landmark"));
	 * placesMap.put("locality", PlaceData.get(i).get("locality"));
	 * placesMap.put("phone", PlaceData.get(i).get("phone"));
	 * placesMap.put("fax", PlaceData.get(i).get("fax"));
	 * placesMap.put("atmInfo", PlaceData.get(i).get("atmInfo"));
	 * placesMap.put("lockerInfo", PlaceData.get(i).get("lockerInfo"));
	 * placesMap.put("weekdayInfo", PlaceData.get(i).get("weekdayInfo"));
	 * placesMap.put("weekendinfo", PlaceData.get(i).get("weekendinfo"));
	 * placesMap.put("weeklyoffinfo", PlaceData.get(i).get("weeklyoffinfo"));
	 * placesMap.put("latitude", PlaceData.get(i).get("latitude"));
	 * placesMap.put("longitude", PlaceData.get(i).get("longitude"));
	 * placesMap.put("flggold", PlaceData.get(i).get("flggold"));
	 * placesMap.put("flgsilver", PlaceData.get(i).get("flgsilver"));
	 * placesMap.put("ifccode", PlaceData.get(i).get("ifccode"));
	 * placesMap.put("type",PlaceData.get(i).get("type"));
	 * 
	 * // Log.d("atmData size in Getalllist=>",""+atmData.size());
	 * atmData.add(placesMap); //tempArray=atmData; }else
	 * if("B".equalsIgnoreCase(PlaceData.get(i).get("type"))) {
	 * 
	 * HashMap<String,String> placesMap=new HashMap<String, String>();
	 * 
	 * placesMap.put("city", PlaceData.get(i).get("city"));
	 * placesMap.put("address", PlaceData.get(i).get("address"));
	 * placesMap.put("landmark", PlaceData.get(i).get("landmark"));
	 * placesMap.put("locality", PlaceData.get(i).get("locality"));
	 * placesMap.put("phone", PlaceData.get(i).get("phone"));
	 * placesMap.put("fax", PlaceData.get(i).get("fax"));
	 * placesMap.put("atmInfo", PlaceData.get(i).get("atmInfo"));
	 * placesMap.put("lockerInfo", PlaceData.get(i).get("lockerInfo"));
	 * placesMap.put("weekdayInfo", PlaceData.get(i).get("weekdayInfo"));
	 * placesMap.put("weekendinfo", PlaceData.get(i).get("weekendinfo"));
	 * placesMap.put("weeklyoffinfo", PlaceData.get(i).get("weeklyoffinfo"));
	 * placesMap.put("latitude", PlaceData.get(i).get("latitude"));
	 * placesMap.put("longitude", PlaceData.get(i).get("longitude"));
	 * placesMap.put("flggold", PlaceData.get(i).get("flggold"));
	 * placesMap.put("flgsilver", PlaceData.get(i).get("flgsilver"));
	 * placesMap.put("ifccode", PlaceData.get(i).get("ifccode"));
	 * placesMap.put("type",PlaceData.get(i).get("type"));
	 * 
	 * // Log.d("branchData size in Getalllist=>",""+branchData.size());
	 * branchData.add(placesMap); }else
	 * if("L".equalsIgnoreCase(PlaceData.get(i).get("type"))) {
	 * 
	 * HashMap<String,String> placesMap=new HashMap<String, String>();
	 * 
	 * placesMap.put("city", PlaceData.get(i).get("city"));
	 * placesMap.put("address", PlaceData.get(i).get("address"));
	 * placesMap.put("landmark", PlaceData.get(i).get("landmark"));
	 * placesMap.put("locality", PlaceData.get(i).get("locality"));
	 * placesMap.put("phone", PlaceData.get(i).get("phone"));
	 * placesMap.put("fax", PlaceData.get(i).get("fax"));
	 * placesMap.put("atmInfo", PlaceData.get(i).get("atmInfo"));
	 * placesMap.put("lockerInfo", PlaceData.get(i).get("lockerInfo"));
	 * placesMap.put("weekdayInfo", PlaceData.get(i).get("weekdayInfo"));
	 * placesMap.put("weekendinfo", PlaceData.get(i).get("weekendinfo"));
	 * placesMap.put("weeklyoffinfo", PlaceData.get(i).get("weeklyoffinfo"));
	 * placesMap.put("latitude", PlaceData.get(i).get("latitude"));
	 * placesMap.put("longitude", PlaceData.get(i).get("longitude"));
	 * placesMap.put("flggold", PlaceData.get(i).get("flggold"));
	 * placesMap.put("flgsilver", PlaceData.get(i).get("flgsilver"));
	 * placesMap.put("ifccode", PlaceData.get(i).get("ifccode"));
	 * placesMap.put("type",PlaceData.get(i).get("type"));
	 * 
	 * // Log.d("branchData size in Getalllist=>",""+branchData.size());
	 * lockerData.add(placesMap); } }
	 * 
	 * Log.e("ATMs List:", ""+atmData); Log.e("Branches List:", ""+branchData);
	 * Log.e("Lockers List:", ""+lockerData);
	 * 
	 * }catch (Exception e) { e.printStackTrace(); }
	 * 
	 * //
	 * Log.d("PlaceData.size()",""+PlaceData.size()+"atm==>"+""+atmData.size()
	 * +"branch="+""+branchData.size()); for (int i = 0; i <PlaceData.size();
	 * i++) { latitudeE6=Double.parseDouble(PlaceData.get(i).get("latitude"));
	 * longitudeE6=Double.parseDouble(PlaceData.get(i).get("longitude"));
	 * 
	 * 
	 * // Log.d("latitude & longitude=",PlaceData.get(i).get("latitude")+"==	"+
	 * PlaceData.get(i).get("longitude"));
	 * 
	 * mapOverlays = mapview.getOverlays();
	 * 
	 * Set ATMs overlays.
	 * 
	 * if("A".equalsIgnoreCase(PlaceData.get(i).get("type"))) { GeoPoint point =
	 * new GeoPoint((int)(latitudeE6*1E6),(int)(longitudeE6*1E6)); Drawable
	 * atm_icon = getResources().getDrawable(R.drawable.atm_icon);
	 * CustomItemizedOverlay itemizedOverlay = new
	 * CustomItemizedOverlay(atm_icon,AtmBrancheLockerActivity.this);
	 * OverlayItem overlayitem = new OverlayItem(point, "RBL Bank ATM",
	 * PlaceData.get(i).get("address"));
	 * itemizedOverlay.addOverlay(overlayitem);
	 * mapOverlays.add(itemizedOverlay); MapController mapController =
	 * mapview.getController(); // mapController.animateTo(point);
	 * mapController.setZoom(AppConstants.INTZOOM_LEVEL); }
	 * 
	 * 
	 * Set Branches overlays.
	 * 
	 * if("B".equalsIgnoreCase(PlaceData.get(i).get("type"))) { GeoPoint point =
	 * new GeoPoint((int)(latitudeE6*1E6),(int)(longitudeE6*1E6)); Drawable
	 * branch_icon = getResources().getDrawable(R.drawable.branch_icon);
	 * CustomItemizedOverlay itemizedOverlay = new
	 * CustomItemizedOverlay(branch_icon,ctx,itm); OverlayItem overlayitem = new
	 * OverlayItem(point, "RBL Bank", PlaceData.get(i).get("address"));
	 * itemizedOverlay.addOverlay(overlayitem);
	 * mapOverlays.add(itemizedOverlay);
	 * 
	 * MapController mapController = mapview.getController(); //
	 * mapController.animateTo(point);
	 * mapController.setZoom(AppConstants.INTZOOM_LEVEL); }
	 * 
	 * 
	 * Set Lockers overlays.
	 * 
	 * if("L".equalsIgnoreCase(PlaceData.get(i).get("type"))) { GeoPoint point =
	 * new GeoPoint((int)(latitudeE6*1E6),(int)(longitudeE6*1E6)); Drawable
	 * branch_icon = getResources().getDrawable(R.drawable.branch_icon);
	 * CustomItemizedOverlay itemizedOverlay = new
	 * CustomItemizedOverlay(branch_icon,ctx,itm); OverlayItem overlayitem = new
	 * OverlayItem(point, "RBL Bank Locker", PlaceData.get(i).get("address"));
	 * itemizedOverlay.addOverlay(overlayitem);
	 * mapOverlays.add(itemizedOverlay);
	 * 
	 * MapController mapController = mapview.getController(); //
	 * mapController.animateTo(point);
	 * mapController.setZoom(AppConstants.INTZOOM_LEVEL); } } }
	 * 
	 * 
	 * 
	 * NEW... Using worklite adapter!!!
	 * 
	 * String responseWL = null; // public String doInBackground(String...
	 * params) { // //String response = null; // String lat=params[0]; // String
	 * lon=params[1]; // try { // // String AdpterName="Others"; // String
	 * procedureName="Geolocator"; // WLProcedureInvocationData invocationData
	 * =new WLProcedureInvocationData(AdpterName, procedureName,true); // //
	 * Object[] parametersObjects=new Object[] {lat, lon, "10", "ALL"}; //
	 * invocationData.setParameters(parametersObjects); // // WLRequestOptions
	 * requestOptions=new WLRequestOptions(); //
	 * requestOptions.setTimeout(600000); //
	 * //LoginEntity.getInstance().userInfoSet
	 * .setLoggedinusername(v_username.getText().toString()); // WLClient
	 * wlClient=WLClient.getInstance(); // //
	 * wlClient.invokeProcedure(invocationData, new WLResponseListener() { // //
	 * @Override // public void onSuccess(WLResponse wlResp) { // // TODO
	 * Auto-generated method stub // responseWL = wlResp.getResponseText(); //
	 * // } // // @Override // public void onFailure(WLFailResponse wlFailResp)
	 * { // // TODO Auto-generated method stub //
	 * Log.e("GetAllList-doInBackground",
	 * "WLFailResponse ErrorMsg:"+wlFailResp.getErrorMsg
	 * ()+", RespText:"+wlFailResp.getResponseText()); // } // }); // // // }
	 * catch (Exception e1) { // e1.printStackTrace(); // } // // return
	 * responseWL; // }
	 * 
	 * 
	 * Using server... Working as per need
	 * 
	 * public String doInBackground(String... params) { String response = null;
	 * String lat=params[0]; String lon=params[1]; //
	 * Log.d("lat n long=>",""+lat +"==="+lon); try { // URL PostUrl = new
	 * URL("http://app.mapmyindia.com/hdfcapi/poi.do"); URL PostUrl = new
	 * URL(ConstantURLs.RBL_ABL_URL); List<NameValuePair> nameValuePairs = new
	 * ArrayList<NameValuePair>(4); if(gpsFlag) { nameValuePairs.add(new
	 * BasicNameValuePair("x", CurrentLat)); nameValuePairs.add(new
	 * BasicNameValuePair("y", CurrentLon));
	 * 
	 * //in RBL it is wrong... nameValuePairs.add(new BasicNameValuePair("y",
	 * CurrentLat)); nameValuePairs.add(new BasicNameValuePair("x",
	 * CurrentLon));
	 * 
	 * } else { nameValuePairs.add(new BasicNameValuePair("y", lat));
	 * nameValuePairs.add(new BasicNameValuePair("x", lon));
	 * 
	 * nameValuePairs.add(new BasicNameValuePair("x", "72.844359"));
	 * nameValuePairs.add(new BasicNameValuePair("y", "19.2054962"));
	 * 
	 * nameValuePairs.add(new BasicNameValuePair("x", lat));
	 * nameValuePairs.add(new BasicNameValuePair("y", lon)); }
	 * nameValuePairs.add(new BasicNameValuePair("radius", "10"));
	 * //nameValuePairs.add(new BasicNameValuePair("radius", ""+sRadius));
	 * nameValuePairs.add(new BasicNameValuePair("type", "ALL")); ProxyUrlUtil
	 * pU = new ProxyUrlUtil();
	 * Log.d("URL hit=>","https://180.179.117.36/rbl/geolocator"
	 * +nameValuePairs); response =
	 * pU.getPostXML(PostUrl,AtmBrancheLockerActivity.this,nameValuePairs);
	 * Log.d("Responmse=>",""+response); } catch (MalformedURLException e) {
	 * e.printStackTrace(); } catch (IOException e1) { e1.printStackTrace(); }
	 * 
	 * return response; }
	 */
	}
	String lat = null, lon = null;

	private class GetSearchData extends UserTask<String, Void, String> {
		String type;

		public void onPreExecute() {
			try {
				if (mPdialog == null)
					mPdialog = new ProgressDialog(
							AtmBrancheLockerActivityD.this);

				mPdialog.setMessage("Getting Search Results...");
				mPdialog.setIndeterminate(true);
				mPdialog.setCancelable(true);
				mPdialog.show();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		public void onPostExecute(String data) {

			if (mPdialog != null) {
				if (mPdialog.isShowing())
					mPdialog.dismiss();
			}

			if (data == null || data.length() < 20) {
				return;
			}
			try {
				JSONObject jObj = null;
				// JSONObject jObjj = null;
				JSONArray alternates = null;
				try {
					jObj = new JSONObject(data);

					alternates = jObj.getJSONArray("alternates");

				} catch (JSONException e) {
					e.printStackTrace();
				}

				JSONObject resultObj = jObj.getJSONObject("result");
				type = resultObj.getString("type");
				String address = resultObj.getString("address");
				String level = resultObj.getString("lev");

				JSONObject position = resultObj.getJSONObject("pos");
				lat = position.getString("lat");
				lon = position.getString("lon");

				// Log.d("alternates.length()=>",""+alternates.length());
				for (int i = 0; i < alternates.length(); i++) {
					try {
						String row = alternates.optString(i, "default");
						JSONObject robject = (JSONObject) new JSONTokener(row)
								.nextValue();

						String atlAddress = robject.getString("address");
						String atlLev = robject.getString("lev");

						HashMap<String, String> altMap = new HashMap<String, String>();
						altMap.put("address", atlAddress);
						altMap.put("lev", atlLev);

						suggestData.add(altMap);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				HashMap<String, String> resultMap = new HashMap<String, String>();
				resultMap.put("type", type);
				resultMap.put("address", address);
				resultMap.put("lat", lat);
				resultMap.put("lon", lon);

				searchData.add(resultMap);

				// tempArray=searchData;
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Log.d("type=>",""+type);
			if (type.equalsIgnoreCase("exact")) {
				for (int i = 0; i < searchData.size(); i++) {
					Double latitude = Double.parseDouble(searchData.get(i).get(
							"lat"));
					Double longitude = Double.parseDouble(searchData.get(i)
							.get("lon"));
					// Log.d("latitude & longitude=",latitude+"==	"+longitude);

					mapOverlays = mapview.getOverlays();
					GeoPoint point = new GeoPoint((int) (latitude * 1E6),
							(int) (longitude * 1E6));
					MapController mapController = mapview.getController();
					mapController.animateTo(point);
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}
				CallWLAdapter(lat, lon);

				// mGetAllList = (GetAllList) new GetAllList().execute(lat,lon);

			}
		}

		@Override
		public String doInBackground(String... params) {
			String response = null;
			String address = params[0];
			// Log.d("address=>",""+address);
			try {
				URL PostUrl = new URL(
						"http://apis.mapmyindia.com/v2.0/geocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json");
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						1);
				nameValuePairs.add(new BasicNameValuePair("addr", address));

				ProxyUrlUtil pU = new ProxyUrlUtil();

				// try {
				// Log.d("dataurl=>",""+PostUrl.toURI());
				// } catch (URISyntaxException e) {
				// e.printStackTrace();
				// }
				response = pU.getPostXML(PostUrl,
						AtmBrancheLockerActivityD.this, nameValuePairs);
				// Log.d("Responmse=>",""+response);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			return response;
		}

	}

	List<GeoPoint> routeData;
	String[] route;
	HashMap<String, String> latlong = new HashMap<String, String>();
	private AtmBrancheLockerActivityD activity;
	GeoPoint gp1, gp2, dest;
	String distance;
	String time;

	/*
	 * Due to not showing Curr-loc data, i build it. Currently not used in
	 * current flow...
	 */
	class getLocality extends UserTask<String, Void, Void> {

		private Exception exception;

		@Override
		public void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}

		public Void doInBackground(String... loc) {
			String fromX = loc[0];
			String fromY = loc[1];
			// ........NEW...
			try {
				// Log.d("lat n lon=>",""+fromY+"="+fromX);
				SAXParserFactory mySAXParserFactory = SAXParserFactory
						.newInstance();
				SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
				XMLReader myXMLReader = mySAXParser.getXMLReader();
				String url = "http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x="
						+ fromX + "&y=" + fromY;
				// String
				// url="http://apis.mapmyindia.com/v2.0/reversegeocode/key=2c07ede70ef00dd05ab7db0366e64ecd&rtype=json&x=77.2094&y=28.5229";
				// Log.d("url string=>",""+url);
				URL myUrl = new URL(url);

				curLocationDetail = new ArrayList<ContactDetails>();
				ContactsParser parse = new ContactsParser(curLocationDetail);
				myXMLReader.setContentHandler(parse);
				myXMLReader.parse(new InputSource(myUrl.openStream()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Log.d("contactDetail.size()=>",""+curLocationDetail.size());
			for (int i = 0; i < curLocationDetail.size(); i++) {
				Locality = curLocationDetail.get(i).getName();
				stateName = curLocationDetail.get(i).getSname();
				curCity = curLocationDetail.get(i).getLocality();
				// Log.d("Locality-state n city=>","curCity:"+curCity+", stateName:"+stateName+", Locality:"+Locality);

			}
			// NEW END...........
			return null;

		}

		public void onPostExecute(String res) {
			// TODO: do something with the feed
		}
	}

	/**
	 * shows Branch overlays on Map.
	 */
	public void branchOverlays() {
		try {
			branchFlag = true;
			atmFlag = false;
			showlistFlag = false;
			lockerFlag = false;
			mapview.invalidate();
			mapOverlays.clear();
			// Log.d("map.isClickable()",""+map.isClickable()+"=====>"+branchData.size()+"PlaceData.size()=====>"+PlaceData.size());
			for (int i = 0; i < PlaceData.size(); i++) {

				latitudeE6 = Double.parseDouble(PlaceData.get(i)
						.get("latitude"));
				longitudeE6 = Double.parseDouble(PlaceData.get(i).get(
						"longitude"));
				mapOverlays = mapview.getOverlays();

				if (PlaceData.get(i).get("type").equalsIgnoreCase("B")) {
					GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
							(int) (longitudeE6 * 1E6));
					Drawable branch_icon = getResources().getDrawable(
							R.drawable.branch_icon);
					CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
							branch_icon, ctx, this);
					OverlayItem overlayitem = new OverlayItem(point,
							"RBL Bank", PlaceData.get(i).get("address"));
					itemizedOverlay.addOverlay(overlayitem);
					mapOverlays.add(itemizedOverlay);
					MapController mapController = mapview.getController();
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}
			}
			if (branchFlag) {
				BranchDataAdapter mAdapter = new BranchDataAdapter(
						AtmBrancheLockerActivityD.this, branchData);
				listview.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * shows atm overlays on Map.
	 */
	public void atmOverlays() {
		try {
			atmFlag = true;
			branchFlag = false;
			showlistFlag = false;
			lockerFlag = false;
			mapview.invalidate();
			mapOverlays.clear();
			for (int i = 0; i < PlaceData.size(); i++) {

				latitudeE6 = Double.parseDouble(PlaceData.get(i)
						.get("latitude"));
				longitudeE6 = Double.parseDouble(PlaceData.get(i).get(
						"longitude"));
				mapOverlays = mapview.getOverlays();

				if (PlaceData.get(i).get("type").equalsIgnoreCase("A")) {
					GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
							(int) (longitudeE6 * 1E6));
					Drawable atm_icon = getResources().getDrawable(
							R.drawable.atm_icon);
					CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
							atm_icon, ctx, this);
					OverlayItem overlayitem = new OverlayItem(point,
							"RBL Bank ATM", PlaceData.get(i).get("address"));
					itemizedOverlay.addOverlay(overlayitem);
					mapOverlays.add(itemizedOverlay);
					MapController mapController = mapview.getController();
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}
			}
			if (atmFlag) {
				ATMDataAdapter mAdapter = new ATMDataAdapter(
						AtmBrancheLockerActivityD.this, atmData);
				listview.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * shows locker overlays on Map.
	 */
	public void lockerOverlays() {
		try {
			atmFlag = false;
			branchFlag = false;
			showlistFlag = false;
			lockerFlag = true;
			mapview.invalidate();
			mapOverlays.clear();
			for (int i = 0; i < PlaceData.size(); i++) {

				latitudeE6 = Double.parseDouble(PlaceData.get(i)
						.get("latitude"));
				longitudeE6 = Double.parseDouble(PlaceData.get(i).get(
						"longitude"));
				mapOverlays = mapview.getOverlays();

				if ("L".equalsIgnoreCase(PlaceData.get(i).get("type"))) {
					GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
							(int) (longitudeE6 * 1E6));
					Drawable atm_icon = getResources().getDrawable(
							R.drawable.atm_icon);
					CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
							atm_icon, ctx, this);
					OverlayItem overlayitem = new OverlayItem(point,
							"RBL Bank Locker", PlaceData.get(i).get("address"));
					itemizedOverlay.addOverlay(overlayitem);
					mapOverlays.add(itemizedOverlay);
					MapController mapController = mapview.getController();
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}
			}
			if (lockerFlag) {
				// change adp. later...
				ATMDataAdapter mAdapter = new ATMDataAdapter(
						AtmBrancheLockerActivityD.this, lockerData);
				listview.setAdapter(mAdapter);
				Log.e("LOCKERS LIST DATA-", "" + lockerData);
				mAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onClick(View v) {
		int id = v.getId();
		switch (id) {

		case R.id.navhome:
			try {
				hideBtnDirection = false;
				showlistFlag = true;
				branchFlag = false;
				atmFlag = false;
				lockerFlag = false;
				animateToFlag = false;
				// suggestlistFlag=false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				navhome.setBackgroundResource(R.drawable.home_new_hover);
				navbranch.setBackgroundResource(R.drawable.branch_new);
				navatm.setBackgroundResource(R.drawable.atm_new);
				navlock.setBackgroundResource(R.drawable.locker_new);

				map.setVisibility(View.VISIBLE);
				map.setBackgroundResource(R.drawable.map_clicked);
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);

				if (directionFlag) {
					directionFlag = false;
					dirshowlist.setVisibility(View.GONE);
					showText.setVisibility(View.GONE);
					ChangeStart.setVisibility(View.GONE);
				}
				if (!mapOverlays.isEmpty()) {
					mapOverlays.clear();
				}
				mapview.setVisibility(View.VISIBLE);
				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);

				relative.setVisibility(View.GONE);
				if (flag) {
					inrel.setVisibility(View.GONE);
					relative.setVisibility(View.GONE);
					btnDirection = (ImageView) findViewById(R.id.btnDirection);
					btnDirection.setVisibility(View.GONE);
				}

				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				// offersrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				if (!searcharea.getText().toString().equalsIgnoreCase("")
						&& !searcharea.getText().toString()
								.equalsIgnoreCase("Near Me")) {
					areaName = searcharea.getText().toString();
					mGetSearchData = (GetSearchData) new GetSearchData()
							.execute(areaName);
				} else {
					if (responce != null)
						parseandlocate(responce);
					else
						CallWLAdapter(CurrentLat, CurrentLon);

				}

				/*
				 * GeoPoint point = new
				 * GeoPoint((int)(Currlatitude*1E6),(int)(Currlongitude*1E6));
				 * //SHows custom popup on overlayclick
				 * mapOverlays=mapview.getOverlays(); Drawable atm_icon =
				 * getResources().getDrawable(R.drawable.arrow);
				 * CustomItemizedOverlay itemizedOverlay = new
				 * CustomItemizedOverlay(atm_icon,ctx,itm); //
				 * LocationDetailPopup itemizedOverlay = // new
				 * LocationDetailPopup(atm_icon); // OverlayItem overlayitem =
				 * // new OverlayItem(point, "RBL Bank", "Current Location"); //
				 * Log.d("cityName=>",""+Locality); OverlayItem overlayitem =
				 * new OverlayItem(point, "Current Location", Locality);
				 * itemizedOverlay.addOverlay(overlayitem);
				 * //mapOverlays.clear(); mapOverlays.add(itemizedOverlay);
				 * MapController mapController = mapview.getController();
				 * mapController.animateTo(point);
				 * mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				 */
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case R.id.navbranch:
			try {
				hideBtnDirection = false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				navhome.setBackgroundResource(R.drawable.home_new);
				navbranch.setBackgroundResource(R.drawable.branch_new_hover);
				navatm.setBackgroundResource(R.drawable.atm_new);
				navlock.setBackgroundResource(R.drawable.locker_new);

				map.setVisibility(View.VISIBLE);
				// map.setBackgroundResource(R.drawable.map);//gc...
				map.setBackgroundResource(R.drawable.map_clicked);
				btnDirection = (ImageView) findViewById(R.id.btnDirection);
				if (btnDirection.isShown()) {
					btnDirection.setVisibility(View.GONE);
				}
				// btnDirection.setBackgroundResource(R.drawable.show_list);
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);
				mapview.setVisibility(View.VISIBLE);
				if (directionFlag) {
					directionFlag = false;
					dirshowlist.setVisibility(View.GONE);
					showText.setVisibility(View.GONE);
					ChangeStart.setVisibility(View.GONE);
				}
				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);

				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				// offersrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				relative.setVisibility(View.GONE);
				branchOverlays();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case R.id.navatm:
			try {
				hideBtnDirection = false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				navhome.setBackgroundResource(R.drawable.home_new);
				navbranch.setBackgroundResource(R.drawable.branch_new);
				navatm.setBackgroundResource(R.drawable.atm_new_hover);
				navlock.setBackgroundResource(R.drawable.locker_new);

				map.setVisibility(View.VISIBLE);
				// map.setBackgroundResource(R.drawable.map);//gc...
				map.setBackgroundResource(R.drawable.map_clicked);
				btnDirection = (ImageView) findViewById(R.id.btnDirection);
				if (btnDirection.isShown()) {
					btnDirection.setVisibility(View.GONE);
				}
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);
				mapview.setVisibility(View.VISIBLE);
				if (directionFlag) {
					directionFlag = false;
					dirshowlist.setVisibility(View.GONE);
					showText.setVisibility(View.GONE);
					ChangeStart.setVisibility(View.GONE);
				}

				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);

				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				// offersrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				relative.setVisibility(View.GONE);
				atmOverlays();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case R.id.navlock:
			try {
				hideBtnDirection = false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				navlock.setBackgroundResource(R.drawable.locker_new_hover);
				navhome.setBackgroundResource(R.drawable.home_new);
				navbranch.setBackgroundResource(R.drawable.branch_new);
				navatm.setBackgroundResource(R.drawable.atm_new);

				map.setVisibility(View.VISIBLE);
				// map.setBackgroundResource(R.drawable.map);//gc...
				map.setBackgroundResource(R.drawable.map_clicked);
				btnDirection = (ImageView) findViewById(R.id.btnDirection);
				if (btnDirection.isShown()) {
					btnDirection.setVisibility(View.GONE);
				}
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);
				mapview.setVisibility(View.VISIBLE);
				if (directionFlag) {
					directionFlag = false;
					dirshowlist.setVisibility(View.GONE);
					showText.setVisibility(View.GONE);
					ChangeStart.setVisibility(View.GONE);
				}

				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);
				// inrel.setVisibility(View.GONE);
				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				// offersrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				relative.setVisibility(View.GONE);
				lockerOverlays();
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;

		/*
		 * Below is for items of 'Drawer'->ATM BRANCH. Need to remove in RBL!
		 */
		case R.id.branch:
			try {
				hideBtnDirection = false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				navhome.setBackgroundResource(R.drawable.home_new);
				navbranch.setBackgroundResource(R.drawable.branch_new_hover);
				navatm.setBackgroundResource(R.drawable.atm_new);

				map.setVisibility(View.VISIBLE);
				// map.setBackgroundResource(R.drawable.map);//gc...
				map.setBackgroundResource(R.drawable.map_clicked);
				btnDirection = (ImageView) findViewById(R.id.btnDirection);
				if (btnDirection.isShown()) {
					btnDirection.setVisibility(View.GONE);
				}
				// btnDirection.setBackgroundResource(R.drawable.show_list);
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);
				mapview.setVisibility(View.VISIBLE);
				if (directionFlag) {
					directionFlag = false;
					dirshowlist.setVisibility(View.GONE);
					showText.setVisibility(View.GONE);
					ChangeStart.setVisibility(View.GONE);
				}
				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);

				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				// offersrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				relative.setVisibility(View.GONE);
				branchOverlays();
			} catch (Exception e) {
				e.printStackTrace();
			}
			// branchOverlays();
			break;
		case R.id.atm:
			try {
				hideBtnDirection = false;
				TextView noffers = (TextView) findViewById(R.id.noffers);
				noffers.setVisibility(View.GONE);
				ImageView alert = (ImageView) findViewById(R.id.alert);
				alert.setVisibility(View.GONE);
				navhome.setBackgroundResource(R.drawable.home_new);
				navbranch.setBackgroundResource(R.drawable.branch_new);
				navatm.setBackgroundResource(R.drawable.atm_new_hover);

				map.setVisibility(View.VISIBLE);
				// map.setBackgroundResource(R.drawable.map);//gc...
				map.setBackgroundResource(R.drawable.map_clicked);
				btnDirection = (ImageView) findViewById(R.id.btnDirection);
				if (btnDirection.isShown()) {
					btnDirection.setVisibility(View.GONE);
				}
				showlist.setVisibility(View.VISIBLE);
				showlist.setBackgroundResource(R.drawable.show_list);
				mapview.setVisibility(View.VISIBLE);
				if (directionFlag) {
					directionFlag = false;
					dirshowlist.setVisibility(View.GONE);
					showText.setVisibility(View.GONE);
					ChangeStart.setVisibility(View.GONE);
				}

				listview = (ListView) findViewById(R.id.slist);
				listview.setVisibility(View.GONE);
				inrel = (LinearLayout) findViewById(R.id.inrel);
				inrel.setVisibility(View.GONE);
				RelativeLayout innrel = (RelativeLayout) findViewById(R.id.innrel);
				innrel.setVisibility(View.GONE);
				// offersrel.setVisibility(View.GONE);
				directionrel.setVisibility(View.GONE);
				relative.setVisibility(View.GONE);
				atmOverlays();
			} catch (Exception e) {
				e.printStackTrace();
			}
			// atmOverlays();
			break;

		default:
			// NavButton.
			break;
		}
	}

	private static final int DIALOG_LIST = 1;
	private Dialog mdialog = null;

	protected Dialog onCreateDialog(int id) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		LayoutInflater inflater = getLayoutInflater();

		switch (id) {
		case DIALOG_LIST:
			// View firstView = inflater.inflate(R.layout.offersdialog, null);
			// builder.setIcon(R.drawable.finance);
			// builder.setView(firstView);
			break;
		}
		mdialog = builder.create();
		return mdialog;
	}

	MapController mapController;
	MyLocationOverlay myLocation;

	@Override
	public void onLocationChanged(Location location) {
		Currlatitude = location.getLatitude();
		Currlongitude = location.getLongitude();

		CurrentLat = String.valueOf(Currlatitude);
		CurrentLon = String.valueOf(Currlongitude);

		String str = "\n CurrentLocation: " + "\n Latitude: " + Currlatitude
				+ "\n Longitude: " + Currlongitude + "\n Accuracy: "
				+ location.getAccuracy();
	}

	private Dialog dialog;
	private View titlteView;
	private GeoPoint geoPoint = null;

	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setItem(OverlayItem item) {
		LayoutInflater infalter = LayoutInflater
				.from(AtmBrancheLockerActivityD.this);
		View addressDialog = infalter.inflate(R.layout.addressdialog, null);
		final Myalert alertDialog = new Myalert(AtmBrancheLockerActivityD.this);
		// alertDialog.setView(bluedialogView);
		alertDialog.setInverseBackgroundForced(true);
		// alertDialog.setView(bluedialogView);
		alertDialog.setView(addressDialog, 0, 0, 0, 0);

		final ImageView close = (ImageView) addressDialog
				.findViewById(R.id.exit);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				alertDialog.cancel();

			}
		});

		((TextView) addressDialog.findViewById(R.id.addtitle)).setText(item
				.getTitle());
		((TextView) addressDialog.findViewById(R.id.adddetails)).setText(item
				.getSnippet());

		// alertDialog.getWindow().setBackgroundDrawable(new
		// ColorDrawable(android.graphics.Color.TRANSPARENT));
		// show it
		alertDialog.show();
	}

	class Myalert extends AlertDialog {

		protected Myalert(Context context, int theme) {
			super(context, theme);// super(context, R.style.ThemeTranslucent);
		}

		protected Myalert(Context context) {
			super(context);
		}

	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(AtmBrancheLockerActivityD.this,
				RBL_iBank.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);

	}

	WLClient mWlClient;
	ProgressDialog mProgressDialog;

	private void CallWLAdapter(final String lattitude, final String longitude) {

		RBLApp app = (RBLApp) getApplication();

		mProgressDialog = ProgressDialog.show(AtmBrancheLockerActivityD.this,
				"Please wait ...", "", true);
		mProgressDialog.setCancelable(true);
		mWlClient = WLClient.createInstance(this);
		mWlClient.connect(new WLResponseListener() {

			@Override
			public void onSuccess(WLResponse arg0) {
				// TODO Auto-generated method stub
				Log.e("  WLConnection onSuccess :=>: ", arg0.getResponseText());

				String AdpterName = "Others";
				String procedureName = "Geolocator";
				final WLProcedureInvocationData invocationData = new WLProcedureInvocationData(
						AdpterName, procedureName, true);
				Object[] parametersObjects = new Object[] { longitude,
						lattitude, "10", "ALL" };
				Log.e("params are ::: ", "lat" + lattitude);
				Log.e("params are ::: ", "lon" + longitude);

				invocationData.setParameters(parametersObjects);

				WLRequestOptions requestOptions = new WLRequestOptions();
				requestOptions.setTimeout(600000);
				mWlClient = WLClient.getInstance();
				mWlClient.invokeProcedure(invocationData,
						new WLResponseListener() {

							@Override
							public void onSuccess(WLResponse arg0) {
								responce = arg0.getResponseJSON().toString();
								parseandlocate(arg0.getResponseJSON()
										.toString());
								mProgressDialog.cancel();
							}

							@Override
							public void onFailure(WLFailResponse arg0) {
								// TODO Auto-generated method stub
								Log.e(" error responce onFailure :: ",
										arg0.getResponseText());
								mProgressDialog.dismiss();
							}
						});

			}

			@Override
			public void onFailure(WLFailResponse arg0) {
				// TODO Auto-generated method stub
				Log.e(" error WLConnection onFailure :: ",
						arg0.getResponseText());
			}
		});

	}

	void parseandlocate(String str) {
		String data = str;
		/*
		 * if(mPdialog!=null) { if(mPdialog.isShowing()) mPdialog.dismiss(); }
		 */
		if (data == null || data.length() < 20) {
			// Toast.makeText(AtmBrancheLockerActivity.this,
			// "No Details Found. Please check your Network Connection.",Toast.LENGTH_SHORT).show();
			return;
		}

		if (PlaceData != null) {
			PlaceData.clear();
			atmData.clear();
			branchData.clear();
			lockerData.clear();
		}

		try {
			JSONObject jObj = null;
			JSONArray list = null;
			JSONObject jObjError = null;
			try {
				jObj = new JSONObject(data);

				list = jObj.getJSONArray("list");

			} catch (JSONException e) {
				// Log.e("JSON Parser", "Error parsing data " + e.toString());
				jObjError = jObj.getJSONObject("list");
				Log.e("IN parseandlocate", "jObjError MSG:" + jObjError);
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Toast.makeText(
								AtmBrancheLockerActivityD.this,
								"Unable to fetch ATMs/Branches. Please try again!",
								1).show();
					}
				});
				return;
			}

			for (int i = 0; i < list.length(); i++) {

				Log.e("** " + i, list.getString(i));
				try {
					String row = list.optString(i, "default");
					JSONObject robject = (JSONObject) new JSONTokener(row)
							.nextValue();
					String City = "";

					City = robject.getString("city");
					String address = robject.getString("address");
					String landmark = robject.getString("landmark");
					CurrentPosition = landmark;
					String locality = robject.getString("locality");
					String phone = robject.getString("phone");
					String fax = robject.getString("fax");
					String atmInfo = robject.getString("atmInfo");
					String lockerInfo = robject.getString("lockerInfo");
					String weekdayInfo = robject.getString("weekdayInfo");
					String weekendinfo = robject.getString("weekendinfo");
					String weeklyoffinfo = robject.getString("weeklyoffinfo");
					String latitude = robject.getString("latitude");
					String longitude = robject.getString("longitude");
					String flggold = robject.getString("flggold");
					String flgsilver = robject.getString("flgsilver");
					String ifsccode = robject.getString("ifccode");
					String type = robject.getString("type");

					// Log.d("json string CIty=>",""+City+"=="+type+"=="+latitude+"=="+longitude);

					HashMap<String, String> placesMap = new HashMap<String, String>();

					placesMap.put("city", City);
					placesMap.put("address", address);
					placesMap.put("landmark", landmark);
					placesMap.put("locality", locality);
					placesMap.put("phone", phone);
					placesMap.put("fax", fax);
					placesMap.put("atmInfo", atmInfo);
					placesMap.put("lockerInfo", lockerInfo);
					placesMap.put("weekdayInfo", weekdayInfo);
					placesMap.put("weekendinfo", weekendinfo);
					placesMap.put("weeklyoffinfo", weeklyoffinfo);
					placesMap.put("latitude", latitude);
					placesMap.put("longitude", longitude);
					placesMap.put("flggold", flggold);
					placesMap.put("flgsilver", flgsilver);
					placesMap.put("ifccode", ifsccode);
					placesMap.put("type", type);

					// placeDetail.add(cplaceDetails.copy());
					// Log.d("Placedata size in Getalllist=>",""+PlaceData.size());
					PlaceData.add(placesMap);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}

			/*
			 * Setting list data for ATMs, Branches and Lockers.
			 */
			for (int i = 0; i < PlaceData.size(); i++) {
				if ("A".equalsIgnoreCase(PlaceData.get(i).get("type"))) {
					HashMap<String, String> placesMap = new HashMap<String, String>();

					placesMap.put("city", PlaceData.get(i).get("city"));
					placesMap.put("address", PlaceData.get(i).get("address"));
					placesMap.put("landmark", PlaceData.get(i).get("landmark"));
					placesMap.put("locality", PlaceData.get(i).get("locality"));
					placesMap.put("phone", PlaceData.get(i).get("phone"));
					placesMap.put("fax", PlaceData.get(i).get("fax"));
					placesMap.put("atmInfo", PlaceData.get(i).get("atmInfo"));
					placesMap.put("lockerInfo",
							PlaceData.get(i).get("lockerInfo"));
					placesMap.put("weekdayInfo",
							PlaceData.get(i).get("weekdayInfo"));
					placesMap.put("weekendinfo",
							PlaceData.get(i).get("weekendinfo"));
					placesMap.put("weeklyoffinfo",
							PlaceData.get(i).get("weeklyoffinfo"));
					placesMap.put("latitude", PlaceData.get(i).get("latitude"));
					placesMap.put("longitude", PlaceData.get(i)
							.get("longitude"));
					placesMap.put("flggold", PlaceData.get(i).get("flggold"));
					placesMap.put("flgsilver", PlaceData.get(i)
							.get("flgsilver"));
					placesMap.put("ifccode", PlaceData.get(i).get("ifccode"));
					placesMap.put("type", PlaceData.get(i).get("type"));

					// Log.d("atmData size in Getalllist=>",""+atmData.size());
					atmData.add(placesMap);
					// tempArray=atmData;
				} else if ("B".equalsIgnoreCase(PlaceData.get(i).get("type"))) {

					HashMap<String, String> placesMap = new HashMap<String, String>();

					placesMap.put("city", PlaceData.get(i).get("city"));
					placesMap.put("address", PlaceData.get(i).get("address"));
					placesMap.put("landmark", PlaceData.get(i).get("landmark"));
					placesMap.put("locality", PlaceData.get(i).get("locality"));
					placesMap.put("phone", PlaceData.get(i).get("phone"));
					placesMap.put("fax", PlaceData.get(i).get("fax"));
					placesMap.put("atmInfo", PlaceData.get(i).get("atmInfo"));
					placesMap.put("lockerInfo",
							PlaceData.get(i).get("lockerInfo"));
					placesMap.put("weekdayInfo",
							PlaceData.get(i).get("weekdayInfo"));
					placesMap.put("weekendinfo",
							PlaceData.get(i).get("weekendinfo"));
					placesMap.put("weeklyoffinfo",
							PlaceData.get(i).get("weeklyoffinfo"));
					placesMap.put("latitude", PlaceData.get(i).get("latitude"));
					placesMap.put("longitude", PlaceData.get(i)
							.get("longitude"));
					placesMap.put("flggold", PlaceData.get(i).get("flggold"));
					placesMap.put("flgsilver", PlaceData.get(i)
							.get("flgsilver"));
					placesMap.put("ifccode", PlaceData.get(i).get("ifccode"));
					placesMap.put("type", PlaceData.get(i).get("type"));

					// Log.d("branchData size in Getalllist=>",""+branchData.size());
					branchData.add(placesMap);
				} else if ("L".equalsIgnoreCase(PlaceData.get(i).get("type"))) {

					HashMap<String, String> placesMap = new HashMap<String, String>();

					placesMap.put("city", PlaceData.get(i).get("city"));
					placesMap.put("address", PlaceData.get(i).get("address"));
					placesMap.put("landmark", PlaceData.get(i).get("landmark"));
					placesMap.put("locality", PlaceData.get(i).get("locality"));
					placesMap.put("phone", PlaceData.get(i).get("phone"));
					placesMap.put("fax", PlaceData.get(i).get("fax"));
					placesMap.put("atmInfo", PlaceData.get(i).get("atmInfo"));
					placesMap.put("lockerInfo",
							PlaceData.get(i).get("lockerInfo"));
					placesMap.put("weekdayInfo",
							PlaceData.get(i).get("weekdayInfo"));
					placesMap.put("weekendinfo",
							PlaceData.get(i).get("weekendinfo"));
					placesMap.put("weeklyoffinfo",
							PlaceData.get(i).get("weeklyoffinfo"));
					placesMap.put("latitude", PlaceData.get(i).get("latitude"));
					placesMap.put("longitude", PlaceData.get(i)
							.get("longitude"));
					placesMap.put("flggold", PlaceData.get(i).get("flggold"));
					placesMap.put("flgsilver", PlaceData.get(i)
							.get("flgsilver"));
					placesMap.put("ifccode", PlaceData.get(i).get("ifccode"));
					placesMap.put("type", PlaceData.get(i).get("type"));

					// Log.d("branchData size in Getalllist=>",""+branchData.size());
					lockerData.add(placesMap);
				}
			}

			/*
			 * Log.e("ATMs List:", ""+atmData); Log.e("Branches List:",
			 * ""+branchData); Log.e("Lockers List:", ""+lockerData);
			 */

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Log.d("PlaceData.size()",""+PlaceData.size()+"atm==>"+""+atmData.size()+"branch="+""+branchData.size());
		for (int i = 0; i < (PlaceData.size() - 1); i++) {
			try {
				latitudeE6 = Double.parseDouble(PlaceData.get(i)
						.get("latitude"));
				longitudeE6 = Double.parseDouble(PlaceData.get(i).get(
						"longitude"));

				// Log.d("latitude & longitude=",PlaceData.get(i).get("latitude")+"==	"+PlaceData.get(i).get("longitude"));

				mapOverlays = mapview.getOverlays();
				/*
				 * Set ATMs overlays.
				 */
				if ("A".equalsIgnoreCase(PlaceData.get(i).get("type"))) {

					// New...
					// Adding a marker
					/*
					 * MarkerOptions marker = new MarkerOptions() .position(new
					 * LatLng(latitudeE6, longitudeE6))
					 * .title(branchDataList.get(i).get("NAME").trim())
					 * .snippet(branchDataList.get(i).get("BUILDINGNAME").trim()
					 * + "\n")
					 * .icon(BitmapDescriptorFactory.fromResource(R.drawable
					 * .branches_icon));
					 * //marker.icon(BitmapDescriptorFactory.defaultMarker
					 * (BitmapDescriptorFactory.HUE_AZURE));
					 * 
					 * googleMap.addMarker(marker);
					 */
					// END---------------------------

					GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
							(int) (longitudeE6 * 1E6));
					Drawable atm_icon = getResources().getDrawable(
							R.drawable.atm_icon);
					final CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
							atm_icon, AtmBrancheLockerActivityD.this);
					OverlayItem overlayitem = new OverlayItem(point,
							"RBL Bank ATM", PlaceData.get(i).get("address"));
					// itemizedOverlay.addOverlay(overlayitem);
					runOnUiThread(new Runnable() {
						public void run() {
							mapOverlays.add(itemizedOverlay);

						}
					});

					// mapOverlays.add(itemizedOverlay);
					MapController mapController = mapview.getController();
					mapController.animateTo(point);
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}

				/*
				 * Set Branches overlays.
				 */
				if ("B".equalsIgnoreCase(PlaceData.get(i).get("type"))) {

					// New...
					// Adding a marker
					/*
					 * MarkerOptions marker = new MarkerOptions() .position(new
					 * LatLng(latitudeE6, longitudeE6))
					 * .title(branchDataList.get(i).get("NAME").trim())
					 * .snippet(branchDataList.get(i).get("BUILDINGNAME").trim()
					 * + "\n")
					 * .icon(BitmapDescriptorFactory.fromResource(R.drawable
					 * .branches_icon));
					 * //marker.icon(BitmapDescriptorFactory.defaultMarker
					 * (BitmapDescriptorFactory.HUE_AZURE));
					 * 
					 * googleMap.addMarker(marker);
					 */
					// END---------------------------

					GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
							(int) (longitudeE6 * 1E6));
					Drawable branch_icon = getResources().getDrawable(
							R.drawable.branch_icon);
					final CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
							branch_icon, ctx, itm);
					OverlayItem overlayitem = new OverlayItem(point,
							"RBL Bank", PlaceData.get(i).get("address"));
					itemizedOverlay.addOverlay(overlayitem);
					// mapOverlays.add(itemizedOverlay);
					runOnUiThread(new Runnable() {
						public void run() {
							mapOverlays.add(itemizedOverlay);

						}
					});

					MapController mapController = mapview.getController();
					mapController.animateTo(point);
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}

				/*
				 * Set Lockers overlays.
				 */
				if ("L".equalsIgnoreCase(PlaceData.get(i).get("type"))) {

					// New...
					// Adding a marker
					/*
					 * MarkerOptions marker = new MarkerOptions() .position(new
					 * LatLng(latitudeE6, longitudeE6))
					 * .title(branchDataList.get(i).get("NAME").trim())
					 * .snippet(branchDataList.get(i).get("BUILDINGNAME").trim()
					 * + "\n")
					 * .icon(BitmapDescriptorFactory.fromResource(R.drawable
					 * .branches_icon));
					 * //marker.icon(BitmapDescriptorFactory.defaultMarker
					 * (BitmapDescriptorFactory.HUE_AZURE));
					 * 
					 * googleMap.addMarker(marker);
					 */
					// END---------------------------
					GeoPoint point = new GeoPoint((int) (latitudeE6 * 1E6),
							(int) (longitudeE6 * 1E6));
					Drawable branch_icon = getResources().getDrawable(
							R.drawable.branch_icon);
					final CustomItemizedOverlay itemizedOverlay = new CustomItemizedOverlay(
							branch_icon, ctx, itm);
					OverlayItem overlayitem = new OverlayItem(point,
							"RBL Bank Locker", PlaceData.get(i).get("address"));
					itemizedOverlay.addOverlay(overlayitem);
					runOnUiThread(new Runnable() {
						public void run() {
							mapOverlays.add(itemizedOverlay);

						}
					});

					MapController mapController = mapview.getController();
					mapController.animateTo(point);
					mapController.setZoom(AppConstants.INTZOOM_LEVEL);
				}

			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("LOgs ", e.toString());
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.e("LOgs/// ", e.toString());

			}
		}
	}

}
