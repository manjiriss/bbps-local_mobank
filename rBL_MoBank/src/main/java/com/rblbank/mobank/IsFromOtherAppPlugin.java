package com.rblbank.mobank;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author awysocki
 */

public class IsFromOtherAppPlugin extends CordovaPlugin {
	public static final String MOBANK = "rblMobank";

	@Override
	public boolean execute(String action, JSONArray data,
			final CallbackContext callbackContext) throws JSONException {

		if (MOBANK.equals(action)) {
			JSONObject obj = new JSONObject();
			obj.put("custId", RBL_iBank.custBean.getCustId());
			obj.put("custPWD",
					RBL_iBank.custBean.getCustPWD());
			obj.put("custLogFlg",
					RBL_iBank.custBean.getCustLogFlg());
			obj.put("module", RBL_iBank.custBean.getModule());
			obj.put("deviceId", RBL_iBank.custBean.getDeviceId());
			// System.out.println("Response Needed" + data.getString(0));
			callbackContext.success(obj);
			return true;

		}

		return false;
	}
}