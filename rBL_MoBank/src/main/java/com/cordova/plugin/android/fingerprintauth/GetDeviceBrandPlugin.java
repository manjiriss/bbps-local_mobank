package com.cordova.plugin.android.fingerprintauth;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

import com.rblbank.mobank.RBL_iBank;

public class GetDeviceBrandPlugin extends CordovaPlugin {

	public static final String DeviceName = "device_name";

	@Override
	public boolean execute(String action, String rawArgs,
			CallbackContext callbackContext) throws JSONException {

		if (action.equals(DeviceName)) {

			String result = Build.BRAND;

			
			JSONObject obj = new JSONObject();
			obj.put("DeviceName", result);
			
			callbackContext.success(obj);
		}
		return false;

	}
}
