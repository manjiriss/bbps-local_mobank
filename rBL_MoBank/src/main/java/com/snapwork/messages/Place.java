package com.snapwork.messages;

public class Place {

	// private String result;
	/*
	 * private String num; private String type; private String status; private
	 * String radius; private String xLongitude; private String yLatitude;
	 * private String pois;
	 */
	private String poi;
	private String poi_id;
	private String landmark;
	private String address;
	private String city;
	private String locality;
	private String state;
	private String phone;
	private String fax;
	private String atm_info;
	private String locker_info;
	private String weekday_info;
	private String weekend_info;
	private String weeklyoff_info;
	private String latitude;
	private String longitude;
	private String type;
	private String flggold;
	private String flgsilver;
	private String ifscode;

	public Place() {
		super();
	}

	public String getPoi_id() {
		return poi_id;
	}

	public void setPoi_id(String poi_id) {
		this.poi_id = poi_id;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getAtm_info() {
		return atm_info;
	}

	public void setAtm_info(String atm_info) {
		this.atm_info = atm_info;
	}

	public String getLocker_info() {
		return locker_info;
	}

	public void setLocker_info(String locker_info) {
		this.locker_info = locker_info;
	}

	public String getWeekday_info() {
		return weekday_info;
	}

	public void setWeekday_info(String weekday_info) {
		this.weekday_info = weekday_info;
	}

	public String getWeekend_info() {
		return weekend_info;
	}

	public void setWeekend_info(String weekend_info) {
		this.weekend_info = weekend_info;
	}

	public String getWeeklyoff_info() {
		return weeklyoff_info;
	}

	public void setWeeklyoff_info(String weeklyoff_info) {
		this.weeklyoff_info = weeklyoff_info;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String gettype() {
		return type;
	}

	public void settype(String type) {
		this.type = type;
	}

	public String getFlggold() {
		return flggold;
	}

	public void setFlggold(String flggold) {
		this.flggold = flggold;
	}

	public String getFlgsilver() {
		return flgsilver;
	}

	public void setFlgsilver(String flgsilver) {
		this.flgsilver = flgsilver;
	}

	public String getIfscode() {
		return ifscode;
	}

	public void setIfscode(String ifscode) {
		this.ifscode = ifscode;
	}

	public String getPoi() {
		return poi;
	}

	public void setPoi(String poi) {
		this.poi = poi;
	}

}
