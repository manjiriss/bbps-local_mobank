package com.snapwork.messages;

import com.google.android.maps.OverlayItem;

public interface ReplyItem {

	public void setItem(OverlayItem item);
}
