package com.snapwork.widget;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;


import com.rblbank.mobank.R;

public class QuickAction extends CustomPopupWindow {
	private final View root;
	private final LayoutInflater inflater;
	private final Context context;

	public QuickAction(View anchor) {
		super(anchor);
		context = anchor.getContext();
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		root = (ViewGroup) inflater.inflate(R.layout.offersdialog, null);

		setContentView(root);

	}

	/**
	 * Show popup window
	 */
	public void show() {
		preShow();
		root.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		window.showAtLocation(this.anchor, Gravity.NO_GRAVITY,
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	}
}
/*
 * wlServerProtocol = https wlServerHost = m.ratnakarbank.co.in wlServerPort =
 * 443 wlServerContext = /RBL_iBank/ wlAppId = RBL_iBank wlAppVersion = 1.0
 * GcmSenderId = enableSettings = true testWebResourcesChecksum = false
 * ignoredFileExtensions = webResourcesSize=2878382
 * wlUid=M+Alj/BJC9amOPx0eND1LA== wlPlatformVersion=6.1.0.01.20140518-1532
 * wlMainFilePath=index.html
 */