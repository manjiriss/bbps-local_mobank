package com.snapwork.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.snapwork.messages.ContactDetails;

public class ContactsParser extends DefaultHandler {
	Boolean celement = false;
	String cvalue = null;
	public List<ContactDetails> contacts;
	public static ContactDetails contact;

	public ContactsParser() {
		contacts = new ArrayList<ContactDetails>();
	}

	public ContactsParser(List<ContactDetails> contacts) {
		super();
		this.contacts = contacts;
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		celement = true;

		try {

			if (localName.equals("result")) {
				contact = new ContactDetails();
			} else if (localName.equals("district")) {
				/** Get attribute value */
				contact = new ContactDetails();
				contacts.add(contact);

				String name = attributes.getValue("name");

			} else if (localName.equals("state")) {
				String name = attributes.getValue("name");
				contact.setSname(name);
			} else if (localName.equals("city_town_village")) {
				String name = attributes.getValue("name");
				contact.setName(name);
			} else if (localName.equals("locality")) {
				String name = attributes.getValue("name");
				contact.setLocality(name);
			} else if (localName.equals("pincode")) {
				String name = attributes.getValue("name");
				contact.setPincode(name);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {

		celement = false;

		if (localName.equalsIgnoreCase("result")) {
			contact.setCity_town_village(cvalue);
			// Log.i("data",cvalue);
			// places.add(placelist);

		}
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {

		if (celement) {
			cvalue = new String(ch, start, length);
			celement = false;
		}
	}

}
