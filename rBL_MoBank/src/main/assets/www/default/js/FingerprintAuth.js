
/* JavaScript content from js/FingerprintAuth.js in folder common */
function FingerprintAuth() {

}

FingerprintAuth.prototype.show = function (params, successCallback, errorCallback) {
    cordova.exec(
        successCallback,
        errorCallback,
        "FingerprintAuth",  // Java Class
        "authenticate", // action
        [ // Array of arguments to pass to the Java class
            params
        ]
    );
};

FingerprintAuth.prototype.isAvailable = function (successCallback, errorCallback) {
    cordova.exec(
        successCallback,
        errorCallback,
        "FingerprintAuth",  // Java Class
        "availability", // action
        [{}]
    );
};

//FingerprintAuth.show({
//    clientId: "myAppName",
//    clientSecret: "a_very_secret_encryption_key"
//}, successCallback, errorCallback);

FingerprintAuth = new FingerprintAuth();
module.exports = FingerprintAuth;

function isTouchIDAvailable(){
FingerprintAuth.isAvailable(isAvailableSuccess, isAvailableError);
}

function successCallback(result) {
//alert("successCallback(): " + JSON.stringify(result));
	
//if (result.withFingerprint) {
//alert("Successfully authenticated using a fingerprint");
//} else if (result.withPassword) {
//alert("Authenticated with backup password");
//}

	window.location.hash="#loginTouchID";

}

function errorCallback(error) {
//alert("cancel clicked : "+error);
$(".h_title").html("Login");
loadViewModel("login");
// "IF user clicks on cancel"
}


function isAvailableSuccess(result) {
    //alert("FingerprintAuth available: " + JSON.stringify(result));
    if (result.isAvailable) {
    //alert("Touch ID is Available");
   if((statusforTouchId == "true") && (isRootedDevice == false)){
//if(statusforTouchId == "true"){
  //  alert("device is in list")
        FingerprintAuth.show({
                    clientId: "TestFP",
                    clientSecret: "a_very_secret_encryption_key"
                }, successCallback, errorCallback);
    }

    else{
   // alert("inside else");
    $(".h_title").html("Login");
        loadViewModel("login");

    }
    }
    else
    {
      $(".h_title").html("Login");
            loadViewModel("login");
    }
}

function isAvailableError(message) {
   //alert("isAvailableError(): " + message);
	//alert("Fingerprint authentication not available for this device"+message);
	$(".h_title").html("Login");
    loadViewModel("login");
}