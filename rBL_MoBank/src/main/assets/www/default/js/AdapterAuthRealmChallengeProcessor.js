//
///* JavaScript content from js/AdapterAuthRealmChallengeProcessor.js in folder common */
//
///* JavaScript content from js/AdapterAuthRealmChallengeProcessor.js in folder common */
///*
//*  Licensed Materials - Property of IBM
//*  5725-G92 (C) Copyright IBM Corp. 2006, 2013. All Rights Reserved.
//*  US Government Users Restricted Rights - Use, duplication or
//*  disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
//*/
//
//var adapterAuthRealmChallengeHandler = WL.Client.createChallengeHandler("AdapterAuthRealm");
//
//adapterAuthRealmChallengeHandler.isCustomResponse = function(response) {
//	if (!response || !response.responseJSON	|| response.responseText === null) {
//		return false;
//	}
//	if (typeof(response.responseJSON.authRequired) !== 'undefined'){
//		return true;
//	} else {
//		return false;
//	}
//};
//
//adapterAuthRealmChallengeHandler.handleChallenge = function(response){
//	var authRequired = response.responseJSON.authRequired;
//
//	if (authRequired == true){
//		navigator.notification.alert("Your session has timed out!");
//		//$("#contentData").load("Views/login/login.html", null, function (response, status, xhr) {
//        //    if (status != "error") {}
//			busyInd.hide();
//			window.location.hash = "#logout";
//            busyInd.hide();
//        //});
//
//	} else if (authRequired == false){
//
//		if(CurrentSessionId == "" || CurrentSessionId == null){
//			busyInd.hide();
//			window.location.hash = "#logout";
//		}
//		else{
//			var invocationData = {
//								adapter : "API_Adapter",
//								procedure : "storeSession",
//								parameters : [fldloginUser],
//								compressResponse : true
//							};
//
//							WL.Client.invokeProcedure(invocationData, {
//								onSuccess : function Lsuccess(res){},
//								onFailure : function Lfaulure(res){},
//								timeout: timeout
//							});
//			window.location.hash = "#Home";
//		}
//		//window.location = "#Home";
//		adapterAuthRealmChallengeHandler.submitSuccess();
//	}
//};
//
//
///* $("#AuthSubmitButton").bind('click', function () {
//	var username = $("#AuthUsername").val();
//	var password = $("#AuthPassword").val();
//
//	var invocationData = {
//		adapter : "AuthenticationAdapter",
//		procedure : "submitAuthentication",
//		parameters : [ username, password ]
//	};
//
//	adapterAuthRealmChallengeHandler.submitAdapterAuthentication(invocationData, {});
//}); */
//
//$("#AuthCancelButton").bind('click', function () {
//	$("#AppDiv").show();
//	$("#AuthDiv").hide();
//	adapterAuthRealmChallengeHandler.submitFailure();
//});

var adapterAuthRealmChallengeHandler = WL.Client.createChallengeHandler("AdapterAuthRealm");

adapterAuthRealmChallengeHandler.isCustomResponse = function(response) {
	if (!response || !response.responseJSON	|| response.responseText === null) {
		return false;
	}
	if (typeof(response.responseJSON.authRequired) !== 'undefined'){
		return true;
	} else {
		return false;
	}
};

adapterAuthRealmChallengeHandler.handleChallenge = function(response){
	var authRequired = response.responseJSON.authRequired;

	if (authRequired == true){
//		navigator.notification.alert("Your session has timed out!");
//			busyInd.hide();
//			window.location.hash = "#logout";
//            busyInd.hide();

            var invocationData = {
					adapter : "API_Adapter",
					procedure : "submitAuthenticationNew",
					parameters : [ fldloginUser, fldloginUser ]
				};

				adapterAuthRealmChallengeHandler.submitAdapterAuthentication(invocationData, {});



	} else if (authRequired == false){

		if(CurrentSessionId == "" || CurrentSessionId == null){
			busyInd.hide();
			sessionTimeOutMsg="Your session has timed out!";
			window.location.hash = "#logout";
		}
		else{
			var invocationData = {
								adapter : "API_Adapter",
								procedure : "storeSession",
								parameters : [fldloginUser],
								compressResponse : true
							};

							WL.Client.invokeProcedure(invocationData, {
								onSuccess : function Lsuccess(res){},
								onFailure : function Lfaulure(res){},
								timeout: timeout
							});
			window.location.hash = "#Home";
		}
		//window.location = "#Home";
		adapterAuthRealmChallengeHandler.submitSuccess();
	}
};