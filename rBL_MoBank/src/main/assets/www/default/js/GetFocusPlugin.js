var plugin = function(){
}
module.exports = new plugin();
/*
For Phonegap android builds perform Init on deviceReady event.
*/

/**
SDK Initialization part
**/
plugin.prototype.init = function (success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "init", []);
}


/**
SDK status check
**/
plugin.prototype.sdkState = exports.sdkState = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "sdkState", []);
};

/**
SDK User Service Level
**/
plugin.prototype.userServiceLevel = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "userServiceLevel", []);
};

/**
SDK Client Service Level
**/
plugin.prototype.clientServiceLevel = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "clientServiceLevel", []);
};

/**
User Registration
**/
// Register User attributes
// userid = String (Unique Identifier for the user)
plugin.prototype.registerUserById = function(userid, success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "registerUserId", [userid]);
};

/**
User Registration
**/
// Register User attributes
// userId = String
// email = String
// Note: please maintain above order while passing
plugin.prototype.registerUserByMailId = function(userid, email, success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "registerUserMailId", [userid, email]);
};

/**
User Registration
**/
// Register User attributes
// userId = String
// email = String
// name = String
// Note: please maintain above order while passing
plugin.prototype.registerUserByName = function(userid, email, name, success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "registerUserName", [userid, email, name]);
};

/**
Update User profile
**/
// Update User attributes
// userId = String
// email = String
// name = String
// Note: please maintain above order while passing
plugin.prototype.updateUser = function(userid, email, name, success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "updateUser", [userid, email, name]);
};

/**
SDK Fetch Current User information
**/
plugin.prototype.fetchUserInfo = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "fetchUserInfo", []);
};

/**
SDK User De-Registration
**/
plugin.prototype.unRegisterUser = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "unRegisterUser", []);
};

/**
SDK Location Prediction
**/
plugin.prototype.predictLocation = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "predictLocation", []);
};

/**
SDK Location Prediction
**/
// Predict Location attributes
// event = String (Example - Rebook, Cinema, Blackberry etc.,.)
plugin.prototype.predictLocationParams = function(event, success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "predictLocationParams", [event]);
};

/**
SDK Location Prediction
**/
// Predict Location attributes
// event = Object (Example - {sports-Rebook, Entertainment-Cinema, Fashion-USPolo}
plugin.prototype.predictLocationParamsMap = function(eventProperties, success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "predictLocationParamsMap", [eventProperties]);
};

/**
SDK Enabling Background Tracking
**/
plugin.prototype.enableTracking = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "enableTracking", []);
};

/**
SDK Disable Background Tracking
**/
plugin.prototype.disableTracking = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "disableTracking", []);
};

/**
SDK Disable current User
**/
plugin.prototype.disableUser = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "disableUser", []);
};

/**
SDK Enable current User
**/
plugin.prototype.enableUser = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "enableUser", []);
};

/**
SDK Disable Current User Device
**/
plugin.prototype.disableUserDevice = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "disableUserDevice", []);
};

/**
SDK Enable Current User Device
**/
plugin.prototype.enableUserDevice = function(success, error) {
    cordova.exec(success, error, "FocusSDKPlugin", "enableUserDevice", []);
};


