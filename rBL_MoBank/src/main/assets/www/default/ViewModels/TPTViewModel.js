
/* JavaScript content from ViewModels/TPTViewModel.js in folder common */

/* JavaScript content from ViewModels/TPTViewModel.js in folder common */
﻿  var TPTViewModel = function () {

        var self = this;
        self.selAccount = ko.observable();
        self.fromAccountList = ko.observableArray([]);
        self.toAccountList = ko.observableArray([]);
        self.fldFromAcctNo = ko.observable();
        self.fldToAcctNo = ko.observable();
        self.benetype = ko.observable('');
		self.selAccount = ko.observable();
		self.selAcctTemp = ko.observable();
		self.fldmobileemail = ko.observable();
        self.rtgsTxnList = ko.observableArray([]);
        self.fldSelTxnRefNo = ko.observable();
		self.accountStmtTxns = ko.observableArray([]);
        self.cpqQuesList = ko.observableArray([]);
        
        self.impsdetails = ko.observableArray([]);
        self.selRefno = ko.observable('');
        self.fldRefNodata = ko.observable('');
        
        //tpi vari
        self.benefTPT = ko.observableArray([]);
        self.benefNEFT = ko.observableArray([]);
        self.benefRTGS = ko.observableArray([]);
        
        // For Beneficiery
        var PayeeNo = "";
        var PayeeName = "";
        var PayeeNickName = "";
        var IFSCCode = "";
        var BankName = "";
        var BranchName = "";
        var City = "";
        
        
        self.previousPage = ko.observable();
        
		//Within bank
        this.callTPT03 = function(){
		if(window.navigator.onLine){
					reqParams = {};
					reqParams["RQLoginUserId"] = LoggedInUserID;
					reqParams["RQDeviceFamily"] = Device_Platform;
					reqParams["RQDeviceFormat"] = Device_Model;
					reqParams["RQOperationId"] = "TPTFNDTFR";
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = CurrentSessionId;
					reqParams["RQTransSeq"] = "01";
					fldjsessionid="";
//					busyInd.show();
					customBusyShow();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "RRTPT03",
							parameters : [fldjsessionid,reqParams],
							compressResponse : true
					};
					
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : self.rrtpt03Success,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
        };
        
        this.rrtpt03Success = function(result){
        	
        invocationResult = result.invocationResult;
        if(invocationResult.isSuccessful) {
			if(invocationResult.RBL){
        		if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE == '0'){
						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
						var currentuser = currentreguserid;
						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
						if(currentuser == responseuser && CurrentSessionId == responsesessionid){
        			
							fromAccdata = invocationResult.RBL.Response.validAcct.acctdetails;
							toAccdata = invocationResult.RBL.Response.tptacctdetails;
							
							nbrofsavingacc =invocationResult.RBL.Response.savingacctcount;
							nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
		 
							totAccount = parseInt(nbrofsavingacc) + parseInt(nbrofcurrentacc);	    		
			
							self.fromAccountList.removeAll();
							self.toAccountList.removeAll();
							
							$(fromAccdata).each(function(index, obj) {
								displaytxt = obj.acctno+" - "+obj.branchname;
								accountValue = obj.acctno+"#"+obj.acctbalance+"#"+obj.branchname+"#"+obj.currency;
								self.fromAccountList.push({ acctno: obj.acctno, acctType: obj.acctType, acctbalance: obj.acctbalance, branchname: obj.branchname, currency: obj.currency,  displaytxt:displaytxt, accountValue: accountValue });
							}); 
							
							$(toAccdata).each(function(index, obj) {
								displaytxt = obj.benefname;
								accountValue = obj.acctno+"#"+obj.benefname;
								self.toAccountList.push({ acctno: obj.acctno, benefname: obj.benefname, displaytxt:displaytxt, accountValue: accountValue });
							}); 
							
							$("#contentData").load("Views/TPT/rrtpt03.html", null, function (response, status, xhr) {
								if (status != "error") {}	 
								if(invocationResult.RBL.Response.tptacctdetails!==undefined){
									if(toAccdata.length!=0){
										$("#ftransfer").show();
										$("#accExitsMsg").hide();
									}else{
										$("#accExitsMsg").show();
										$("#ftransfer").hide();
									}	
								}					
								else{
									$("#accExitsMsg").show();
									$("#ftransfer").hide();
								}	
								ko.applyBindings(self, $(".dynamic-page-content").get(0));
//								busyInd.hide();		
								customBusyHide();
							}); 
						}
						else{
//								busyInd.hide();
								customBusyHide();
								navigator.notification.alert("Your session has timed out!");
								window.location.hash = "#logout";
						}			
					}
					else{
//						busyInd.hide();
						customBusyHide();
						invalidresponse();
					}
        		}else{
//								busyInd.hide();
        						customBusyHide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
			}else{
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
//        		busyInd.hide();
        	    customBusyHide();
        	}
        }else{
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
//        		 busyInd.hide();
        		 customBusyHide();
        }
        	//}
//        	busyInd.hide();
            customBusyHide();
        };
        
        
        this.rrtpt03Submit = function(){
           
        	if($("#frmtpt03").valid()){
				$('#confirm_button1').prop('disabled', true);
				var benefno = $('#fldFromAcctNo').val();
				benefno_acct_no($('#fldToAcctNo').val().split("#")[0]);
				benef_amnt($('#fldTxnAmount').val());
				benef_currency(benefno.split("#")[3]);
				benefdesc($('#fldTxnDesc').val());
				beneffrom_accnt_no(benefno.split("#")[0]);
				benef_name($('#fldToAcctNo').val().split("#")[1]);
				busyInd.hide();
        	    GetOTP();	
        	}    	
        };
        
           
        this.callTPT04 = function(){
        	
		    $("#contentData").load("Views/TPT/rrtpt04.html", null, function (response, status, xhr) {
                if (status != "error") {}	
				OTP_number = invocationResult1.RBL.Response.otprefno;
				$('#OTP_Ref').val(OTP_number);
				if(invocationResult1.RBL.Response.otprequired == 'Y'){
				$('#OTP_No').show();
				}
				else{
					$('#OTP_No').hide();
				}
				
                $("#fldfromacctno").html(beneffrom_accnt_no());
                $("#fldtoacctno").html(benefno_acct_no());
            	$("#fldTxnAmount").html("₹ "+formatAmt(parseFloat(benef_amnt())));
            	$("#TxnAmount").html(benef_amnt());
				$("#fldTxnDesc").html(benefdesc());
				$('#acctCur').html(benef_currency());
            	                
                ko.applyBindings(self, $(".dynamic-page-content").get(0));
				busyInd.hide();				
            }); busyInd.hide();
        };
        
        
    this.rrtpt04Submit = function(){
	if(window.navigator.onLine){
        if($("#frmtpt04").valid()){
        	var $form = $("#frmtpt04");
        	    	    	
        	reqParams = {};
			reqParams["RQLoginUserId"] = LoggedInUserID;
        	reqParams["RQDeviceFamily"] = Device_Platform;
        	reqParams["RQDeviceFormat"] = Device_Model;
        	reqParams["RQOperationId"] = "TPTFNDTFR";
        	reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQBeneName"] = benef_name();
        	reqParams["RQFromAcctNo"] = $('#fldfromacctno').text();
	    	reqParams["RQToAcctNo"] = $('#fldtoacctno').text();
			reqParams["RQTxnAmount"] = $('#TxnAmount').text();
	    	reqParams["RQTransSeq"] = "03";
        	reqParams["RQDesc"] = $('#fldTxnDesc').text();
        	
			reqParams["RQOTP"] = $('#OTP_No_Val').val();
			reqParams["RQRefNo"] = $('#OTP_Ref').val();
			reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
			

	    	fldjsessionid = '';
//	    	busyInd.show();
	    	customBusyShow();
	    	if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
	    			adapter : "API_Adapter",
    	        	procedure : "RRTPT04",
	        		parameters : [fldjsessionid,reqParams],
					compressResponse : true
	    	};
        	
        	WL.Logger.debug(invocationData, '');
        
        	WL.Client.invokeProcedure(invocationData, {
        		onSuccess : rrtptResponse,
        		onFailure : AdapterFail,
        		timeout: timeout
        	}); 
   	    }
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
    };
		
		rrtptResponse = function(result){
		
			invocationResult = result.invocationResult;
			if(invocationResult.isSuccessful) {
				if(invocationResult.RBL.Response || invocationResult.RBL.Response == ''){
					
        			amttxn = invocationResult.RBL.Response.amttxn;
        			reply = invocationResult.RBL.STATUS.MESSAGE;
        			fromacctno = invocationResult.RBL.Response.fromacctno+" "+invocationResult.RBL.Response.customername;
        			brnname = invocationResult.RBL.Response.bankname;
        			toacctno = invocationResult.RBL.Response.toacctno;
        			txndesc = invocationResult.RBL.Response.txndesc;
					codtxnrefno = invocationResult.RBL.Response.txnrefno;

        			$("#contentData").load("Views/TPT/rrtpt05.html", null, function (response, status, xhr) {
        				if(invocationResult.RBL.STATUS.CODE == '0'){
    	                	$(".success_msg").show();
//    	                	busyInd.hide();
    	                	customBusyHide();
							accountList.removeAll();}
    	                else{
							$(".rsaOOBErr").show();
							$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
    	                }
    	                $("#refno").html(codtxnrefno);
    	                $("#amount").html("₹ "+formatAmt(parseFloat(amttxn)));
    	                $("#txnStatus").html(reply);
    	                $("#fromAccount").html(fromacctno);
    	                $("#toAccount").html(toacctno);
    	                $("#description").html(txndesc);
    	                ko.applyBindings(self, $(".dynamic-page-content").get(0));
//						busyInd.hide();
    	                customBusyHide();
        			}); 
			    //}
				}else{
//						busyInd.hide();
						customBusyHide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
				//else{alert(invocationResult.RBL.STATUS.MESSAGE);}
			} 
//			busyInd.hide();
			customBusyHide();
		};
		//within end
		

		//NEFT start
		self.tpn04Page = function(){
		if(window.navigator.onLine){
			fldjsessionid = '';
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "NFTFNDTFR";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				
//				busyInd.show();
				customBusyShow();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetNeft",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
				WL.Logger.debug(invocationData, '');
					
				WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrtpn01Response,
						onFailure : AdapterFail,
						timeout: timeout
				});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
		};
		
		rrtpn01Response = function(result){
				invocationResult = result.invocationResult;
				accountListFt.removeAll();
				rdAccountList.removeAll();
			if(invocationResult.isSuccessful){
			if(invocationResult.RBL){
				if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE == '0'){
						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
						var currentuser = currentreguserid;
						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
						if(currentuser == responseuser && CurrentSessionId == responsesessionid){
							
							if(invocationResult.RBL.Response.tptacctdetails!=undefined){
								if(invocationResult.RBL.Response.tptacctdetails.length!=0){
									acctdtls = invocationResult.RBL.Response.validAcct.acctdetails;
									custdtls = invocationResult.RBL.Response.CustDetails;
									var idx = 1;
									$(acctdtls).each(function(index, obj) {
										strid = "item"+idx;
										
										$(custdtls).each(function(j, obj1) {
											custnames="";
											if(obj.acctindex == obj1.acctindex){
												var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
												displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
												accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
											}
										});
										idx++;
									});
									tptacctdtls = invocationResult.RBL.Response.tptacctdetails;
									var idx1 = 1;
									$(tptacctdtls).each(function(index, obj) {
										strid = "item"+idx1;
										rdAccountList.push({ codacctno: obj.acctno+"#"+obj.benefname+"#"+obj.ifsccode+"#"+obj.acctno+"#"+obj.bankname+"#"+obj.beneaddr+"",  displaytxt: obj.benefname, strid:strid });
										idx1++;
									});
								}
						    }							
						
							$("#contentData").load("Views/TPT/rrtpn04.html", null, function (response, status, xhr) {
								if (status != "error") {}

								if(invocationResult.RBL.Response.tptacctdetails!==undefined){
								
									$("#nftfundtrns").show();
									$("#accExitsMsg").hide();
									
									if(tptacctdtls.length!=0){
										$('#tptCont').show();
									}else{
										$('#accExitsMsg').show();
									}	
								}					
								else{
									$("#accExitsMsg").show();
									$("#nftfundtrns").hide();
								}						
									//$('#cutoffmsgBox2').show();
									ko.applyBindings(self, $(".dynamic-page-content").get(0));
							});
//							busyInd.hide();
							customBusyHide();
						}
						else{
//								busyInd.hide();
								customBusyHide();
								navigator.notification.alert("Your session has timed out!");
								window.location.hash = "#logout";
						}
					}
					else{
//						busyInd.hide();
						customBusyHide();
						invalidresponse();
					}
				}else{
//								busyInd.hide();
								customBusyHide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
			}else{
//       		 busyInd.hide();
				 customBusyHide();
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
			}

			}; 
//			busyInd.hide();
			customBusyHide();
		};
			
		 
		 self.rrtpn04Submit = function(){
				if($("#rrtpn04").valid()){
				$('#confirm_button1').prop('disabled', true);
				var $form = $("#rrtpn04");
	        	rsaDataArray = $form.serializeArray();    	
	        	
				var benefno = $('#fldBenefDetail').val();
				benefno_acct_no(benefno.split("#")[0]);
				benef_IFSC(benefno.split("#")[2]);
				benef_amnt($('#fldTxnAmount').val());
				benef_name(benefno.split("#")[1]);
				benefdesc($('#fldTxnDesc').val());
				beneffrom_accnt_no($('#fldAcctNo').val().split("#")[0]);
				benef_bankname(benefno.split("#")[4]);
				benef_addr(benefno.split("#")[5]);
				sender_name($('#fldAcctNo').val().split("#")[1]);
				busyInd.hide();
				GetOTP();
			};
		};
			
		//OTP	
		GetOTP = function(){
		if(window.navigator.onLine){
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "OTPGENREQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetOTP",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
				busyInd.show();
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : GetOTPResponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
		};	

		GetOTPResponse = function(result){
			invocationResult1 = result.invocationResult;
			if(invocationResult1.RBL){
				if(invocationResult1.isSuccessful) {
					if(invocationResult1.RBL.Response){	
						if(window.location.hash == "#rrtpn04"){
							window.location = "#rrtpn05";
						}
						if(window.location.hash == "#rrtpt03"){
							window.location = "#rrtpt04";
						}
						if(window.location.hash == "#rrp2a01"){
							busyInd.hide();
							window.location = "#rrp2a02";
						}
						if(window.location.hash == "#rrrtg04"){
							window.location = "#rrrtg05";
						}
						else{
							busyInd.hide();
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
						}
					}else{
							busyInd.hide();
							handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
					}		
				}
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			busyInd.hide();
		};


		//NEFT submit	
		self.tpn05Page = function(){
				//accstmtdata = accStmtData();
					$("#contentData").load("Views/TPT/rrtpn05.html", null, function (response, status, xhr) {
						if (status != "error") {}	
							
						OTP_number = invocationResult1.RBL.Response.otprefno;
						$('#OTP_Ref').val(OTP_number);
						
						if(invocationResult1.RBL.Response.otprequired == 'Y'){
							$('#OTP_No').show();
						}
						else{
							$('#OTP_No').hide();
						}

							fldAcctNo = beneffrom_accnt_no();
							$('.fldAcctNo').html(fldAcctNo);
							$('.ifsccode').html(benef_IFSC());
							$('.narrative').html(benef_name());
							$('.codacctno').html(benefno_acct_no());
							$('.bankname').html(benef_bankname());
							$('.fldtxnamount').html("₹ "+formatAmt(parseFloat(benef_amnt()))); 
							$('.fldtxndesc').html(benefdesc());
							$('.txnamount').html(benef_amnt())
							
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});
					 busyInd.hide();
		};
		 
		 
		self.rrtpn05Submit = function(){
		if(window.navigator.onLine){
			if($("#frmtpn05").valid()){
        			acctno = $('.fldAcctNo').text();
        			benefname = $('.narrative').text();
        			ifsccode = $('.ifsccode').text();
        			benefacctno = $('.codacctno').text();
        			bankdesc = $('.bankname').text();
        			txnamount = $('.txnamount').text();
        			txndesc = $('.fldtxndesc').text();
					benefbankname= $('.bankname').text();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "NFTFNDTFR";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQSndName"] = sender_name();
				reqParams["RQFromAcctNo"] = acctno;
				reqParams["RQToAcctNo"] = benefacctno;
				reqParams["RQBeneName"] = benefname;
				reqParams["RQBeneBank"] = benefbankname;
				reqParams["RQBeneAddress"] = benef_addr();
				reqParams["RQDesc"] = txndesc;
				reqParams["RQTxnAmount"] = txnamount;
				reqParams["RQTransSeq"] = '03';
				reqParams["RQIFSCCode"] = ifsccode;
				reqParams["RQOTP"] = $('#OTP_No_Val').val();
				reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
				reqParams["RQRefNo"] = $('#OTP_Ref').val();
				
				fldjsessionid="";
				
//				busyInd.show();
				customBusyShow();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "NeftSubmit",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : NEFTsubmitresponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	    };
	        
		NEFTsubmitresponse = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.isSuccessful) {
				if(invocationResult.RBL.Response || invocationResult.RBL.Response == ''){
					
					//if(invocationResult.RBL.STATUS.MESSAGE == 'Sucess'){
						$("#contentData").load("Views/TPT/rrtpn06.html", null, function (response, status, xhr) {
    	                 if (status != "error") {}
					
							referenceno = invocationResult.RBL.Response.txnrefno;
    	                 if(invocationResult.RBL.STATUS.CODE == '0'){
    	                	$(".success_msg").show();
							$("#tpn06success").show();
//							busyInd.hide();
							customBusyHide();
							accountList.removeAll();
    	                 }else{
    	                	$(".rsaOOBErr").show();
    	                	$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
    	                	$("#tpn06success").hide();
    	                 }
    	                
    	                 if(referenceno != ''){
	    	                 $(".clsacctno").html(invocationResult.RBL.Response.fromacctno+" "+invocationResult.RBL.Response.customername);
	    	                 $(".clsreferenceno").html(referenceno);
	    	                 $(".clsbenefname").html(invocationResult.RBL.Response.benefname);
	    	                 $(".clsifsccode").html(invocationResult.RBL.Response.ifsccode);
	    	                 $(".clsbenefacctno").html(invocationResult.RBL.Response.toacctno);
	    	                 $(".clsbankdesc").html(invocationResult.RBL.Response.bankname);
	    	                 $(".clstxnamount").html("₹ "+formatAmt(parseFloat(invocationResult.RBL.Response.amttxn)));
	    	                 $(".clstxndesc").html(invocationResult.RBL.Response.txndesc);
    	                 }
    	                ko.applyBindings(self, $(".dynamic-page-content").get(0));
//						busyInd.hide();
    	                customBusyHide();
        			});
					//}
					//else{alert(invocationResult.RBL.STATUS.MESSAGE);}
				}else{
//						busyInd.hide();
						customBusyHide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			} 
//			busyInd.hide();
			customBusyHide();
		};
		//neft end
       
     
		//IMPS fund transfer
            self.callP2A01 = function(){
				if(window.navigator.onLine){
					reqParams = {};
//					busyInd.show();
					customBusyShow();
					
					reqParams["RQLoginUserId"] = LoggedInUserID;
					reqParams["RQDeviceFamily"] = Device_Platform;
					reqParams["RQDeviceFormat"] = Device_Model;
					reqParams["RQOperationId"] = "IMPSP2AFT";
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = CurrentSessionId;
					reqParams["RQTransSeq"] = "01";
					fldjsessionid="";
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetIMPS",
						parameters : [fldjsessionid,reqParams],
						compressResponse : true
					};
					
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrp2a01Success,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}	
            };
             
    rrp2a01Success = function(result){
        $(".h_title").html("Third Party Fund Transfer - IMPS");
        invocationResult = result.invocationResult;
        if(invocationResult.isSuccessful) {
		if(invocationResult.RBL){
            if(invocationResult.RBL.Response){
				if(invocationResult.RBL.STATUS.CODE == '0'){
					var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
					var currentuser = currentreguserid;
					var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
					if(currentuser == responseuser && CurrentSessionId == responsesessionid){
						
						self.fromAccountList.removeAll();
						self.toAccountList.removeAll();
						
						custdtls = invocationResult.RBL.Response.CustDetails;
						fromAccdata = invocationResult.RBL.Response.validAcct.acctdetails;
						toAccdata = invocationResult.RBL.Response.tptacctdetails;
						if(invocationResult.RBL.Response.tptacctdetails){
							nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
							nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
							//nbrtptaccts = invocationResult.RBL.Response.tptacctdetails.length;
							
							//if(nbrtptaccts > 0){
							//if(invocationResult.RBL.Response.tptacctdetails!=undefined){
								if(invocationResult.RBL.Response.tptacctdetails.length!=0){
									$(fromAccdata).each(function(index, obj) {
													$(custdtls).each(function(j, obj1) {
																	 if(obj.acctindex == obj1.acctindex){
																	 displaytxt = $.trim(obj.acctno)+" - "+obj.branchname;
																	 accountValue = obj.acctno+"#"+obj1.custName;
																	 self.fromAccountList.push({ codacctno: obj.acctno, acctType: obj.acctType, acctbalance: "? "+formatAmt(parseFloat(obj.acctbalance)), acctbranch: obj.branchname, namccyshrt: obj.currency, displaytxt:displaytxt, accountValue: accountValue });
																	 }
																	 });
									});
								
									$(toAccdata).each(function(index, obj) {
												  displaytxt = $.trim(obj.benefname);
												  accountValue = obj.benefname;
												  self.toAccountList.push({ codacctno: obj.acctno, narrative: obj.benefname, displaytxt:displaytxt, accountValue: accountValue, ifsccode: obj.ifsccode, acctType: obj.acctType, bankname: obj.bankname, benef_addr: obj.beneaddr});
									});
								}
							
							$("#contentData").load("Views/TPT/rrp2a01.html", null, function (response, status, xhr) {
									if (status != "error") {}
									if(invocationResult.RBL.Response.tptacctdetails!==undefined){
										$("#accexitdata").show(); 
										$("#accExitsMsg").hide(); 
									}					
									else{
										$("#accexitdata").hide();
										$("#accExitsMsg").show();
									}					   
									ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
							});
						}
						else{
							$("#contentData").load("Views/TPT/rrp2a01.html", null, function (response, status, xhr) {
												   
												   $("#accexitdata").hide();
												   $("#accExitsMsg").show();
												   
												   ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
							});
//							busyInd.hide();
							customBusyHide();

						}
					}
					else{
//							busyInd.hide();
							customBusyHide();

							navigator.notification.alert("Your session has timed out!");
							window.location.hash = "#logout";
					}
//					busyInd.hide();
					customBusyHide();

				}
				else{
//					busyInd.hide();
					customBusyHide();

					invalidresponse();
				}
            }else{
            	//to handle NRE/NRO message
//            				busyInd.hide();
            				customBusyHide();

        					if(invocationResult.RBL.STATUS.CODE == '1002'){
        						navigator.notification.alert(invocationResult.RBL.STATUS.MESSAGE)	
        					}
        					else{
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
		}
		}else{
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
//        		 busyInd.hide();
        		 customBusyHide();

			}
        }
//		busyInd.hide();
        customBusyHide();

    };

              
              
            self.showBenefDetails = function(){
				busyInd.hide();
              	selaccno = self.fldToAcctNo();
              	accdata = self.toAccountList();
              	busyInd.hide();
              	if(selaccno != '' && selaccno != null && selaccno != undefined){
              		
					$(accdata).each(function(index, befdet) {
              		
						if(befdet.narrative == selaccno){
              			
							$("#strbenefAccno").html(befdet.codacctno);
							$("#strbenefIFSC").html(befdet.ifsccode);
							$("#strbenefbank").html(befdet.bankname);
                      	
						acctype = befdet.acctType;
                      	$("#strbenefacctype").html(befdet.benef_addr);
                      	
                      	$("#fldBeneAcct").val(befdet.codacctno);
                      	$("#fldIFSCCode").val(befdet.ifsccode);
                      	$("#fldBenefAcctType").val(acctype);
                      	$("#fldNamBenef").val(befdet.benefname);
                    	$("#fldBenefDetail_txt").val($( "#fldBenefDetail option:selected" ).text());
                  
                      	$("#fldBeneId").val(index);
						}
					});
              	} busyInd.hide();
            };
              
              
          self.rrp2a01Submit = function(){
				
				if($("#frmp2a01").valid()){
				$('#confirm_button1').prop('disabled', true);
					var $form = $("#frmp2a01");
					rsaDataArray = $form.serializeArray();
					//busyInd.show();
					//var benefno = $('#fldBenefDetail').val();
					benefno_acct_no($('#strbenefAccno').text());
					benef_IFSC($('#strbenefIFSC').text());
					benef_amnt($('#fldTxnAmount').val());
					benef_name($('#fldBenefDetail').val());
					benefdesc($('#fldRmrk').val());
					beneffrom_accnt_no($('#fldAcctNo').val().split("#")[0]);
					benef_bankname($("#strbenefbank").text());
					sender_name($('#fldAcctNo').val().split("#")[1]);
					benef_addr($('#strbenefacctype').text());
					
					busyInd.hide();
					GetOTP();
					
				} //busyInd.hide();   	
            };      
          
          self.callP2A02 = function(){
          	busyInd.hide();
				$("#contentData").load("Views/TPT/rrp2a02.html", null, function (response, status, xhr) {
					if (status != "error") {}	
						OTP_number = invocationResult1.RBL.Response.otprefno;
						$('#OTP_Ref').val(OTP_number);
						busyInd.hide();
						if(invocationResult1.RBL.Response.otprequired == 'Y'){
							busyInd.hide();
							$('#OTP_No').show();
						}
						else{
							$('#OTP_No').hide();
						}
							fldAcctNo = beneffrom_accnt_no();
							$('.fldAcctNo').html(fldAcctNo);
							$('.ifsccode').html(benef_IFSC());
							$('.narrative').html(benef_name());
							$('.codacctno').html(benefno_acct_no());
							$('.fldtxnamount').html("₹ "+formatAmt(parseFloat(benef_amnt()))); 
							$('.fldtxndesc').html(benefdesc());
							$('.txnamount').html(benef_amnt())
							$('.bankname').html(benef_bankname());
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});
					busyInd.hide();
			};
          
          this.rrp2a02Submit = function(){
		  if(window.navigator.onLine){
			if($("#frmrrp2a").valid()){
					acctno = $('.fldAcctNo').text();
        			//referenceno = invocationResult.RBL.Response.txnrefno;
        			benefname = $('.narrative').text();
        			ifsccode = $('.ifsccode').text();
        			benefacctno = $('.codacctno').text();
        			bankdesc = $('.bankname').text();
        			txnamount = $('.txnamount').text();
        			txndesc = $('.fldtxndesc').text();	

//                    var RQisCCPayment=null;
//                    if ($("#RQisCCPayment").is(":checked")){
//                        RQisCCPayment='Y';
//                    }else{
//                        RQisCCPayment='N';
//                    }
					
				reqParams = {};
				
// 				busyInd.show();
				customBusyShow();
				
            	reqParams["RQLoginUserId"] = LoggedInUserID;
            	reqParams["RQDeviceFamily"] = Device_Platform;
            	reqParams["RQDeviceFormat"] = Device_Model;
            	reqParams["RQOperationId"] = "IMPSP2AFT";
            	reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQSndName"] = sender_name();
            	reqParams["RQFromAcctNo"] = acctno;
				reqParams["RQToAcctNo"] = benefacctno;
				reqParams["RQBeneName"] = benefname;
				reqParams["RQBeneBank"] = bankdesc;
				reqParams["RQBeneAddress"] = benef_addr();
				reqParams["RQIFSCCode"] = ifsccode;
				reqParams["RQDesc"] = txndesc;
				reqParams["RQToAccType"] = "";
				reqParams["RQTxnAmount"] = txnamount;
				reqParams["RQOTP"] = $('#OTP_No_Val').val();
				reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
				reqParams["RQRefNo"] =  $('#OTP_Ref').val();
				reqParams["RQTransSeq"] = "03";
				reqParams["RQisCCPayment"] = 'N';
				
            	fldjsessionid="";
            	// busyInd.show();
            	if(lgnout == false){ window.location.href = "#logout";}
            	
            	//IMPS OTP fail redirect
            	pageImpsOtp=true;
            	
            	var invocationData = {
					adapter : "API_Adapter",
                	procedure : "IMPSSubmit",
                	parameters : [fldjsessionid,reqParams],
					compressResponse : true
            	};
            	
            	WL.Client.invokeProcedure(invocationData, {
            		onSuccess : rrp2a02Success,
            		onFailure : AdapterFail,	    		
            		timeout: timeout
            	});
			}
			}else{ 
				navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
				//IMPS OTP fail redirect
				window.location.hash = "#rrftr01";
		}
		};
	
	
	rrp2a02Success = function(result){
		invocationResult = result.invocationResult;
		
		//IMPS OTP fail redirect
		pageImpsOtp=false;
		
			if(invocationResult.isSuccessful) {
				if(invocationResult.RBL.Response || invocationResult.RBL.Response == ''){
					
					//if(invocationResult.RBL.STATUS.CODE == '0'){
					$("#contentData").load("Views/TPT/rrp2a03.html", null, function (response, status, xhr) {
    	                if (status != "error") {}
					
						referenceno = invocationResult.RBL.Response.txnrefno;
    	                if(invocationResult.RBL.STATUS.CODE == '0'){
    	                	$(".success_msg").show();
							$("#tpn06success").show();
//							busyInd.hide();
							customBusyHide();
							accountList.removeAll();
    	                }else{
    	                	$(".rsaOOBErr").show();
    	                	$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
    	                	$("#tpn06success").hide();
    	                }
    	                
    	                if(referenceno != ''){
	    	                 $(".clsacctno").html(invocationResult.RBL.Response.fromacctno+" "+invocationResult.RBL.Response.customername);
	    	                 $(".clsreferenceno").html(referenceno);
	    	                 $(".clsbenefname").html(invocationResult.RBL.Response.benefname);
	    	                 $(".clsifsccode").html(invocationResult.RBL.Response.ifsccode);
	    	                 $(".clsbenefacctno").html(invocationResult.RBL.Response.toacctno);
	    	                 $(".clsbankdesc").html(invocationResult.RBL.Response.bankname);
	    	                 $(".clstxnamount").html("₹ "+formatAmt(parseFloat(invocationResult.RBL.Response.amttxn)));
	    	                 $(".clstxndesc").html(invocationResult.RBL.Response.txndesc);
    	                }
    	                ko.applyBindings(self, $(".dynamic-page-content").get(0));
//						busyInd.hide();
    	                customBusyHide();
        			});
					//}
					//else{alert(invocationResult.RBL.STATUS.MESSAGE);}
				}else{
//						busyInd.hide();
						customBusyHide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
						//IMPS OTP fail redirect
						window.location.hash = "#rrftr01";
				}
			} 
//			busyInd.hide();
			customBusyHide();
	};
	
	//view NEFT status
	this.callTPV01 = function(){
		 if(window.navigator.onLine){
		busyInd.show();
			reqParams = {};
		    reqParams["RQLoginUserId"] = LoggedInUserID;
			reqParams["RQDeviceFamily"] = Device_Platform;
			reqParams["RQDeviceFormat"] = Device_Model;
			reqParams["RQOperationId"] = "ACCSUMINQ";
			reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQTransSeq"] = "01";
    		
	    	fldjsessionid = '';
	    	
	    	if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
	    			adapter : "API_Adapter",
				    procedure : "GetAccounts",
	        		parameters : [fldjsessionid,reqParams],
					compressResponse : true
	    	};
	    	
	    	WL.Client.invokeProcedure(invocationData, {
	    		onSuccess : self.rrtpv01Success,
	    		onFailure : AdapterFail,	    		
	    		timeout: timeout
	    	});
    	 
      	  $("#contentData").load("Views/TPT/rrtpv01.html", null, function (response, status, xhr) {
			//window.location = "#rrtpv01";
			 ko.applyBindings(self, $(".dynamic-page-content").get(0));
			});	
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
        };
		
        this.rrtpv01Success = function(result){
        	
        	invocationResult = result.invocationResult;
        	if(invocationResult.isSuccessful){
			if(invocationResult.RBL){
				if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE == '0'){
						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
						var currentuser = currentreguserid;
						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
						if(currentuser == responseuser && CurrentSessionId == responsesessionid){
							custdtls = invocationResult.RBL.Response.CustDetails;
							itemdata = invocationResult.RBL.Response.acctdetails;
							nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
						    nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
						    accountListFt.removeAll();
						    var idx = 1; 
						    $(itemdata).each(function(index, obj) {
								 strid = "item"+idx;
								 custnames = "";
								
								 $(custdtls).each(function(j, obj1) {
								
									 if(obj.acctindex == obj1.acctindex){
										
									 }
								 });
								 if(window.location.hash == '#rrtpv01'){
								 displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
								}
								 if(window.location.hash == '#rrasm01'){
									 acctbalance = "₹ " +formatAmt(parseFloat(obj.acctbalance));
								 }else{
									 acctbalance = "₹ " +formatAmt(parseFloat(obj.acctbalance));
								 }
								
								 accountListFt.push({ codacctno: obj.acctno, acctType: obj.acctType, acctbalance: acctbalance, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
								 
								 idx++;
							});
						}
						else{
							busyInd.hide();
							navigator.notification.alert("Your session has timed out!");
							window.location.hash = "#logout";
						}
					}
					else{
						busyInd.hide();
						invalidresponse();
					}
			    }else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			
			}else{
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			}
			 busyInd.hide();
        };
       
		
	self.calltpvView = function(){
		 if(window.navigator.onLine){
		if($("#frmtpv01").valid()){
        	reqParams = {};
	    	reqParams["RQLoginUserId"] = LoggedInUserID;
	    	reqParams["RQDeviceFamily"] = Device_Platform;
	    	reqParams["RQDeviceFormat"] =Device_Model;
	    	reqParams["RQOperationId"] ="VIWNFTTFR";
	    	reqParams["RQAcctNo"] = $("#fldAcctNo").val();
	    	reqParams["RQRefNo"] = $("#fldTxnRefNo").val();
	    	reqParams["RQFromDate"] = "";
	    	reqParams["RQToDate"] = "";
			reqParams["RQStatus"] = "All";
	    	reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQTransSeq"] = "02";
			
	    	fldjsessionid = "";
	    	
	    	busyInd.show();
	    	if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
	    			adapter : "API_Adapter",
	        		procedure : "GetNeftstatus",
	        		parameters : [fldjsessionid,reqParams],
					compressResponse : true
	    	};
	    	
	    	WL.Client.invokeProcedure(invocationData, {
	    		onSuccess : self.rrtpv01ListSuccess,
	    		onFailure : AdapterFail,	    		
	    		timeout: timeout
	    	});
        }
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	};
	
	this.rrtpv01ListSuccess = function(result){
    	busyInd.hide();
    	invocationResult = result.invocationResult;
    	if(invocationResult.isSuccessful) {
    		if(invocationResult.RBL.Response){
    			accStmtData(invocationResult);    			
    			window.location = "#rrtpv02";
				//busyInd.hide();
    		}else{
					busyInd.hide();
					handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
    	}
		//busyInd.hide();
    };

	
self.rrtpv02 = function(){
		busyInd.show();
		
    	invocationResult = accStmtData();
				
		if(invocationResult.RBL.STATUS.CODE == '0'){
			acctcurr="";
				txndata = invocationResult.RBL.Response.txndetails;
				accno = invocationResult.RBL.Response.RQAcctNo;
				
				self.accountStmtTxns.removeAll();
				$(txndata).each(function(index, obj) {
					
					self.accountStmtTxns.push({ dattxn:getFormattedDate(obj.txnenddate), amnttxn: formatAmt(parseFloat(obj.txnamount)), txndesc:obj.txndesc, datvalue:getFormattedDate(obj.txnstartdate), Refno:obj.txnrefno, Benefname: obj.benefname, txnutr:obj.utr, txnstatus:obj.status });
				});
    	if (status != "error") {}	
			if(self.accountStmtTxns().length === 0){
				busyInd.hide();
				$("#ftsatus").hide();
				$("#acnt").hide();
				$("#accExitsMsg").show();
			}else{
				
				busyInd.hide();
				$("#acnt").show();
				$("#divaccno").html(accno);
				$("#ftsatus").show();
				$("#accExitsMsg").hide();
			}
		}
		else{
			busyInd.hide();
			$("#acnt").hide();
			$('#acntnumber').hide();
			$("#ftsatus").hide();
			$(".rsaOOBErr").show();
			$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
		}
		busyInd.hide();
    };
	
	//RTGS start
		self.rrrtg04Page = function(){
		 if(window.navigator.onLine){
			fldjsessionid = '';
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "RTGFNDTFR";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				
//				busyInd.show();
				customBusyShow();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetRtgs",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
				WL.Logger.debug(invocationData, '');
				
				WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrrtg04Response,
						onFailure : AdapterFail,
						timeout: timeout
				});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
		};
		
		rrrtg04Response = function(result){
			invocationResult = result.invocationResult;
				accountListFt.removeAll();
				rdAccountList.removeAll();
			if(invocationResult.isSuccessful) {
			if(invocationResult.RBL){
				if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE == '0'){
						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
						var currentuser = currentreguserid;
						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
						if(currentuser == responseuser && CurrentSessionId == responsesessionid){
							//if(invocationResult.faml.response.rc.returncode == 0){
							//accStmtData(invocationResult.RBL.Response.acctdetails); 
							if(invocationResult.RBL.Response.tptacctdetails!=undefined){
								if(invocationResult.RBL.Response.tptacctdetails.length!=0){
									acctdtls = invocationResult.RBL.Response.validAcct.acctdetails;
									custdtls = invocationResult.RBL.Response.CustDetails;
									var idx = 1;
									$(acctdtls).each(function(index, obj) {
										strid = "item"+idx;
										
										$(custdtls).each(function(j, obj1) {
											custnames="";
											if(obj.acctindex == obj1.acctindex){
												var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
												displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
												accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
											}
										});
										idx++;
									});
									tptacctdtls = invocationResult.RBL.Response.tptacctdetails;
									var idx1 = 1;
									$(tptacctdtls).each(function(index, obj) {
										strid = "item"+idx1;
										rdAccountList.push({ codacctno: obj.acctno+"#"+obj.benefname+"#"+obj.ifsccode+"#"+obj.acctno+"#"+obj.bankname+"#"+obj.beneaddr+"",  displaytxt: obj.benefname, strid:strid });
										idx1++;
									});
								}
							}							
					
							$("#contentData").load("Views/TPT/rrrtg04.html", null, function (response, status, xhr) {
								if (status != "error") {}
								

							   if(invocationResult.RBL.Response.tptacctdetails!==undefined){
								
									$("#nftfundtrns").show();
									$("#accExitsMsg").hide();
									
								if(tptacctdtls.length!=0){
										$('#tptCont').show();
									}else{
										$('#TptNoMsg').show();
									}	
								 }					
								else{
									$("#accExitsMsg").show();
									$("#nftfundtrns").hide();
								 }						
									$('#cutoffmsgBox2').show();
									ko.applyBindings(self, $(".dynamic-page-content").get(0));
							});
//							busyInd.hide();
							customBusyHide();
						}
						else{
//							busyInd.hide();
							customBusyHide();
							navigator.notification.alert("Your session has timed out!");
							window.location.hash = "#logout";
						}
					}
					else{
//						busyInd.hide();
						customBusyHide();
						invalidresponse();
					}
				}else{
//								busyInd.hide();
								customBusyHide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
//        		busyInd.hide();
        		customBusyHide();
			}
			}; 
//			busyInd.hide();
			customBusyHide();
		};
			
		 
		 self.rrrtg04Submit = function(){
				if($("#rrrtg04").valid()){
				$('#confirm_button1').prop('disabled', true);
				var $form = $("#rrrtg04");
	        	rsaDataArray = $form.serializeArray();    	
	        	
				var benefno = $('#fldBenefDetail').val();
				benefno_acct_no(benefno.split("#")[0]);
				benef_IFSC(benefno.split("#")[2]);
				benef_amnt($('#fldTxnAmount').val());
				benef_name(benefno.split("#")[1]);
				benefdesc($('#fldTxnDesc').val());
				beneffrom_accnt_no($('#fldAcctNo').val().split("#")[0]);
				benef_bankname(benefno.split("#")[4]);
				benef_addr(benefno.split("#")[5]);
				sender_name($('#fldAcctNo').val().split("#")[1]);
				busyInd.hide();
				GetOTP();
			};
		};

			self.rrrtg05Page = function(){
				//accstmtdata = accStmtData();
					$("#contentData").load("Views/TPT/rrrtg05.html", null, function (response, status, xhr) {
						if (status != "error") {}	
							
						OTP_number = invocationResult1.RBL.Response.otprefno;
						$('#OTP_Ref').val(OTP_number);
						
						if(invocationResult1.RBL.Response.otprequired == 'Y'){
							$('#OTP_No').show();
						}
						else{
							$('#OTP_No').hide();
						}

							fldAcctNo = beneffrom_accnt_no();
							$('.fldAcctNo').html(fldAcctNo);
							$('.ifsccode').html(benef_IFSC());
							$('.narrative').html(benef_name());
							$('.codacctno').html(benefno_acct_no());
							$('.bankname').html(benef_bankname());
							$('.fldtxnamount').html("₹ "+formatAmt(parseFloat(benef_amnt()))); 
							$('.fldtxndesc').html(benefdesc());
							$('.txnamount').html(benef_amnt())
							
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});
					 busyInd.hide();
			};
		 
		 
		self.rrrtg05Submit = function(){
		 if(window.navigator.onLine){
			if($("#rrrtg05").valid()){
        			acctno = $('.fldAcctNo').text();
        			//referenceno = invocationResult.RBL.Response.txnrefno;
        			benefname = $('.narrative').text();
        			ifsccode = $('.ifsccode').text();
        			benefacctno = $('.codacctno').text();
        			bankdesc = $('.bankname').text();
        			txnamount = $('.txnamount').text();
        			txndesc = $('.fldtxndesc').text();
					benefbankname= $('.bankname').text();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "RTGFNDTFR";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQSndName"] = sender_name();
				reqParams["RQFromAcctNo"] = acctno;
				reqParams["RQToAcctNo"] = benefacctno;
				reqParams["RQBeneName"] = benefname;
				reqParams["RQBeneBank"] = benefbankname;
				reqParams["RQBeneAddress"] = benef_addr();
				reqParams["RQDesc"] = txndesc;
				reqParams["RQTxnAmount"] = txnamount;
				reqParams["RQTransSeq"] = '03';
				reqParams["RQIFSCCode"] = ifsccode;
				reqParams["RQOTP"] = $('#OTP_No_Val').val();
				reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
				reqParams["RQRefNo"] = $('#OTP_Ref').val();
				
				fldjsessionid="";
				
//				busyInd.show();
				customBusyShow();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "RtgsSubmit",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				//WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : Rtgssubmitresponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	    };
	        
		Rtgssubmitresponse = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.isSuccessful) {
				if(invocationResult.RBL.Response || invocationResult.RBL.Response == ''){
					
					$("#contentData").load("Views/TPT/rrrtg06.html", null, function (response, status, xhr) {
    	                 if (status != "error") {}
					
							referenceno = invocationResult.RBL.Response.txnrefno;
    	                 if(invocationResult.RBL.STATUS.CODE == '0'){
    	                	$(".success_msg").show();
							$("#tpn06success").show();
//							busyInd.hide();
							customBusyHide();
							accountList.removeAll();
    	                 }else{
    	                	$(".rsaOOBErr").show();
    	                	$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
    	                	$("#tpn06success").hide();
    	                 }
    	                
    	                 if(referenceno != ''){
	    	                 $(".clsacctno").html(invocationResult.RBL.Response.fromacctno+" "+invocationResult.RBL.Response.customername);
	    	                 $(".clsreferenceno").html(referenceno);
	    	                 $(".clsbenefname").html(invocationResult.RBL.Response.benefname);
	    	                 $(".clsifsccode").html(invocationResult.RBL.Response.ifsccode);
	    	                 $(".clsbenefacctno").html(invocationResult.RBL.Response.toacctno);
	    	                 $(".clsbankdesc").html(invocationResult.RBL.Response.bankname);
	    	                 $(".clstxnamount").html("₹ "+formatAmt(parseFloat(invocationResult.RBL.Response.amttxn)));
	    	                 $(".clstxndesc").html(invocationResult.RBL.Response.txndesc);
    	                 }
    	                ko.applyBindings(self, $(".dynamic-page-content").get(0));
//						busyInd.hide();
    	                customBusyHide();
        			});
					//}
					//else{alert(invocationResult.RBL.STATUS.MESSAGE);}
				}else{
//						busyInd.hide();
						customBusyHide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			} 
//			busyInd.hide();
			customBusyHide();
		};
		//RTGS end
		
		
		//Pay 2 Contact starts
	      
		  self.P2C_Details = function(){
      	if(window.navigator.onLine){
//				busyInd.show();
      			customBusyShow();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "ACCSUMINQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				
				fldjsessionid="";
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetAccounts",
						parameters : [fldjsessionid,reqParams],
						compressResponse : true
				};
				
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : P2C_DetailsSuccess,
					onFailure : AdapterFail,	    		
					timeout: timeout
				});
								
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}
			
		};
		
		
		P2C_DetailsSuccess = function(result){
//		busyInd.hide();
		customBusyHide();
		invocationResult = result.invocationResult;
		//console.log(JSON.stringify(result));
		
			if(invocationResult.isSuccessful) {
				if(invocationResult.RBL){
					if(invocationResult.RBL.Response){
						$(".h_title").html("P2C");
						$(".back .back").show();
						$("#contentData").load("Views/TPT/P2C_Details.html", null, function (response, status, xhr) {
							
							var Alert = new CustomAlert();
							var statuscheck = localStorage.getItem("status");
							  if(statuscheck!='olduser'){	
								  Alert.render(msg);	
							  } 
							
						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
						var currentuser = currentreguserid;
						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
						if(currentuser == responseuser && CurrentSessionId == responsesessionid){
							 custdtls = invocationResult.RBL.Response.CustDetails;
							 itemdata = invocationResult.RBL.Response.acctdetails;
							 nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
							 nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
							
							 totAccount = parseInt(nbrofsavingacc) + parseInt(nbrofcurrentacc);
							 

							 var P2CAccountList=[];
							 $(itemdata).each(function(index, obj) {
								 custnames = "";
								var P2CacctType="";
								var P2Cacctbalance="";
								var P2Cdisplaytxt="";
								
								 $(custdtls).each(function(j, obj1) {
								
									 if(obj.acctindex == obj1.acctindex){
									 
										 if(obj1.custRelationationation == 'MAIN HOLDER OF ACCOUNT' || obj1.custRelationationation == 'SOW' || obj1.custRelationationation == 'GUA' || obj1.custRelationationation == 'JAF' || obj1.custRelationationation == 'JAO' || obj1.custRelationationation == 'NOM' || obj1.custRelationationation == 'TRU' || obj1.custRelationationation == 'JOO' || obj1.custRelationationation == 'JOF' || obj1.custRelationationation == 'AUS' || obj1.custRelationationation == 'GUR' || obj1.custRelationationation == 'THR' || obj1.custRelationationation == 'SOL' || obj1.custRelationationation == 'DEV' || obj1.custRelationationation == 'VAL' || obj1.custRelationationation == 'CUS' || obj1.custRelationationation == 'CON' || obj1.custRelationationation == 'REL'){
											custnames += obj1.custName+"  ";
										 }
									 }
								 });
								 displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;

									P2CAccountList.push({acctType:obj.acctType,acctNumberDetails:displaytxt,acctBal:obj.acctbalance,acctCurr:obj.currency,acctNum:obj.acctno,acctCustNames:custnames});
								checkbalance = parseFloat(obj.acctbalance);

							 });
							 
							 var numberOfAccts=P2CAccountList.length;
							 var P2CAcctlist = $("#P2CAcctNo") ;
							 if(numberOfAccts==1){
								 $("#P2CAcctNo option[value='default']").remove();
								 $('#P2CacctType').html('');
								 $('#P2CacctBal').html('');
								 $('#P2CacctType').html(P2CAccountList[0].acctType);
								 $('#P2CacctCustName').val(P2CAccountList[0].acctCustNames);
								var accBal = P2CAccountList[0].acctBal;
									var accCur = P2CAccountList[0].acctCurr;
									var curr = accCur;
									var acctbalance = null;
									if (curr == "INR" || curr == "inr" || curr == "Inr") {
										acctbalance = "₹ " + formatAmt(parseFloat(accBal));
									} else {
										acctbalance = formatAmt(parseFloat(accBal));
									}
								 $('#P2CacctBal').html(acctbalance);
								 P2CAcctlist.append("<option value="+P2CAccountList[0].acctType+" name="+P2CAccountList[0].acctNum+" accBal="+P2CAccountList[0].acctBal+" accCur="+P2CAccountList[0].acctCurr+" acctCustNames="+btoa(P2CAccountList[0].acctCustNames)+">"+P2CAccountList[0].acctNumberDetails+"</option>");
							 }else{
								 for(i=0;i<numberOfAccts;i++){
									 P2CAcctlist.append("<option value="+P2CAccountList[i].acctType+" name="+P2CAccountList[i].acctNum+" accBal="+P2CAccountList[i].acctBal+" accCur="+P2CAccountList[i].acctCurr+" acctCustNames="+btoa(P2CAccountList[i].acctCustNames)+">"+P2CAccountList[i].acctNumberDetails+"</option>");
							 		}
							 }
						}
						else{
							navigator.notification.alert("Your session has timed out!");
							window.location.hash = "#logout";
						}
						ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
					});
					}else{
							handleError(invocationResult.RBL.Response, invocationResult.RBL);
					}
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
				}
			}
	};
	
	
    self.P2CDetailsSubmit = function(){   
        if(window.navigator.onLine){
            if($("#frmP2CDetails").valid()){
                                      
                                      var REQUEST="INITIATE_CHEQUE_ISSUANCE";
                                      var CUSTOMER_ID= btoa(LoggedInUserID);
                                      var ACC_NO=booksStore(btoa($("#P2CAcctNo").find('option:selected').attr("name")));
                                      var ACC_CUST_NAME=btoa($("#P2CacctCustName").val());
                                      var BENE_NAME=btoa($("#p2cbene_name").val());
                                      var BENE_MOBILENO=booksStore(btoa($("#p2cbene_mobileno").val()));
                                      var SESSIONID=btoa("1111111113");
                                      var APPID=btoa("555557");
                                      
                                      var inputCHQ_AMT = $("#p2cchq_amt").val();
                                      while(inputCHQ_AMT.charAt(0) === '0'){
                                      inputCHQ_AMT = inputCHQ_AMT.substr(1);
                                      }
                
                                        if(inputCHQ_AMT != ''){
                                            
                                      
                                      var CHQ_AMT=booksStore(btoa(inputCHQ_AMT));
                                      var REMARK=btoa($("#p2cremark").val());
                                      var LoggedInCUSTOMER_ID= LoggedInUserID;
                                      
//                                    busyInd.show();
                                      customBusyShow();
                                      if(lgnout == false){ window.location.href = "#logout";}
                                      var invocationData = {
                                   adapter : "API_Adapter",
                                 procedure :"chequeIssuance",
                                parameters : [REQUEST,CUSTOMER_ID,ACC_NO,ACC_CUST_NAME,BENE_NAME,BENE_MOBILENO,SESSIONID,APPID,CHQ_AMT,REMARK,LoggedInCUSTOMER_ID],
                          compressResponse : true
                                      };
                                      
                                      WL.Client.invokeProcedure(invocationData, {
                                                                onSuccess : P2CDetailsSubmitSuccess,
                                                                onFailure : AdapterFail,
                                                                timeout: timeout
                                                                });
       
            }
                else{
                    WL.SimpleDialog.show(
                                         "Alert", "Please enter valid Amount.",
                                         [{text: "OK", handler: function() {
                                           $("#p2cchq_amt").val('');
                                           $("#p2cchq_amt").focus();
                                          
                                          WL.Logger.debug("OK button pressed"); }
                                          }]
                                         );
                    
                }
            }
        }else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        }
        
    };


P2CDetailsSubmitSuccess = function(result){ 
//	busyInd.hide();
	customBusyHide();
	invocationResult = result.invocationResult;
	if(invocationResult.RBL){
		handleErrorP2C(invocationResult.RBL);
	}
	if(invocationResult.FIXML.Body.ChequeIssuence){
		var Status=invocationResult.FIXML.Body.ChequeIssuence.Status;
		if(Status=="Success"){
			
			var ACC_NO=$("#P2CAcctNo").find('option:selected').attr("name");
			var BENE_NAME=$("#p2cbene_name").val();
			var BENE_MOBILENO=$("#p2cbene_mobileno").val();
			var inputCHQ_AMT = $("#p2cchq_amt").val();
			var ACC_CUST_NAME=$("#P2CacctCustName").val();
			while(inputCHQ_AMT.charAt(0) === '0'){
				inputCHQ_AMT = inputCHQ_AMT.substr(1);
			}
			
			
			var REMARK=$("#p2cremark").val();
			localStorage.setItem("ACC_NO",booksStore(btoa(ACC_NO)));
			localStorage.setItem("BENE_NAME",booksStore(BENE_NAME));
			localStorage.setItem("BENE_MOBILENO",booksStore(btoa(BENE_MOBILENO)));
			localStorage.setItem("CHQ_AMT",booksStore(btoa(inputCHQ_AMT)));
			localStorage.setItem("REMARK",booksStore(REMARK));
			localStorage.setItem("ACC_CUST_NAME",booksStore(ACC_CUST_NAME));
			
			
			$("#contentData").load("Views/TPT/P2C_OTP.html", null, function (response, status, xhr) {
				var RefNo=invocationResult.FIXML.Body.ChequeIssuence.RefNo;
				$("#REF_NO").val('');
				$("#REF_NO").val(RefNo);
				activeP2Cpage=true;
				
				P2CgetOTP();
				$("#P2CgetResendOTP").click(function(){
					P2CgetOTP();
				});
				 ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
				 
			});
			
		}
		else{
			WL.SimpleDialog.show(
					"Alert", "Problem Occured! Try again.", 
					[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
					}]
					);
		}
		
	}else if(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail){
		
		var errorMsgP2CDetailsSubmitSuccess=invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail.ErrorDesc;
		
		if(errorMsgP2CDetailsSubmitSuccess=="Amount Exceeds"){
			WL.SimpleDialog.show(
					"Alert", "Amount limit exceeded: Your P2C limit is ₹ 10,000 per day", 
					[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
					}]
					);
		}else if(errorMsgP2CDetailsSubmitSuccess=="Cheque amount should be greater than 1 rupee"){
			WL.SimpleDialog.show(
					"Alert", "Amount should be greater than 1 rupee", 
					[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
					}]
					);
		}else{
			navigator.notification.alert(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail.ErrorDesc);
		}
	}
	else{
		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	}			
}; 	

//P2C OTP	
function P2CgetOTP(){
if(window.navigator.onLine){
	 busyInd.show();
		reqParams = {};
		reqParams["RQLoginUserId"] = LoggedInUserID;
		reqParams["RQDeviceFamily"] = Device_Platform;
		reqParams["RQDeviceFormat"] = Device_Model;
		reqParams["RQOperationId"] = "OTPGENREQ";
		reqParams["RQClientAPIVer"] = RQClientAPIVer;
		reqParams["SessionId"] = CurrentSessionId;
		reqParams["RQTransSeq"] = "01";
		fldjsessionid="";
		
		//busyInd.show();
		if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
			adapter : "API_Adapter",
			procedure : "GetOTP",
			parameters : [fldjsessionid,reqParams],
			compressResponse : true
		};

		WL.Client.invokeProcedure(invocationData, {
			onSuccess : GetOTPResponseP2C,
			onFailure : AdapterFail,
			timeout: timeout
		});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	}
};	

GetOTPResponseP2C = function(result){
	invocationResult1 = result.invocationResult;
		if(invocationResult1.isSuccessful) {
			if(invocationResult1.RBL.Response){	
				busyInd.hide();
				var statusCode=invocationResult1.RBL.STATUS.CODE;
				if(statusCode==0){
					if(invocationResult1.RBL.RequestParams.RQLoginUserId==LoggedInUserID){
						OTP_number = invocationResult1.RBL.Response.otprefno;
						$("#requestId").val("");
						$("#requestId").val(OTP_number);
//						WL.SimpleDialog.show(
//								"Alert", "OTP is sent to your Registered Mobile Number.", 
//								[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
//								}]
//							);
					}else{
						WL.SimpleDialog.show(
								"Alert", "Invalid UserID(CIF)", 
								[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
								}]
							);
						window.location.hash = "#logout";
					}
					
				}
				else{
					WL.SimpleDialog.show(
							"Alert", "OTP Generation Failure! Try again by clicking Resend OTP.", 
							[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
							}]
						);
				}
			}else{
					busyInd.hide();
					handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
			}		
		}
	busyInd.hide();
};


self.p2cOtpSubmit = function(){
	if(window.navigator.onLine){
		if($("#frmP2Cotp").valid()){
		var channelId="MBAUTH";
		var CIF=LoggedInUserID;
		var requestId=$("#requestId").val();
		var otp=booksStore($("#p2cOTPval").val());
		$("#p2cOTPval").val('');

			busyInd.show();
			if(lgnout == false){ window.location.href = "#logout";}
			var invocationData = {
				adapter : "API_Adapter",
				procedure :"OTPValidateService",
				parameters : [channelId,CIF,requestId,otp],
				compressResponse : true
			};
	
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : p2cOtpSubmitSuccess,
				onFailure : AdapterFail,
				timeout: timeout
			});
		}
	}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	}
};


p2cOtpSubmitSuccess = function(result){ 
	busyInd.hide();
	invocationResult = result.invocationResult;
	console.log(JSON.stringify(invocationResult));
	if(invocationResult.RBL){
		handleErrorP2C(invocationResult.RBL);
	}
	if(invocationResult.otpValResponse){
		var Status=invocationResult.otpValResponse.status;
		if(Status=="VALID"){
			issueCheque();	
		}else if(Status=="INVALID_OTP"){
			WL.SimpleDialog.show(
					"Alert", "Invalid OTP", 
					[{text: "OK", handler: function() {
						WL.Logger.debug("OK button pressed"); }
					}]
					);
		}else if(Status=="MAX_ATTEMPT_EXCEEDED"){
			localStorage.setItem("ACC_NO",'');
			localStorage.setItem("BENE_NAME",'');
			localStorage.setItem("BENE_MOBILENO",'');
			localStorage.setItem("CHQ_AMT",'');
			localStorage.setItem("REMARK",'');
			localStorage.setItem("ACC_CUST_NAME",'');
			WL.SimpleDialog.show(
					"Alert", "Maximum Attempts Exceeded! Try again.", 
					[{text: "OK", handler: function() {
						window.location.hash = '#rrftr01';
						WL.Logger.debug("OK button pressed"); }
					}]
					);
		}
		else{
			localStorage.setItem("ACC_NO",'');
			localStorage.setItem("BENE_NAME",'');
			localStorage.setItem("BENE_MOBILENO",'');
			localStorage.setItem("CHQ_AMT",'');
			localStorage.setItem("REMARK",'');
			localStorage.setItem("ACC_CUST_NAME",'');

			WL.SimpleDialog.show(
					"Alert", "Problem Occured! Try again.", 
					[{text: "OK", handler: function() {
						window.location.hash = '#rrftr01';
						WL.Logger.debug("OK button pressed"); }
					}]
					);
		}
	}
	else{
		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	}			
}; 	

function issueCheque(){
	if(window.navigator.onLine){
		activeP2Cpage=false;		
		var REQUEST="ISSUE_CHEQUE";
		var CUSTOMER_ID= btoa(LoggedInUserID);
		var ACC_NO=localStorage.getItem("ACC_NO");
		var ACC_CUST_NAME=btoa(booksStore(localStorage.getItem("ACC_CUST_NAME")));
		var BENE_NAME=btoa(booksStore(localStorage.getItem("BENE_NAME")));
		var BENE_MOBILENO=localStorage.getItem("BENE_MOBILENO");
		var SESSIONID=btoa("1111111113");
		var APPID=btoa("555557");
		var REF_NO=booksStore(btoa($("#REF_NO").val()));
		var CHQ_AMT=localStorage.getItem("CHQ_AMT");
		var REMARK=btoa(booksStore(localStorage.getItem("REMARK")));
		var LoggedInCUSTOMER_ID= LoggedInUserID;
		
			
		busyInd.show();
			if(lgnout == false){ window.location.href = "#logout";}
			var invocationData = {
				adapter : "API_Adapter",
				procedure :"issueCheque",
				parameters : [REQUEST,CUSTOMER_ID,ACC_NO,ACC_CUST_NAME,BENE_NAME,BENE_MOBILENO,SESSIONID,APPID,REF_NO,CHQ_AMT,REMARK,LoggedInCUSTOMER_ID],
				compressResponse : true
			};
	
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : issueChequeSucess,
				onFailure : AdapterFail,
				timeout: timeout
			});
	}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	}
}


issueChequeSucess = function(result){ 
	busyInd.hide();
	invocationResult = result.invocationResult;
	
	localStorage.setItem("ACC_NO",'');
	localStorage.setItem("BENE_NAME",'');
	localStorage.setItem("BENE_MOBILENO",'');
	localStorage.setItem("CHQ_AMT",'');
	localStorage.setItem("REMARK",'');
	localStorage.setItem("ACC_CUST_NAME",'');
	
	if(invocationResult.RBL){
		handleErrorP2C(invocationResult.RBL);
	}
	if(invocationResult.FIXML.Body.IssuanceResponse){
		var Status=invocationResult.FIXML.Body.IssuanceResponse.Status;
		if(Status=="Success"){
			var VoucherId=invocationResult.FIXML.Body.IssuanceResponse.VoucherId;
//			var Message=invocationResult.FIXML.Body.IssuanceResponse.Message;
			var Message="Transaction Successful !";
			WL.SimpleDialog.show(
					"Alert", Message, 
					[{text: "OK", handler: function() {
						window.location.hash = '#rrftr01';
						WL.Logger.debug("OK button pressed"); }
					}]
					);
			
		}
		else{
			WL.SimpleDialog.show(
					"Alert", "Problem Occured! Try again.", 
					[{text: "OK", handler: function() {
						window.location.hash = '#rrftr01';
						WL.Logger.debug("OK button pressed"); }
					}]
					);
		}
		
	}else if(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail){
		navigator.notification.alert(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail.ErrorDesc);
	}
	else{
		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	}			
}; 	
//P2C ends
		
		

//P2C transaction history	
self.P2C_History = function(){
	if(window.navigator.onLine){
		busyInd.show();
		reqParams = {};
		reqParams["RQLoginUserId"] = LoggedInUserID;
		reqParams["RQDeviceFamily"] = Device_Platform;
		reqParams["RQDeviceFormat"] = Device_Model;
		reqParams["RQOperationId"] = "ACCSUMINQ";
		reqParams["RQClientAPIVer"] = RQClientAPIVer;
		reqParams["SessionId"] = CurrentSessionId;
		reqParams["RQTransSeq"] = "01";
		
		fldjsessionid="";
		
		if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
				adapter : "API_Adapter",
				procedure : "GetAccounts",
				parameters : [fldjsessionid,reqParams],
				compressResponse : true
		};
		
		WL.Client.invokeProcedure(invocationData, {
			onSuccess : P2C_HistoryResponse,
			onFailure : AdapterFail,	    		
			timeout: timeout
		});
		
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
};	


P2C_HistoryResponse = function(result){
busyInd.hide();
invocationResult = result.invocationResult;
	if(invocationResult.isSuccessful) {
		if(invocationResult.RBL){
			if(invocationResult.RBL.Response){
				$(".h_title").html("P2C Transaction History");
				$(".back .back").show();
				$("#contentData").load("Views/TPT/P2C_History.html", null, function (response, status, xhr) {
					
					localStorage.setItem("statusHistory","olduser");
					
					 ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
				
				var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
				var currentuser = currentreguserid;
				var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
				if(currentuser == responseuser && CurrentSessionId == responsesessionid){
					 custdtls = invocationResult.RBL.Response.CustDetails;
					 itemdata = invocationResult.RBL.Response.acctdetails;
					 nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
					 nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
					
					 totAccount = parseInt(nbrofsavingacc) + parseInt(nbrofcurrentacc);

					 var P2CAccountList=[];
					 $(itemdata).each(function(index, obj) {
						 displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;

							P2CAccountList.push({acctNumberDetails:displaytxt,acctNum:obj.acctno});	

					 });
					 
					 var numberOfAccts=P2CAccountList.length;
					 var P2CAcctlist = $("#P2CAcctNoHistory") ;
					 for(i=0;i<numberOfAccts;i++){
						 P2CAcctlist.append("<option value="+P2CAccountList[i].acctNum+" name="+P2CAccountList[i].acctNum+">"+P2CAccountList[i].acctNumberDetails+"</option>");
					 	}
					 
				}
				else{
					navigator.notification.alert("Your session has timed out!");
					window.location.hash = "#logout";
				}
			});
			}else{
					handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
		}else{
			navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
		}
	}
};

self.P2CHistorySubmit = function(){
			if(window.navigator.onLine){
				if($("#frmP2CHtry").valid()){
				var REQUEST="CHEQUE_HISTORY";
				var CUSTOMER_ID= btoa(LoggedInUserID);
				var ACC_NO=booksStore(btoa($("#P2CAcctNoHistory").find('option:selected').val()));
				var APPID=btoa("555557");
				var VOUCHER_ID=booksStore(btoa($("#p2cVoucherNumber").val()));
				var DATA="VOUCHERID";
				
					busyInd.show();
					if(lgnout == false){ window.location.href = "#logout";}
					var invocationData = {
						adapter : "API_Adapter",
						procedure :"chequeIssuanceHistory",
						parameters : [REQUEST,CUSTOMER_ID,ACC_NO,APPID,VOUCHER_ID,DATA],
						compressResponse : true
					};
			
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : P2CHistorySubmitSucess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		self.P2CHistoryAll = function(){
			if(window.navigator.onLine){
				var REQUEST="CHEQUE_HISTORY";
				var CUSTOMER_ID= btoa(LoggedInUserID);
				var ACC_NO=booksStore(btoa($("#P2CAcctNoHistory").find('option:selected').val()));
				var APPID=btoa("555557");
				var VOUCHER_ID="";
				var	DATA="HISTORY";
				var LoggedInCUSTOMER_ID= LoggedInUserID;
				
					busyInd.show();
					if(lgnout == false){ window.location.href = "#logout";}
					var invocationData = {
						adapter : "API_Adapter",
						procedure :"chequeIssuanceHistory",
						parameters : [REQUEST,CUSTOMER_ID,ACC_NO,APPID,VOUCHER_ID,DATA,LoggedInCUSTOMER_ID],
						compressResponse : true
					};
			
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : P2CHistorySubmitSucess,
						onFailure : AdapterFail,
						timeout: timeout
					});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		
		P2CHistorySubmitSucess = function(result){ 
			busyInd.hide();
			invocationResult = result.invocationResult;
			if(invocationResult.RBL){
				handleErrorP2C(invocationResult.RBL);
			}
			if(invocationResult.FIXML.Body.historyResponse){			
				var historyCount=parseInt(invocationResult.FIXML.Body.historyResponse.historyCount);
				if(historyCount>0){
					$("#contentData").load("Views/TPT/P2C_HistoryDetails.html", null, function (response, status, xhr) {
					historyDataList = invocationResult.FIXML.Body.historyResponse.history;
					 var P2ChistoryDataList=[];
					 $(historyDataList).each(function(index, obj) {
						 var accountNO=obj.accountNO;
						 var chequeIssuedDate=obj.chequeIssuedDate;
						 var RefNo=obj.RefNo;
						 var Amount=obj.Amount;
						 var beneficiaryName=obj.beneficiaryName;
						 var Remark=obj.Remark;
						 var voucherId=obj.voucherId;
						 var STATUS=obj.STATUS;
						 var p2cMoreInfo=null;
						 var p2cStatus=null;
							if(STATUS=="Pending"){
								p2cMoreInfo="Payment initiated and payee is yet to claim the amount";
								p2cStatus="Pending";
							}else if(STATUS=="STOP PAYMENT"){
								p2cMoreInfo="Payment stopped by you for the payee";	
								p2cStatus="Payment Stopped";
							}else if(STATUS=="success"){
								p2cMoreInfo="Amount claimed by the payee";	
								p2cStatus="Transaction Successful";
							}else if(STATUS=="EXPIRED"){
								p2cMoreInfo="Payee failed to claim the amount in 3 days";	
								p2cStatus="Transaction Expired";
							}else if(STATUS=="FAIL"){
								p2cMoreInfo="Failure due to server connection error";	
								p2cStatus="Transaction Failed";
							}else if(STATUS=="BLOCKED"){
								p2cMoreInfo="Incorrect Passcode entered by the payee thrice";	
								p2cStatus="Transaction Blocked";
							}else{
								p2cMoreInfo="Not Avilable";
								p2cStatus="Not Avilable";
							}
							

						 P2ChistoryDataList.push({accountNO:obj.accountNO,chequeIssuedDate:obj.chequeIssuedDate,RefNo:obj.RefNo,Amount:obj.Amount,beneficiaryName:obj.beneficiaryName,Remark:obj.Remark,voucherId:obj.voucherId,STATUS:p2cStatus,moreInfo:p2cMoreInfo});	

					 });
	
					 $("#P2CHistoryAccNumDetails").html('');
					 var list = $("#P2CHistoryAccNumDetails") ;
for(i=0;i<historyCount;i++){
	
list.append("<div class='topBox' style='margin-top: 2%'>"+
						"<div class='bgff5354'> <span class='info_row_left'>Date</span> <span class='info_row_right f18'>"+formatTranHisDate(P2ChistoryDataList[i].chequeIssuedDate)+"</span>"+
							"<div class='clearfix'></div>"+
						"</div>"+
			    "</div>" +
		"<div class='summblock listItems'>" +
		"<div class='info_row'> <span class='info_row_left'>Account Number</span> <span class='info_row_right f18'>"+P2ChistoryDataList[i].accountNO+"</span>" +
			"<div class='clearfix'></div>" +
		"</div>" +		
 "</div>" +
		"<div class='summblock' style='color: #fff;'>"+

			"<div class='summblock listItems'>"+
					
	
				
				"<div class='info_row backColorp2c'> <span class='info_row_left'>Reference Number</span> <span class='info_row_right f18'>"+P2ChistoryDataList[i].RefNo+"</span>"+
					"<div class='clearfix'></div>"+
				"</div>"+
				
				"<div class='info_row backColorp2c'> <span class='info_row_left'>Amount</span> <span class='info_row_right f18'>"+P2ChistoryDataList[i].Amount+"</span>"+
					"<div class='clearfix'></div>"+
				"</div>"+
				
				"<div class='info_row backColorp2c'> <span class='info_row_left'>Payee Name</span> <span class='info_row_right'>"+P2ChistoryDataList[i].beneficiaryName+"</span>"+
					"<div class='clearfix'></div>"+
				"</div>"+
				
				"<div class='info_row backColorp2c'> <span class='info_row_left'>Description</span> <span class='info_row_right'>"+P2ChistoryDataList[i].Remark+"</span>"+
					"<div class='clearfix'></div>"+
				"</div>"+
				
				"<div class='info_row backColorp2c'> <span class='info_row_left'>Voucher Number</span> <span class='info_row_right'>"+P2ChistoryDataList[i].voucherId+"</span>"+
					"<div class='clearfix'></div>"+
				"</div>"+
					
				"<div class='info_row backColorp2c'> <span class='info_row_left'>Status</span> <span class='info_row_right'>"+P2ChistoryDataList[i].STATUS+"&nbsp;<span id='img_info' onclick='viewMoreInfo("+i+")'><img src='img/p2c_info.png'></img></span></span>"+
				"<div class='clearfix'></div>"+
				"<div class='info_row backColorp2c' style='display: none; text-align:center; color:#2E367A;' id='statusdiv"+i+"'>"+P2ChistoryDataList[i].moreInfo+"</div>"+
					"<div class='clearfix'></div>"+
				"</div>"+
					
			"</div>"+
		"</div>");
if(P2ChistoryDataList[i].STATUS=="Pending"){
	list.append("<div class='info_row backColorp2c'> " +
			"<span class='info_row_left'><button class='buttonP2C' type='button' onclick='resendPasscodeP2C("+P2ChistoryDataList[i].accountNO+","+P2ChistoryDataList[i].RefNo+")'>Resend Passcode</button></span>" +
			"<span class='info_row_right'><button class='buttonP2C' type='button' onclick='stopPaymentP2C("+P2ChistoryDataList[i].accountNO+","+P2ChistoryDataList[i].RefNo+")'>Stop Payment</button></span>" +
			"<div class='clearfix'></div>" +
			"</div>");
}

}
					ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});	
				}
				else if(historyCount==0){
					WL.SimpleDialog.show(
							"Alert", "No Records Found !", 
							[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
							}]
							);
				}
				
				
			}else if(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail){
				navigator.notification.alert(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail.ErrorDesc);
			}
			else{
	 		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
			}			
		}; 	
		
		

		// ----------------Add Benificary-----------------------
			
	        self.PayeeWithinBankSubmit = function(){

				if($("#frmPayeeWithinBank").valid()){

					//Here we will check for the account no is valid or not
					if (lgnout == false) {
	    				window.location.href = "#logout";
	    			}

					//Disable the submit button to resolve the issue of Null data.
        			$("#Within_Bank_Submit").prop('disabled', true);


	    			//busyInd.show();
        			customBusyShow();
	    			var invocationData = {
	    					adapter : "API_Adapter",
	    					procedure : "checkForInternalAccount",
	    					parameters : [ $('#Payee_No').val(),LoggedInUserID ],
	    					compressResponse : true
	    			};
	    			jsonAsString = JSON.stringify(invocationData);
	    			WL.Client.invokeProcedure(invocationData, {
	    				onSuccess : checkForInternalAccountSucess,
	    				onFailure : AdapterFail,
	    				timeout : timeout
	    			});
					
				} 
	        };
	        
	        
	        checkForInternalAccountSucess = function(result) {
	        	
	        	
	        	invocationResult = result.invocationResult;
	        		
	        	//console.log(JSON.stringify(invocationResult));
	        	
	        	if (invocationResult.RBL) {
	        		//Enable the submit button to resolve the issue of Null data.
        			$("#Within_Bank_Submit").prop('disabled', false);
	                handleErrorP2C(invocationResult.RBL);
	            }
	        	
	            if (invocationResult.FIXML.Body.AcctInqResponse) {
	            	
	            	if(invocationResult.FIXML.Body.AcctInqResponse.AcctInqRs)
	            		{
	            		if(invocationResult.FIXML.Body.AcctInqResponse.AcctInqRs.CustId)
	            			{
	            				if(invocationResult.FIXML.Body.AcctInqResponse.AcctInqRs.CustId.PersonName)
	            					{
	            					if(invocationResult.FIXML.Body.AcctInqResponse.AcctInqRs.CustId.PersonName.Name)
	            						{
	            						
	            						
	            							//save all information
	            							PayeeNo = $('#Payee_No').val();
	            							PayeeName = $.trim(invocationResult.FIXML.Body.AcctInqResponse.AcctInqRs.CustId.PersonName.Name);
	            							PayeeNickName = $.trim($('#Payee_Nick_Name').val());
	            							
	            							//show confirmatoion page
	            							tptViewModelCopy.previousPage = "Payee_Within";
	            							
	            							 
	            							 
	            							$("#contentData").load("Views/Payee/WithinBankDetails.html", null, function (response, status, xhr) {
	            								if (status != "error") {}	
	            							
	            										$('.AccNo').html(PayeeNo);
	            										$('.PName').html(PayeeName);
	            										$('.PNickName').html(PayeeNickName);

	            										ko.applyBindings(self, $(".dynamic-page-content").get(0));
	            								});
	            							
	            							//busyInd.hide();
	            							customBusyHide();
	            							return;
	            							
	            							
	            						}
	            					
	            					}
	            				
	            			}
	            		
	            		}
	            	
	            	
	            }
	            
	            //busyInd.hide();
	            customBusyHide();
	            
	          //Enable the submit button to resolve the issue of Null data.
    			$("#Within_Bank_Submit").prop('disabled', false);

	            if (invocationResult.FIXML.Body.Error)
	            	{
	            	   if(invocationResult.FIXML.Body.Error.FIBusinessException)
	            		   {
	            		   
	                       alert(invocationResult.FIXML.Body.Error.FIBusinessException.ErrorDetail.ErrorDesc);
	                       
	                       return;
	            		   }
	            	   else if(invocationResult.FIXML.Body.Error.FISystemException)
	            		   {
	            		   
	                       navigator.notification.alert(invocationResult.FIXML.Body.Error.FISystemException.ErrorDetail.ErrorDesc);
	                       return;
	            		   }
	            	}
	            
	            	
	            	navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	                       	
	        	
	        };     
	        
	        
	       self.setPage = function()
	       {
	    	   	PayeeNo = $('#Payee_No').val();
	    	   	RePayeeNo = $('#RePayee_No').val();
	   			PayeeName = $.trim($('#Payee_Name').val());
	   			PayeeNickName = $.trim($('#Payee_Nick_Name').val());
	   			IFSCCode = $('#Payee_IFSC').val().toUpperCase();
	   			//alert(PayeeNo);
	       };
	        self.openPage = function(page)
	        {
	        	//alert(page);
	        	
	        	if(page == '#PayeeWithinBank')
	        		{
	        		$('#Payee_No').val(PayeeNo);
	    			$('#Payee_Name').val(PayeeName);
	    			$('#Payee_Nick_Name').val(PayeeNickName);
	        		}
	        	else if(page == '#Payee_NEFT_IMPS' || page == '#Payee_RTGS')
	        		{
	        			$('#Payee_No').val(PayeeNo);
	        			$('#RePayee_No').val(RePayeeNo);
	        			$('#Payee_Name').val(PayeeName);
	        			$('#Payee_Nick_Name').val(PayeeNickName);
	        			if(selectedIFSCIndex != -1)
	    				{
	    					$('#Payee_IFSC').val(IFSCList[selectedIFSCIndex].IFSCCODE);
	    				}
	    			else 
	    				{
	    					$('#Payee_IFSC').val(IFSCCode);
	    				}
	  			
	        		}      		
	        	
	        };
	        
	        // -----------------IFSC Verification for NEFT &
			// IMPS---------------------------
	        
	        self.Payee_NEFT_IMPSSubmit = function(){
	        	if(window.navigator.onLine){
	        		if($("#frmPayee_NEFT_IMPS").valid()){        			
	        			
	        			//Disable the submit button to resolve the issue of Null data.
	        			$("#NEFTIMPSSubmit").prop('disabled', true);

	        			PayeeNo = $('#Payee_No').val();
	        			PayeeName = $.trim($('#Payee_Name').val());
	        			PayeeNickName = $.trim($('#Payee_Nick_Name').val());
//	        			IFSCCode = $('#Payee_IFSC').val();
	        			IFSCCode = $('#Payee_IFSC').val().toUpperCase();

	        			reqParams = [];
	        			var reqParams = {};
	        			var IFSCDetails = [];
	        			reqParams.IFSCDetails = IFSCDetails;
	        			var IFSCDetail = {
	        					"OperationId": "NEFTREQ",
	        					"SearchQuery":"IFSC",
	        					"BeneIFSCCode": IFSCCode
	        			}

	        			reqParams.IFSCDetails.push(IFSCDetail);
	        			

	        			if (lgnout == false) {
	        				window.location.href = "#logout";
	        			}
	        			//busyInd.show();
	        			customBusyShow();
	        			var invocationData = {
	        					adapter : "API_Adapter",
	        					procedure : "ifscDetails",
	        					parameters : [ booksStore(LoggedInUserID), booksStore(JSON.stringify(reqParams))],
	        					compressResponse : true
	        			};
	        			jsonAsString = JSON.stringify(invocationData);
	        			WL.Client.invokeProcedure(invocationData, {
	        				onSuccess : getIFSCDetailsSuccess,
	        				onFailure : AdapterFail,
	        				timeout : timeout
	        			});
	        		} 
	        	}
	        	else{
	        		navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
	        	}   	
	        };    
	        
	        getIFSCDetailsSuccess = function(result) {
	        	self.setPage();
	        	invocationResult = result.invocationResult;
	        	if(invocationResult.isSuccessful)
	        	{
	        		if(invocationResult.ResponseMessageInfo)
	    			{
	        			if(invocationResult.ResponseMessageInfo.Message)
	    				{
	    					//busyInd.hide();
	        				customBusyHide();
	            			navigator.notification.alert("Invalid IFSC Code.");
	            			//Enable the submit button to resolve the issue of Null data.
	            			$("#NEFTIMPSSubmit").prop('disabled', false);

	            			return;
	    				}
	        			
	        			var ifscdata = invocationResult.ResponseMessageInfo;
	        			if(invocationResult.ResponseMessageInfo.length!=0){
	        			
	        		$("#contentData").load("Views/Payee/NEFT_IMPS_Details.html", null, function (response, status, xhr) {
	        			if (status != "error") {}	

	        			$(ifscdata).each(function(index, obj) {
	        				tptViewModelCopy.previousPage = "Payee_NEFT";
	        				BankName = $.trim(obj.BankName);
		        			BranchName = $.trim(obj.BranchhName);
		        			City = $.trim(obj.City);       				 
	       				 
	            			$('.AccNo').html(PayeeNo);
	            			$('.PName').html(PayeeName);
	            			$('.PNickName').html(PayeeNickName);
	            			$('.IFSCCode').html(IFSCCode);
	            			$('.BankName').html(BankName);
	            			$('.BranchName').html(BranchName);
	            			$('.City').html(City);

	            			//busyInd.hide();
	            			customBusyHide();
	            			ko.applyBindings(self, $(".dynamic-page-content").get(0));
	        			});
	        			
	        		});
	        	}
	        			else{
	            			//busyInd.hide();
	        				customBusyHide();
	            			navigator.notification.alert("Invalid IFSC Code.");
	            			//Enable the submit button to resolve the issue of Null data.
	            			$("#NEFTIMPSSubmit").prop('disabled', false);
	            			}
	    			}
	        		
	        		else{
	        			//busyInd.hide();
	        			customBusyHide();
	        			navigator.notification.alert("Invalid IFSC Code.");
	        			//Enable the submit button to resolve the issue of Null data.
	        			$("#NEFTIMPSSubmit").prop('disabled', false);
	        			}
	        	}
	        	else{
	        		//busyInd.hide();
	        		customBusyHide();
	        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	        		//Enable the submit button to resolve the issue of Null data.
	        		$("#NEFTIMPSSubmit").prop('disabled', false);
	        	}
	        	
	        	//busyInd.hide();
	        	customBusyHide();
	        };
	        
	        
	        // ----------------------IFSC Verification for
			// RTGS---------------------------------------
	        
	        
	        self.Payee_RTGSSubmit = function(){
	        	if(window.navigator.onLine){
	        	if($("#frmPayee_RTGS").valid()){
	        		selectedIFSCIndex = -1;
	        		
	        		//Disable the submit button to resolve the issue of Null data.
        			$("#PayeeRTGSSubmit").prop('disabled', true);
        			//alert(encodeURIComponent($.trim($('#Payee_Name').val())));
	        		PayeeNo = $('#Payee_No').val();
	        		PayeeName = $.trim($('#Payee_Name').val());
	        		PayeeNickName = $.trim($('#Payee_Nick_Name').val());
	        		IFSCCode = $('#Payee_IFSC').val().toUpperCase();

	        		var reqParams = {};
	        		var IFSCDetails = [];        		

	        		reqParams.IFSCDetails = IFSCDetails;
	        		var IFSCDetail = {
	        				"OperationId": "RTGSREQ",
	    					"SearchQuery":"IFSC",
	        				"BeneIFSCCode": IFSCCode
	        		}

	        		reqParams.IFSCDetails.push(IFSCDetail);

	        		if (lgnout == false) {
	        			window.location.href = "#logout";
	        		}

	        		//busyInd.show();
	        		customBusyShow();
	        		
	        		var invocationData = {
	        				adapter : "API_Adapter",
	        				procedure : "ifscDetails",
	        				parameters : [ booksStore(LoggedInUserID), booksStore(JSON.stringify(reqParams))],
	        				compressResponse : true
	        		};
	        		
	  
	        		WL.Client.invokeProcedure(invocationData, {
	        			onSuccess : getIFSC_RTGS_DetailsSuccess,
	        			onFailure : AdapterFail,
	        			timeout : timeout
	        		});
	        	}
	        	}
	        	else{
	        		navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	        	}
	        	// busyInd.hide();
	        };

	        getIFSC_RTGS_DetailsSuccess = function(result) {
	        	
	        	self.setPage();
	        	invocationResult = result.invocationResult;
	        	if(invocationResult.isSuccessful)
	        	{
	        		if(invocationResult.ResponseMessageInfo)
	    			{
	        			if(invocationResult.ResponseMessageInfo.Message)
	    				{
	    					//busyInd.hide();
	    					customBusyHide();
	            			navigator.notification.alert("Invalid IFSC Code.");
	            			//Enable the submit button to resolve the issue of Null data.
	            			$("#PayeeRTGSSubmit").prop('disabled', false);
	            			return;
	    				}
	        			
	        			var ifscdata = invocationResult.ResponseMessageInfo;
	        			if(invocationResult.ResponseMessageInfo.length!=0){
	        			
	        		$("#contentData").load("Views/Payee/RTGS_Details.html", null, function (response, status, xhr) {
	        			if (status != "error") {}	

	        			$(ifscdata).each(function(index, obj) {
	        				tptViewModelCopy.previousPage = "Payee_RTGS";
	        				BankName = $.trim(obj.BankName);
		        			BranchName = $.trim(obj.BranchhName);
		        			City = $.trim(obj.City);       				 
	       				 
	            			$('.AccNo').html(PayeeNo);
	            			$('.PName').html(PayeeName);
	            			$('.PNickName').html(PayeeNickName);
	            			$('.IFSCCode').html(IFSCCode);
	            			$('.BankName').html(BankName);
	            			$('.BranchName').html(BranchName);
	            			$('.City').html(City);

	            			//busyInd.hide();
	            			customBusyHide();
	            			ko.applyBindings(self, $(".dynamic-page-content").get(0));
	        			});
	        			
	        		});
	        	}
	        			else{
	            			//busyInd.hide();
	        				customBusyHide();
	            			navigator.notification.alert("Invalid IFSC Code.");
	            			//Enable the submit button to resolve the issue of Null data.
	            			$("#PayeeRTGSSubmit").prop('disabled', false);
	            			}
	    			}
	        		
	        		else{
	        			//busyInd.hide();
	        			customBusyHide();
	        			navigator.notification.alert("Invalid IFSC Code.");
	        			//Enable the submit button to resolve the issue of Null data.
            			$("#PayeeRTGSSubmit").prop('disabled', false);
	        			}
	        	}
	        	else{
	        		//busyInd.hide();
	        		customBusyHide();
	        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	        		//Enable the submit button to resolve the issue of Null data.
        			$("#PayeeRTGSSubmit").prop('disabled', false);
	        	}
	        	
	        	//busyInd.hide();
	        	customBusyHide();
	        };
	      
	        
	        //------Search IFSC
	        
	        self.IFSC_SearchSubmit = function(){
	        	if(window.navigator.onLine){
	        	if($("#frmIFSC_Search").valid()){
	        		
	        		//Disable the submit button to resolve the issue of Null data.
        			$("#IFSCSearchSubmit").prop('disabled', true);


	        		$("#Bank_Name").blur();
	        		$("#Branch_Name").blur();
	        		$("#City_Name").blur();
	        		
	        		var BankNameSearch;
	        		var BranchNameSearch;
	        		var CitySearch;
	        		var operationID='';
	        		
	        		BankNameSearch = $.trim($('#Bank_Name').val().toUpperCase());
	        		BranchNameSearch = $.trim($('#Branch_Name').val().toUpperCase());
	        		CitySearch = $.trim($('#City_Name').val().toUpperCase());
	        		
	        		switch(BeneficieryOptState)
		        	{

		        	case BeneficieryOpt.RTGSAddition :
		    		{
		        		//$(".h_title").html("Add Payee RTGS");
		        	   operationID = "RTGSREQ";
		    		   break;
		    		}
		        	case BeneficieryOpt.EXTERNALAddition :
		    		{
		        		//$(".h_title").html("Add Payee NEFT/IMPS");
		        		operationID = "NEFTREQ";
		    		   break;
		    		}
		        	
		        	default: {
		               // alert("case default");
		            }
		        	
		        	}
	        		
	        		var reqParams = {};
	        		var IFSCDetails = [];        		

	        		reqParams.IFSCDetails = IFSCDetails;
	        		var IFSCDetail = {
	        				"OperationId": operationID,
	    					"SearchQuery":"other",
	        				"BeneBankName": BankNameSearch,
	        				"BeneCITY": CitySearch,
	        				"BeneBRANCHNAME": BranchNameSearch,
	        		}
	        		reqParams.IFSCDetails.push(IFSCDetail);

	        		if (lgnout == false) {
	        			window.location.href = "#logout";
	        		}

	        		//busyInd.show();
	        		customBusyShow();
	        		var invocationData = {
	        				adapter : "API_Adapter",
	        				procedure : "ifscDetails",
	        				parameters : [ booksStore(LoggedInUserID), booksStore(JSON.stringify(reqParams))],
	        				compressResponse : true
	        		};
	        		
	  
	        		WL.Client.invokeProcedure(invocationData, {
	        			onSuccess : IFSC_SearchSubmitSuccess,
	        			onFailure : AdapterFail,
	        			timeout : timeout
	        		});
	        	}
	        	}
	        	else{
	        		navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	        	}
	        	// busyInd.hide();
	        };
	        
	        IFSC_SearchSubmitSuccess = function(result) 
	        {
	        		
				//busyInd.hide();
				customBusyHide();
	        	invocationResult = result.invocationResult;
	        	if(invocationResult.isSuccessful){
	        		if(invocationResult.ResponseMessageInfo)
	    			{
	        			
	        			IFSCList.length = 0;
	        			var ifscdata = invocationResult.ResponseMessageInfo;
	        			
	        			if(invocationResult.ResponseMessageInfo.length!=0){
	     
	        				if(invocationResult.ResponseMessageInfo.Message)
	        				{
	        					//busyInd.hide();
	        					customBusyHide();
	                			navigator.notification.alert(invocationResult.ResponseMessageInfo.Message);
	                			//Enable the submit button to resolve the issue of Null data.
	                			$("#IFSCSearchSubmit").prop('disabled', false);
	        				}
	        				else 
	        				{
	        					$(ifscdata).each(function(index, obj) {
	    							
	    							var City = $.trim(obj.City);
	    							var BranchhName = $.trim(obj.BranchhName);
	    							var BankName = $.trim(obj.BankName);
	    							var IFSCCODE = obj.IFSCCODE;
	    							var Operration = obj.Operration;
	    							
	    							
	    							IFSCList.push({ City: obj.City,
	    						BranchhName : obj.BranchhName,
	    						IFSCCODE:	obj.IFSCCODE, 
	    							BankName : obj.BankName,
	    							Operration: obj.Operration});
	    						});
	            				
	        					//Enable the submit button to resolve the issue of Null data.
	                			$("#IFSCSearchSubmit").prop('disabled', false);
	                    			
	                    			$("#dialogboxbodynew").html('');
	            					 var list = $("#dialogboxbodynew") ;
	            					 list.append("<br>");
	            					 for(i=0;i<IFSCList.length;i++){
	            	if(i==0)
	            		{
	            		var Alert = new CustomAlertNew();
	        			Alert.render(msg);
	            		}
	            						 list.append("<a id='IFSCIndex' href='javascript:void(0)'  onclick='return ifscListClick("+i+")'> <div>" +
	            								 IFSCList[i].BankName + "<br>" + IFSCList[i].BranchhName + ",&nbsp;" + IFSCList[i].City + "<br>" + IFSCList[i].IFSCCODE + "<hr></div></a>");
	             							 
	            						 
	            					 }
	        				}
	        				
	        				
	        				
	        			
	        			}
	        		else{
	        			//busyInd.hide();
	        			customBusyHide();
	        			navigator.notification.alert("No Record Found");
	        			//Enable the submit button to resolve the issue of Null data.
            			$("#IFSCSearchSubmit").prop('disabled', false);
	        		}
	    			}
	        		else{
	        			//busyInd.hide();
	        			customBusyHide();
	        			navigator.notification.alert("No Record Found");
	        			//Enable the submit button to resolve the issue of Null data.
            			$("#IFSCSearchSubmit").prop('disabled', false);
	        		}
	        	}
	        	else {
	        		//busyInd.hide();
	        		customBusyHide();
	        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	        		//Enable the submit button to resolve the issue of Null data.
        			$("#IFSCSearchSubmit").prop('disabled', false);
	        	}
	        	
	        	
	        	//busyInd.hide();
	        	customBusyHide();
	        };
	        
	        
	        self.WithinBankDetailsModifySubmit = function()
	        {
	        	if(window.navigator.onLine){
	        	if($("#frmDetailsIMPS").valid()){
	        		
	        		//Here we will set the Nick name and Payee name
	    			PayeeName =  $.trim($('#Payee_Name').val()); 
	    			//PayeeNickName = $.trim($('#Payee_Nick_Name').val());
	    			
	    			self.WithinBankDetailsSubmit();
	        	}
	        	}
	        	else{
	        		navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	        	}
	        	// busyInd.hide();
	        
	        };
	         
	// ----------Add Benificary Submit-----------------
	        
	        self.WithinBankDetailsSubmit = function(){        	
	        	

	        	//Here we will be adding validation to resolbe null issue.
				if(BeneficieryOptState == BeneficieryOpt.InternalAddition)
				{
					if(PayeeNo==null || PayeeName==null || PayeeNickName==null)
						{
						   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						   return;
						}
					else if(PayeeNo.length<=0 || PayeeName.length<=0 || PayeeNickName.length<=0)
					{
						   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						   return;
						}
				}
				else if(BeneficieryOptState == BeneficieryOpt.InternalDeletion)
					{

					if(PayeeNo==null || PayeeNickName==null)
					{
					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					   return;
					}
				else if(PayeeNo.length<=0 || PayeeNickName.length<=0)
				{
					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					   return;
					}
					}
				else
					{

					if(PayeeNo==null || PayeeName==null || PayeeNickName==null || IFSCCode==null || BranchName==null || BankName==null)
					{
					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					   return;
					}
					else if(PayeeNo.length<=0 || PayeeName.length<=0 || PayeeNickName.length<=0 || IFSCCode.length<=0 || BranchName.length<=0 || BankName.length<=0)
					{
						   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						   return;
						}
					}


	        	if(window.navigator.onLine){
	        	
				$("#contentData").load("Views/Payee/Payee_OTP.html", null, function (response, status, xhr) {
					if (status != "error") {}
					
					activeAddBenpage=true;			
					
					var operationID = "";
	            	var Action = "";
	            	            	
					switch(BeneficieryOptState)
		        	{
		        	case BeneficieryOpt.InternalAddition :
		    		{
		        		$(".h_title").html("Within RBL Bank");
		        		operationID = "INTERNALREQ";
	            		Action = "ADD";
		    		   break;
		    		}
		        	case BeneficieryOpt.RTGSAddition :
		    		{
		        		$(".h_title").html("RTGS");
		        		operationID = "RTGSREQ";
	            		Action = "ADD";
		    		   break;
		    		}
		        	case BeneficieryOpt.EXTERNALAddition :
		    		{
		        		$(".h_title").html("NEFT/IMPS");
		        		operationID = "EXTERNALREQ";
		        		Action = "ADD";
		    		   break;
		    		}
		        	case BeneficieryOpt.InternalModification :
		    		{
		        		$(".h_title").html("Within RBL Bank");
		        		operationID = "INTERNALREQ";
	            		Action = "Mod";
		    		   break;
		    		}
		        	case BeneficieryOpt.RTGSModification :
		    		{
		        		$(".h_title").html("RTGS");
		        		operationID = "RTGSREQ";
	            		Action = "Mod";
		    		   break;
		    		}
		        	case BeneficieryOpt.EXTERNALModification :
		    		{
		        		$(".h_title").html("NEFT/IMPS");
		        		operationID = "EXTERNALREQ";
	            		Action = "Mod";
		    		   break;
		    		}
		        	case BeneficieryOpt.InternalDeletion :
		    		{
		        		$(".h_title").html("Within RBL Bank");
		        		operationID = "INTERNALREQ";
	            		Action = "Del";
		    		   break;
		    		}
		        	case BeneficieryOpt.RTGSDeletion :
		    		{
		        		$(".h_title").html("RTGS");
		        		operationID = "RTGSREQ";
	            		Action = "Del";
		    		   break;
		    		}
		        	case BeneficieryOpt.EXTERNALDeletion :
		    		{
		        		$(".h_title").html("NEFT/IMPS");
		        		operationID = "EXTERNALREQ";
	            		Action = "Del";
		    		   break;
		    		}
		        	
		        	default: {
		               // alert("case default");
		            }
		        	
		        	}
					
					// Here we need to create a entity for the Beneficiery before
					// switching to OTP screen
					// First empty the structure
					makeEmptyStructureForBeneficiery();
	    			 
	    			// Assign data
		    		objBeneficiery.LoginUserId = LoggedInUserID;
		    		objBeneficiery.BENE_SEQNO = "10448";
		    		objBeneficiery.OperationId = operationID;
		    		objBeneficiery.Action = Action;
		    		objBeneficiery.BeneAccNo = PayeeNo;
		    		objBeneficiery.BeneName = PayeeName;
		    		objBeneficiery.BeneNickName = PayeeNickName;
		    		objBeneficiery.BeneBankName = BankName;
		    		objBeneficiery.BeneBankBranch = BranchName;
		    		objBeneficiery.BeneIFSCCode = IFSCCode;
		    		objBeneficiery.BeneAddr1 = City; //We are sending City(For Addition only. For modification API side is handling)
		    		objBeneficiery.BeneAddr2 = "";
		    		objBeneficiery.BeneAccTyp = "10";
		    		objBeneficiery.BeneAddr3 = "";
		    		objBeneficiery.BeneAccCurr = "";
		    		objBeneficiery.BeneTransPWD = "";
		    		objBeneficiery.BeneSeqNo = "";	    		
		    		
					P2CgetOTP();
					$("#ResendOTP").click(function(){
						P2CgetOTP();
					});
					
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});        	
	        
	    }else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
	    }
	        };

	        
	        
	// ------------------OTP Submit Common-----------------
	      
	        
	        self.PayeeOtpSubmit = function () {
	            if (window.navigator.onLine) {
	                if ($("#frmPayee_OTP").valid()) {
	                    var channelId = "MBAUTH";
	                    var CIF = LoggedInUserID;
	                    var requestId = $("#requestId").val();
	                    var otp = booksStore($("#BenOTPval").val());
	                    
	                    objBeneficiery.RQRefNo = requestId;
	                	//alert(objBeneficiery.RQRefNo);
	                	
	                    
	                    objBeneficiery.RQOTP = $("#BenOTPval").val();
	                   // alert(objBeneficiery.RQOTP);
	                    
	                    $("#BenOTPval").val('');

	                   // busyInd.show();
//	                    if (lgnout == false) { window.location.href = "#logout"; }
//	                    var invocationData = {
//	                        adapter: "API_Adapter",
//	                        procedure: "OTPValidateService",
//	                        parameters: [channelId, CIF, requestId, otp],
//	                        compressResponse: true
//	                    };
	//
//	                    WL.Client.invokeProcedure(invocationData, {
//	                        onSuccess: BenOtpSubmitSuccess,
//	                        onFailure: AdapterFail,
//	                        timeout: timeout
//	                    });
	                    
	                    AddBenificary();
	                }
	            } else {
	                navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
	            }
	        };

	        
	        
	        //-------------------------------------------------
	        
//	        BenOtpSubmitSuccess = function (result) {
//	            busyInd.hide();
//	            invocationResult = result.invocationResult;
//	            console.log(JSON.stringify(invocationResult));
//	            if (invocationResult.RBL) {
//	                handleErrorP2C(invocationResult.RBL);
//	            }
//	            if (invocationResult.otpValResponse) {
//	                var Status = invocationResult.otpValResponse.status;
//	                if (Status == "VALID") {
//	                	
//	                	alert("valid otp");
//	                	
	// 
//	                	//var requestId = $("#requestId").val();
//	                	
//	                	
//	                  //  var otp = booksStore($("#BenOTPval").val());
//	           
//	                   
//	                	
//	                	
//	        			AddBenificary();
//	                } else if (Status == "INVALID_OTP") {
//	                    alert("Invalid OTP");
	//
//	                } else if (Status == "MAX_ATTEMPT_EXCEEDED") {
	// 
//	                    alert("Maximum Attempts Exceeded! Try again.");
//	                    window.location.hash = '#rrftr01';
	//
//	                }
//	                else {
	//
//	                    alert("Problem Occured! Try again.");
//	                    window.location.hash = '#rrftr01';
	//
//	                }
//	            }
//	            else {
//	                navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
//	            }
//	        };
	        
	        //-------**********-----------------------------
	                
	        
	        // Function to modify the beneficiery
	        
	        self.BeneficieryModifySubmit = function(formElement){
	        	
	            if(window.navigator.onLine){                
	                                	
						$("#contentData").load("Views/ModifyPayee/Payee_Modify_Ext_RTGS.html", null, function (response, status, xhr) {
							if (status != "error") {}	
							
							tptViewModelCopy.previousPage = "Payee_Modify_Details"
							
							switch(BeneficieryOptState)
				        	{
				        	
				        	
				        	case BeneficieryOpt.RTGSModification :
				    		{
				        		
				        		$(".h_title").html("RTGS");
				    		   break;
				    		}
				    		
				        	case BeneficieryOpt.EXTERNALModification :
				    		{
				        		
				        		$(".h_title").html("NEFT/IMPS");
				    		   break;
				    		}
				    		default :
				    		{
				    			
				    			}
				    		}
													
							var obj = BenePayeeList[selectedIndexOfBeneficiery];	        		
			        		
			        		$('.AccNo').html(obj.acctno);
			    			$('#Payee_Name').val(obj.benefname);
			    			$('.Payee_Nick_Name').html(obj.benefnickname);
			    			$('.IFSCCode').html(obj.ifsccode);
			    				
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
							});        	
	                
	            }else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
	            }
	            
	        };
	        
	        
	        
	        function AddBenificary(){

	        	//Here we will be adding validation to resolbe null issue.
				if(BeneficieryOptState == BeneficieryOpt.InternalAddition)
				{
					if(objBeneficiery.BeneAccNo==null || objBeneficiery.BeneName==null || objBeneficiery.BeneNickName==null)
						{
						   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						   return;
						}
					else if(objBeneficiery.BeneAccNo.length<=0 || objBeneficiery.BeneName.length<=0 || objBeneficiery.BeneNickName.length<=0)
					{
						   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						   return;
						}
				}
				else if(BeneficieryOptState == BeneficieryOpt.InternalDeletion)
					{
					if(objBeneficiery.BeneAccNo==null || objBeneficiery.BeneNickName==null)
					{
					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					   return;
					}
				else if(objBeneficiery.BeneAccNo.length<=0 || objBeneficiery.BeneNickName.length<=0)
				{
					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					   return;
					}
					}
				else
					{

					if(objBeneficiery.BeneAccNo==null || objBeneficiery.BeneName==null || objBeneficiery.BeneNickName==null || objBeneficiery.BeneIFSCCode==null || objBeneficiery.BeneBankName==null || objBeneficiery.BeneBankBranch==null)
					{
					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					   return;
					}
					else if(objBeneficiery.BeneAccNo.length<=0 || objBeneficiery.BeneName.length<=0 || objBeneficiery.BeneNickName.length<=0 || objBeneficiery.BeneIFSCCode.length<=0 || objBeneficiery.BeneBankName.length<=0 || objBeneficiery.BeneBankBranch.length<=0)
					{
						   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						   return;
						}
					}



	        	if(window.navigator.onLine){
	        			
	        	if(lgnout == false){ window.location.href = "#logout";}
	        		
	        	//busyInd.show();
	            customBusyShow();
	        	var reqParams = {};
	    		var BeneReq = [];
	    		//objBeneficiery.LoginUserId = null;
	    		
	    		reqParams.BeneReq = BeneReq;
	    		var entity = {
	    				
	    				"LoginUserId": objBeneficiery.LoginUserId,
	    				"RQRefNo":objBeneficiery.RQRefNo,
	    				"RQOTP":objBeneficiery.RQOTP,
	    		 		"BENE_SEQNO": objBeneficiery.BENE_SEQNO,
	    		        "OperationId": objBeneficiery.OperationId,
	    		        "Action": objBeneficiery.Action,
	    		        "BeneAccNo": objBeneficiery.BeneAccNo,
	    		        "BeneName": objBeneficiery.BeneName,
	    		        "BeneNickName": objBeneficiery.BeneNickName,
	    		        "BeneBankName": objBeneficiery.BeneBankName,
	    		        "BeneBankBranch": objBeneficiery.BeneBankBranch,
	    		        "BeneIFSCCode": objBeneficiery.BeneIFSCCode,
	    		        "BeneAddr1": objBeneficiery.BeneAddr1,
	    		        "BeneAddr2": objBeneficiery.BeneAddr2,
	    		        "BENE_TYPE": objBeneficiery.BeneAccTyp,
	    		        "BeneAddr3": objBeneficiery.BeneAddr3,
	    		        "BeneAccCurr": objBeneficiery.BeneAccCurr,
	    		        "BeneTransPWD": objBeneficiery.BeneTransPWD,
	    		        "BeneSeqNo": objBeneficiery.BeneSeqNo
	    		}

	    		reqParams.BeneReq.push(entity);
//	    		console.log(JSON.stringify(reqParams));
	    		
	        			var invocationData = {
	        				adapter : "API_Adapter",
	        				procedure :"beneficiaryService",
	        				parameters : [ booksStore(JSON.stringify(reqParams)),booksStore(LoggedInUserID) ],
	        				compressResponse : true
	        			};
	        			
	        			//console.log(JSON.stringify(invocationData));
	        			
	        			WL.Client.invokeProcedure(invocationData, {
	        				onSuccess : AddBenificarySuccess,
	        				onFailure : AdapterFail,
	        				timeout: timeout
	        			});
	        			
	        			
	        			
	        	}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	        	}
	        };


	        AddBenificarySuccess = function(result)
	        { 
	        	//busyInd.hide();
	        	customBusyHide();
	        	invocationResult = result.invocationResult;

	        	//console.log(JSON.stringify(invocationResult));
	        	
	        	if(invocationResult.isSuccessful) 
	        	{

	        		if(invocationResult.ResponseMessageInfo)
	        		{
	        			if(invocationResult.ResponseMessageInfo.STATUS)
	        			{
	        				if(invocationResult.ResponseMessageInfo.STATUS.CODE == '0'){


	        					//Give a call for beneficiary success
	        					BeneficierySucess();
	        					return;
	        				}
	        			}
	        		}

	        		//busyInd.hide();
	        		customBusyHide();
	        		if(invocationResult.ResponseMessageInfo)
	        		{
	        			if(invocationResult.ResponseMessageInfo.STATUS)
	        			{
	        				if(invocationResult.ResponseMessageInfo.STATUS.MESSAGE)
	        				{
	        					WL.SimpleDialog.show(
	        							"Alert", invocationResult.ResponseMessageInfo.STATUS.MESSAGE, 
	        							[{text: "OK", handler: function() {
	        								window.location.hash = '#rrftr01';
	        								WL.Logger.debug("OK button pressed"); }
	        							}]
	        					);

	        					return;
	        				}
	        				else 
	        				{

	        					if(invocationResult.STATUS)
	        					{
	        						navigator.notification.alert(invocationResult.STATUS.MESSAGE);
	        						return;
	        					}

	        				}
	        			}
	        		}

	        	}
	        	//busyInd.hide();
	        	customBusyHide();
	        	navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");


	        };
	        
	        // After OTP is verified and Beneficiery sucess
	        BeneficierySucess = function(){
	       
	        activeAddBenpage=false;
	        
	        switch(BeneficieryOptState)
	    	{
	    	case BeneficieryOpt.InternalAddition :
	    		{
	    		$("#contentData").load("Views/Payee/WithinBankSuccess.html", null, function (response, status, xhr) {
					if (status != "error") {}
					
					tptViewModelCopy.previousPage = "Success_Page";
					
					$('.AccNo').html(PayeeNo);
					$('.PName').html(PayeeName);
					$('.PNickName').html(PayeeNickName);
					
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});
	    		   break;
	    		}
	    	case BeneficieryOpt.RTGSAddition :
			{
	    		$("#contentData").load("Views/Payee/External_RTGS_Success.html", null, function (response, status, xhr) {
					if (status != "error") {}
					
					tptViewModelCopy.previousPage = "Success_Page";
					
					$('.AccNo').html(PayeeNo);
	    			$('.PName').html(PayeeName);
	    			$('.PNickName').html(PayeeNickName);
	    			$('.IFSCCode').html(IFSCCode);
	    			$('.BankName').html(BankName);
	    			$('.BranchName').html(BranchName);
	    			$('.City').html(City);
	    			$('.BeneNote').html("Success ! Your payee has been added Successfully. You may transfer fund to this payee after 12 hours.");
					
					ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});
			   break;
			}
	    	case BeneficieryOpt.EXTERNALAddition :
			{
	    		$("#contentData").load("Views/Payee/External_RTGS_Success.html", null, function (response, status, xhr) {
					if (status != "error") {}
					
					tptViewModelCopy.previousPage = "Success_Page";
					
					$('.AccNo').html(PayeeNo);
	    			$('.PName').html(PayeeName);
	    			$('.PNickName').html(PayeeNickName);
	    			$('.IFSCCode').html(IFSCCode);
	    			$('.BankName').html(BankName);
	    			$('.BranchName').html(BranchName);
	    			$('.City').html(City);
	    			$('.BeneNote').html("Success ! Your payee has been added Successfully. You may transfer fund to this payee upto ₹ 25,000 for next 12 hours.");
					
					ko.applyBindings(self, $(".dynamic-page-content").get(0));
					});
			   break;
			}
	    	case BeneficieryOpt.InternalModification :
			{
	    		
			   break;
			}
	    	case BeneficieryOpt.RTGSModification :
			{
				WL.SimpleDialog.show(
						"Alert", "Success ! Your payee has been modified Successfully.", 
						[{text: "OK", handler: function() {
							window.location.hash = '#rrftr01';
							WL.Logger.debug("OK button pressed"); }
						}]
						);
				
			   break;
			}
	    	case BeneficieryOpt.EXTERNALModification :
			{
				WL.SimpleDialog.show(
						"Alert", "Success ! Your payee has been modified Successfully.", 
						[{text: "OK", handler: function() {
							window.location.hash = '#rrftr01';
							WL.Logger.debug("OK button pressed"); }
						}]
						);
	    		
			   break;
			}
	    	case BeneficieryOpt.InternalDeletion :
			{
	    		    		
				WL.SimpleDialog.show(
						"Alert", "Payee Deleted Successfully.", 
						[{text: "OK", handler: function() {
							window.location.hash = '#rrftr01';
							WL.Logger.debug("OK button pressed"); }
						}]
						);
				
			   break;
			}
	    	case BeneficieryOpt.RTGSDeletion :
			{
				WL.SimpleDialog.show(
						"Alert", "Payee Deleted Successfully.", 
						[{text: "OK", handler: function() {
							window.location.hash = '#rrftr01';
							WL.Logger.debug("OK button pressed"); }
						}]
						);
	    		
			   break;
			}
	    	case BeneficieryOpt.EXTERNALDeletion :
			{
				WL.SimpleDialog.show(
						"Alert", "Payee Deleted Successfully.", 
						[{text: "OK", handler: function() {
							window.location.hash = '#rrftr01';
							WL.Logger.debug("OK button pressed"); }
						}]
						);
				
			   break;
			}
	    	default: {
	           // alert("case default");
	        }
	    	
	    	}           
	            
	        };
	        	
	        
	        // View beneficiery
	        self.PayeeListDetails = function(){
				
				if(window.navigator.onLine){
					
					var RQOperationId = "";
					var methodName = "";
					
					switch(BeneficieryOptState)
		        	{
		        	
		        	case BeneficieryOpt.InternalModification :
		    		{
		        		RQOperationId = "TPTFNDTFR";
		        		methodName =  "RRTPT03";
		    		   break;
		    		}
		        	case BeneficieryOpt.RTGSModification :
		    		{
		        		RQOperationId = "RTGFNDTFR";
		        		methodName =  "GetRtgs";
		    		   break;
		    		}
		        	case BeneficieryOpt.EXTERNALModification :
		    		{
		        		RQOperationId = "NFTFNDTFR";
		        		methodName =  "GetNeft";
		    		   break;
		    		}
		        	
		        	default: {
		               // alert("case default");
		            }
		        	
		        	}				
					
					reqParams = {};
					//busyInd.show();
					customBusyShow();
					reqParams["RQLoginUserId"] = LoggedInUserID;
					reqParams["RQDeviceFamily"] = Device_Platform;
					reqParams["RQDeviceFormat"] = Device_Model;
					reqParams["RQOperationId"] = RQOperationId;
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = CurrentSessionId;
					reqParams["RQTransSeq"] = "01";
					fldjsessionid="";
					// busyInd.show();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : methodName,
						parameters : [fldjsessionid,reqParams],
						compressResponse : true
					};
					
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : PayeeListDetailsSucess,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}
	        };
	        
	        
	        PayeeListDetailsSucess = function(result){ 
	        	
	        //	console.log(JSON.stringify(result));   
	        	
	            invocationResult = result.invocationResult;          
	            
	            BenePayeeList.length = 0;
	            var list1 = [];
	            
	            if(invocationResult.isSuccessful) {
	    		if(invocationResult.RBL){
	                if(invocationResult.RBL.Response){
	    				if(invocationResult.RBL.STATUS.CODE == '0'){
	    					
	    					var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
	    					var currentuser = currentreguserid;
	    					var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
	    					if(currentuser == responseuser && CurrentSessionId == responsesessionid){
	    						// alert("1");
	    						
	    						
	    						// alert("2");
	    						if(invocationResult.RBL.Response.tptacctdetails){
	    							
	    							toAccdata = invocationResult.RBL.Response.tptacctdetails;
	    							
	    								if(invocationResult.RBL.Response.tptacctdetails.length!=0){
	    									
	    									switch(BeneficieryOptState)
	    						        	{
	    						        	
	    						        	case BeneficieryOpt.InternalModification :
	    						    		{
	    						        		$(".h_title").html("Within RBL Bank");
	    						    		   break;
	    						    		}
	    						        	case BeneficieryOpt.RTGSModification :
	    						    		{
	    						        		$(".h_title").html("RTGS");
	    						    		   break;
	    						    		}
	    						        	case BeneficieryOpt.EXTERNALModification :
	    						    		{
	    						        		$(".h_title").html("NEFT/IMPS");
	    						    		   break;
	    						    		}
	    						        	
	    						        	default: {
	    						               // alert("case default");
	    						            }
	    						        	
	    						        	}
	   									
	    									// alert("3");
	    									$("#contentData").load("Views/ModifyPayee/PayeeList.html", null, function (response, status, xhr) {
	    										
//	    									$(toAccdata).each(function(index, obj) {
//	    										
//	    										var benefname = $.trim(obj.benePName);
//	    										var bankname = $.trim(obj.bankname);
//	    										var benefnickname = $.trim(obj.benefname);
//	    										var ifsccode = obj.ifsccode;
//	    										var acctno = obj.acctno;
//	    										
//	    										
//	    										list1.push({ benefname: obj.benePName,
//	    										bankname : obj.bankname,
//	    										ifsccode:	obj.ifsccode, 
//	    										benefnickname : obj.benefname,
//	    										acctno: obj.acctno});
//	    									});
	    										
	    										$(toAccdata).each(function(index, obj) {
	        										
	        										var benefname = $.trim(obj.benePName);
	        										var bankname = $.trim(obj.bankname);
	        										var benefnickname = $.trim(obj.benefname);
	        										var ifsccode = obj.ifsccode;
	        										var acctno = obj.acctno;
	        										var beneaddr = obj.beneaddr;
	        										
	        										//Here we will check for the empty record or null record
	        										if(acctno != null || benefnickname!=null)
	        											{
	        											   if(acctno.length >0 && benefnickname.length>0)
	        												   {
	        												   list1.push({ benefname: obj.benePName,
	        		        										bankname : obj.bankname,
	        		        										ifsccode:	obj.ifsccode,
	        		        										benefnickname : obj.benefname,
	        		        										acctno: obj.acctno,
	        		        										beneaddr : obj.beneaddr
	        		        										});
	        												   }
	        											}




	        									});
	    									

	    									BenePayeeList = list1.slice(0);

	    									BenePayeeList.sort(function(a,b) 
	    											{
	    										
	    											var x = a.benefnickname.toLowerCase();

	    											var y = b.benefnickname.toLowerCase();	

	    											return x < y ? -1 : x > y ? 1 : 0;
	    										


	    											});

	    									
	    									 $("#BenePayeeList").html('');
	    									 var list = $("#BenePayeeList") ;
	    									 
	    				for(i=0;i<BenePayeeList.length;i++){    					
	    					
	    					var strname = BenePayeeList[i].benefnickname;
	    					
	    					

	    				list.append(
	    						// href='#Payee_Beneficiery_Details'
	    						"<a id='BeneIndex' href='javascript:void(0)'  onclick='return Payee_Beneficiery_Details("+i+")'>" +
	    						"<div class='info_row odd'>" +
	    							"<span class='info_row_menu_right page_title_carousel'>"  + strname + "</span>" +
	    							"<span class='right_arrow'><img src='img/rarrow.png' /></span>" +
	    						"</div>" +
	    						"</a>" +
	    						
	    						"<div class='clearfix'></div>" +
	    						"<div class='linedivider'></div>" +
	    						"<div class='clearfix'></div>" );   				

	    				}
	    				
//	    				list.append(
//	    						
//						"<input type='hidden' id='totalelement' value='" +BenePayeeList.length + "' />" );
	    				

	    				// }    						 
	    						   
	    									ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
	    							});
	    						}
	    						}
	    						else
	    							{
	    							//busyInd.hide();
	    							customBusyHide();
	    							$("#contentData").load("Views/ModifyPayee/PayeeList.html", null, function (response, status, xhr) {
	    								var target = "";
	    								
	    								switch(BeneficieryOptState)
							        	{
							        	
							        	case BeneficieryOpt.InternalModification :
							    		{
							        		$(".h_title").html("Within RBL Bank");
							        		target = "#PayeeWithinBank";
							    		   break;
							    		}
							        	case BeneficieryOpt.RTGSModification :
							    		{
							        		$(".h_title").html("RTGS");
							        		target = "#Payee_RTGS";
							        		
							    		   break;
							    		}
							        	case BeneficieryOpt.EXTERNALModification :
							    		{
							        		$(".h_title").html("NEFT/IMPS");
							        		target = "#Payee_NEFT_IMPS";
							        		
							    		   break;
							    		}
							        	
							        	default: {
							               // alert("case default");
							            }
							        	
							        	}
	    							
	    							$("#emptylist").show();
	    							//alert("1");
	    							
	    							 document.getElementById("payeelink").href=target; 
	    							 //alert("2");
	    							
	    							ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
	    							
	    							});
								
//	    							WL.SimpleDialog.show(
//	    									"Alert", "No Records Found !", 
//	    									[{text: "OK", handler: function() {window.location.hash = '#ModifyPayee_Home';}
//	    									}]
//	    									);
	    							}
	    						
	    					}
	    					else{
	    							//busyInd.hide();
	    						customBusyHide();
	    							navigator.notification.alert("Your session has timed out!");
	    							window.location.hash = "#logout";
	    					}
	    					//busyInd.hide();
	    					customBusyHide();
	    				}
	    				else{
	    					//busyInd.hide();
	    					customBusyHide();
	    					invalidresponse();
	    				}
	                }else{
	                	// to handle NRE/NRO message
	                				//busyInd.hide();
	                	customBusyHide();
	            					if(invocationResult.RBL.STATUS.CODE == '1002'){
	            						navigator.notification.alert(invocationResult.RBL.STATUS.MESSAGE)	
	            					}
	            					else{
	    								handleError(invocationResult.RBL.Response, invocationResult.RBL);
	    			}
	    		}
	    		}else{
	            		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	            		//busyInd.hide();
	            		 customBusyHide();
	    			}
	            }
	    		//busyInd.hide();
	            customBusyHide();
	    		};
	        
	        
	        self.payee_Bene_Details = function()
	        {        	
	        	
	        	var pageName = "DetailsIMPS";
	        	
	        	if (BeneficieryOptState == BeneficieryOpt.InternalModification)
	        		{
	        		pageName = "DetailsWithinBank"
	        		}        		
	        	
	        	$("#contentData").load("Views/ModifyPayee/" + pageName + ".html", null, function (response, status, xhr) {				
	        		
	        		var obj = BenePayeeList[selectedIndexOfBeneficiery];        		
	        		
	        		$('.AccNo').html(obj.acctno);
	        		PayeeNo =  obj.acctno;
	    						
	    			$('.PNickName').html(obj.benefnickname);
					PayeeNickName = $.trim(obj.benefnickname);
	    			
	    			switch(BeneficieryOptState)
	            	{
	            	
	            	case BeneficieryOpt.InternalModification :
	        		{
	            		$(".h_title").html("Within RBL Bank");
	        		   break;
	        		}
	            	case BeneficieryOpt.RTGSModification :
	        		{
	            		$(".h_title").html("RTGS");
	            		$('.bankName').html(obj.bankname);
	            		BankName = $.trim(obj.bankname);
	    				$('.IFSCCode').html(obj.ifsccode);
	    				IFSCCode = obj.ifsccode;
	    				$('.PName').html(obj.benefname);
	        			PayeeName =  $.trim(obj.benefname); 
	    				BranchName = obj.beneaddr;
	    			    City = "";
	    				
	        		   break;
	        		}
	            	case BeneficieryOpt.EXTERNALModification :
	        		{
	            		$(".h_title").html("NEFT/IMPS");
	            		$('.bankName').html(obj.bankname);
	            		BankName = $.trim(obj.bankname);
	    				$('.IFSCCode').html(obj.ifsccode);
	    				IFSCCode = obj.ifsccode;
	    				$('.PName').html(obj.benefname);
	        			PayeeName =  $.trim(obj.benefname); 
	    				BranchName = obj.beneaddr;
	    			    City = "";
	        		   break;
	        		}
	            	
	            	default: {
	                   // alert("case default");
	                }
	            	}
	    			
	    			
	                ko.applyBindings(self, $(".dynamic-page-content").get(0));
					
				});
	        };
	        
	        self.deleteBeneficiary = function()
	        {
	        	
	        	switch(BeneficieryOptState)
	        	{
	        	
	        	case BeneficieryOpt.InternalModification :
	        	{
	        		BeneficieryOptState =  BeneficieryOpt.InternalDeletion;
	        	   break;
	        	}
	        	case BeneficieryOpt.RTGSModification :
	        	{
	        		BeneficieryOptState =  BeneficieryOpt.RTGSDeletion;
	        	   break;
	        	}
	        	case BeneficieryOpt.EXTERNALModification :
	        	{
	        		BeneficieryOptState =  BeneficieryOpt.EXTERNALDeletion;
	        	   break;
	        	}
	        	
	        	default: {
	               // alert("case default");
	            }
	        	
	        	}
	        	
	        	//loadViewModel("Delete_Beneficiary");
	        	self.WithinBankDetailsSubmit();
	        	
	        }; 
			
		
};
    
 function getFormattedDate(date)
{
    return date.substring(8,10) + "-" + date.substring(5,7)+"-"+date.substring(0,4);
}

function getFormattedTime(date)
{
    return date.substring(8,16);
}	
     
 function onNetworkCheck(){
			return ;
} 
 
 
 function viewMoreInfo(i){
	 $("#statusdiv"+i).css('display','inline-block');
 }




function resendPasscodeP2C(accountNO,RefNo){
	if(window.navigator.onLine){
		var REQUEST="RESEND_PASSCODE";
		var CUSTOMER_ID=btoa(LoggedInUserID);
		var ACC_NO=booksStore(btoa(accountNO));
		var APPID=btoa("555557");
		var REF_NO=booksStore(btoa(RefNo));
		var SESSIONID=btoa("1111111113");
		var LoggedInCUSTOMER_ID= LoggedInUserID;
			
		busyInd.show();
			if(lgnout == false){ window.location.href = "#logout";}
			var invocationData = {
				adapter : "API_Adapter",
				procedure :"resendPasscode",
				parameters : [REQUEST,CUSTOMER_ID,ACC_NO,APPID,REF_NO,SESSIONID,LoggedInCUSTOMER_ID],
				compressResponse : true
			};
	
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : resendPasscodeP2CSucess,
				onFailure : AdapterFail,
				timeout: timeout
			});
	}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	}
}


resendPasscodeP2CSucess = function(result){ 
	busyInd.hide();
	invocationResult = result.invocationResult;
	if(invocationResult.RBL){
		handleErrorP2C(invocationResult.RBL);
	}
	if(invocationResult.FIXML.Body.PasscodeResponse){
		var Status=invocationResult.FIXML.Body.PasscodeResponse.Status;
		if(Status=="Success"){
			var Message=invocationResult.FIXML.Body.PasscodeResponse.Message;
			WL.SimpleDialog.show(
					"Alert", "Passcode sent successfully !", 
					[{text: "OK", handler: function() {
//						window.location.hash = '#rrftr01';
						WL.Logger.debug("OK button pressed"); }
					}]
					);
			
		}
		else{
			WL.SimpleDialog.show(
					"Alert", "Problem Occured! Try again.", 
					[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
					}]
					);
		}
		
	}else if(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail){
		navigator.notification.alert(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail.ErrorDesc);
	}
	else{
		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
	}			
}; 	





function stopPaymentP2C(accountNO,RefNo){
	WL.SimpleDialog.show(
			"Confirm", "Are you sure you want to Stop Payment ?", 
			[{text: "Yes", handler: function() {
				
	if(window.navigator.onLine){
		var REQUEST="STOP_CHEQUE";
		var CUSTOMER_ID=btoa(LoggedInUserID);
		var ACC_NO=booksStore(btoa(accountNO));
		var APPID=btoa("555557");
		var REF_NO=booksStore(btoa(RefNo));
		var LoggedInCUSTOMER_ID= LoggedInUserID;
			
		busyInd.show();
			if(lgnout == false){ window.location.href = "#logout";}
			var invocationData = {
				adapter : "API_Adapter",
				procedure :"stopPayment",
				parameters : [REQUEST,CUSTOMER_ID,ACC_NO,APPID,REF_NO,LoggedInCUSTOMER_ID],
				compressResponse : true
			};
	
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : stopPaymentP2CSucess,
				onFailure : AdapterFail,
				timeout: timeout
			});
	}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
	}
	
			}
			},
			{text: "Cancel", handler: function() {WL.Logger.debug("Cancel button pressed"); }
			}
			]);
}

stopPaymentP2CSucess = function(result){ 
	busyInd.hide();
	invocationResult = result.invocationResult;
	if(invocationResult.RBL){
		handleErrorP2C(invocationResult.RBL);
	}
	if(invocationResult.FIXML.Body.StopPaymentResponse){
		var Status=invocationResult.FIXML.Body.StopPaymentResponse.Status;
		if(Status=="Success"){
			var Message="Payment Stopped Successfully !";
			WL.SimpleDialog.show(
					"Alert", Message, 
					[{text: "OK", handler: function() {
						window.location.hash = '#rrftr01';
						WL.Logger.debug("OK button pressed"); }
					}]
					);
			
		}
		else{
			WL.SimpleDialog.show(
					"Alert", "Problem Occured! Try again.", 
					[{text: "OK", handler: function() {WL.Logger.debug("OK button pressed"); }
					}]
					);
		}
		
	}else if(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail){
		navigator.notification.alert(invocationResult.FIXML.Body.Error.ESBBusinessException.ErrorDetail.ErrorDesc);
	}
	else{
		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
		busyInd.hide();
	}			
}; 	


function formatTranHisDate(date){
	var newDate=new Date(date).toLocaleString('en-GB');               
	var sptdate = String(newDate).split("/");
	var myDay = sptdate[0];
	var myMonth = sptdate[1];
	var myYear = sptdate[2].substring(0,4);
	var formatedDate = myDay + "-" + myMonth + "-" + myYear;
	return formatedDate;
}
 