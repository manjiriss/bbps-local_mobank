
/* JavaScript content from ViewModels/AccountsViewModel.js in folder common */


  var AccountsViewModel = function () {

        var self = this;

         //For Credit Score
              var emailforcreditscore = "";
              var accno_selected = "";
        
        self.curraccbalval = ko.observable();
		curraccbalval1 = ko.observable();
		curraccbalvalcheck = ko.observable();
        self.selAccount = ko.observable();
        self.accountStmtTxns = ko.observableArray([]);
		self.fixedDepositList = ko.observableArray([]);
        self.recurringDepositList = ko.observableArray([]);
        self.selFssSavingAcc = ko.observable();
        self.selFssFDAcc = ko.observable();
        self.fssSavingAccountList = ko.observableArray([]);
        self.fssFDAccountList = ko.observableArray([]);
        self.fldFromAcctNo = ko.observable();
        self.fldToAcctNo = ko.observable();
        self.selAcctTemp = ko.observable();
        self.fldIntCrAcctNoTemp = ko.observable();
        self.rdYearList = ko.observableArray([]);
        self.rdMonthList = ko.observableArray([]);
        self.tdsArray1 = ko.observableArray([]);
    	self.tdsArray2 = ko.observableArray([]);
    	
    	self.agencyList = ko.observableArray([]);
        self.selAgency = ko.observable();
        self.accountAgencyList = ko.observableArray([]);
        self.selAccountAgency = ko.observable();
        self.holdingDetails = ko.observableArray([]);
        
        self.FDaccountList = ko.observableArray([]);
        self.FDSelaccountList = ko.observableArray([]);
        self.fldFDAcctNo = ko.observable();
        
        self.MMIDaccountList = ko.observableArray([]);
        self.fldAcctNo =  ko.observable();
		
		self.myfavmenu = ko.observable();
        self.PerLzdmenuList = ko.observable();
		
		 self.PerLzdmenuListAccount = ko.observableArray([]);
        self.PerLzdmenuListTransfer = ko.observableArray([]);
        self.PerLzdmenuListBills = ko.observableArray([]);
        self.PerLzdmenuListCards = ko.observableArray([]);
        self.PerLzdmenuListDemat = ko.observableArray([]);
        self.PerLzdmenuListdtcards = ko.observableArray([]);
        self.PerLzdmenuListfcatis = ko.observableArray([]);
        
        self.PerLzdmenuListoAlert = ko.observableArray([]);
        self.PerLzdmenuListotherCnt = ko.observableArray([]);
		
        self.MyPerlzdMenus = ko.observable();
        
        self.myfavmenuOtherCount = ko.observable();
        self.MyFmnu = ko.observableArray([]);
        
        self.openFDAccList = ko.observableArray([]);
        self.selOpenFD = ko.observable();
        self.prodtypeList = ko.observableArray([]);
        self.prodtype = ko.observable();
        self.creditAccno = ko.observable();
        self.prodTypeData = ko.observable();
		
		frmacntfd = "";
		amntfd = "";
		fdmonths = "";
		fddays = "";
		naturefd = "";
		naturefdval = "";
		maturityinstu = "";
		maturityinstuval = "";
		
		//Account for rbl//
        self.getAccountSummary = function(){
				accountList.removeAll();
				if(accountList().length === 0){
				if(window.navigator.onLine){
				
					busyInd.show();
					reqParams = {};
					//reqParams["RQLoginUserId"] = WL.Client.getUserPref("loggeduserid");
					reqParams["RQLoginUserId"] = LoggedInUserID;
					reqParams["RQDeviceFamily"] = Device_Platform;
					reqParams["RQDeviceFormat"] = Device_Model;
					reqParams["RQOperationId"] = "ACCSUMINQ";
					reqParams["RQClientAPIVer"] = RQClientAPIVer;
					reqParams["SessionId"] = CurrentSessionId;
					reqParams["RQTransSeq"] = "01";
					
					fldjsessionid="";
					//fldjsessionid = WL.Client.getUserPref("fldSessionId");
					
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "GetAccounts",
							parameters : [fldjsessionid,reqParams],
							compressResponse : true
					};
					
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrasmSuccess,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
					
					 if(window.location.hash == '#rrasm01'){
						 templateId = "rrasm01";
					 }else{
						 templateId = "accountSummary";
					 }
					$("#contentData").load("Views/Accounts/"+templateId+".html", null, function (response, status, xhr) {
						if (status != "error") {}
						ko.applyBindings(self, $(".dynamic-page-content").get(0));
						
					});
					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
					}

					
				 }else{
					busyInd.show();
					 if(window.location.hash == '#rrasm01'){
						 templateId = "rrasm01";
					 }else{
						 templateId = "accountSummary";
					 }
					
					 $("#contentData").load("Views/Accounts/"+templateId+".html", null, function (response, status, xhr) {
						 if (status != "error") {}
						 ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
						 if(accountList().length > 0 && window.location.hash == '#rrasm01'){
							 $('.autoslide-slider3').iosSlider({
								desktopClickDrag: true,
								snapToChildren: true,
								infiniteSlider: false,
								autoSlide: false,
								/*scrollbar: true,
								autoSlideTransTimer: 0,*/
								onSlideComplete: slideComplete,
								navNextSelector: $('.autoslide-slider3 .next'),
								navPrevSelector: $('.autoslide-slider3 .prev')
							});
						}
						busyInd.hide();
					});
					
				}
			};
        
        rrasmSuccess = function(result){
			
			invocationResult = result.invocationResult;
				if(invocationResult.isSuccessful) {
					if(invocationResult.RBL){
						if(invocationResult.RBL.Response){
							var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
							var currentuser = currentreguserid;
							var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
							if(currentuser == responseuser && CurrentSessionId == responsesessionid){
								 custdtls = invocationResult.RBL.Response.CustDetails;
								 itemdata = invocationResult.RBL.Response.acctdetails;
								 nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
								 nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;
								
								 totAccount = parseInt(nbrofsavingacc) + parseInt(nbrofcurrentacc);
								 if(totAccount > 0)
									 accSlider(true);
								 else
									 accSlider(false);
								
								 accountList.removeAll();
								 var idx = 1;
								 $(itemdata).each(function(index, obj) {
									 strid = "item"+idx;
									 custnames = "";
									
									 $(custdtls).each(function(j, obj1) {
									
										 if(obj.acctindex == obj1.acctindex){
										 
											 if(obj1.custRelationationation == 'MAIN HOLDER OF ACCOUNT' || obj1.custRelationationation == 'SOW' || obj1.custRelationationation == 'GUA' || obj1.custRelationationation == 'JAF' || obj1.custRelationationation == 'JAO' || obj1.custRelationationation == 'NOM' || obj1.custRelationationation == 'TRU' || obj1.custRelationationation == 'JOO' || obj1.custRelationationation == 'JOF' || obj1.custRelationationation == 'AUS' || obj1.custRelationationation == 'GUR' || obj1.custRelationationation == 'THR' || obj1.custRelationationation == 'SOL' || obj1.custRelationationation == 'DEV' || obj1.custRelationationation == 'VAL' || obj1.custRelationationation == 'CUS' || obj1.custRelationationation == 'CON' || obj1.custRelationationation == 'REL'){
												custnames += obj1.custName+"  ";
											 }
										 }
									 });
									 displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
									 
									 var curr = obj.currency;
									 if(window.location.hash == '#rrasm01'){
										if(curr == "INR"|| curr == "inr" || curr == "Inr"){
										 acctbalance = "₹ " +formatAmt(parseFloat(obj.acctbalance));
										 }
										 else{
											 acctbalance = formatAmt(parseFloat(obj.acctbalance));
										 }
									 }else{
										 acctbalance = "₹ " +formatAmt(parseFloat(obj.acctbalance));
									 }
									checkbalance = parseFloat(obj.acctbalance);
									 accountList.push({codacctno: obj.acctno, acctType: obj.acctType, acctbalance: acctbalance, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid, checkbalance:checkbalance });
									 
									 idx++;
								 });
								
								 if(window.location.hash == '#rrasm01'){
									 if(totAccount == 0){
										 $("#accExitsMsg").show();
										//$("#wrapper").css("top","110px");
									 }else{
										 $("#accExitsMsg").hide();
										//$("#wrapper").css("top","96px");
									 }
								 }else{
									 if(totAccount == 0){
										 $("#accExitsMsg").show();		    			
									 }else{
										 $("#accExitsMsg").hide();		    			
									 }
								 }
								
								if(accountList().length > 0 && window.location.hash == '#rrasm01'){
									$('.autoslide-slider3').iosSlider({
										desktopClickDrag: true,
										snapToChildren: true,
										infiniteSlider: false,
										autoSlide: false,
										/*scrollbar: true,
										autoSlideTransTimer: 0,*/
										onSlideComplete: slideComplete,
										navNextSelector: $('.autoslide-slider3 .next'),
										navPrevSelector: $('.autoslide-slider3 .prev')
									});
									busyInd.hide();
								}
								else{
										busyInd.hide();
										handleError(invocationResult.RBL.Response);
									//window.location = "#login";
									}
							}
							else{
								busyInd.hide();
								navigator.notification.alert("Your session has timed out!");
								window.location.hash = "#logout";
							}
						}else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
					}else{
						navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						busyInd.hide();
					}
				}
					busyInd.hide();
		};
        
        self.accountStmtDetails = function(accnodet){
        	
            selectedAccount({ accno: accnodet.codacctno, displaytxt: accnodet.displaytxt, acctbalance: accnodet.acctbalance, fldFCDBRequestId: accnodet.fldFCDBRequestId, acctType: accnodet.acctType });
            randomintstr = parseInt(Math.random()*1000000000, 10);
            checkState=1;
            window.location = "#accountStatment";
			busyInd.hide();
            //self.viewSelectedAccountStatement();
        };
        
        self.viewSelectedAccountStatement = function(){
				if(window.navigator.onLine){
				var currAccData = selectedAccount();
				
				fldAcctNo = currAccData.accno;            
				curraccbalval = currAccData.acctbalance;
				acctType =  currAccData.acctType;
				fldAcctNo_txt = currAccData.displaytxt;
				fldjsessionid="";
				reqParams = {};
				
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "ACCSTMINQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				//reqParams["SessionId"] = "1990009769OKYLLDDP";
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQAccountNumber"] =fldAcctNo;
				reqParams["RQFromDate"] = "";
				reqParams["RQToDate"] = "";
				reqParams["RQTRNTYPE"] = "All";
				
				reqParams["RQTransSeq"] = "02";
				
				if(fldAcctNo != undefined){
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "AccountDetails",
							parameters : [fldjsessionid,reqParams],
							compressResponse : true
					};
					WL.Logger.debug(invocationData, '');
					
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : self.accountStmtDetSubmitSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}else{
					busyInd.hide();		
				}
				
				busyInd.hide();	
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
				}

			};
        
        self.accountStmtDetSubmitSuccess = function(result){
			//busyInd.show();
				var fldAcctNo = reqParams.RQAccountNumber;
				invocationResult = result.invocationResult;
				if(invocationResult.isSuccessful) {
					if(invocationResult.RBL.Response){	
						self.accountStmtTxns.removeAll();
						checkState=0;
						txndata = invocationResult.RBL.Response.txndetails.transaction;
						
						accno = fldAcctNo;
						acctype = "SAVINGS";
						txntype = "A";
						acctcurr = "INR";
						closingbalance = invocationResult.RBL.Response.closingbalance;
						fromdt = "";        	
						todate = "";	
						period = "";
						
						$(txndata).each(function(index, obj) {
							
							if(obj.codedrcr == 'C')
								amtLbl = 'Deposit';
							else
								amtLbl = 'Withdrawal';
							
							self.accountStmtTxns.push({ dattxn:getFormattedDate(obj.txndate), amttxn: formatAmt(parseFloat(obj.txnamt)), amtLbl: amtLbl, refchqno: obj.refchqno, txttxndesc: obj.txndesc, datvalue:getFormattedDate(obj.valuedate), balaftertxn: formatAmt(parseFloat(obj.balaftertxn)), txntype:txntype });
						});  
						
						$("#contentData").load("Views/Accounts/accountStatment.html", null, function (response, status, xhr) {
							if (status != "error") {}
							$("#acctType").html("");
							$("#acctType").html(acctype);
								ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
						});
						busyInd.hide();
					}
					else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
					}
						
				}
				else{	
						busyInd.hide();
						handleError(invocationResult.RBL.Response);
						if(checkState){
							checkState=0;
							window.location="#rrasm01";
						}
						busyInd.hide();
				}
					//}
				//}
				busyInd.hide();
		};
        
        self.showSelectedAccount = function(){
			//busyInd.show();
        	selaccno = self.selAccount();
        	accdata = accountList();
        	
        	if(selaccno != '' && selaccno != null && selaccno != undefined){
        	$(accdata).each(function(index, accnodet) {
        		
        		if(accnodet.codacctno == selaccno){
        			selectedAccount({ accno: accnodet.codacctno, displaytxt: accnodet.displaytxt, acctbalance: accnodet.acctbalance, fldFCDBRequestId: accnodet.fldFCDBRequestId, acctType: accnodet.acctType, checkbalance:accnodet.checkbalance});
        	    	
                	var currAccData = selectedAccount();
                    fldAcctNo = currAccData.accno;            
                    curraccbalval = currAccData.acctbalance;
                    acctType = currAccData.acctType;
                	fldAcctNo_txt = currAccData.displaytxt;
                	fldjsessionid = '';
                	// reqParams = {};
        	    	
        	    // reqParams["RQLoginUserId"] = "8000601010014200";
				// reqParams["RQDeviceFamily"] = "Android";
				// reqParams["RQDeviceFormat"] = Device_Model;
				// reqParams["RQOperationId"] = "ACCSTMINQ";
				// reqParams["RQClientAPIVer"] = RQClientAPIVer;
				// reqParams["SessionId"] = "1990009769OKYLLDDP";
				// reqParams["RQAccountNumber"] =fldAcctNo;
				// reqParams["RQFromDate"] = "";
				// reqParams["RQToDate"] = "";
				// reqParams["RQTRNTYPE"] = "All";
        	    	
        	    	// if(fldAcctNo != undefined){
        		    	// var invocationData = {
        		    			// adapter : "API_Adapter",
            	        		// procedure : "AccountDetails",
            	        		// parameters : [fldjsessionid,reqParams],
            	        		// compressResponse : true
        		    	// };
        		    	//WL.Logger.debug(invocationData, '');
        		    	
        		    	// WL.Client.invokeProcedure(invocationData, {
        		    		// onSuccess : self.accountStmtDetSubmitSuccess,
        		    		// onFailure : AdapterFail,
        		    	// });
                	// }else{
                		// busyInd.hide();		
                	// }
                	
        	    	//self.selAccount(fldAcctNo);
        	    	$("#acctType").html(acctType);
        	    	//$("#curraccbalval").html("Rs. "+curraccbalval);
        	    	self.curraccbalval(curraccbalval);
					curraccbalval1("Available Balance "+curraccbalval);
					curraccbalvalcheck(currAccData.checkbalance);
					curraccbalvalue = curraccbalvalcheck();
					busyInd.hide();
        		}
        	});
        	} busyInd.hide();
        };
        
        self.showAccountStatementData = function(){
        	$("#contentData").load("Views/Accounts/accountStatment.html", null, function (response, status, xhr) {
                if (status != "error") {}
                    ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
			busyInd.hide();
        };
        
        self.getAccountsList = function(){
	    	
			if(accountList().legth == 0){
			if(window.navigator.onLine){
	    	reqParams = {};
	    	
			reqParams["RQLoginUserId"] = LoggedInUserID;
			reqParams["RQDeviceFamily"] = Device_Platform;
			reqParams["RQDeviceFormat"] = Device_Model;
			reqParams["RQOperationId"] = "ACCSTMINQ";
			reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQTransSeq"] = "01";
    		busyInd.show();
    		fldjsessionid="";
	    	if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
	    			adapter : "API_Adapter",
	        		procedure : "AccountDetails",
	        		parameters : [fldjsessionid,reqParams],
	        		compressResponse : true
	    	};
	    	
	    	WL.Client.invokeProcedure(invocationData, {
	    		onSuccess : accountStmtSuccess,
	    		onFailure : AdapterFail,	    		
	    		timeout: timeout
	    	});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

    	}
		busyInd.hide();
    };
    
	//account statement show
       self.ViewAccountStatements = function(){
		
    	accstmtdata = accStmtData();
    	acctcurr="";
		
    	txndata = invocationResult.RBL.Response.txndetails.transaction;
    	accno = invocationResult.RBL.Response.acctdetails.acctno.AcctId;
    	acctype = invocationResult.RBL.Response.acctdetails.acctType;
    	txntype = invocationResult.RBL.Response.txndetails.transaction.txndesc;
    	//acctcurr = invocationResult.RBL.Response.AcctCurr;
    	// if(typeof(invocationResult.RBL.Response.AcctCurr)=='object'){
    		// acctcurr = invocationResult.RBL.Response.AcctCurr[0];
    		
    	// };"balaftertxn": {
                           // "amountValue":
		//busyInd.show();
    	closingbalance = invocationResult.RBL.Response.acctdetails.currency;
    	fromdt = invocationResult.RBL.Response.fromdate;        	
    	todate = invocationResult.RBL.Response.todate;	
    	period = fromdt+" - "+todate;
    	
    	self.accountStmtTxns.removeAll();
    	$(txndata).each(function(index, obj) {
    		
    		if(obj.codedrcr == 'C')
    			amtLbl = 'Deposit';
    		else
    			amtLbl = 'Withdrawal';
    		
    		self.accountStmtTxns.push({ dattxn:getFormattedDate(obj.txndate), amttxn: formatAmt(parseFloat(obj.txnamt)), amtLbl: amtLbl, refchqno: obj.refchqno, txttxndesc: obj.txndesc, datvalue:getFormattedDate(obj.valuedate), balaftertxn: formatAmt(parseFloat(obj.balaftertxn)), txntype:txntype });
    	});
    	
    	if(acctype == 'SAVING' || acctype == 'Saving' || acctype == 'saving'){
    		acctypeLabel = "Savings Account No.";
    		savingAccno = invocationResult.RBL.Response.acctdetails.acctno;
    	}else{
    		acctypeLabel = "Current Account No.";
    		savingAccno = invocationResult.RBL.Response.acctdetails.acctno;
    	}
    	busyInd.hide();
    	if (status != "error") {}	
        $("#divaccno").html("");
     	//$("#accPeriod").html(period);
     	$("#acctypeLabel").html(acctypeLabel);
     	$("#savingAccno").html(savingAccno);
     	$("#acctType").html(acctype);
		busyInd.hide();
     	if(self.accountStmtTxns().length === 0){
     		$("#accExitsMsg").show();
     	}else{
			busyInd.hide();
     		$("#accExitsMsg").hide();
     	}
		busyInd.hide();
     	$("#closingbalval").html(closingbalance);
		
    };

		
		
    self.ViewCheckStatements = function(){
		self.selAccount(null);
		//busyInd.show();        	
    	if(accountList().legth == 0){
			if(window.navigator.onLine){
				busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "VIWCHQSTS";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetAccounts",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : accountStmtSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

    	}
		 //busyInd.hide();
	};
	
	accountStmtSuccess = function(result){
    	//busyInd.show();
    	invocationResult = result.invocationResult;
    	if(invocationResult.isSuccessful) {
    		if(invocationResult.RBL){	
    		if(invocationResult.RBL.Response!==""){
    			custdtls = "";
    			//if(invocationResult.RBL.Response.CustDetails)
    			custdtls = invocationResult.RBL.Response.CustDetails;
				itemdata = invocationResult.RBL.Response.acctdetails;
			    nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
			    nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;	
				totAccount = parseInt(nbrofsavingacc) + parseInt(nbrofcurrentacc);
	    		
	    		if(window.location.hash == '#rrftr02'){
	    			if(totAccount <= 1){
		    			$("#accExitsMsg").show();
						$("#fndtransfer").hide();
		    		}else{
		    			$("#accExitsMsg").hide();
						$("#fndtransfer").show();
		    		}
	    		}else{
		    		if(totAccount > 0){
		    			$("#accExitsMsg").hide();
						$("#fndtransfer").show();
		    		}else{
		    			$("#accExitsMsg").show();
						$("#fndtransfer").hide();
		    		}
	    		}
	    		accountList.removeAll();
	    		var idx = 1;
	    		$(itemdata).each(function(index, obj) {
	    			strid = "item"+idx;
	    			custnames = "";
	    			if(window.location.hash == '#rrftr02')
	    				displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
	    			else
	    				displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
	    			
	    			if(custdtls != ''){
	    				$(custdtls).each(function(j, obj1) {
	    	    			
    	    				if(obj.acctindex == obj1.acctindex){
    	    					if(obj1.custRel == 'SOW' || obj1.custRel == 'JOF' || obj1.custRel == 'JOO' || obj1.custRel == 'AUS'){
    	    						custnames += obj1.userName+"  ";
    	    					}
    	    				}
    	    			});
	    			}
	    			
	    			accountValue = $.trim(obj.acctno)+"#"+obj.namccyshrt+"#"+$.trim(obj.acctbalance);
	    		   
					 accountList.push({ codacctno: obj.acctno, acctType: obj.acctType, acctbalance: "₹ " +formatAmt(parseFloat(obj.acctbalance)), acctbranch: obj.acctbranch, custnames: custnames, namccyshrt: obj.namccyshrt, strid:strid, displaytxt:displaytxt,accountValue: accountValue });
	    		    idx++;
	    		}); 
	    		
    		}else{
					busyInd.hide();
					handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
    	 }else{
				busyInd.hide();
				handleErrorNoResponse();
			}
    	}
    	busyInd.hide();
    };
	
    
	//ACcount statement submit
    accountStmtSubmit = function(){
	if(window.navigator.onLine){
    	if($("#frmsin01").valid()){
			busyInd.show();
        	fldAcctNo = self.selAccount();
        	fldAcctNo_txt = $("#fldAcctNo option:selected").text();
            fldTxnType = $("#fldTxnType").val();
        	fldTxnType_txt = $("#fldTxnType option:selected").text();
        	fldNbrStmt = $("#fldNbrStmt").val();
        	fldNbrStmt_txt = $("#fldNbrStmt option:selected").text();
			
			reqParams = {};
			reqParams["RQLoginUserId"] = LoggedInUserID;
			reqParams["RQDeviceFamily"] = Device_Platform;
			reqParams["RQDeviceFormat"] = Device_Model;
			reqParams["RQOperationId"] = "ACCSTMINQ";
			reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQAccountNumber"] = fldAcctNo;
			reqParams["RQTRNTYPE"] = "All";
		    reqParams["RQTransSeq"] = "02";
	    	
        	fldjsessionid="";
	    	if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
	    			adapter : "API_Adapter",
	        		procedure : "RRSIN02",
	        		parameters : [fldjsessionid,reqParams],
	        		compressResponse : true
	    	};
	    	WL.Logger.debug(invocationData, '');
	    	
	    	WL.Client.invokeProcedure(invocationData, {
	    		onSuccess : accountStmtSubmitSuccess,
	    		onFailure : AdapterFail,	    		
	    		timeout: timeout
	    	});
    	}
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

    };
    
    
  //Single ACcount statement submit
    self.accountStmtSubmitSingle = function(){
    	if(window.navigator.onLine){
    			busyInd.show();
    			
    			reqParams = {};
    			reqParams["RQLoginUserId"] = LoggedInUserID;
    			reqParams["RQDeviceFamily"] = Device_Platform;
    			reqParams["RQDeviceFormat"] = Device_Model;
    			reqParams["RQOperationId"] = "ACCSTMINQ";
    			reqParams["RQClientAPIVer"] = RQClientAPIVer;
    			reqParams["SessionId"] = CurrentSessionId;
    			reqParams["RQAccountNumber"] = fldAcctNo;
    			reqParams["RQTRNTYPE"] = "All";
    		    reqParams["RQTransSeq"] = "02";
    	    	
            	fldjsessionid="";
    	    	if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
    	    			adapter : "API_Adapter",
    	        		procedure : "RRSIN02",
    	        		parameters : [fldjsessionid,reqParams],
    	        		compressResponse : true
    	    	};
    	    	WL.Logger.debug(invocationData, '');
    	    	
    	    	WL.Client.invokeProcedure(invocationData, {
    	    		onSuccess : accountStmtSubmitSuccess,
    	    		onFailure : AdapterFail,	    		
    	    		timeout: timeout
    	    	});
    		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
    		}

        };

    
    
    accountStmtSubmitSuccess = function(result){
    	busyInd.hide();
    	invocationResult = result.invocationResult;
		
    	if(invocationResult.isSuccessful) {
			if(invocationResult.RBL){
				if(invocationResult.RBL.Response){
					if(invocationResult.RBL.STATUS.CODE == '0'){
						busyInd.hide();
						accStmtData(invocationResult.RBL);    			
						window.location = "#rrsin02";
					}
					else{
							busyInd.hide();
							navigator.notification.alert(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
					}
					//busyInd.hide();
				}else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
			}else{
        		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
        	}
    	}
		busyInd.hide();
    };
	
    /*-Between my accounts start----*/
   this.callrrftr02 = function(){
	//busyInd.show();
		//if(accountList().length == 0){	
		if(window.navigator.onLine){
			reqParams = {};
			reqParams["RQLoginUserId"] = LoggedInUserID;
			reqParams["RQDeviceFamily"] =Device_Platform;
			reqParams["RQDeviceFormat"] =Device_Model;
			reqParams["RQOperationId"] ="SLFFNDTFR";
			reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQTransSeq"] = "01";
//			busyInd.show();
			customBusyShow();
			fldjsessionid = "";
			if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetAccounts",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
			};
			
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : callrrftr02Success,
				onFailure : AdapterFail,	    		
				timeout: timeout
			});
    	//}
			$("#contentData").load("Views/Accounts/rrftr02.html", null, function (response, status, xhr) {
				if (status != "error") {}
				 ko.applyBindings(self, $(".dynamic-page-content").get(0)); 
				//busyInd.hide();  
			});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	//busyInd.hide();
    };
    
    
    
    callrrftr02Success = function(result){
    	//busyInd.show();
    	invocationResult = result.invocationResult;
    	if(invocationResult.isSuccessful) {
    		if(invocationResult.RBL){	
    		if(invocationResult.RBL.Response!==""){
    			custdtls = "";
    			//if(invocationResult.RBL.Response.CustDetails)
    			custdtls = invocationResult.RBL.Response.CustDetails;
				itemdata = invocationResult.RBL.Response.acctdetails;
			    nbrofsavingacc = invocationResult.RBL.Response.savingacctcount;
			    nbrofcurrentacc = invocationResult.RBL.Response.currentacctcount;	
				totAccount = parseInt(nbrofsavingacc) + parseInt(nbrofcurrentacc);
	    		
	    		if(window.location.hash == '#rrftr02'){
	    			if(totAccount <= 1){
		    			$("#accExitsMsg").show();
						$("#fndtransfer").hide();
		    		}else{
		    			$("#accExitsMsg").hide();
						$("#fndtransfer").show();
		    		}
	    		}else{
		    		if(totAccount > 0){
		    			$("#accExitsMsg").hide();
						$("#fndtransfer").show();
		    		}else{
		    			$("#accExitsMsg").show();
						$("#fndtransfer").hide();
		    		}
	    		}
	    		accountList.removeAll();
	    		var idx = 1;
	    		$(itemdata).each(function(index, obj) {
	    			strid = "item"+idx;
	    			custnames = "";
	    			if(window.location.hash == '#rrftr02')
	    				displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
	    			else
	    				displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
	    			
	    			if(custdtls != ''){
	    				$(custdtls).each(function(j, obj1) {
	    	    			
    	    				if(obj.acctindex == obj1.acctindex){
    	    					if(obj1.custRel == 'SOW' || obj1.custRel == 'JOF' || obj1.custRel == 'JOO' || obj1.custRel == 'AUS'){
    	    						custnames += obj1.userName+"  ";
    	    					}
    	    				}
    	    			});
	    			}
	    			
	    			accountValue = $.trim(obj.acctno)+"#"+obj.namccyshrt+"#"+$.trim(obj.acctbalance);
	    		   
					 accountList.push({ codacctno: obj.acctno, acctType: obj.acctType, acctbalance: "₹ " +formatAmt(parseFloat(obj.acctbalance)), acctbranch: obj.acctbranch, custnames: custnames, namccyshrt: obj.namccyshrt, strid:strid, displaytxt:displaytxt,accountValue: accountValue });
	    		    idx++;
	    		}); 
	    		
    		}else{
//					busyInd.hide();
    				customBusyHide();
					handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
    	 }else{
//				busyInd.hide();
    		 	customBusyHide();
				handleErrorNoResponse();
			}
    	}
//    	busyInd.hide();
    	customBusyHide();
    };
    
    
    
    
    
    this.rrftr02Submit = function(){
		if($("#frmftr02").valid()){
			$('#confirm_button1').prop('disabled', true);
			//fldLoginUserId = WL.Client.getUserPref("loggeduserid");
			fldFromAcctNo = $.trim(self.fldFromAcctNo());
			fldFromAcctNo_txt = $("#fldFromAcctNo option:selected").text();
			beneffrom_accnt_no(fldFromAcctNo_txt.split("-")[0]);
			fldToAcctNo = $.trim(self.fldToAcctNo());
			fldToAcctNo_txt = $("#fldToAcctNo option:selected").text();
			benefno_acct_no(fldToAcctNo_txt.split("-")[0]);
			benef_amnt($("#fldAmtTxn").val()); 
			benefdesc($('#fldTxnDesc').val());
			GetOTP();
			busyInd.hide();
		}
    	busyInd.hide();
    }; 
    
    this.callrrftr03 = function(){
	    $("#contentData").load("Views/Accounts/rrftr03.html", null, function (response, status, xhr) {
            //if (status != "error") {}	
			OTP_number = invocationResult1.RBL.Response.otprefno;
			$('#OTP_Ref').val(OTP_number);
			if(invocationResult1.RBL.Response.otprequired == 'Y'){
				$('#OTP_No').show();
			}
			else{
				$('#OTP_No').hide();
			}
				fldAcctNo = beneffrom_accnt_no();
				$("#fldfromacctno").html(fldAcctNo);
				$("#fldtoacctno").html(benefno_acct_no());
				$("#fldamttxn").html("₹ "+formatAmt(parseFloat(benef_amnt())));
				$("#fldamttxnamnt").html(benef_amnt());
				$("#fldTxnDesc").html(benefdesc());
          
            ko.applyBindings(self, $(".dynamic-page-content").get(0));
			busyInd.hide();			
        });
	    busyInd.hide();
    };
    
    //between my accounts submit
    this.rrftr03Submit = function(){
		if(window.navigator.onLine){
    	if($("#frmReqAccStmtConf").valid()){
    	    reqParams = {};
			reqParams["RQLoginUserId"] = LoggedInUserID;
        	reqParams["RQDeviceFamily"] = Device_Platform;
        	reqParams["RQDeviceFormat"] = Device_Model;
        	reqParams["RQOperationId"] = "SLFFNDTFR";
        	reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
        	reqParams["RQFromAcctNo"] = $("#fldfromacctno").text();
    	    reqParams["RQToAcctNo"] = $("#fldtoacctno").text();  
    	    reqParams["RQDesc"] = $("#fldTxnDesc").text(); 
			reqParams["RQTxnAmount"] = $('#fldamttxnamnt').text();
			reqParams["RQOTP"] = $('#OTP_No_Val').val();
			reqParams["RQRefNo"] = $('#OTP_Ref').val();
			reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
	    	reqParams["RQTransSeq"] = "03";
			fldjsessionid="";
//			busyInd.show();
			customBusyShow();
			if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
    			adapter : "API_Adapter",
        		procedure : "RRFTR03",
        		parameters : [fldjsessionid,reqParams],
        		compressResponse : true
			};
    	
			WL.Logger.debug(invocationData, '');
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : self.rrftr04Response,
				onFailure : AdapterFail,
				timeout: timeout
			});
    	
    	}
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		//busyInd.hide();
    };
	
    this.rrftr04Response = function(result){
    	invocationResult = result.invocationResult;
		if(invocationResult.isSuccessful) {
			if(invocationResult.RBL.Response){
				$("#contentData").load("Views/Accounts/rrftr04.html", null, function (response, status, xhr) {
					
						
						if(invocationResult.RBL.STATUS.CODE == '0'){
								$('.success_msg').show();
								$("#tpn06success").show();
//								busyInd.hide();
								customBusyHide();
						}
						else{
							$("#tpn06success").hide();
							$('.rsaOOBErr').show();
							$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
//							busyInd.hide();
							customBusyHide();
							accountList.removeAll();
						}
						referenceno = invocationResult.RBL.Response.txnrefno;
						if(referenceno != ''){
								$(".clsacctno").html(invocationResult.RBL.Response.fromacctno);
								$(".clsreferenceno").html(referenceno);
								$("#txnStatus").html(invocationResult.RBL.STATUS.MESSAGE);
								$(".clsbenefacctno").html(invocationResult.RBL.Response.toacctno);
							   
								$(".clstxnamount").html("₹ "+formatAmt(parseFloat(invocationResult.RBL.Response.amttxn)));
								$(".clstxndesc").html(benefdesc());
						}
					
				}); 
			}else{
//						busyInd.hide();
						customBusyHide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
		}
//		busyInd.hide();
		customBusyHide();
    };

	
    /*----end----*/
    
	
	//check book//
    self.CheckBReq = function(){
		self.selAccount(null);
    	
		busyInd.show();        	
    	if(accountList().legth == 0)
		{
				// reqParams = {};
    
				// reqParams["RQLoginUserId"] = "8000601010014200";
				// reqParams["RQDeviceFamily"] = Device_Platform;
				// reqParams["RQDeviceFormat"] = Device_Model;
				// reqParams["RQOperationId"] = "RQCHQBOOK";
				// reqParams["RQAcctNo"] = $('#').val();
				// reqParams["RQClientAPIVer"] = RQClientAPIVer;
				// reqParams["SessionId"] = "1990009769OKYLLDDP";
				// reqParams["RQTransSeq"] = "01";
    	        // fldjsessionid="";
				// var invocationData = {
					// adapter : "API_Adapter",
					// procedure : "GetCheqbk",
					// parameters : [fldjsessionid,reqParams],
					// compressResponse : true
				// };
    	
				// WL.Logger.debug(invocationData, '');
				// WL.Client.invokeProcedure(invocationData, {
					// onSuccess : accountStmtSuccess,
					// onFailure : AdapterFail,
					// timeout: timeout
				// });
				
		}
    	
		$("#contentData").load("Views/Accounts/rrcbr02.html", null, function (response, status, xhr) {
					if (status != "error") {}	                
					ko.applyBindings(self, $(".dynamic-page-content").get(0));
					busyInd.hide();					
		});
		//busyInd.hide();
	};
	
	//cheque book request
    self.chequeBookReqConfirm = function(){
		if(window.navigator.onLine){
        if($("#frmReqAccStmt").valid()){
			busyInd.show();
    		fldFromAcctNo_txt = $("#fldAcctNo option:selected").text();
			var temp = fldFromAcctNo_txt.split("-")[0];

				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "RQCHQBOOK";
				reqParams["RQAcctNo"] = temp;
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
    	        
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetCheqbk",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrcbr03Response,
					onFailure : AdapterFail,
					timeout: timeout
				});
		}
		else{busyInd.hide();}
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

	};
	
	//cheque book request Single Account
    self.chequeBookReqConfirmSingle = function(){
		if(window.navigator.onLine){
			busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "RQCHQBOOK";
				reqParams["RQAcctNo"] = fldAcctNo;
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
    	        
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetCheqbk",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrcbr03Response,
					onFailure : AdapterFail,
					timeout: timeout
				});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

	};
			
    self.CheckBReqCmfrmsub = function(){
			
			accstmtdata = accStmtData();
				
			$("#contentData").load("Views/Accounts/rrcbr03.html", null, function (response, status, xhr) {
				if (status != "error") {}	
				if(invocationResult.RBL.STATUS.CODE=="0"){
					$("#fldacctnodet").html(invocationResult.RBL.Response.acctno);
					$("#fldacctno").val(invocationResult.RBL.Response.acctno);
					$("#fldaddr1").html(invocationResult.RBL.Response.addr1);
					$("#fldaddr2").html(invocationResult.RBL.Response.addr2);
					$("#fldaddr3").html(invocationResult.RBL.Response.addr3);
					$("#fldcustcity").html(invocationResult.RBL.Response.city);
					$("#fldcuststate").html(invocationResult.RBL.Response.state);
					$("#fldzip").html(invocationResult.RBL.Response.zipcode);
					// $("#fldRequestId").val(fldFCDBRequestId);
					$("#fldAcctDetail").val(invocationResult.RBL.Response.customername);
					$("#fldcntry").html(invocationResult.RBL.Response.country);
				}
				else{
						$("#submitrequest").hide();
						$('#frmReqAccStmtConf').hide();
    	                $(".rsaOOBErr").show();
    	                $(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
				}
          
				ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
			});
		busyInd.hide();   
	};
			
    self.CheckBReqCmfrmsubSucces = function(){
				accstmtdata = accStmtData();
	      
				codtxnrefno = accstmtdata.Response.txnrefno; 
				fldAcctDetail = accstmtdata.RequestParams.RQAcctNo;
				fldaddr1 = accstmtdata.Response.addr1;
				fldaddr2 = accstmtdata.Response.addr2;
				fldaddr3 = accstmtdata.Response.addr3;
				fldcustcity = accstmtdata.Response.city;
				fldcuststate = accstmtdata.Response.state;
				fldzip = accstmtdata.Response.zipcode;
				fldcntry = accstmtdata.Response.country;
    	
				$("#contentData").load("Views/Accounts/rrcbr04.html", null, function (response, status, xhr) {
					if (status != "error") {}	
					if(invocationResult.RBL.STATUS.CODE == '0'){
						$('.success_msg').show();
						$("#codtxnrefno").html(codtxnrefno);
						$("#fldAcctDetail").html(fldAcctDetail);
						$("#fldaddr1").html(fldaddr1);
						$("#fldaddr2").html(fldaddr2);
						$("#fldaddr3").html(fldaddr3);
						$("#fldcustcity").html(fldcustcity);
						$("#fldcuststate").html(fldcuststate);
						$("#fldzip").html(fldzip);
						$("#fldcntry").html(fldcntry);
					}
					else{
						$('.summblock').hide();
						$('.rsaOOBErr').show();
						$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
					}
					ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
				});
				busyInd.hide();   
			};
			
    rrcbr03Response = function(result){
				busyInd.hide();
				invocationResult = result.invocationResult;
				if(invocationResult.isSuccessful) {
					if(invocationResult.RBL.Response){	
						//if(invocationResult.RBL.Response.rc.returncode == 0){
							accStmtData(invocationResult.RBL);    			
							window.location = "#rrcbr03";
						//}else{
							//handleError(invocationResult.faml.response);
						//}
					}
				}
	};
    
    self.reqChequeBookConfirmSubmit = function(){
			if(window.navigator.onLine){
				if($("#frmReqAccStmtConf").valid()){
				busyInd.show();        	
				fldChecked = $("#fldChecked").val();
				fldChecked_txt = $("#fldChecked option:selected").text();
				fldAcctNo = $("#fldacctno").val();
				fldcustcity = $("#fldcustcity").html();
				fldaddr1 = $("#fldaddr1").html();
				fldaddr2 = $("#fldaddr2").html();
				fldaddr3 = $("#fldaddr3").html();
				fldcuststate = $("#fldcuststate").html();
				fldcntry = $("#fldcntry").html();
				fldzip = $("#fldzip").html();
 	
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "RQCHQBOOK";
				reqParams["RQAcctNo"] = fldAcctNo;
				reqParams["RQAddr1"] = fldaddr1;
				reqParams["RQAddr2"] = fldaddr2;
				reqParams["RQAddr3"] = fldaddr3;
				reqParams["RQCity"] = fldcustcity;
				reqParams["RQState"] = fldcuststate;
				reqParams["RQCountry"] = fldcntry;
				reqParams["RQZipcode"] = fldzip;
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] ="02";
    	
    	         fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetChequebookSubmit",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrcbr04Response,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
				
	};  
    
    rrcbr04Response = function(result){
			busyInd.hide();
				invocationResult = result.invocationResult;
				if(invocationResult.isSuccessful) {
					if(invocationResult.RBL.Response){	
						//if(invocationResult.faml.response.rc.returncode == 0){
							accStmtData(invocationResult.RBL);    			
							window.location = "#rrcbr04";
						//}else{
						//handleError(invocationResult.faml.response);
						//}
					}else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
					}
				}
			};
			
    self.StopPaYmntcheck = function(){
				self.selAccount(null);
				//busyInd.show();
				$("#contentData").load("Views/Accounts/rrsch01.html", null, function (response, status, xhr) {
					if (status != "error") {}	                
					ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
				});
				 busyInd.hide();
	};
			
    self.rrsch01Submit = function(){
       
		if($("#frmstopcheque").valid()){
			stopChqReason($('#fldChqStopRsn').val().split("#")[1]);
			stopChqReasoncode($('#fldChqStopRsn').val().split("#")[0]);
			startingChqNo($('#fldChqStrNo').val());
			endingChqNo($('#fldChqEndNo').val());
			benefno_acct_no($('#fldAcctNo').val());
			window.location = "#rrsch02";
		}
		 busyInd.hide();
	};
    
			
    self.rrsch02Submit = function(){
	if(window.navigator.onLine){
				fldAcctNo = $("#fldacctno").html();
				fldChqEndNo = $(".endchqno").html();
				fldChqStrNo = $(".startchqno").html();
				fldChqStopRsn = $(".reasoncode").html();
				reqParams = {};
				busyInd.show();
				reqParams["RQTransSeq"] = "02";
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "STPCHQPAY";
				reqParams["RQAcctNo"] = fldAcctNo;
				reqParams["RQStartChequeNo"] = fldChqStrNo;
				reqParams["RQReason"] = fldChqStopRsn;
		        fldjsessionid="";
    	
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "ChequeCancel",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrsch03Response,
					onFailure : AdapterFail,
					timeout: timeout
				});
				 //busyInd.hide();
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

	};
			
    rrsch03Response = function(result){
				//busyInd.hide();
				invocationResult = result.invocationResult;
				if(invocationResult.isSuccessful) {
					if(invocationResult.RBL.Response){	
						//if(invocationResult.faml.response.rc.returncode == 0){
							accStmtData(invocationResult.RBL);    			
							window.location = "#rrsch03";
						}else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
					}
				// }
		busyInd.hide();
	};
			
    this.StopPaYmntcheckRes = function(){
				//accstmtdata = accStmtData();
    	$("#fldfromacctno").html(beneffrom_accnt_no());
		
				fldacctno = benefno_acct_no();
				fldChqStrNo = startingChqNo();
				fldChqEndNo = endingChqNo();
				fldchqstoprsn = stopChqReason();
				fldchqstoprsncode = stopChqReasoncode();
    	
				$("#contentData").load("Views/Accounts/rrsch02.html", null, function (response, status, xhr) {
					if (status != "error") {}	
            
					$("#fldacctno").html(fldacctno);
				
					if(fldChqStrNo != fldChqEndNo){
						$(".startchqno").html(fldChqStrNo);
						$(".endchqno").html(fldChqEndNo);
						$(".chqreason").html(fldchqstoprsn);
						$(".reasoncode").html(fldchqstoprsncode);
						if(fldChqEndNo != '')
							$("#EndCheque").show();
						else{
							$(".startchqlbl").html("Cheque number");
							$("#EndCheque").hide();
							$("#resonclass").removeClass( "odd" ).addClass( "even" );
						}
					}else{
						$("#EndCheque").hide();
						$("#resonclass").removeClass( "odd" ).addClass( "even" );
						$(".startchqno").html(fldChqStrNo);
						$(".startchqlbl").html("Cheque No");
						$(".chqreason").html(fldchqstoprsn);
						$(".reasoncode").html(fldchqstoprsncode);
					}
          
					ko.applyBindings(self, $(".dynamic-page-content").get(0));
					busyInd.hide();   
				});
				 busyInd.hide();
			};
			
    self.StopPaYmntcheckRes3 = function(){
				accstmtdata = accStmtData();
				fldacctno = accstmtdata.Response.acctno;
				fldChqStrNo = accstmtdata.Response.startchequeno;
				fldChqEndNo = accstmtdata.Response.endchequeno;
				fldchqstoprsn = accstmtdata.Response.chqstopreason;
				//fldchqstoprsn = stopChqReasoncode();
				fldAcctDetail = accstmtdata.Response.acctno;        
				codtxnrefno = accstmtdata.Response.txnrefno;
				var stoprsn="";
				if(fldchqstoprsn == "001"){ var stoprsn = "CHEQUE LOST";}
				if(fldchqstoprsn == "013"){var stoprsn = "CHEQUE BOOK STOLEN";}
				if(fldchqstoprsn == "010"){var stoprsn = "STOP PAYMENT";}

				
				$("#contentData").load("Views/Accounts/rrsch03.html", null, function (response, status, xhr) {
					if (status != "error") {}	
            
					$("#fldacctno").html(fldacctno);
					$("#codtxnrefno").html(codtxnrefno);
					
					if(invocationResult.RBL.STATUS.CODE =="0"){
						//$(".success_msg p").html(successmessage);
						$(".success_msg").show();
					}
					else{
						$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
						$(".rsaOOBErr").show();
					}
					//if(fldChqStrNo != fldChqEndNo){
						$(".startchqno").html(fldChqStrNo);
						$(".endchqno").html(fldChqEndNo);
						$(".chqreason").html(stoprsn);
						$(".chqreason").html(accstmtdata.RequestParams.chqstopreason);
						//if(fldChqEndNo != '')
						//	$("#EndCheque").show();
						//else{
						//	$(".startchqlbl").html("Cheque No.");
							$("#EndCheque").hide();
						//	$("#resonclass").removeClass( "even" ).addClass( "odd" );
						//}
					//}else{
					//	$("#EndCheque").hide();
					//	$("#resonclass").removeClass( "even" ).addClass( "odd" );
					//	$(".startchqno").html(fldChqStrNo);
					//	$(".startchqlbl").html("Cheque No.");
					//	$(".chqreason").html(stoprsn);
					//}
					$("#fldAcctDetail").val(fldAcctDetail);
          
					ko.applyBindings(self, $(".dynamic-page-content").get(0));
					busyInd.hide();					
				});
				 busyInd.hide();
			};
			
		//cheque status	
		self.rrcsi01Submit = function(){
		if(window.navigator.onLine){
			if($("#frmchequestatus").valid()){
				busyInd.show();        	
    	
				CodAcctNo = $.trim(self.selAccount());
				fldChequeNo = $("#fldChequeNo").val();
				beneffrom_accnt_no(CodAcctNo);
				benefCheqNO(fldChequeNo);
				fldjsessionid="";

				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "VIWCHQSTS";
				reqParams["RQAcctNo"] = CodAcctNo ; 
				reqParams["RQChequeNo"] = fldChequeNo;
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "02";
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetChequeStatus",
					parameters : [fldjsessionid, reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrcsi02Response,
					onFailure : AdapterFail,
					timeout: timeout
				});
				}
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
    	
			};
			
    rrcsi02Response = function(result){
				 
				invocationResult = result.invocationResult;
				if(invocationResult.isSuccessful) {
						if(invocationResult.RBL.Response){	
    			
							if(window.location.hash == "#rrcsi02"){
								if(invocationResult.RBL.STATUS.CODE=="0"){
									$('.summblock').show();
									$(".rsaOOBErr").hide();
									codacctno = beneffrom_accnt_no();
									chequestatus = invocationResult.RBL.Response.chequestatus;
									chequeno = benefCheqNO();         	
					
									$("#codacctno").html(codacctno);
									$("#chequeno").html(chequeno);
									$("#chequestatus").html(chequestatus);
								}
								else{$('.summblock').hide();
									$(".rsaOOBErr").show();
									$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
								}
	                
							}else{
								accStmtData(invocationResult.RBL);
								busyInd.hide();								
								window.location = "#rrcsi02";
							}
						}else{
								busyInd.hide();
								handleError(invocationResult.RBL.Response, invocationResult.RBL);
						}
					
				}				
				busyInd.hide();
	};
	
    self.GetCheckStatus = function(){
					accstmtdata = accStmtData();
					if(accstmtdata.STATUS.CODE=="0"){
						codacctno = beneffrom_accnt_no();
						chequestatus = accstmtdata.Response.chequestatus;
						chequeno = benefCheqNO();        	
						$("#codacctno").html(codacctno);
						$("#chequeno").html(chequeno);
						$("#chequestatus").html(chequestatus);
						busyInd.hide();
					}
					else{
						$('.summblock').hide();
						$(".rsaOOBErr").show();
    	                $(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);	
					}
					busyInd.hide();
	};
			
         //OTP	
		GetOTP = function(){
		if(window.navigator.onLine){
			 busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "OTPGENREQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				
				//busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetOTP",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : GetOTPResponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
				}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}

		};	
	   
		GetOTPResponse = function(result){
			invocationResult1 = result.invocationResult;
				if(invocationResult1.isSuccessful) {
					if(invocationResult1.RBL.Response){	
						if(window.location.hash == "#rrftr02"){
							busyInd.hide();
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
							window.location = '#rrftr03';
						}
						else if(window.location.hash == "#FD_opening"){
							OTP_number = invocationResult1.RBL.Response.otprefno;
							fdConfirm(OTP_number);
						}
						else if(window.location.hash == "#RD_opening"){
							OTP_number = invocationResult1.RBL.Response.otprefno;
							rdConfirm(OTP_number);
						}
						else{
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
						}
					}else{
							busyInd.hide();
							handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
					}		
				}
			busyInd.hide();
		};
		
		self.getFDAccountsList = function(){
			if(window.navigator.onLine){
				//$('#confirm_button1').prop('disabled', true);
				busyInd.show();
				var customerID = LoggedInUserID;
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "FDgetAllAccounts",
					parameters : [customerID]
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : FDAcntsSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		
		FDAcntsSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getAllAccountsResponse){
				if(invocationResult.getAllAccountsResponse.numAccounts > 0){
				if(invocationResult.getAllAccountsResponse.accountsArray){
					busyInd.hide();
					FDaccountList.removeAll();
					var idx = 1;
					itemdata = invocationResult.getAllAccountsResponse.accountsArray.account;
					
						$(itemdata).each(function(index, obj) {
							strid = "item"+idx;
							displaytxt = $.trim(obj.accountNo)+"-"+obj.branchName;
							accountValue = $.trim(obj.accountNo);
							FDaccountList.push({ codacctno: obj.accountNo, acctbranch: obj.branchName, strid:strid, displaytxt:displaytxt,accountValue: accountValue });
							idx++;
						});
						window.location.hash = '#FD_Accounts';
				}
				
				else if(invocationResult.Fault){
					busyInd.hide();
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
				}else{
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
				}
				else{
					busyInd.hide();
					nofd = 'true';
					window.location.hash = '#FD_Accounts';
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		fdacntsSubmit = function(){ 
			if(window.navigator.onLine){
				if($("#frmfdacnts").valid()){
					//$('#confirm_button1').prop('disabled', true);
					busyInd.show();
					var customerID = LoggedInUserID;
					acntnumber = $("#fdAcctNo option:selected").val();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "FDgetAllAccountsDetails",
						parameters : [customerID,acntnumber]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : FDdetailsSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		FDdetailsSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getAccountDetailResponse){
				if(invocationResult.getAccountDetailResponse){
					busyInd.hide();
					FDaccountDetails.removeAll();
					var idx = 1;
					itemdata = invocationResult.getAccountDetailResponse;
						$(itemdata).each(function(index, obj) {
							
							dateformatting(obj.openingDate);
							openingDate = dateformat; 
							dateformatting(obj.maturityDate);
							maturityDate = dateformat;
							if(obj.currentDepositAmount){
								balance = "₹ " +formatAmt(parseFloat(obj.currentDepositAmount));
							}
							else{
								balance = '-';
							}
							interest = obj.netInterestRate+"% (p.a)";
							accountTermMonth = obj.accountTerm.months;
							accountTermDays = obj.accountTerm.days;
							maturityAmount = "₹ " +formatAmt(parseFloat(obj.maturityAmount));
							currentDepositAmount = "₹ " +formatAmt(parseFloat(obj.currentDepositAmount));
							intialDepositAmount = "₹ " +formatAmt(parseFloat(obj.intialDepositAmount));
							Tenure = accountTermMonth+" Months "+accountTermDays+" Days";
							branch = obj.branchName;
							branch = branch.toLowerCase();
							branch1 = branch.substr('0','1').toUpperCase();
							branch2 = branch.slice('1');
							branchname = branch1+branch2;
							FDaccountDetails.push({acctnumber:obj.accountNo, openingDate:openingDate, maturityAmnt:maturityAmount, maturityDate:maturityDate, branch:branchname, currency:obj.currencyCode, depositDate:'-', intialDepositAmount:intialDepositAmount, interest:interest,currentDepositAmount:currentDepositAmount,accountTermMonth:accountTermMonth,accountTermDays:accountTermDays, Tenure:Tenure,balance:balance});
							// idx++;
						});
						window.location.hash = '#FD_summary';
				}
				else if(invocationResult.Fault){
					busyInd.hide();
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
				}else{
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
			else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			

		};
		
		self.getRDAccountsList = function(){
			if(window.navigator.onLine){
				//$('#confirm_button1').prop('disabled', true);
				busyInd.show();
				var customerID = LoggedInUserID;
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "RDgetAllAccounts",
					parameters : [customerID]
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : RDAcntsSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		RDAcntsSuccess = function(result){
			invocationResult = result.invocationResult;
			try{
			invocationResult1 = invocationResult.text;
			invocationResult2 = JSON.parse(invocationResult1);
			if(invocationResult2.getAllAccountsResponse){
				if(invocationResult2.getAllAccountsResponse.numAccounts > 0){
				if(invocationResult2.getAllAccountsResponse.accountsArray){
					busyInd.hide();
					RDaccountList.removeAll();
					var idx = 1;
					itemdata = invocationResult2.getAllAccountsResponse.accountsArray.account;
					
						$(itemdata).each(function(index, obj) {
							strid = "item"+idx;
							displaytxt = $.trim(obj.accountNo)+"-"+obj.branchName;
							accountValue = $.trim(obj.accountNo);
							RDaccountList.push({ codacctno: obj.accountNo, acctbranch: obj.branchName, strid:strid, displaytxt:displaytxt,accountValue: accountValue });
							idx++;
						});
						window.location.hash = '#RD_Accounts';
				}
				else if(invocationResult2.Fault){
					busyInd.hide();
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult2.Fault.Reason.Text);
				}else{
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
				}
				else{
					busyInd.hide();
					nord = 'true';
					window.location.hash = '#RD_Accounts';
				}
			}else if(invocationResult2.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult2.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			}
			catch(e){
				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		rdacntsSubmit = function(){ 
			if(window.navigator.onLine){
				if($("#frmrdacnts").valid()){
					//$('#confirm_button1').prop('disabled', true);
					busyInd.show();
					var customerID = LoggedInUserID;
					acntnumber = $("#fdAcctNo option:selected").val();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "RDgetAllAccountsDetails",
						parameters : [customerID,acntnumber]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : RDdetailsSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
  
		RDdetailsSuccess = function(result){
			invocationResult = result.invocationResult;
			try{
			invocationResult1 = invocationResult.text;
			invocationResult2 = JSON.parse(invocationResult1);
			if(invocationResult2.getAccountDetailResponse){
				if(invocationResult2.getAccountDetailResponse){
					busyInd.hide();
					RDaccountDetails.removeAll();
					var idx = 1;
					itemdata = invocationResult2.getAccountDetailResponse;
						$(itemdata).each(function(index, obj) {
							// strid = "item"+idx;
							// displaytxt = $.trim(obj.accountNo)+"-"+obj.branchName;
							// accountValue = $.trim(obj.accountNo);
							dateformatting(obj.openDate);
							openingDate = dateformat;
							dateformatting(obj.maturityDate);
							maturityDate  = dateformat;
							dateformatting(obj.nextDueDate);
							nextDueDate  = dateformat; 
							maturityAmount = "₹ " +formatAmt(parseFloat(obj.maturityAmount));
							paidAmount = "₹ " +formatAmt(parseFloat(obj.paidAmount));
							balance = "₹ " +formatAmt(parseFloat(obj.balance));
							accountTermMonth = obj.accountTerm.months;
							accountTermDays = obj.accountTerm.days;
							instalmentsDelayed = obj.instalmentsDelayed;
							instalmentsMissed = obj.instalmentsMissed;
							instalmentsPaid = obj.instalmentsPaid;
							interest = obj.netInterestRate+"% (p.a)";
							branch = obj.branchName;
							branch = branch.toLowerCase();
							instalmentAmount = "₹ " +formatAmt(parseFloat(obj.instalmentAmount));
							Tenure = accountTermMonth+" Months "+accountTermDays+" Days";
							RDaccountDetails.push({acctnumber:obj.accountNo,accountTermMonth:accountTermMonth, accountTermDays:accountTermDays, instalmentsDelayed:instalmentsDelayed,instalmentsMissed:instalmentsMissed,instalmentsPaid:instalmentsPaid,instalmentAmount:instalmentAmount,openingDate:openingDate, maturityAmnt:maturityAmount, maturityDate:maturityDate, branch:branch, currency:obj.currencyCode, depositDate:'-', balance:balance, interest:interest,paidAmount:paidAmount,Tenure:Tenure,nextDueDate:nextDueDate });
							// idx++;
						});
						window.location.hash = '#RD_summary';
				}
				else if(invocationResult2.Fault){
					busyInd.hide();
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult2.Fault.Reason.Text);
				}else{
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult2.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult2.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
			}
			catch(e){
				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
		};
		
		
		self.getFDOpenSchemes = function(){
			if(window.navigator.onLine){
				//$('#confirm_button1').prop('disabled', true);
				busyInd.show();
				var customerID = LoggedInUserID;
				if(accountList().length != 0){
					temp= accountList();
					dbtaccntnumber = temp[0].codacctno;
				}
				else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					return;
				}
				//dbtaccntnumber ="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetFDOpenSchemes",
					parameters : [customerID,dbtaccntnumber]
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : FDOpenSchemesSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		FDOpenSchemesSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getAllSchemesResponse){
				if(invocationResult.getAllSchemesResponse.schemeArray){
					busyInd.hide();
					FDaccountSchemes.removeAll();
					var idx = 1;
					itemdata = invocationResult.getAllSchemesResponse.schemeArray.scheme;
						$(itemdata).each(function(index, obj) {
							
							natureOfDeposit = obj.natureOfDeposit;
							code = obj.code;
							minDepositAmount = obj.minDepositAmount;
							maxDepositAmount = 10000000;
							minDays = obj.minTerm.days;
							maxDays = "29";
							minMonths = obj.minTerm.months;
							maxMonths = obj.maxTerm.months;
							minterm = minDays+ " Days "+minMonths+" Months";
							maxterm = maxDays+ " Days "+maxMonths+" Months";
							minmonthvalid = "Min "+minMonths+" Months to Max "+maxMonths+" Months"
							maxdaysvalid = "Min "+minDays+" Days to Max "+maxDays+" Days"
							amountvalid = "Min ₹"+formatAmt(parseFloat(minDepositAmount))+" to Max ₹"+formatAmt(parseFloat(maxDepositAmount));
							swiperid = "swiper"+index;
							FDaccountSchemes.push({minterm:minterm,maxterm:maxterm, natureOfDeposit:natureOfDeposit,code:code, minDepositAmount:minDepositAmount, maxDepositAmount:maxDepositAmount,minDays:minDays,maxDays:maxDays,minMonths:minMonths,maxMonths:maxMonths,minmonthvalid:minmonthvalid,maxdaysvalid:maxdaysvalid,amountvalid:amountvalid,swiperid:swiperid});
						});
					$("#contentData").load("Views/Deposits/fd-account-opening.html", null, function (response, status, xhr) {
						if (status != "error") {}
					   
						 ko.applyBindings(self, $(".content").get(0));
							$(document).find('#1').show();
					});
						//window.location.hash = '#FD_summary';
				}
				else if(invocationResult.Fault){
					busyInd.hide();
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
				}else{
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
			else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			

		};
		
		fdopenconfirm = function(){
			if(window.navigator.onLine){
				if($("#frmedopen").valid()){
				naturefdval = $('#NatureDeposit').val();
				nature = naturefdval - 1 ;
				temp = FDaccountSchemes();
				current_nature = temp[nature];
				mindaysval = current_nature.minDays;
				maxdaysval = current_nature.maxDays;
				minmonthsval = current_nature.minMonths;
				maxmonthsval = current_nature.maxMonths;
				minamnt = current_nature.minDepositAmount;
				maxamnt = current_nature.maxDepositAmount;
				if($('#NatureDeposit').val() == 1){days = mySwiper.activeIndex;}
				if($('#NatureDeposit').val() == 2){days = mySwiper1.activeIndex;}
				if($('#NatureDeposit').val() == 3){days = mySwiper2.activeIndex;}
				
				if(minamnt > $('.fldDepositAmt:visible').val()){
					navigator.notification.alert("Please enter valid Amount. Valid Amount ₹: "+minamnt+" to ₹"+maxamnt);
					return;
				}
				if(maxamnt < $('.fldDepositAmt:visible').val()){
					navigator.notification.alert("Please enter valid Amount. Valid Amount ₹: "+minamnt+" to ₹"+maxamnt);
					return;
				}
				if(maxmonthsval < $('.fldDepositMonth:visible').val() || minmonthsval > $('.fldDepositMonth:visible').val()){
					navigator.notification.alert("Please enter valid tenure. Valid Tenure : "+minmonthsval+" Months to "+maxmonthsval+" Months");
					return;
				}
				if(mindaysval != '0'){
					if(days < mindaysval && $('.fldDepositMonth:visible').val() == 0){
						navigator.notification.alert("Please enter valid tenure. Valid Tenure : "+mindaysval+" Days to "+maxdaysval+" Days");
						return;
					}
				}
				if(mindaysval != '0'){
					if(days < mindaysval && $('.fldDepositMonth:visible').val() == ''){
						navigator.notification.alert("Please enter valid tenure. Valid Tenure : "+mindaysval+" Days to "+maxdaysval+" Days");
						return;
					}
				}
				if(maxdaysval != '0'){
					if(days > maxdaysval){
						navigator.notification.alert("Please enter valid tenure. Valid Tenure : "+mindaysval+" Days to "+maxdaysval+" Days");
						return;
					}
				}
				if(mindaysval == '0' && maxdaysval == "0"){
					if(days > 29){
						navigator.notification.alert("Please enter valid tenure. Valid Tenure : "+mindaysval+" Days to 29 Days");
						return;
					}
				}
					frmacntfd = $('#fldFromAcctNo').val();
					amntfd = $('.fldDepositAmt:visible').val();
					fdmonths = $('.fldDepositMonth:visible').val();
					fddays = days;
					naturefd = $("#NatureDeposit option:selected").html();
					maturityinstu = $("#maturityInstructions option:selected").html();
					maturityinstuval = $('#maturityInstructions').val();
					
					var customerID = LoggedInUserID;
					dbtaccntnumber = $('#fldFromAcctNo').val();
					natureOfDeposit = parseFloat(naturefdval);
					maturityInstruction = parseFloat(maturityinstuval);
					depositCurrency = "INR";
					depositAmount =  parseFloat(amntfd);
					days = parseFloat(fddays);
					if(!days){days = 0 ;}
					months = parseFloat(fdmonths);
					busyInd.show();
					if(lgnout == false){ window.location.href = "#logout";}
					var invocationData = {
						adapter : "API_Adapter",
						procedure :"FDOpenConfirm",
						parameters : [customerID,dbtaccntnumber,natureOfDeposit,maturityInstruction,depositCurrency,depositAmount,days,months]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : FDOpenConfirmSucess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		FDOpenConfirmSucess = function(result){ 
			invocationResult = result.invocationResult;
			if(invocationResult.getMaturityDetailsResponse){
				busyInd.hide();
				$("#contentData").load("Views/Deposits/fd-Confirm.html", null, function (response, status, xhr){
					if (status != "error") {}
						$('.fldAcctNo').html(frmacntfd);
						$('.depositamnt').html("₹ " +formatAmt(amntfd));  
						$('.nature').html(naturefd);  
						$('.depositperiod').html(fdmonths+" Months "+fddays+" Days");  
						$('.maturity').html(maturityinstu);
						$('.rateinterest').html(invocationResult.getMaturityDetailsResponse.netInterestRate+"% (p.a)");
						var today = new Date();
						var dd = today.getDate();
						var mm = today.getMonth()+1; //January is 0!
						var yyyy = today.getFullYear();

						if(dd<10) {
							dd='0'+dd
						} 

						if(mm<10) {
							mm='0'+mm
						} 
						today = yyyy+'-'+mm+'-'+dd;
						dateformatting(today);
						depositdate = dateformat;
						$('.depositdate').html(depositdate);
						dateformatting(invocationResult.getMaturityDetailsResponse.maturityDate);
						maturitydate = dateformat;
						$('.maturitydate').html(maturitydate);
						$('.maturityamnt').html("₹ " +formatAmt(invocationResult.getMaturityDetailsResponse.maturityAmount));
						temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
						$('.request').html(temp);
						$('.maturityinstruc').html();
					ko.applyBindings(self, $(".content").get(0));
				});
					
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
			else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		FDconfirmforotp = function(){
			if($('#fldChecked').val() == 'true'){
				GetOTP();
			}
			else{
				self.getFDOpenSchemes();
			}
		};
		
		fdConfirm = function(OTP_No){
					$('#OTP_Ref').val(OTP_No);
					$('#otphit').show();
					$('#OTP_No').show();
					$('#accept').hide();
					$('#confirmbutton').hide();
					temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
					$('.request').html(temp);  					
		};
		
		FDSubmit = function(){
			if(window.navigator.onLine){
				if($("#frmfd").valid()){
					busyInd.show();
					uniqueRequestNo = $('.request').html();
					var customerID = LoggedInUserID;
					dbtaccntnumber = $(".fldAcctNo").html();
					natureOfDeposit = parseFloat(naturefdval);
					maturityInstruction = parseFloat(maturityinstuval);
					depositCurrency = "INR";
					depositAmount =  parseFloat(amntfd);
					days = parseFloat(fddays);
					if(!days){days = 0 ;}
					months = parseFloat(fdmonths);
					otpkey = $('#OTP_Ref').val();
					ottpval = $('#OTP_No_Val').val();
					if(lgnout == false){ window.location.href = "#logout";}
					var invocationData = {
						adapter : "API_Adapter",
						procedure :"FDOpenSubmit",
						parameters : [uniqueRequestNo,customerID,dbtaccntnumber,natureOfDeposit,maturityInstruction,depositCurrency,depositAmount,days,months,otpkey,ottpval]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : FDOpenSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		FDOpenSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.openAccountResponse){
				busyInd.hide();
					
				$("#contentData").load("Views/Deposits/fd-account-success.html", null, function (response, status, xhr) {
					if (status != "error") {}
					$('.success_msg').show();
					   $('.clsacctno').html(invocationResult.openAccountResponse.depositAccountNo);
					ko.applyBindings(self, $(".content").get(0));
				});

			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
			else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		self.getRDOpenPage =  function(){
		$("#contentData").load("Views/Deposits/rd-account-opening.html", null, function (response, status, xhr) {
            if (status != "error") {}
           
             ko.applyBindings(self, $(".content").get(0));                   
        });
		};
		
		rdopensubmit = function(){ 
			if(window.navigator.onLine){
				if($("#frmrdopen").valid()){
					GetOTP();
					frmacntfd = $('#fldFromAcctNo').val();
					amntfd = $('#fldDepositAmt').val();
					fdmonths = $('#fldDepositMonth').val();
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		rdConfirm = function(OTP_No){
			$("#contentData").load("Views/Deposits/rd-Confirm.html", null, function (response, status, xhr){
				if (status != "error") {}
					$('.fldAcctNo').html(frmacntfd);
					$('.depositamnt').html("₹ " +formatAmt(amntfd));  
					$('.depositperiod').html(fdmonths+" Months");  
					$('#OTP_Ref').val(OTP_No);
					temp = LoggedInUserID+""+Math.floor(Math.random()*9000) + 1000;
					$('.request').html(temp);  					
				ko.applyBindings(self, $(".content").get(0));
			});
		};
		
		RDSubmit = function(){
			
			if(window.navigator.onLine){
				if($("#frmrd").valid()){
					busyInd.show();
					uniqueRequestNo = $('.request').html();
					var customerID = LoggedInUserID;
					dbtaccntnumber = $(".fldAcctNo").html();
					depositCurrency = "INR";
					depositAmount =  parseFloat(amntfd);
					months = parseFloat(fdmonths);
					otpkey = $('#OTP_Ref').val();
					ottpval = $('#OTP_No_Val').val();
					if(lgnout == false){ window.location.href = "#logout";}
					var invocationData = {
						adapter : "API_Adapter",
						procedure :"RDOpenSubmit",
						parameters : [uniqueRequestNo,customerID,dbtaccntnumber,depositCurrency,depositAmount,months,otpkey,ottpval]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : RDOpenSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		RDOpenSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.openAccountResponse){
				busyInd.hide();
					
				$("#contentData").load("Views/Deposits/rd-account-success.html", null, function (response, status, xhr) {
					if (status != "error") {}
					$('.success_msg').show();
					   $('.clsacctno').html(invocationResult.openAccountResponse.depositAccountNo);
					   $('.refno').html(invocationResult.openAccountResponse.siRefNo);
					ko.applyBindings(self, $(".content").get(0));
				});

			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}
			else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
		
		
		self.getLoanAccountsList = function(){
			if(window.navigator.onLine){
				//$('#confirm_button1').prop('disabled', true);
				busyInd.show();
				var customerID = LoggedInUserID;
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "LoangetAllAccounts",
					parameters : [customerID]
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : LoanAcntsSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		
		LoanAcntsSuccess = function(result){
			invocationResult1 = result.invocationResult;
			try{
			invocationResult = invocationResult1.text;
			invocationResult2 = JSON.parse(invocationResult);
			if(invocationResult2.getAllAccountsResponse){
				if(invocationResult2.getAllAccountsResponse.numAccounts > 0){
					if(invocationResult2.getAllAccountsResponse.accountsArray){
						busyInd.hide();
						LoanaccountList.removeAll();
						var idx = 1;
						itemdata = invocationResult2.getAllAccountsResponse.accountsArray.account;
						
							$(itemdata).each(function(index, obj) {
								strid = "item"+idx;
								displaytxt = $.trim(obj.accountNo)+"-"+obj.branchName;
								accountValue = $.trim(obj.accountNo);
								LoanaccountList.push({ codacctno: obj.accountNo, acctbranch: obj.branchName, strid:strid, displaytxt:displaytxt,accountValue: accountValue });
								idx++;
							});
							window.location.hash = '#Loan_Acnts';
					}
					
					else if(invocationResult2.Fault){
						busyInd.hide();
						//$('#confirm_button1').prop('disabled', false);
						navigator.notification.alert(invocationResult2.Fault.Reason.Text);
					}else{
						//$('#confirm_button1').prop('disabled', false);
						navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
						busyInd.hide();
					}
				}
				else{
					busyInd.hide();
					navigator.notification.alert("You do not have any Loan Account with the Bank.");
					window.location.hash = '#rrasm01';
				}
			}else if(invocationResult2.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			}
			catch(e){
				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			
		};
		
		loanacntsSubmit = function(){ 
			if(window.navigator.onLine){
				if($("#frmloanacnts").valid()){
					//LoandetailsSuccess();
					busyInd.show();
					var customerID = LoggedInUserID;
					acntnumber = $("#fdAcctNo option:selected").val();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "LoangetAllAccountsDetails",
						parameters : [customerID,acntnumber]
					};
			
					WL.Logger.debug(invocationData, '');
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : LoandetailsSuccess,
						onFailure : AdapterFail,
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}

		};
		
		LoandetailsSuccess = function(result){
			invocationResult1 = result.invocationResult;
			try{
			invocationResult = invocationResult1.text;
			invocationResult2 = JSON.parse(invocationResult);
			if(invocationResult2.getAccountDetailResponse){
				if(invocationResult2.getAccountDetailResponse){
					busyInd.hide();
					LoanaccountDetails.removeAll();
					var idx = 1;
					itemdata = invocationResult2.getAccountDetailResponse;
						$(itemdata).each(function(index, obj) {
							
							dateformatting(obj.firstDisbursementDate);
							firstDisbursementDate = dateformat; 
							dateformatting(obj.lastDisbursementDate);
							lastDisbursementDate = dateformat;
							interest = obj.netInterestRate+"%";
							emiDue = "₹ " +formatAmt(parseFloat(obj.emiDue));
							emiAmount = "₹ " +formatAmt(parseFloat(obj.emiAmount));
							sanctionedAmount = "₹ " +formatAmt(parseFloat(obj.sanctionedAmount));
							disbursedAmount = "₹ " +formatAmt(parseFloat(obj.disbursedAmount));
							outstandingPrincipal = "₹ " +formatAmt(parseFloat(obj.outstandingPrincipal));
							feesDue = "₹ " +formatAmt(parseFloat(obj.feesDue));
							LoanaccountDetails.push({acctnumber:obj.accountNo, productName:obj.productName, firstDisbursementDate:firstDisbursementDate, sanctionedAmount:sanctionedAmount, lastDisbursementDate:lastDisbursementDate, branch:'-', currency:obj.currencyCode, emiDue:emiDue, feesDue:feesDue, outstandingPrincipal:outstandingPrincipal, interest:interest,disbursedAmount:disbursedAmount,numPendingInstallments:obj.numPendingInstallments,emiAmount:emiAmount,interestDue:obj.interestDue});
							// idx++;
						});
						window.location.hash = '#Loan_summary';
				}
				else if(invocationResult2.Fault){
					busyInd.hide();
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult2.Fault.Reason.Text);
				}else{
					//$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult2.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult2.Fault.Reason.Text);
			}else if(invocationResult2.RBL){
				busyInd.hide();
				handleError(invocationResult2.RBL.Response, invocationResult.RBL);
			}
			else{
				//$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
			}
			catch(e){
				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}

		};


		//---notification start -->
        			self.notificationlist = function(){
        				//start--- notification list
        				busyInd.show();
        				//alert(deviceId);
        				var reqParams = {
        						  'appname' :'moBank',
        						  'platform' :'Android',
        						  'cifid' : LoggedInUserID,
        						   'deviceid': deviceId

        					}

        				var invocationData = {
        						   adapter : "WINEAdapter",
        						   procedure : "getNotificationList",
        						   parameters : [JSON.stringify(reqParams)],
        						   compressResponse : true
        			    };

        				WL.Client.invokeProcedure(invocationData, {
        						   onSuccess : getlistnotificationSucess,
        						   onFailure : AdapterFail,
        						   timeout: timeout
        				});


        			};
        			getlistnotificationSucess = function(result){

        			//	alert("getlistnotificationSucess");
        				invocationResult = result.invocationResult;
        				//alert(JSON.stringify(invocationResult));

        				if(invocationResult.isSuccessful) {
        					if(invocationResult.root){

        						 var notificationDtl= invocationResult.root;
        						 var notificationItem= notificationDtl.Item;

        						// alert(JSON.stringify(notificationItem));
        						// alert(JSON.stringify(invocationResult));

        						 //-- start each list
        						 var notificationDataList=[];
        						if(notificationItem!=null && notificationItem!=''){
        						 $(notificationItem).each(function(index, obj) {

        							 var NotificationID=obj.NotificationID;
        							 var SentDateTime=obj.SentDateTime;
        							 var Message=obj.Message;
        							 var Image1=obj.Image;
        							 var Deeplinking=obj.Deeplinking; //Clickable:Yes
        							/* var DeeplinkingType=obj.DeeplinkingType; //URL / Page
        							 var action=obj.Action;
        							 var AzureNotificationId=obj.AzureNotificationId;

        							 var ClientID=obj.ClientID;
        							 var AppName=obj.AppName;
        							 var NumberOfUsers=obj.NumberOfUsers;
        							 var Tags=obj.Tags;
        							 var IsDeepLinking=obj.IsDeepLinking;*/

        							 notificationDataList.push({NotificationID:obj.NotificationID,ClientID:obj.ClientID,AppName:obj.AppName,NumberOfUsers:obj.NumberOfUsers,SentDateTime:obj.SentDateTime,Message:obj.Message,Tags:obj.Tags,IsDeepLinking:obj.IsDeepLinking,Image1:obj.Image,AzureNotificationId:obj.AzureNotificationId,Deeplinking:obj.Deeplinking});
        						 });
        						 //-- End each list

        					 $("#contentData").load("Views/Menu/notificationlist.html", null, function (response, status, xhr) {
        						 $("#NotificationList").html('');
        						 $(".h_title").html("Notifications");
        						 var list = $("#NotificationList");
        						// notificationDataList.reverse(); Latest above

        						 for(i=0;i<notificationDataList.length;i++){


        							 ////////////////////////////////////
        							 	var img=notificationDataList[i].Image1;
        							 	/*var d = new Date(notificationDataList[i].SentDateTime);

        								var month=('0'+(d.getMonth() + 1)).slice(-2);
        								var day=('0'+(d.getDate())).slice(-2);

        								var h=d.getHours();
        								//var h=d.setHours(d.getHours() - d.getTimezoneOffset() / 60);
        								// alert(h);
        						        var m=d.getMinutes();
        						        //var s=d.getSeconds();
        						        var ampm=h >= 12 ? 'PM' : 'AM';
        						        h = h % 12;
        						        h= h ? h : 12; // the hour '0' should be '12'
        						        m = m < 10 ? '0'+m : m;

        						      //  s = s <10 ? '0'+s: s;
        						        var datestring = day + "-" + month + "-" + d.getFullYear() + "&nbsp; " +h+ ":" +m+" " +ampm;*/
        								//var datestring = day + "-" + month + "-" + d.getFullYear() + "&nbsp; " + d.getHours() + ":" + d.getMinutes();
        								var msg=notificationDataList[i].Message;

        								var html_str="<div class='polaroid'>";

        							 	html_str+="<div class='container_inbox'>";
        							 	 //////////////---URL n Page deeplinking----///////////////////////
        								//alert( notificationDataList[i].Deeplinking);
        							 //	inboxlink='';
        								if(notificationDataList[i].Deeplinking!="null" && notificationDataList[i].Deeplinking!="" && notificationDataList[i].Deeplinking!=null){
        									var dep=notificationDataList[i].Deeplinking;
        									var temp = new Array();
        									temp = dep.split(",");//Deeplinking.split(",");
        									 var DeeplinkingType;
        									 var Action;
        									 var Clickable;
        									 var preIndex=0;
        									 var res='';
        									 //alert(temp);
        									 for(var j=0; j < temp.length; j++){
        										// alert(temp[j]);
        										 	if(temp[j].toLowerCase()=="clickable:yes")
        										 		Clickable='yes';
        										 	if(temp[j].toLowerCase()=="deeplinkingtype:url")
        										 		DeeplinkingType='url';
        										 	else if(temp[j].toLowerCase()=="deeplinkingtype:page")
        										 		DeeplinkingType='page';
        										 	if (temp[j].indexOf("Action") >= 0){
        										 		 preIndex = temp[j].indexOf(':');
        										 		 res = temp[j].substring(preIndex+1);
        										 	}

        									 }
        									if(Clickable=='yes'){
        										 if(DeeplinkingType=='page'){
        											 html_str+="<a onClick='return pageDeeplinking_inbox("+i+");'><input type='hidden' id='hdn"+i+"' value='"+res+"' class='blue-text' />";
        										 }
        										 else if(DeeplinkingType=='url'){
        											 html_str+="<a onClick='return openLink_inbox("+i+");' target='_blank' class='blue-text'><input type='hidden' id='hdn"+i+"' value='"+res+"' />";
        										 }
        									 }
        								}
        							 	html_str+="<p class='blue-text p1'>" +msg+ "</p><p class='p2' style='color:#000000;'>"+notificationDataList[i].SentDateTime+"</p></div>";
        							 	if(img!="null" && img!=null && img!=undefined && img!="undefined")
        							 	{
        							 		var w=$("#NotificationList").width()-31;
        							 		html_str+="<p style='padding-left:5px;padding-right:5px;max-height:205px;'><img src='"+img+"' id='img_notification"+i+"' style='width:100%;max-height:200px;'></p>";
        							 		//////html_str+="<p style='padding-left:5px;padding-right:5px;'><img src='"+img+"' id='img_notification"+i+"' style='width:100%;'></p>";// style='width:"+w+"px;'
        							 		//html_str+="<div style='width: 100%;height: 205px;overflow: hidden;'padding-left:5px;padding-right:5px;><img src='"+img+"' id='img_notification"+i+"' style='width:100%;height: 200px;margin: 5px;'></p>";
        							 		//////html_str+="<p style='padding-left:5px;padding-right:5px;padding-bottom:5px;text-align:center;'><img src='"+img+"' id='img_notification"+i+"' style='max-height:200px;margin-left:auto;margin-right:auto;max-width:100%;'></p>";
        							 	}
        							 	if(notificationDataList[i].Deeplinking!="null"){
        							 		if(Clickable=='Yes')
        							 			html_str+="</a>";
        							 	}
        							 	html_str+="</div>";
        		    					list.append(html_str);
        			    		}
        						busyInd.hide();
        						ko.applyBindings(self, $(".dynamic-page-content").get(0));
        					});

        					 }else{
        						 navigator.notification.alert("There are no notification in your inbox");
        						 busyInd.hide();
        						 window.location.hash = "#Home";
        					 }
        				}
        				}
        			};
        			//end--- notification list
        			//////////--------------------------------------------------------

				//Credit Score
        		//account list for credit score
        		self.getAccountsListForCreditScore = function(){
        			if(window.navigator.onLine){

        				previousSession = CurrentSessionId;

        				fldjsessionid = '';
        					reqParams = {};
        					reqParams["RQLoginUserId"] = LoggedInUserID;
        					reqParams["RQDeviceFamily"] = Device_Platform;
        					reqParams["RQDeviceFormat"] = Device_Model;
        					reqParams["RQOperationId"] = "NFTFNDTFR";
        					reqParams["RQClientAPIVer"] = RQClientAPIVer;
        					reqParams["SessionId"] = CurrentSessionId;
        					reqParams["RQTransSeq"] = "01";

        					busyInd.show();
        					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
        						adapter : "API_Adapter",
        						procedure : "GetNeft",
        						parameters : [fldjsessionid,reqParams],
        						compressResponse : true
        					};
        					WL.Logger.debug(invocationData, '');

        					WL.Client.invokeProcedure(invocationData, {
        							onSuccess : getAccountsListForCreditScoreSuccess,
        							onFailure : AdapterFail,
        							timeout: timeout
        					});
        					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}
        			};


        			getAccountsListForCreditScoreSuccess = function(result){

        				if(CurrentSessionId != previousSession)
        				{
        					busyInd.hide();
        					return;
        				}

        				invocationResult = result.invocationResult;

        //				CreditScoreMessage = invocationResult.RBL.CreditScoreMessage;
        //				AmountReq = invocationResult.RBL.AmountReq;

        				accountListFt.removeAll();
        				rdAccountList.removeAll();
        			if(invocationResult.isSuccessful){
        			if(invocationResult.RBL){
        				if(invocationResult.RBL.Response){
        					if(invocationResult.RBL.STATUS.CODE == '0'){
        						var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
        						var currentuser = currentreguserid;
        						var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
        						if(currentuser == responseuser && CurrentSessionId == responsesessionid){

        							if(invocationResult.RBL.Response.CustDetails!=undefined){
        								if(invocationResult.RBL.Response.CustDetails.length!=0){
        									acctdtls = invocationResult.RBL.Response.validAcct.acctdetails;
        									custdtls = invocationResult.RBL.Response.CustDetails;
        									var idx = 1;
        									$(acctdtls).each(function(index, obj) {
        										strid = "item"+idx;

        										$(custdtls).each(function(j, obj1) {
        											custnames="";
        											if(obj.acctindex == obj1.acctindex){
        												var actbal = "₹ "+ parseFloat(obj.acctbalance);
        												displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
        												//accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
        												accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+"#"+actbal, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
        											}
        										});
        										idx++;
        									});

        								}
        						    }

        							$("#contentData").load("Views/CreditScore/CreditScoreSelectAccount.html", null, function (response, status, xhr) {
        								if (status != "error") {}

        								$(".h_title").html("View my Credit Score");
        								$("#nftfundtrns").show();
        								activecreditpage=false;

        								//Amount for Experian Validation
        								//self.ExperianAmount();

        									ko.applyBindings(self, $(".dynamic-page-content").get(0));
        							});
        							busyInd.hide();
        						}
        						else{
        								busyInd.hide();
        								navigator.notification.alert("Your session has timed out!");
        								window.location.hash = "#logout";
        						}
        					}
        					else{
        						busyInd.hide();
        						invalidresponse();
        					}
        				}else{
        								busyInd.hide();
        								handleError(invocationResult.RBL.Response, invocationResult.RBL);
        						}
        			}else{
        	    		 navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        	    		busyInd.hide();
        			}

        			}
        			busyInd.hide();
        		};


        		//Experian Amount from Services

        		self.ExperianAmount = function(){
        			if(window.navigator.onLine){
        				if($("#frmCreditScoreSelectAccount").valid()){
        				//alert("exp check");
        				//Save current session id
        				previousSession = CurrentSessionId;

        					busyInd.show();

        					if(lgnout == false){ window.location.href = "#logout";}

        					var invocationData = {
        							adapter : "API_Adapter",
        							procedure : "ExperianScoreAmount",
        							parameters : [booksStore(LoggedInUserID)],
        							compressResponse : true
        					};

        					WL.Client.invokeProcedure(invocationData, {
        						onSuccess : ExperianAmountSuccess,
        						onFailure : AdapterFail,
        						timeout: timeout
        					});

        			}
        			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}

        		};

        		ExperianAmountSuccess = function(result){
        			//If session id is expired then dont show screen
        			if(CurrentSessionId != previousSession)
        			{
        				busyInd.hide();
        				return;
        			}
        			//alert("exp success");

        			ExperianAmountResult = result.invocationResult;

        			if(ExperianAmountResult.isSuccessful){
        				ExperianAmountResult = ExperianAmountResult.CreditScoreCharges.ServiceCharge;
        				//alert(ExperianAmountResult);
        				self.CreditScoreSelectAccountSubmit();
        			}

        			else{
        				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        			}
        			busyInd.hide();
        		};



        		self.CreditScoreSelectAccountSubmit = function(){
        			if(window.navigator.onLine){
        				//Save current session id
        				previousSession = CurrentSessionId;

        				accno_selected = self.selAccount();
        				var acc_balance = parseFloat((accno_selected.split("₹")[1]));
        				accno_selected = (accno_selected.split("#")[0]);

        				//alert("CreditScoreSelectAccountSubmit");
        				//alert(acc_balance);
        				//acc_balance=200;
        				if (acc_balance >= ExperianAmountResult)
        					{
        					//alert("get details");
        					busyInd.show();

        					var CustomerDetailsReq = {
        							"CustomerDetailsReq":
        							{
        								"RQACCNTNO":accno_selected,
        							    "CIF_ID": LoggedInUserID //---- 12th oct change -added line
        							}
        					}

        					if(lgnout == false){ window.location.href = "#logout";}

        					var invocationData = {
        							adapter : "API_Adapter",
        							procedure : "CreditScoreCustDetails",
        							parameters : [booksStore(JSON.stringify(CustomerDetailsReq)),booksStore(LoggedInUserID)],
        							compressResponse : true
        					};

        					WL.Client.invokeProcedure(invocationData, {
        						onSuccess : CreditScoreSelectAccountSubmitSuccess,
        						onFailure : AdapterFail,
        						timeout: timeout
        					});
        					}
        				else{
        					navigator.notification.alert("Insufficient Balance. Please try with different account.");
        				}

        			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}

        		};


        		CreditScoreSelectAccountSubmitSuccess = function(result){
        //alert("CreditScoreSelectAccountSubmit");
        			//If session id is expired then dont show screen
        			if(CurrentSessionId != previousSession)
        			{
        				busyInd.hide();
        				return;
        			}
        			CreditScoreResult = result.invocationResult;

        			if(CreditScoreResult.Details)
        			{

        				var det = CreditScoreResult.Details;
        				det_back = CreditScoreResult.Details;   ///----to solve termsncondn back button issue
        				var genderresponse = "";

        				$("#contentData").load("Views/CreditScore/CreditScoreHome.html", null, function (response, status, xhr) {

        					if(det.gender == 1)
        					{
        						genderresponse = "MALE";
        					}
        					else if(det.gender == 2)
        					{
        						genderresponse = "FEMALE";
        					}
        					else
        					{
        						genderresponse = "OTHER";
        					}

        					if(det.AddressLine2 == "" || det.AddressLine2 == null){
        						$("#addressline2").css('display','none');
        					}

        					if(det.AddressLine3 == "" || det.AddressLine3 == null){
        						$("#addressline3").css('display','none');
        					}

        					$('.CustomerName').html(det.firstName + " " + det.middleName + " " + det.surname);
        					$('.DOB').html(det.dob);
        					$('.Address1').html(det.AddressLine1);
        					$('.Address2').html(det.AddressLine2);
        					$('.Address3').html(det.AddressLine3);
        					$('.PAN').html(det.pan);
        					$('.City').html(det.city);
        					//$('.State').html(det.state);
        					$('.Pincode').html(det.pincode);
        //					$('.Gender').html(genderresponse);
        //					$('.Cust_ID').html(det.CustomerID);
        //					$('.Acc_num').html(accno_selected);
        					$('#email').val(det.email.toLowerCase());
        					//$('#email').val(det.email);
        					$('.PhoneNo').html(det.mobileNo);
        					$('.Aadhar').html(det.aadhaar);

        					activecreditpage=true;

        					ko.applyBindings(self, $(".dynamic-page-content").get(0));
        				});
        			}
        			else
        			{
        				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        			}
        			busyInd.hide();
        		};

        		self.CreditScoreHomeSubmit = function(){
        			if(window.navigator.onLine){
        				if($("#frmCreditScoreHome").valid()){

        					//alert(CreditScoreSubmit);
        					if(CreditScoreSubmit == true){
        						//perform final submit
        							emailforcreditscore = $('#email').val();
        							//alert(emailforcreditscore);
        							self.CreditScoreOTPSubmit();
        					}

        					else{
        						//perform otp generation
        						WL.SimpleDialog.show(
        								"Confirmation Declaration",  "I confirm all details as shown in the screen are correct. I understand INR "+ExperianAmountResult+" will be deducted from my account for availing this service.",
        								[{text: "Yes", handler: function() {

        									CreditScoreSubmit = true;
        									CreditScoregetOTP();
        									$("#OTP_CreditScore").css('display','table');
        									$("#termsandconditions").css('display','none');
        									$("#creditscoresubmitbtn").prop('value', 'Submit');

        								}
        								},{text: "No", handler: function() {WL.Logger.debug("First button pressed"); }
        								}])
        					}

        //					if (confirm("Rs "+ ExperianAmountResult +" will be deducted from your account as charge for this transaction.")) {
        //						emailforcreditscore = $('#email').val();
        //						//alert(emailforcreditscore);
        //						self.CreditScoreOTPSubmit();
        //					}
        				}
        			}
        			else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}
        		};

        		//Credit Score OTP submit
        		self.CreditScoreOTPSubmit = function () {
        		    if (window.navigator.onLine) {

        		    	//Save current session id
        				previousSession = CurrentSessionId;

        		            var channelId = "MBAUTH";
        		            var CIF = LoggedInUserID;
        		            var requestId = $("#requestId").val();
        		            var otp = booksStore($("#p2cOTPval").val());
        		            $("#p2cOTPval").val('');

        //		            busyInd.show();
        		            customBusyShow();
        		            if (lgnout == false) { window.location.href = "#logout"; }
        		            var invocationData = {
        		                adapter: "API_Adapter",
        		                procedure: "OTPValidateService",
        		                parameters: [channelId, CIF, requestId, otp],
        		                compressResponse: true
        		            };

        		            WL.Client.invokeProcedure(invocationData, {
        		                onSuccess: CreditScoreOTPSubmitSuccess,
        		                onFailure: AdapterFail,
        		                timeout: timeout
        		            });


        		    } else {
        		        navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        		    }
        		};

        		CreditScoreOTPSubmitSuccess = function (result) {

        			if(CurrentSessionId != previousSession)
        			{
        //				busyInd.hide();
        				customBusyHide();
        				return;
        			}

        		    invocationResult = result.invocationResult;
        		    if (invocationResult.RBL) {
        		        handleErrorP2C(invocationResult.RBL);
        		    }
        		    if (invocationResult.otpValResponse) {
        		        var Status = invocationResult.otpValResponse.status;
        		        if (Status == "VALID") {
        		        	//success call
        		        	//alert("success otp");
        		        	self.CreditScoreFinal();
        		        } else if (Status == "INVALID_OTP") {
        		            alert("Invalid OTP");
        		            customBusyHide();

        		        } else if (Status == "MAX_ATTEMPT_EXCEEDED") {

        		            alert("Maximum Attempts Exceeded! Try again.");
        		            customBusyHide();
        		            window.location.hash = '#rrasm01';
        		        }
        		        else {
        		            alert("Problem Occured! Try again.");
        		            customBusyHide();
        		            window.location.hash = '#rrasm01';
        		        }
        		    }
        		    else {
        		        navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		        customBusyHide();
        		    }
        //		    busyInd.hide();
        //		    customBusyHide();
        		};


        		self.CreditScoreFinal = function(){
        			if(window.navigator.onLine){
        				//alert("otp validation success");
        						var randomnumber = randomString(10); //Changed this to 10 from 16 as we are getting payment failed error for 16 characters
        						var det = CreditScoreResult.Details;

        //						busyInd.show();
        //						customBusyShow();

        						var reqCreditScore =	{ "CreditScoreRequest": {
        							"Header": {
        								"TranID": randomnumber,
        								"Corp_ID": "MOBANK",
        								"Maker_ID": "",
        								"Checker_ID": "",
        								"Approver_ID": ""
        							},
        							"Body": {
        								"allowConsent": "1",
        								"accountnumber": accno_selected,
        								"firstName": det.firstName,
        								"middleName": det.middleName,
        								"surname": det.surname,
        								"dob": det.dob,
        								"gender": det.gender,
        								"mobileNo": det.mobileNo,
        								"telephoneNo": det.telephoneNo,
        								"telephoneType": det.telephoneType,
        								"email": emailforcreditscore,
        								"AddressLine1": det.AddressLine1,
        								"AddressLine2": det.AddressLine2,
        								"AddressLine3": det.AddressLine3,
        								"city": det.city,
        								"state": det.state,
        								"pincode": det.pincode,
        								"pan": det.pan,
        								"passport": det.passport,
        								"aadhaar": det.aadhaar,
        								"voterId": det.voterId,
        								"driverLicense": det.driverLicense,
        								"rationCardNo": det.rationCardNo
        							},
        							"Signature": {
        								"Signature": ""
        							}
        						}
        						}
        						var reqjson = JSON.stringify(reqCreditScore);

        						//Save current session id
        						previousSession = CurrentSessionId;

        						if(lgnout == false){ window.location.href = "#logout";}
        						var invocationData = {
        								adapter : "API_Adapter",
        								procedure : "CreditScoreDetails",
        								parameters : [booksStore(reqjson),booksStore(LoggedInUserID)],
        								compressResponse : true
        						};

        						//alert("call done");

        						WL.Client.invokeProcedure(invocationData, {
        							onSuccess : CreditScoreFinalSubmitSuccess,
        							onFailure : AdapterFail,
        							timeout: timeout
        						});

        					}

        			else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}
        		};


        		CreditScoreFinalSubmitSuccess = function(result){

        			//If session id is expired then dont show screen
        			if(CurrentSessionId != previousSession)
        			{
        //				busyInd.hide();
        				customBusyHide();
        				return;
        			}

        			invocationResult = result.invocationResult;
        			//alert(JSON.stringify(invocationResult));
        			//alert(invocationResult.CreditScoreResponse.Body.BureauScore);
        			if(invocationResult.CreditScoreResponse.Body){

        				if(invocationResult.CreditScoreResponse.Body.BureauScore){

        					$("#contentData").load("Views/CreditScore/CreditScoreResult.html", null, function (response, status, xhr) {
        						$('.scorefigure').html(invocationResult.CreditScoreResponse.Body.BureauScore);

        						ko.applyBindings(self, $(".dynamic-page-content").get(0));
        					});
        				}

        				else{
        					//navigator.notification.alert(invocationResult.CreditScoreResponse.Body.esbreturndesc);
        					//---- 12th oct change
        					if(invocationResult.CreditScoreResponse.Body.esbreturndesc)
        					{
        						WL.SimpleDialog.show(
        	                            "Alert",invocationResult.CreditScoreResponse.Body.esbreturndesc,
        	                            [{
        	                                text: "OK", handler: function () {
        	                                window.location.hash = "#rrasm01";
        	                                }
        	                            }]);
        					}

                            //new error handling as given by Kiran
        					else if(invocationResult.CreditScoreResponse.Body.errorString)
                             {
                             	WL.SimpleDialog.show(
                                           "Alert",invocationResult.CreditScoreResponse.Body.errorString,
                                           [{
                                               text: "OK", handler: function () {
                                               window.location.hash = "#rrasm01";
                                               }
                                           }]);
                             }
        					else
        					{
        					      navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        					}
        				}
        			}

        			else{
        				navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        				}
        //			busyInd.hide();
        			customBusyHide();
        		};


        		//This functions are related to Aadhar Card updation module
        		self.aadharUpdate = function(){

        		};
        		//Accept Aadhar number and insert
        		self.aadharUpdateSubmit = function(){
        			///----

        			//Save current session id
        			previousSession = CurrentSessionId;

        			if(window.navigator.onLine){
        					if($("#frm_aadharUpdate").valid()){
        						aadharInsert = 0;
        						var conf=confirm("Do you want to submit the request?");
        						if(conf==true)
        						{

        							busyInd.show();
        							var AadharDetailsRequest = {
        								"DBInsert":
        								{
        									"AadharNumber":$("#aadharId").val(),
        									"AccountNumber":selaccNo,
        									"AccHolderName":$("#acctHolderName").val(),
        									"CIFID" :LoggedInUserID
        								}
        						}


        						if(lgnout == false){ window.location.href = "#logout";}
        						var invocationData = {
        									adapter : "API_Adapter",
        									procedure : "aadharInsertion",
        									parameters : [booksStore(JSON.stringify(AadharDetailsRequest)),booksStore(LoggedInUserID)], //LoggedInUserID
        									compressResponse : true
        						};

        						WL.Client.invokeProcedure(invocationData, {
        								onSuccess : adharUpdateSuccess,
        								onFailure : AdapterFail,
        								timeout: timeout
        						});
        						}//conf
        						else{
        								//window.location = "#rrasm01";
        							$("#aadharId, #confirmaadharId").val("");//
        							aadharInsert=1;
        								return false;
        						}
        					}



        			}
        			else{
        				navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}

        		};

        		adharUpdateSuccess = function(result){


        			//If session id is expired then dont show screen
        			if(CurrentSessionId != previousSession)
        			{
        				busyInd.hide();
        				return;
        			}

        			 invocationResult = result.invocationResult;
        			 if(invocationResult.isSuccessful)
        			 {
        				// alert("#"+invocationResult.Message);
        				 var st=invocationResult.Status;
        				 var msg=invocationResult.Message;

        				   $("#contentData").load("Views/Aadhar/aadharResponse.html", null, function (response, status, xhr) {

        					   $(".h_title").html("Update/View your Aadhaar");

        						var str = invocationResult.Message;
        						if (str.indexOf("success") !=-1) {
        							var res = str.substring(0, str.indexOf(":"));
        							//$('#response').html("Your Aadhaar update request will be completed in 3 business days.<br><br>Transaction Reference Number-<br>"+res+".");
        							$('#response').html("We have received your request for Aadhaar updation. Please check this space after 3 working days. If your Aadhaar number does not reflect, please walkin to any of the nearest RBL Bank Branch to update the same in our records.<br><br>Transaction Reference Number-<br>"+res+"");

        							ko.applyBindings(self, $(".dynamic-page-content").get(0));
        						}else{
        							$('#response').html(invocationResult.Message);
        						}

        					});

        			 }
        			 else
        			 {
        				navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			 }
        			 busyInd.hide();
        		};

        		self.aadharCheckSubmit=function(){

        			//////////////
        			if(window.navigator.onLine){
        				if($("#frm_aadharCheck").valid()){

        					//Save current session id
        					previousSession = CurrentSessionId;

        				selaccNo=$("#fldAcctNo").val();
        				var randnum=randomStringNum(13);
        				busyInd.show();
        				var CustomerDetailsReq = {
        							"AccDtlsRequest":
        							{
        								"RequestUUID":"Req_"+randnum,
        								"ServiceRequestId":"AcctInq",
        								/*"AcctInq":fldAcctNo,*/
        								"AccID":selaccNo,
        								"CIFID" :LoggedInUserID
        							}
        				}
        				if(lgnout == false){ window.location.href = "#logout";}
        				var invocationData = {
        							adapter : "API_Adapter",
        							procedure : "aadharUpdation",
        							parameters :[booksStore(JSON.stringify(CustomerDetailsReq)),booksStore(LoggedInUserID)] , //LoggedInUserID
        							compressResponse : true
        				};
        				WL.Client.invokeProcedure(invocationData, {
        						onSuccess : adharCheckSuccess,
        						onFailure : AdapterFail,
        						timeout: timeout
        				});
        				}
        			}
        			else{
        				navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}


        		};

        		adharCheckSuccess = function(result){
        			$("#note_joint_acc").css("display","none");
        				/////////////
        			   //If session id is expired then dont show screen
        			   if(CurrentSessionId != previousSession)
        			   {
        			    busyInd.hide();
        			    return;
        			   }

        			   AdharCheckResult = result.invocationResult;
        			 //  alert(JSON.stringify(AdharCheckResult));
        			   //console.log(JSON.stringify(AdharCheckResult));
        			   if(AdharCheckResult.isSuccessful){
        			   if(AdharCheckResult.AccDtlsResponse){

        				  var AccDtl= AdharCheckResult.AccDtlsResponse;
        				   var AccID= AccDtl.AccID;
        				   var Acctstatus= AccDtl.Acctstatus;
        				   var CIFID= AccDtl.CIFID;
        				   var ReqUUID= AccDtl.ReqUUID;
        				   var ServiceRequestId= AccDtl.ServiceRequestId;
        				   var ServiceRequestVersion= AccDtl.ServiceRequestVersion;
        				   var status= AccDtl.Status;
        				   var AccHolderName= AccDtl.AccHolderName;
        				   var isAadharattached= AdharCheckResult.AccDtlsResponse.isAadharattached;
        				   if(AccDtl.AadharNum)
        				    var AadharNum=AccDtl.AadharNum;

        					   if(status=="FAILURE"){

        						   //for update page
        						   $(".h_title").html("Update your Aadhaar");

        								 aadharInsert = 1;
        								 document.getElementById("tptCont1").style.display="block";
        								 document.getElementById("lblaccount").innerHTML=$("#fldAcctNo").val();//fldAcctNo;
        								 document.getElementById("tptCont").style.display="none";
        								 if(AccHolderName!='' || AccHolderName!='undefined')
        									 document.getElementById("acctHolderName").value=AccHolderName;
        								 else
        									 document.getElementById("acctHolderName").value="";
        						  // }
        					   }//--//if failure status
        					   else{

        						   if(isAadharattached=="Y"){

        							   //for aadhar view page
        							   $(".h_title").html("View your Aadhaar");

        								     /*   var msg1= "Aadhaar number "+AadharNum+" is already linked to this account.";
        								     navigator.notification.confirm(msg1, onAdharBack, "Alert", "OK");	*/
        								     aadharInsert = 1;
        									 document.getElementById("tptCont2").style.display="block";
        									 document.getElementById("lblaccount2").innerHTML=AadharNum.slice( 0, 4 )+ " "+AadharNum.slice( 4, 8 )+" "+AadharNum.slice( 8, 12 );
        									 // document.getElementById("lblaccount2").innerHTML="Aadhaar number <b>"+AadharNum+"</b> is already linked to <b>"+$("#fldAcctNo").val()+"</b> account.";
        									 document.getElementById("lblaccount1").innerHTML=$("#fldAcctNo").val();
        									 document.getElementById("tptCont").style.display="none";
        									 // alert($("#fldAcctNo").val());

        							   }
        							   else{
        								   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        							   }
        					   }
        				   }
        				   else
        				   {
        					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        				   }
        				   }
        				   else{
        					   navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        				   }
        			   busyInd.hide();
        		};

        		self.getAadharValidAccountsList = function(){

        			if(window.navigator.onLine){

        				//Save current session id
        				previousSession = CurrentSessionId;

        	///////////////////////-------------------- saving_acc_no
        				var CIFIDReq = {
        					"CIFID" :LoggedInUserID
        				}

        				//busyInd.show();
        				if(lgnout == false){ window.location.href = "#logout";}
        				var invocationData = {
        							adapter : "API_Adapter",
        							procedure : "AadharAccountValidation",
        							parameters :[booksStore(JSON.stringify(CIFIDReq)),booksStore(LoggedInUserID)],
        							compressResponse : true
        				};

        				busyInd.show();
        				WL.Client.invokeProcedure(invocationData, {
        						onSuccess : CIFIDCheckSuccess,
        						onFailure : AdapterFail,
        						timeout: timeout
        				});

        			}
        			else{
        				navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
        			}
        		};

        		CIFIDCheckSuccess = function(result){

        			//If session id is expired then dont show screen
        			if(CurrentSessionId != previousSession)
        			{
        				busyInd.hide();
        				return;
        			}

        				invocationResult = result.invocationResult;
        				//AadharViewList.removeAll();
        				AadharViewList.length = 0;


        				if(invocationResult.isSuccessful) {
        					//alert(JSON.stringify(invocationResult.account_nos));

        					if(invocationResult.account_nos){
        						var str="";
        					 	var list =[];

        							//busyInd.hide();
        						toAccdata=invocationResult.account_nos;

        						$(toAccdata).each(function(index, obj) {

        							// var AadharAccountId1 = $.trim(obj.FORACID);
        							AadharViewList.push({ AadharAccountId1: obj.FORACID });
        				        });
        						//	alert(toAccdata);
        						if(AadharViewList.length>0){
        						 $("#contentData").load("Views/Aadhar/aadharUpdate.html", null, function (response, status, xhr) {

        							// $(".h_title").html("Update your Aadhaar");
        							 $(".h_title").html("Update/View your Aadhaar");
        							 	var selected='';

        								 	for(var i=0;i<AadharViewList.length;i++){
        								 		if(i==0)
        								 		{
        								 			str="";
        										 	str="<select name='fldAcctNo' id='fldAcctNo' ><option value='' >-Select-</option>";
        								 		}
        								 		if(i==1)
        								 			selected="selected=selected";
        								 		else
        								 			selected=""; //"+selected+"

        								 		if ($.inArray(AadharViewList[i].AadharAccountId1, list) == -1){

        								 			str+="<option value='"+AadharViewList[i].AadharAccountId1+"' >"+AadharViewList[i].AadharAccountId1+"</option>";
        								 			list.push(AadharViewList[i].AadharAccountId1);
        								 		}
        								 	}
        							 		str+="</select>";
        							 		$('#aadharlist').html(str);
        							 		//aadharlist
        									ko.applyBindings(self, $(".dynamic-page-content").get(0));

        							 });
        						}else{
        							navigator.notification.alert(invocationResult.account_nos);
        							//navigator.notification.alert("No account applicable to link AADHAR");
        						}

        						}
        						else{
        							busyInd.hide();
        							navigator.notification.alert(invocationResult.account_nos);
        							//navigator.notification.alert("No account applicable to link AADHAR");
        						}
        					}

        				busyInd.hide();

        				};



};


function pageDeeplinking_inbox(ln){
	//alert("pageDeeplinking1 : "+$("#hdn"+ln).val());
	var inboxlink=$("#hdn"+ln).val().toLowerCase();

		if (inboxlink == "accounts"){
		   window.location = "#rrasm01";
		}else if (inboxlink == "fundtransfer"){
		  window.location = "#rrftr01";
		}else if (inboxlink == "billpay"){
		   window.location =  "#billpayment";
		}else if (inboxlink == "creditcard"){
		   window.location = "#CreditCard";
		}
}

function openLink_inbox(ln){
	var inboxlink=$("#hdn"+ln).val();
	if(inboxlink!='null' && inboxlink!=null)
		window.open(inboxlink, '_system');
}

function getImageSize(img, callback) {
    var $img = $(img);

    var wait = setInterval(function() {
        var w = $img[0].naturalWidth,
            h = $img[0].naturalHeight;
        if (w && h) {
            clearInterval(wait);
            callback.apply(this, [w, h]);
        }
    }, 30);
}
 
 function getFormattedDate(date)
{
  
    return date.substring(8,10) + "-" + date.substring(5,7)+"-"+date.substring(0,4);
}

function getFormattedTime(date)
{
    return date.substring(8,16);
}	
 
function onNetworkCheck(){
			return ;
}



function onAdharBack(){
	//return ;
	 window.location = "#rrasm01";
}

//to generate random number for credit score
function randomString(length) {
	var result = '';
	var d = new Date();
	var chars = d.getTime();
	chars = chars + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	for (var i = length; i > 0; --i)
	{
		result += chars[Math.round(Math.random() * (chars.length - 1))];
	}

	return result;
}

//random nuumber for aadhar
function randomStringNum(length) {
	var result = '';
	var d = new Date();
	var chars = d.getTime();
	chars = chars + '';
	for (var i = length; i > 0; --i)
	{
		result += chars[Math.round(Math.random() * (chars.length - 1))];
	}

	return result;
}


//Credit Score OTP OTP
function CreditScoregetOTP() {
    if (window.navigator.onLine) {
        busyInd.show();
        reqParams = {};
        reqParams["RQLoginUserId"] = LoggedInUserID;
        reqParams["RQDeviceFamily"] = Device_Platform;
        reqParams["RQDeviceFormat"] = Device_Model;
        reqParams["RQOperationId"] = "OTPGENREQ";
        reqParams["RQClientAPIVer"] = RQClientAPIVer;
        reqParams["SessionId"] = CurrentSessionId;
        reqParams["RQTransSeq"] = "01";
        fldjsessionid = "";

        //busyInd.show();
        if (lgnout == false) { window.location.href = "#logout"; } var invocationData = {
            adapter: "API_Adapter",
            procedure: "GetOTP",
            parameters: [fldjsessionid, reqParams],
            compressResponse: true
        };

        WL.Client.invokeProcedure(invocationData, {
            onSuccess: CreditScoregetOTPSuccess,
            onFailure: AdapterFail,
            timeout: timeout
        });
    } else {
        navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
    }
};

CreditScoregetOTPSuccess = function (result) {
    invocationResult1 = result.invocationResult;
    if (invocationResult1.isSuccessful) {
        if (invocationResult1.RBL.Response) {
            busyInd.hide();
            var statusCode = invocationResult1.RBL.STATUS.CODE;
            if (statusCode == 0) {
                if (invocationResult1.RBL.RequestParams.RQLoginUserId == LoggedInUserID) {
                    OTP_number = invocationResult1.RBL.Response.otprefno;
                    $("#requestId").val("");
                    $("#requestId").val(OTP_number);
                   // alert("OTP is sent to your Registered Mobile Number.");
                    //WL.SimpleDialog.show(
                    //        "Alert", "OTP is sent to your Registered Mobile Number.",
                    //        [{
                    //            text: "OK", handler: function () { WL.Logger.debug("OK button pressed"); }
                    //        }]
                    //    );
                } else {
                    alert("Invalid UserID(CIF)");
                    window.location.hash = "#logout";
                }

            }
            else {
                alert("OTP Generation Failure! Try again by clicking Resend OTP.");
            }
        } else {
            busyInd.hide();
            handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
        }
    }
    busyInd.hide();
};