
/* JavaScript content from ViewModels/BillPay.js in folder common */
  var BillPay = function () {

        var self = this;
        
        self.curraccbalval = ko.observable();
        self.selAccount = ko.observable();
        self.accountStmtTxns = ko.observableArray([]);
        self.BillerList = ko.observableArray([]);
        self.BillerList1 = ko.observableArray([]);
		self.selBiller = ko.observable();
		self.billoperlist = ko.observableArray([]);
		self.categoryMainList = ko.observableArray([]);
        	self.categoryList = ko.observableArray([]);
        	self.locationList = ko.observableArray([]);
        	self.billersList = ko.observableArray([]);
        	self.utilityPayment = ko.observable();
        	self.utilityLocation = ko.observable();
        	self.utilityBiller = ko.observable();
        	self.previousPage = ko.observable();
        	self.pageType = ko.observable();
		
    /* =============== */
    
    this.rrblp01Page = function(){
		if(window.navigator.onLine){

    	reqParams = {};
			reqParams["RQLoginUserId"] = LoggedInUserID;
			reqParams["RQDeviceFamily"] = Device_Platform;
			reqParams["RQDeviceFormat"] = Device_Model;
			reqParams["RQOperationId"] = "BILLPAYRQ";
			reqParams["RQClientAPIVer"] = RQClientAPIVer;
			reqParams["SessionId"] = CurrentSessionId;
			reqParams["RQTransSeq"] = "01";
		
			fldjsessionid="";
			busyInd.show();
			if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetBillpay",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
			};
					
			WL.Client.invokeProcedure(invocationData, {
				onSuccess : rrblp01Success,
				onFailure : AdapterFail,	    		
				timeout: timeout
			});
		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
    };
		
    rrblp01Success = function(result){
		invocationResult = result.invocationResult; //custbillerinfo
		    if (invocationResult.isSuccessful) {
                if (invocationResult.RBL) {
					if(invocationResult.RBL.STATUS.CODE == '0') {
                        numcustcomp = invocationResult.RBL.Response.custbillercount;
                        //console.log("Biller nobiller " + numcustcomp);
                        self.BillerList.removeAll();
                        self.BillerList1.removeAll();
						accountListFt.removeAll();
                        if (numcustcomp != 0) {
							//accounts
							acctdtls = invocationResult.RBL.Response.validAcct.acctdetails;
							custdtls = invocationResult.RBL.Response.CustDetails;
							var idx = 1;
							$(acctdtls).each(function(index, obj) {
								strid = "item"+idx;
										
								$(custdtls).each(function(j, obj1) {
									custnames="";
									if(obj.acctindex == obj1.acctindex){
										var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
										displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
										accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+'#'+obj.acctbalance, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
									}
								});
								idx++;
							});
							// biller accounts
                            custcompinfo = invocationResult.RBL.Response.custbillerinfo; //.custcompinfo;//custbillerinfo
                            var idx = 1;
                            $(custcompinfo).each(function (index, obj) {
                                //alert(obj.noofbills);
                                if (typeof (obj.custAcctBal) != "undefined") {
                                    custAcctBal = obj.custAcctBal;
                                } else {
                                    custAcctBal = '';
                                }

                                if (typeof (obj.compcurr) != "undefined") {
                                    compcurr = obj.compcurr;
                                } else {
                                    compcurr = '';
                                }
                                if (typeof (obj.acctCurr) != "undefined") {
                                    acctCurr = obj.acctCurr;
                                } else {
                                    acctCurr = '';
                                }
                                if (typeof (obj.billdetails) != "undefined") {
                                    displayValue = obj.billerName + "#" + obj.billAcctno + "#" + custAcctBal + "#" + obj.billcompacctno + "#" + obj.billid + "#" + obj.billCode1 + "#" + obj.billInfoAvailable + "#" + obj.paytype + "#" + obj.billerNickname + "#" + obj.billdetails.billoutstanding + "#" + obj.billdetails.billdate + "#" + obj.billdetails.billno + "#" + obj.billdetails.billpaymentdate + "#" + obj.noofbills + "#" + acctCurr + "#" + compcurr;
                                } else {
                                    displayValue = obj.billerName + "#" + obj.billAcctno + "#" + custAcctBal + "#" + obj.billcompacctno + "#" + obj.billid + "#" + obj.billCode1 + "#" + obj.billInfoAvailable + "#" + obj.paytype + "#" + obj.billerNickname + "#####" + obj.noofbills + "#" + acctCurr + "#" + compcurr;

                                }
                                displaytxt = $.trim(obj.billerName) + "-" + obj.billerNickname;
                                if (obj.noofbills != '0' || obj.billInfoAvailable == 'N') {
                                    strid = "item" + idx;
                                    custnames = "";
                                    self.BillerList.push({
                                        displayvalue1: displayValue,
                                        displaytxt: displaytxt,
                                        strid: strid,
                                        billcompname: obj.billerName,
                                        mnemname: obj.billerNickname
                                    });
                                    idx++;
                                }
                                if (obj.noofbills == '0' && obj.billInfoAvailable != 'N') {
									strid = "item" + idx;
                                    self.BillerList1.push({
                                        displayvalue1: displayValue,
                                        displaytxt: displaytxt,
                                        strid: strid,
                                        billcompname: obj.billerName,
                                        mnemname: obj.billerNickname
                                    });
                                    idx++;
                                }
                            });
                        }

                        $("#contentData").load("Views/BillPay/rrblp01.html", null, function (response, status, xhr) {
                            if (status != "error") {}
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
                            if (numcustcomp == 0) {
                                $("#Nobiller").show();
                                $("#biller").hide();
                            } else {
								//alert(self.BillerList1.length);
								if(self.BillerList1().length == '0'){
									$("#info").hide();
								}
                                $("#Nobiller").hide();
                                $("#biller").show();
                            }
                        });

                    } else {
						busyInd.hide();
						  $("#contentData").load("Views/BillPay/rrblp01.html", null, function (response, status, xhr) {
                            if (status != "error") {}
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
							$("#biller").hide();

							// As per RBL team requirement, we need
                            								// to show option to add biller when no
                            								// biller is assigned to particular
                            								// customer id.
                            								// For this we checked code INV_MSG_079
                            								// and for this code we shown option to
                            								// add biller.
                            								 //alert(invocationResult.RBL.STATUS.CODE);
                            								if ((invocationResult.RBL.STATUS.CODE == 'INV_MSG_073') || (invocationResult.RBL.STATUS.CODE == 'INV_MSG_076') || (invocationResult.RBL.STATUS.CODE == 'INV_MSG_079') || (invocationResult.RBL.STATUS.CODE == 'INV_MSG_096')) {
                            									$("#error").html("Currently no Bills available for payment.");
                            								}

                            								else{

                            									$("#error").html(invocationResult.RBL.STATUS.CODE+ " "	+ invocationResult.RBL.STATUS.MESSAGE);
                            								}

							$("#labelname").hide();
							//$("#error").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
							$("#Nobiller").show();
						 });
					}
                }else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
            }
            busyInd.hide();
		
    };
	
	this.rrblp01Submit = function(){
		if($("#fbiller").valid()){
			busyInd.show();
				selectedacntavlbal($('#fldAcctNo').val().split("#")[2]);
				beneffrom_accnt_no($('#fldAcctNo').val().split("#")[0]);
				utilityname($('#fldBillDetails').val().split("#")[0]);
				billaccountno($('#fldBillDetails').val().split("#")[1]);
				billId($('#fldBillDetails').val().split("#")[4]);
				billCode1($('#fldBillDetails').val().split("#")[5]);
				billoutstanding($('#fldBillDetails').val().split("#")[9]);
				billpaymentdate($('#fldBillDetails').val().split("#")[12]);
				billdate($('#fldBillDetails').val().split("#")[10]);
				billno($('#fldBillDetails').val().split("#")[11]);
				billPayType($('#fldBillDetails').val().split("#")[7]);
					
				busyInd.hide();
			// $("#contentData").load("Views/BillPay/rrblp02.html", null, function (response, status, xhr) {
                // if (status != "error") {}
                // ko.applyBindings(self, $(".dynamic-page-content").get(0));
				
			// });
			window.location.hash = "#rrblp02";
		}
	};
	
	this.rrblp02Page = function(){
			$("#contentData").load("Views/BillPay/rrblp02.html", null, function (response, status, xhr) {
	    	    if (status != "error") {}
				if(billPayType()== "F" || billPayType()== "f"){
					$('#fldCompanyName').html(utilityname());
					$('#fldBillCompId').html(billId());
					$('#fldConsumerNo').html(billaccountno());
					$('#fldFromAcctNo').html(beneffrom_accnt_no());
					$('#fldCustAcctBal').html("₹ " +formatAmt(parseFloat(selectedacntavlbal())));
					$('#billamount1').show();
					$('#billamount').html("₹ " +formatAmt(parseFloat(billoutstanding())));
					$('#billOutStandingDiv').hide();
				}
				else{
					$('#fldCompanyName').html(utilityname());
					$('#fldBillCompId').html(billId());
					$('#fldConsumerNo').html(billaccountno());
					$('#fldFromAcctNo').html(beneffrom_accnt_no());
					$('#fldCustAcctBal').html("₹ " +formatAmt(parseFloat(selectedacntavlbal())));
					$('#billOutStandingDiv').show();
				}
				ko.applyBindings(self, $(".dynamic-page-content").get(0));
	    	});
		busyInd.hide();		
	};
	
	this.rrblp02Submit = function(){
		if($("#rrblp02").valid()){
			$('#confirm_button1').prop('disabled', true);
			var paytype = billPayType();
			if(paytype == "P" || paytype == "p"){
				billoutstanding($('#fldTxnAmt').val());
			}
			//GetOTP();
			//busyInd.hide();
			GetOTP();
			//window.location.hash = "#rrblp03";
		}
	};
		
	this.rrblp03Page = function(){
			
		$("#contentData").load("Views/BillPay/rrblp03.html", null, function (response, status, xhr) {
	    	if (status != "error") {}	
	    	OTP_number = invocationResult1.RBL.Response.otprefno;
			$('#OTP_Ref').val(OTP_number);
			if(invocationResult1.RBL.Response.otprequired == 'Y'){
				$('#OTP_No').show();
			}
			else{
				$('#OTP_No').hide();
			}
			$('#fldCompanyName').html(utilityname());
			$('#fldBillCompId').html(billId());
			$('#fldConsumerNo').html(billaccountno());
			$('#fldFromAcctNo').html(beneffrom_accnt_no());
			$('#fldCustAcctBal').html("₹ " +formatAmt(parseFloat(selectedacntavlbal())));
			$('#billamount').html("₹ " +formatAmt(parseFloat(billoutstanding())));
			ko.applyBindings(self, $(".dynamic-page-content").get(0));
			
		});
		busyInd.hide();	
	};
	
	this.rrblp03Submit = function(){
	if($("#frmftr03").valid()){
		if(window.navigator.onLine){
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "BILLPAYRQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQFromAcctNo"] = beneffrom_accnt_no();
				reqParams["RQBillAccNo"] = billaccountno();
				reqParams["RQBillId"] = billId();
				reqParams["billCode"] = billCode1();
				reqParams["RQBillerName"] = utilityname();
				reqParams["RQBillerNickName"] = utilityname();
				reqParams["billoutstanding"] = billoutstanding();
				reqParams["billpaymentdate"] = billpaymentdate();
				reqParams["billdate"] = billdate();
				reqParams["billno"] = billno();
				reqParams["RQPayType"] = billPayType();
				reqParams["RQTransSeq"] = "03";
				reqParams["RQDesc"] = $('#billdesc').val();
				reqParams["RQOTP"] = $('#OTP_No_Val').val();
				reqParams["RQOTPReq"] = invocationResult1.RBL.Response.otprequired;
				reqParams["RQRefNo"] = $('#OTP_Ref').val();
				
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "billpaysubmit",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrblp03SubmitSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});

		}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	}
	};
	
	rrblp03SubmitSuccess = function(result){
			busyInd.hide();
        	invocationResult = result.invocationResult;
        	if(invocationResult.isSuccessful) {
        		if(invocationResult.RBL){	
					if(invocationResult.RBL.Response || invocationResult.RBL.Response == ''){
							selALLBiller(invocationResult.RBL);    
							window.location = "#rrblp04";
					}else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
					}
        		}else{
					busyInd.hide();
					handleErrorNoResponse();
				}
        	}
	};
	
	this.rrblp04Page = function(){
			busyInd.hide();
			selBillerValue = selALLBiller();
			$("#contentData").load("Views/BillPay/rrblp04.html", null, function (response, status, xhr) {
				if(invocationResult.RBL.STATUS.CODE == '0'){
						$(".success_msg").show();
						$("#tpn06success").show();
						busyInd.hide();
						accountList.removeAll();	
						
						$('#fldCompanyName').html(selBillerValue.Response.custbillerinfo.billerName);
						$('#fldFromAcctNo').html(selBillerValue.RequestParams.RQFromAcctNo);
						//$('#fldCustAcctBal').html(selBillerValue.RequestParams.fldCustAcctBal);
						$('#fldBillCompId').html(selBillerValue.RequestParams.RQBillId);
						$('#fldConsumerNo').html(selBillerValue.Response.custbillerinfo.billAcctno);
						$('#refno').html(selBillerValue.Response.txnrefno);
						//$('#balavailable').html(selBillerValue.response.balavailable);
						//$('#billamount').html(selBillerValue.response.paymentdtls.billamount);
						//balavailableS
						// accountList.removeAll();
						// accountSummList.removeAll(); ("₹ " +formatAmt(parseFloat(billoutstanding())));
						
						$('#balavailableS').html("₹ " +formatAmt(parseFloat(selBillerValue.RequestParams.billoutstanding)));
						$('#billamountS').html("₹ " +formatAmt(parseFloat(selBillerValue.RequestParams.billoutstanding)));
				}
				else{
					busyInd.hide();
					$(".rsaOOBErr").show();
					$(".rsaOOBErr p").html(invocationResult.RBL.STATUS.CODE+": "+invocationResult.RBL.STATUS.MESSAGE);
					$("#tpn06success").hide();
				}
			busyInd.hide();
			if (status != "error") {}	
			ko.applyBindings(self, $(".dynamic-page-content").get(0)); 

			// billOutStanding = selBillerValue.request.fldBillAmt;
			// fldcustacctbal  = selBillerValue.request.fldcustacctbal;
			});
	};
	
	//OTP	
	GetOTP = function(){
		if(window.navigator.onLine){
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "OTPGENREQ";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetOTP",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : GetOTPResponse,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
		}
	};	

		GetOTPResponse = function(result){
			invocationResult1 = result.invocationResult;
			if(invocationResult1.RBL){
				if(invocationResult1.isSuccessful) {
					if(invocationResult1.RBL.Response){	
						if(window.location.hash == "#rrblp02"){
							busyInd.hide();
							$('#confirm_button1').prop('disabled', false);
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
							window.location = '#rrblp03';
						}
						else{
							busyInd.hide();
							OTP_number = invocationResult1.RBL.Response.otprefno;
							$('#OTP_Ref').val(OTP_number);
						}
					}else{
							busyInd.hide();
							handleError(invocationResult1.RBL.Response, invocationResult1.RBL);
					}		
				}
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}
			busyInd.hide();
		};
		
		//mobile Recharge
		this.rrpmb01Page = function(){
			if(window.navigator.onLine){
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetOperator",
						parameters : [],
						compressResponse : true
				};
						
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrpmb01Success,
					onFailure : AdapterFail,	    		
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		rrpmb01Success = function (result){
				invocationResult2 = result.invocationResult;
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "VIWCHQSTS";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetAccounts",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : bankacntSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
		};
		
		bankacntSuccess = function(result){
			invocationResult = result.invocationResult; //custbillerinfo
		    if (invocationResult.isSuccessful) {
                if (invocationResult.RBL) {
				accountListFt.removeAll();
				mobileList.removeAll();
					if(invocationResult.RBL.STATUS.CODE == '0') {
						acctdtls = invocationResult.RBL.Response.acctdetails;
						custdtls = invocationResult.RBL.Response.CustDetails;
						var idx = 1;
							$(acctdtls).each(function(index, obj) {
								strid = "item"+idx;		
								$(custdtls).each(function(j, obj1) {
									custnames="";
									if(obj.acctindex == obj1.acctindex){
										var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
										displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
										accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+'#'+obj.acctbalance, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
									}
								});
								idx++;
						});
						
						$(invocationResult2.RBL.response.Mobile).each(function(index, obj) {
							displaytxt = obj.BillerName;
							value = obj.BillerID+"#"+obj.BillerName;
							mobileList.push({displaytxt:displaytxt,values:value});
						});
						
							
						$("#contentData").load("Views/recharge/rrpmb01.html", null, function (response, status, xhr) {
                            if (status != "error") {}
							
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
                 
						});
					}
				}else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			}
			busyInd.hide();
		};
		
		rrpmb01Submit = function(){
			if($("#frmrrpmb01").valid()){
				$('#confirm_button1').prop('disabled', true);
				busyInd.show();
				var billerID = $('#fldoperator').val().split('#')[0];
				rhgfrmacnt($('#fldFromAcctNo').val().split('#')[0]);
				var subscriberID = $('#fldmobno').val();
				var rechargeAmount = parseFloat($('#fldAmtTxn').val());
				var customerID = LoggedInUserID;
				subscriber_no(subscriberID);
				recharge_amnt(rechargeAmount);
				biller_id(billerID);
				biller_name($('#fldoperator').val().split('#')[1]);
				dbtaccntnumber = rhgfrmacnt();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "validateRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,dbtaccntnumber]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : validateRechargeSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		validateRechargeSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.validateRechargeResponse){
				if(invocationResult.validateRechargeResponse.validateRechargeReply){
						busyInd.hide();
						payment_id(invocationResult.validateRechargeResponse.validateRechargeReply.paymentID);
						rchgotp_key(invocationResult.validateRechargeResponse.validateRechargeReply.otpKey);
						window.location = "#rrpmb02";
				}
				else if(invocationResult.Fault){
					busyInd.hide();
					$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
				}else{
					$('#confirm_button1').prop('disabled', false);
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}			
		};
				
		this.rrpmb02Page = function(){
			$("#contentData").load("Views/recharge/rrpmb02.html", null, function (response, status, xhr) {
                if (status != "error") {}
					busyInd.hide();
					$('#fldfromacctno').html(rhgfrmacnt());
					$('#fldmobno').html(subscriber_no());
					$('#fldmoboperator').html(biller_name());
					$('#fldamttxnamnt').html(recharge_amnt());
					$('#OTP_Ref').val(rchgotp_key());
                ko.applyBindings(self, $(".dynamic-page-content").get(0));
			});
		};
		
		rrpmb02Submit  = function(){
			if($("#frmrrpmb02").valid()){
				busyInd.show();
				var billerID = biller_id();
				var debitAccountID = $('#fldfromacctno').html();
				var subscriberID = $('#fldmobno').html();
				var rechargeAmount = parseFloat($('#fldamttxnamnt').html());
				
				var customerID = LoggedInUserID;
				var paymentID = payment_id();
				var otp = $('#OTP_No_Val').val();
				var otpKey = $('#OTP_Ref').val();
				
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "debitAccountAndRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,paymentID,debitAccountID,otp,otpKey]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : RechargeSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		RechargeSuccess = function(result){
		busyInd.hide();
			invocationResult = result.invocationResult;
			if(invocationResult.debitAccountAndRechargeResponse){
				if(invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply){
					Rchgresponse = invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply;
						$("#contentData").load("Views/recharge/rrpmb03.html", null, function (response, status, xhr) {
							if (status != "error") {}
							if(Rchgresponse.rechargeStatus.code == 'SUCCESS'){
								busyInd.hide();
								$('.success_msg').show();
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
							}
							else{
								$(".rsaOOBErr").show();
								$(".rsaOOBErr p").html(Rchgresponse.rechargeStatus.description);
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
							}
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
							busyInd.hide();
						});
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}	
		};
		
		//dth recharge
		this.rrdth01Page = function(){
			if(window.navigator.onLine){
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
						adapter : "API_Adapter",
						procedure : "GetOperator",
						parameters : [],
						compressResponse : true
				};
						
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : rrdth01Success,
					onFailure : AdapterFail,	    		
					timeout: timeout
				});
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		rrdth01Success = function(result){
			invocationResult2 = result.invocationResult;
				busyInd.show();
				reqParams = {};
				reqParams["RQLoginUserId"] = LoggedInUserID;
				reqParams["RQDeviceFamily"] = Device_Platform;
				reqParams["RQDeviceFormat"] = Device_Model;
				reqParams["RQOperationId"] = "VIWCHQSTS";
				reqParams["RQClientAPIVer"] = RQClientAPIVer;
				reqParams["SessionId"] = CurrentSessionId;
				reqParams["RQTransSeq"] = "01";
				fldjsessionid="";
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "GetAccounts",
					parameters : [fldjsessionid,reqParams],
					compressResponse : true
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : bankdthacntSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
		};
		
		bankdthacntSuccess = function(result){
			invocationResult = result.invocationResult; 
		    if (invocationResult.isSuccessful) {
                if (invocationResult.RBL) {
					accountListFt.removeAll();
					dthList.removeAll();
					if(invocationResult.RBL.STATUS.CODE == '0') {
						acctdtls = invocationResult.RBL.Response.acctdetails;
						custdtls = invocationResult.RBL.Response.CustDetails;
						var idx = 1;
							$(acctdtls).each(function(index, obj) {
								strid = "item"+idx;		
								$(custdtls).each(function(j, obj1) {
									custnames="";
									if(obj.acctindex == obj1.acctindex){
										var actbal = "₹ "+formatAmt(parseFloat(obj.acctbalance));
										displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
										accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+'#'+obj.acctbalance, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
									}
								});
								idx++;
						});
						
						$(invocationResult2.RBL.response.DTH).each(function(index, obj) {
							displaytxt = obj.BillerName;
							value = obj.BillerID+"#"+obj.BillerName;
							dthList.push({displaytxt:displaytxt,values:value});
						});
							
						$("#contentData").load("Views/recharge/rrdth01.html", null, function (response, status, xhr) {
                            if (status != "error") {}		
                            ko.applyBindings(self, $(".dynamic-page-content").get(0));
						});
						busyInd.hide();
					}
				}else{
						busyInd.hide();
						handleError(invocationResult.RBL.Response, invocationResult.RBL);
				}
			}
			busyInd.hide();
		};
		
		rrdth01Submit = function(){
			if($("#frmrrdth01").valid()){
				$('#confirm_button1').prop('disabled', true);
				var billerID = $('#fldoperator').val().split('#')[0];
				rhgfrmacnt($('#fldFromAcctNo').val().split('#')[0]);
				var subscriberID = $('#fldsubno').val();
				var rechargeAmount = parseFloat($('#fldAmtTxn').val());
				var customerID = LoggedInUserID;
				subscriber_no(subscriberID);
				recharge_amnt(rechargeAmount);
				biller_id(billerID);
				biller_name($('#fldoperator').val().split('#')[1]);
				dbtaccntnumber = rhgfrmacnt();
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "validateRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,dbtaccntnumber]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : validateDTHRechargeSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		validateDTHRechargeSuccess = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.validateRechargeResponse){
				if(invocationResult.validateRechargeResponse.validateRechargeReply){
					busyInd.hide();
						payment_id(invocationResult.validateRechargeResponse.validateRechargeReply.paymentID);
						rchgotp_key(invocationResult.validateRechargeResponse.validateRechargeReply.otpKey);
						window.location = "#rrdth02";
						
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}
			else if(invocationResult.Fault){
				busyInd.hide();
				$('#confirm_button1').prop('disabled', false);
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
				$('#confirm_button1').prop('disabled', false);
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}				
		};
		
		this.rrdth02Page = function(){
			$("#contentData").load("Views/recharge/rrdth02.html", null, function (response, status, xhr) {
                if (status != "error") {}
					$('#fldfromacctno').html(rhgfrmacnt());
					$('#fldsubno').html(subscriber_no());
					$('#flddthoperator').html(biller_name());
					$('#fldamttxnamnt').html(recharge_amnt());
					$('#OTP_Ref').val(rchgotp_key());
                ko.applyBindings(self, $(".dynamic-page-content").get(0));
				busyInd.hide();
			});
		};
		
		rrdth02Submit  = function(){
			if($("#frmrrdth02").valid()){
				var billerID = biller_id();
				var debitAccountID = $('#fldfromacctno').html();
				var subscriberID = $('#fldsubno').html();
				var rechargeAmount = parseFloat($('#fldamttxnamnt').html());
				var customerID = LoggedInUserID;
				var paymentID = payment_id();
				var otp = $('#OTP_No_Val').val();
				var otpKey = $('#OTP_Ref').val();
				busyInd.show();
				if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
					adapter : "API_Adapter",
					procedure : "debitAccountAndRecharge",
					parameters : [customerID, billerID, subscriberID, rechargeAmount,paymentID,debitAccountID,otp,otpKey]
					
				};
    	
				WL.Logger.debug(invocationData, '');
				WL.Client.invokeProcedure(invocationData, {
					onSuccess : RechargedthSuccess,
					onFailure : AdapterFail,
					timeout: timeout
				});
			}
		};
		
		RechargedthSuccess = function(result){
		busyInd.hide();
			invocationResult = result.invocationResult;
			if(invocationResult.debitAccountAndRechargeResponse){
				if(invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply){
					Rchgresponse = invocationResult.debitAccountAndRechargeResponse.debitAccountAndRechargeReply;
						$("#contentData").load("Views/recharge/rrdth03.html", null, function (response, status, xhr) {
							if (status != "error") {}
							if(Rchgresponse.rechargeStatus.code == 'SUCCESS'){
								$('.success_msg').show();
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
								busyInd.hide();
							}
							else{
								$(".rsaOOBErr").show();
								$(".rsaOOBErr p").html(Rchgresponse.rechargeStatus.description);
								$('.bankacctno').html(rhgfrmacnt());
								$('.referenceno').html(Rchgresponse.TransactionID);
								$('.operator').html(biller_name());
								$('.subnumber').html(subscriber_no());
								$('.txnamnt').html(recharge_amnt());
								$('.txnStatus').html(Rchgresponse.rechargeStatus.description);
								busyInd.hide();
							}
							ko.applyBindings(self, $(".dynamic-page-content").get(0));
							busyInd.hide();
						});
						busyInd.hide();
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}
			}else if(invocationResult.Fault){
				busyInd.hide();
				navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
        		navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		busyInd.hide();
			}	
		};
	
		//Recharge Status
		this.rrcrs01Page = function(){
			$("#contentData").load("Views/recharge/rrcrs01.html", null, function (response, status, xhr) {
                if (status != "error") {}                         
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
		};
		
		rrcrs01Submit = function(){
			if(window.navigator.onLine){
				if($("#frmstatus01").valid()){
					var customerID = LoggedInUserID;
					busyInd.show();
					var TransactionID = $('#fldTxnRefNo').val().toUpperCase();
					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "getRechargeStatus",
							parameters : [customerID,TransactionID],
							compressResponse : true
					};
							
					WL.Client.invokeProcedure(invocationData, {
						onSuccess : rrcsr01Success,
						onFailure : AdapterFail,	    		
						timeout: timeout
					});
				}
			}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
			}
		};
		
		rrcsr01Success = function(result){
			invocationResult = result.invocationResult;
			if(invocationResult.getRechargeStatusResponse){
				if(invocationResult.getRechargeStatusResponse.getRechargeStatusReply){
					busyInd.hide();
					if(window.navigator.onLine){
						busyInd.show();
						if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
							adapter : "API_Adapter",
							procedure : "GetOperator",
							parameters : [],
							compressResponse : true
						};
						WL.Client.invokeProcedure(invocationData, {
							onSuccess : getoperatorsucess,
							onFailure : AdapterFail,	    		
							timeout: timeout
						});
					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK"); 
					}
				}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
				}	
			}else if(invocationResult.Fault){
					busyInd.hide();
					navigator.notification.alert(invocationResult.Fault.Reason.Text);
			}else if(invocationResult.RBL){
				busyInd.hide();
				handleError(invocationResult.RBL.Response, invocationResult.RBL);
			}else{
					navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
					busyInd.hide();
			}
		};
		
		getoperatorsucess = function(result){
			invocationResult3 = result.invocationResult;
			var id = invocationResult.getRechargeStatusResponse.getRechargeStatusReply.billerID;
			var temp = getObjects(invocationResult3, 'BillerID', id);
			biller_name(temp[0].BillerName);
			window.location="#rrcrs02";
		};
		
		this.rrcrs02Page = function(){
			busyInd.hide();
			$("#contentData").load("Views/recharge/rrcrs02.html", null, function (response, status, xhr) {
				busyInd.hide();
                if (status != "error") {}
					$('#operator_name').html(biller_name());
					$('#subcriber_no').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.subscriberID);
					$('#rchg_amnt').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.rechargeAmount);
					$('#rchg_status').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.rechargeStatus.description);
					$('#txn_ref_no').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.bankRefNo);
					$('#txn_date').html(invocationResult.getRechargeStatusResponse.getRechargeStatusReply.paymentDate);
                ko.applyBindings(self, $(".dynamic-page-content").get(0));                   
            });
		};
		
		function getObjects(obj, key, val) {
			var objects = [];
			for (var i in obj) {
				if (!obj.hasOwnProperty(i)) continue;
				if (typeof obj[i] == 'object') {
					objects = objects.concat(getObjects(obj[i], key, val));
				} else if (i == key && obj[key] == val) {
					objects.push(obj);
				}
			}
			return objects;
		}


		// Biller module
        	// This function is used to fetch the category list
        	this.getCategory = function() {

        		if (window.navigator.onLine) {

        			// Save current session id
        			previousSession = CurrentSessionId;

        			busyInd.show();

        			var getCategories = {
        				"getCategories" : {
        					"BillDeskMode" : "REGISTERPAY",
        					"Channel" : "MOB"
        				}
        			}

        			if (lgnout == false) {
        				window.location.href = "#logout";
        			}

        			var invocationData = {
        				adapter : "API_Adapter",
        				procedure : "biller",
        				parameters : [ booksStore(JSON.stringify(getCategories)),
        						booksStore(LoggedInUserID) ],
        				compressResponse : true
        			};

        			WL.Client.invokeProcedure(invocationData, {
        				onSuccess : getCategoriesSuccess,
        				onFailure : AdapterFail,
        				timeout : timeout
        			});

        		} else {
        			navigator.notification
        					.confirm(
        							"Please ensure that you have network connectivity and try again!",
        							onNetworkCheck, "Connection Error", "OK");
        		}
        	};

        	// Sucess function of get categories
        	getCategoriesSuccess = function(result) {

        		if (previousSession != CurrentSessionId) {
        			return;
        		}
        		// #################
        		invocationResult = result.invocationResult;
        		// alert(JSON.stringify(invocationResult));
        		//console.log(JSON.stringify(invocationResult));
        		// alert("1");
        		self.categoryList.removeAll();
        		self.categoryMainList.removeAll();
        		// alert("2");
        		if (invocationResult.isSuccessful) {

        			if (invocationResult.getCategoriesResponse) {
        				// alert("3");
        				if (invocationResult.getCategoriesResponse.getCategoryList) {
        					var allData = invocationResult.getCategoriesResponse.getCategoryList;
        					// alert("4");
        					var categories = allData.Category;
        					var locationss = allData.biller_presence;
        					// alert(locationss[0]);

        					for (var i = 0; i < categories.length; i++) {
        						// var Category = $.trim(categories[i]);
        						self.categoryList.push({
        							Category : $.trim(categories[i])
        						});
        					}

        					for (var i = 0; i < locationss.length; i++) {
        						// var Location = $.trim(locationss[i]);
        						self.categoryMainList.push({
        							BillerPresence : $.trim(locationss[i])
        						});
        					}
        					///
        					for (var i = 0; i < self.categoryMainList().length; i++) {
        						// var Location = $.trim(locationss[i]);
        						self.locationList.push({
        							BillerPresence : $.trim(self.categoryMainList()[i].BillerPresence)
        						});
        					}
        					////
        					//alert(categoryMainList.length);
        				}
        			}

        			// alert("5");
        			// Check for the length of categories. If it is greater than 0 then
        			// show error otherwise show add biller UI.
        			$("#contentData").load(	"Views/Biller/BillerHome.html",null,
        					function(response, status, xhr) {
        						if (status != "error") {
        						}
        						$(".h_title").html("Add Biller - Utility Payments");
        						if (self.categoryList().length == 0) {

        							$("#NoCategory").show();
        							$("#YesCategory").hide();
        						} else {
        							$("#NoCategory").hide();
        							$("#YesCategory").show();
        						}

        						ko.applyBindings(self, $(".dynamic-page-content").get(0));
        					});

        		} else {
        			navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        			busyInd.hide();
        		}
        		busyInd.hide();
        	};

        	// var getCategories = {"getCategoriesResponse": {"getCategoryList": {
        	// "Category": [
        	// "Donations",
        	// "Electricity",
        	// "Gas",
        	// "Insurance",
        	// "ISP",
        	// "Mutual Funds",
        	// "Rental Payments",
        	// "Subscriptions",
        	// "Tax",
        	// "Telecom"
        	// ],
        	// "biller_presence": [
        	// "Assam",
        	// "Bihar",
        	// "Chennai",
        	// "Chhattisgarh",
        	// "Delhi",
        	// "Delhi NCR",
        	// "Gujarat",
        	// "Haryana",
        	// "Himachal Pradesh",
        	// "Jammu and Kashmir"
        	// ]
        	// }}}

        	// alert(JSON.stringify(getCategories));
        	//
        	// var allData = getCategories.getCategoriesResponse.getCategoryList;
        	//
        	// var categories = allData.Category;
        	// var locationss = allData.biller_presence;
        	// alert(locationss[0]);
        	//
        	// for (var i = 0; i < categories.length; i++)
        	// {
        	// //var Category = $.trim(categories[i]);
        	// self.categoryList.push({ Category: $.trim(categories[i])
        	// });
        	// }
        	//
        	//
        	// //Here we will enable the drop down of Location.
        	// self.categoryMainList.removeAll();
        	//
        	// for (var i = 0; i < locationss.length; i++)
        	// {
        	// //var Location = $.trim(locationss[i]);
        	// self.categoryMainList.push({ BillerPresence: $.trim(locationss[i])
        	// });
        	// }
        	//
        	//
        	//
        	//
        	//
        	// busyInd.hide();
        	//
        	// //Check for the length of categories. If it is greater than 0 then show
        	// error otherwise show add biller UI.
        	// $("#contentData").load("Views/Biller/BillerHome.html", null, function
        	// (response, status, xhr) {
        	// if (status != "error") {}
        	// $(".h_title").html("Add Biller");
        	// if (self.categoryList().length == 0) {
        	//
        	// $("#NoCategory").show();
        	// $("#YesCategory").hide();
        	// }
        	// else
        	// {
        	// $("#NoCategory").hide();
        	// $("#YesCategory").show();
        	// }
        	//
        	// ko.applyBindings(self, $(".dynamic-page-content").get(0));
        	// });
        	// return;
        	// //###############
        	// invocationResult = result.invocationResult;
        	//
        	// console.log(JSON.stringify(invocationResult));
        	// //alert(JSON.stringify(invocationResult))
        	//
        	// self.categoryMainList.removeAll();
        	// if (invocationResult.isSuccessful) {
        	//
        	// if(invocationResult.getCategoriesResponse)
        	// {
        	//
        	// if(invocationResult.getCategoriesResponse.length!=0)
        	// {
        	//
        	// categories = invocationResult.getCategoriesResponse;
        	//
        	// $(categories).each(function(index, obj) {
        	//
        	// var Category = $.trim(obj.Category);
        	// var Sub_Category = $.trim(obj.Sub_Category);
        	// var BillerPresence = $.trim(obj.biller_presence);
        	//
        	// if(Category == 'Gas' || Category == 'Tax' || Category == 'Electricity' ||
        	// Category == 'Water' || Category == 'Telecom' )
        	// {
        	// self.categoryMainList.push({ Category: obj.Category,
        	// Sub_Category : obj.Sub_Category,
        	// BillerPresence: obj.biller_presence})
        	// }
        	//
        	// });
        	//
        	// }
        	//
        	//
        	// }
        	//
        	// self.categoryList.removeAll();
        	//
        	// var l = self.categoryMainList().length;
        	// var i = 0;
        	// var j= 0;
        	// for( i=0; i<l; i++)
        	// {
        	//
        	// if(self.categoryList().length == 0)
        	// {
        	// self.categoryList.push(self.categoryMainList()[i]);
        	// continue;
        	// }
        	//
        	// var res = false;
        	//
        	// for(j=0; j<self.categoryList().length; j++)
        	// {
        	// if(self.categoryList()[j].Category ==
        	// self.categoryMainList()[i].Category)
        	// {
        	// res = true;
        	// break;
        	// }
        	// }
        	//
        	// if(res == false)
        	// {
        	// self.categoryList.push(self.categoryMainList()[i]);
        	// }
        	//
        	//
        	// }
        	//
        	//
        	//
        	// //Check for the length of categories. If it is greater than 0 then show
        	// error otherwise show add biller UI.
        	// $("#contentData").load("Views/Biller/BillerHome.html", null, function
        	// (response, status, xhr) {
        	// if (status != "error") {}
        	// $(".h_title").html("Add Biller");
        	// if (self.categoryList().length == 0) {
        	//
        	// $("#NoCategory").show();
        	// $("#YesCategory").hide();
        	// }
        	// else
        	// {
        	// $("#NoCategory").hide();
        	// $("#YesCategory").show();
        	// }
        	//
        	// ko.applyBindings(self, $(".dynamic-page-content").get(0));
        	// });
        	//
        	//
        	//
        	// }
        	// else
        	// {
        	// navigator.notification.alert("We apologize this facility is temporarily
        	// unavailable.Please try later. ");
        	// busyInd.hide();
        	// }
        	//
        	// busyInd.hide();
        	//
        	//
        	// };

        	// Select sunscriber of utility payment drop down
        	this.utilityPayment.subscribe(function(newValue) {
        		// Here we will enable the drop down of Location.
        		self.locationList.removeAll();

        		if (newValue == null) {
        			return;
        		}

        		// Here we will enable the drop down of Location.

        		for (var i = 0; i < self.categoryMainList().length; i++) {
        			// var Location = $.trim(locationss[i]);
        			self.locationList.push({
        				BillerPresence : $.trim(self.categoryMainList()[i].BillerPresence)
        			});
        		}

        		// var l = self.categoryMainList().length;
        		// var i = 0;
        		// var j= 0;
        		// for( i=0; i<l; i++)
        		// {
        		// if(newValue == self.categoryMainList()[i].Sub_Category)
        		// {
        		// self.locationList.push(self.categoryMainList()[i]);
        		// }
        		// }
        	}, this);

        	// Select sunscriber of utility location drop down
        	this.utilityLocation.subscribe(function(newValue) {
        		if (newValue != null) {
        			self.getBillers();
        		} else {
        			self.billersList.removeAll();
        		}

        	}, this);

        	// Select sunscriber of utility biller drop down
        	this.utilityBiller.subscribe(function(newValue) {
        		if (newValue != null) {

        			// alert(newValue);
        		} else {
        			var accCur = "";

        		}

        	}, this);

        	// This function is used to get the billers list
        	this.getBillers = function() {
        		if (window.navigator.onLine) {

        			// Save current session id
        			previousSession = CurrentSessionId;

        			busyInd.show();

        			var getBillers = {
        				"getBillers" : {
        					"BillDeskMode" : "REGISTERPAY",
        					"Channel":"MOB",
        					"Category" : "",//$('#utilityPayment').val(),// 'Utility',
        					// "Sub_Category":"",//$('#utilityPayment').val()
        					"BillerPresence" : $('#utilityLocation').val()
        				}
        			}

        			if (lgnout == false) {
        				window.location.href = "#logout";
        			}

        			var invocationData = {
        				adapter : "API_Adapter",
        				procedure : "biller",
        				parameters : [ booksStore(JSON.stringify(getBillers)),
        						booksStore(LoggedInUserID) ],
        				compressResponse : true
        			};

        			WL.Client.invokeProcedure(invocationData, {
        				onSuccess : getBillersSuccess,
        				onFailure : AdapterFail,
        				timeout : timeout
        			});

        		} else {
        			navigator.notification
        					.confirm(
        							"Please ensure that you have network connectivity and try again!",
        							onNetworkCheck, "Connection Error", "OK");
        		}
        	};

        	// Get biller list sucess function
        	getBillersSuccess = function(result) {

        		if (previousSession != CurrentSessionId) {
        			return;
        		}

        		invocationResult = result.invocationResult;
        //alert("billersList : "+(JSON.stringify(invocationResult)));
        		self.billersList.removeAll();
        		if (invocationResult.isSuccessful) {

        			if (invocationResult.getBillersResponse) {

        				if (invocationResult.getBillersResponse.length != 0) {

        					categories = invocationResult.getBillersResponse;
        					var iindex = 1;
        					$(categories).each(function(index, obj) {

        						var BillerId = $.trim(obj.BillerId);
        						var BillerName = $.trim(obj.BillerName);
        						var BillerType = $.trim(obj.BillerType);
        						self.billersList.push({
        							BillerId : obj.BillerId,
        							BillerName : obj.BillerName,
        							BillerType : obj.BillerType,
        							index : iindex
        						})

        						iindex = iindex + 1;

        					});

        				}
        			}

        		} else {
        			navigator.notification
        					.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        			busyInd.hide();
        		}

        		busyInd.hide();

        		if (self.billersList().length == 0) {
        			navigator.notification.confirm("No Biller record found!",
        					onNetworkCheck, "Connection Error", "OK");
        		}

        	};

        	this.frmBillerHomeSubmit = function() {
        		if ($("#frmBillerHome").valid()) {

        			// Here give call to GetAuthenticator.
        			self.getAuthenticator();

        			return;
        		}
        	};

        	// This function is used to get the Authenticator list
        	this.getAuthenticator = function() {
        		if (window.navigator.onLine) {

        			// Save current session id
        			previousSession = CurrentSessionId;

        			busyInd.show();

        			// Get index
        			var iindex = $('#utilityBiller').val();
        			var index = iindex - 1;

        			Addbiller_id = self.billersList()[index].BillerId;
        			billersType = self.billersList()[index].BillerType;
        			Addbiller_name = self.billersList()[index].BillerName;

        			labeldetails1 = "";
        			labeldetails2 = "";
        			labeldetails3 = "";
        			labeldetails4 = "";
        			labeldetails5 = "";
        			valuedetails1 = "";
        			valuedetails2 = "";
        			valuedetails3 = "";
        			valuedetails4 = "";
        			valuedetails5 = "";

        			var getAuthenticators = {
        				"getAuthenticators" : {
        					"BillDeskMode" : "REGISTERPAY",
        					"BillerId" : Addbiller_id
        				}
        			}
        			//alert(JSON.stringify(getAuthenticators));
        			if (lgnout == false) {
        				window.location.href = "#logout";
        			}

        			var invocationData = {
        				adapter : "API_Adapter",
        				procedure : "biller",
        				parameters : [ booksStore(JSON.stringify(getAuthenticators)),
        						booksStore(LoggedInUserID) ],
        				compressResponse : true
        			};
        			//alert(JSON.stringify(invocationData));
        			WL.Client.invokeProcedure(invocationData, {
        				onSuccess : getAuthenticatorSucess,
        				onFailure : AdapterFail,
        				timeout : timeout
        			});

        		} else {
        			navigator.notification
        					.confirm(
        							"Please ensure that you have network connectivity and try again!",
        							onNetworkCheck, "Connection Error", "OK");
        		}
        	};

        	// Get Authenticator list sucess function
        	getAuthenticatorSucess = function(result) {

        		if (previousSession != CurrentSessionId) {
        			return;
        		}

        		invocationResult = result.invocationResult;

        		//console.log(JSON.stringify(invocationResult));
        		//alert(JSON.stringify(invocationResult));
        		authenticatorList.length = 0;

        		if (invocationResult.isSuccessful) {

        			if (invocationResult.getAuthenticatorsResponse) {

        				if (invocationResult.getAuthenticatorsResponse.AuthenticatorList.length != 0) {

        					categories = invocationResult.getAuthenticatorsResponse.AuthenticatorList;

        					$(categories)
        							.each(
        									function(index, obj) {

        										var Auth_validation = "";
        										var Auth_validation_mesg = "";
        										var Authenticator = "";
        										var ValidationType = "";
        										var keyNames = [];
        										if (obj != null) {
        											keyNames = Object.keys(obj);
        										}

        										for (i = 0; i < keyNames.length; i++) {
        											// Becasue of some message issues
        											// key's values are saved in diff
        											// named values
        											if (keyNames[i].indexOf("Auth_validation_mesg") != -1) {
        												Authenticator = $.trim(obj[keyNames[i]]);
        											}
        											else if (keyNames[i].indexOf("Auth_validation") != -1) {
        												Auth_validation_mesg = $.trim(obj[keyNames[i]]);
        												//alert(Auth_validation_mesg);
        												// Now here check for the ~
        												// character for the regular
        												// expression string. If string
        												// contains ~ then we need to
        												// show the Dropdown otherwise
        												// show textfield.
        												var msg=Auth_validation_mesg.toString();
        												//alert(msg);
        												var first=msg.charAt(0);
        												var last=msg.slice(-1);
        												if((first=='^') && (last=='$')){
        													Auth_validation_mesg = Auth_validation_mesg.replace(/\\\\/g,"\\");
        													ValidationType = "0"; // Means
        																			// show
        																			// simple
        																			// textfield
        												}
        												else{
        													if (Auth_validation_mesg.indexOf("~") !== -1) {
        														ValidationType = "1"; // Means
        																				// show
        																				// drop
        																				// down
        													} /*else {
        														Auth_validation_mesg = Auth_validation_mesg.replace(/\\\\/g,"\\");
        														ValidationType = "0"; // Means
        																				// show
        																				// simple
        																				// textfield
        													}	*/
        												}


        												//alert(Auth_validation_mesg+"--"+ValidationType);

        											} else if (keyNames[i].indexOf("Authenticator") != -1) {
        												Auth_validation = $.trim(obj[keyNames[i]]);
        											}

        										}

        										if (Authenticator != "NA") { /// if not get
        											authenticatorList
        													.push({
        														Auth_validation : Auth_validation,
        														Auth_validation_mesg : Auth_validation_mesg,
        														ValidationType : ValidationType,
        														Authenticator : Authenticator
        													});
        										}

        									});

        					// For showing error messages
        					if (authenticatorList.length > 0) {
        						switch (authenticatorList.length) {
        						case 1:
        							labeldetails1 = authenticatorList[0].Auth_validation;
        							break;
        						case 2:
        							labeldetails1 = authenticatorList[0].Auth_validation;
        							labeldetails2 = authenticatorList[1].Auth_validation;
        							break;
        						case 3:
        							labeldetails1 = authenticatorList[0].Auth_validation;
        							labeldetails2 = authenticatorList[1].Auth_validation;
        							labeldetails3 = authenticatorList[2].Auth_validation;
        							break;
        						case 4:
        							labeldetails1 = authenticatorList[0].Auth_validation;
        							labeldetails2 = authenticatorList[1].Auth_validation;
        							labeldetails3 = authenticatorList[2].Auth_validation;
        							labeldetails4 = authenticatorList[3].Auth_validation;
        							break;
        						case 5:
        							labeldetails1 = authenticatorList[0].Auth_validation;
        							labeldetails2 = authenticatorList[1].Auth_validation;
        							labeldetails3 = authenticatorList[2].Auth_validation;
        							labeldetails4 = authenticatorList[3].Auth_validation;
        							labeldetails5 = authenticatorList[4].Auth_validation;
        							break;
        						default:
        							break;
        						}

        					}

        					if (authenticatorList.length > 0) {
        						$("#contentData").load("Views/Biller/AddBillerDetails.html",null,
        								function(response, status, xhr) {

        									self.previousPage = "AddBiller_Home";
        									$(".payeeConfirmHeader").html(Addbiller_name);
        									$(".h_title").html("Add Biller - Utility Payments");
        									$("#biller_type").val(billersType);
        									for(var z=1;z<=5;z++){
        										$("#fieldsetdetails"+z).css('display', 'none');
        										$("#fieldsetdetails"+z+"_drop").css('display', 'none');
        									}
        									/*$("#fieldsetdetails1").css('display', 'none');
        									$("#fieldsetdetails2").css('display', 'none');
        									$("#fieldsetdetails3").css('display', 'none');
        									$("#fieldsetdetails4").css('display', 'none');
        									$("#fieldsetdetails5").css('display', 'none');
        									$("#fieldsetdetails1_drop").css('display', 'none');
        									$("#fieldsetdetails2_drop").css('display', 'none');
        									$("#fieldsetdetails3_drop").css('display', 'none');
        									$("#fieldsetdetails4_drop").css('display', 'none');
        									$("#fieldsetdetails5_drop").css('display', 'none');*/

        									for(var x=0;x<authenticatorList.length;x++){

        										var y=x+1;
        										//alert(authenticatorList[x].ValidationType);
        										if (authenticatorList[x].ValidationType == "1") {

        											$("#fieldsetdetails"+y+"_drop").css('display','block');
        											//$("#fieldsetdetails"+y+"_drop").addClass("bgff5354");
        											$("#inputdetails"+y+"_drop").css('color','#193080');

        											var strArray = authenticatorList[x].Auth_validation_mesg.split('~')
        											var s = "<option value=''>-Select-</option>";;
        											for (var i = 0; i < strArray.length; i++) {
        												s += "<option value="
        													+ strArray[i] + ">"
        													+ strArray[i]
        												+ "</option>";
        											}
        											$("#inputdetails"+y+"_drop").html(s);
        										}
        										else {
        											$("#fieldsetdetails"+y).css('display', 'block');
        										}


        									$(".labeldetails"+y).html(authenticatorList[x].Auth_validation);
        									}

        									ko.applyBindings(self, $(".dynamic-page-content").get(0));

        								});

        					}

        				}
        			}

        		} else {
        			navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        			busyInd.hide();
        		}

        		busyInd.hide();

        		if (self.billersList().length == 0) {
        			navigator.notification.confirm("No Biller record found!",onNetworkCheck, "Connection Error", "OK");
        		}

        	};

        	// This function is used to submit the details of biller
        	this.AddBillerDetailsSubmit = function() {

        		if ($("#frmAddBillerDetails").valid()) {

        			// First retrieve all the information in global variables
        			billersShortName = $('#biller_short_name').val();

        			if($('#inputdetails1').val().length > 0)
        				{
        				valuedetails1 = $('#inputdetails1').val();
        				}
        			else
        				{
        				valuedetails1 = $('#inputdetails1_drop').val();
        				}

        			if($('#inputdetails2').val().length > 0)
        			{
        			valuedetails2 = $('#inputdetails2').val();
        			}
        		else
        			{
        			valuedetails2 = $('#inputdetails2_drop').val();
        			}

        			if($('#inputdetails3').val().length > 0)
        			{
        			valuedetails3 = $('#inputdetails3').val();
        			}
        		else
        			{
        			valuedetails3 = $('#inputdetails3_drop').val();
        			}

        			if($('#inputdetails4').val().length > 0)
        			{
        			valuedetails4 = $('#inputdetails4').val();
        			}
        		else
        			{
        			valuedetails4 = $('#inputdetails4_drop').val();
        			}

        			if($('#inputdetails5').val().length > 0)
        			{
        			valuedetails5 = $('#inputdetails5').val();
        			}
        		else
        			{
        			valuedetails5 = $('#inputdetails5_drop').val();
        			}

        			$("#contentData").load("Views/Biller/AddBillerConfirm.html",null,function(response, status, xhr) {

        						self.previousPage = "AddBiller_Add";
        						$(".payeeConfirmHeader").html(Addbiller_name);
        						$(".h_title").html("Add Biller - Utility Payments");
        						$(".BillerType").html(billersType);
        						$(".BillerShortName").html(billersShortName);

        						if (labeldetails1.length > 0) {
        							$("#fieldsetconfirm1").css('display', 'block');
        							$('.labelconfirm1').html(labeldetails1);
        							$('.value1').html(valuedetails1);

        						}

        						if (labeldetails2.length > 0) {
        							$("#fieldsetconfirm2").css('display', 'block');
        							$('.labelconfirm2').html(labeldetails2);
        							$('.value2').html(valuedetails2);

        						}

        						if (labeldetails3.length > 0) {
        							$("#fieldsetconfirm3").css('display', 'block');
        							$('.labelconfirm3').html(labeldetails3);
        							$('.value3').html(valuedetails3);

        						}

        						if (labeldetails4.length > 0) {
        							$("#fieldsetconfirm4").css('display', 'block');
        							$('.labelconfirm4').html(labeldetails4);
        							$('.value4').html(valuedetails4);

        						}

        						if (labeldetails5.length > 0) {
        							$("#fieldsetconfirm5").css('display', 'block');
        							$('.labelconfirm5').html(labeldetails5);
        							$('.value5').html(valuedetails5);

        						}
        						ko.applyBindings(self, $(".dynamic-page-content")
        								.get(0));

        					});

        		}
        	};

        	// This function is used to submit the details of biller after confirmation
        	this.AddBillerConfirmSubmit = function() {

        		if ($("#frmAddBillerConfirm").valid()) {

        			if (window.navigator.onLine) {

        				// Save current session id
        				previousSession = CurrentSessionId;

        				busyInd.show();

        				var addBiller = {};

        				switch (authenticatorList.length) {
        				case 1:

        					addBiller = {
        						"addBiller" : {
        							"UniqueRefNo" : randomStringNum(8),
        							"Channel" : "MOB",
        							"CustomerId" : LoggedInUserID,
        							"BillerId" : Addbiller_id,
        							"ShortName" : billersShortName,
        							"Authenticators" : [ valuedetails1 ]
        						}
        					};
        					break;
        				case 2:

        					addBiller = {
        						"addBiller" : {
        							"UniqueRefNo" : randomStringNum(8),
        							"Channel" : "MOB",
        							"CustomerId" : LoggedInUserID,
        							"BillerId" : Addbiller_id,
        							"ShortName" : billersShortName,
        							"Authenticators" : [ valuedetails1, valuedetails2 ]
        						}
        					};
        					break;
        				case 3:
        					addBiller = {
        						"addBiller" : {
        							"UniqueRefNo" : randomStringNum(8),
        							"Channel" : "MOB",
        							"CustomerId" : LoggedInUserID,
        							"BillerId" : Addbiller_id,
        							"ShortName" : billersShortName,
        							"Authenticators" : [ valuedetails1, valuedetails2,
        									valuedetails3 ]
        						}
        					};
        					break;
        				case 4:
        					addBiller = {
        						"addBiller" : {
        							"UniqueRefNo" : randomStringNum(8),
        							"Channel" : "MOB",
        							"CustomerId" : LoggedInUserID,
        							"BillerId" : Addbiller_id,
        							"ShortName" : billersShortName,
        							"Authenticators" : [ valuedetails1, valuedetails2,
        									valuedetails3, valuedetails4 ]
        						}
        					};
        					break;
        				case 5:
        					addBiller = {
        						"addBiller" : {
        							"UniqueRefNo" : randomStringNum(8),
        							"Channel" : "MOB",
        							"CustomerId" : LoggedInUserID,
        							"BillerId" : Addbiller_id,
        							"ShortName" : billersShortName,
        							"Authenticators" : [ valuedetails1, valuedetails2,
        									valuedetails3, valuedetails4, valuedetails5 ]
        						}
        					};
        					break;
        				default:
        					break;

        				}

        				//alert(JSON.stringify(addBiller));

        				if (lgnout == false) {
        					window.location.href = "#logout";
        				}

        				var invocationData = {
        					adapter : "API_Adapter",
        					procedure : "biller",
        					parameters : [ booksStore(JSON.stringify(addBiller)),
        							booksStore(LoggedInUserID) ],
        					compressResponse : true
        				};

        				WL.Client.invokeProcedure(invocationData, {
        					onSuccess : addBillerSucess,
        					onFailure : AdapterFail,
        					timeout : timeout
        				});

        			} else {
        				navigator.notification
        						.confirm(
        								"Please ensure that you have network connectivity and try again!",
        								onNetworkCheck, "Connection Error", "OK");
        			}

        		}
        	};

        	// Get Add biller list sucess function
        	addBillerSucess = function(result) {

        		if (previousSession != CurrentSessionId) {
        			return;
        		}

        		invocationResult = result.invocationResult;

        		// alert(JSON.stringify(invocationResult));

        		if (invocationResult.isSuccessful) {

        			if (invocationResult.addBillerResponse) {
        				if (invocationResult.addBillerResponse.addBillerReply) {
        					if (invocationResult.addBillerResponse.addBillerReply.Valid) {
        						if (invocationResult.addBillerResponse.addBillerReply.Valid == "Y") {
        							busyInd.hide();

        							WL.SimpleDialog
        									.show(
        											"Alert",
        											Addbiller_name+" "+invocationResult.addBillerResponse.addBillerReply.ErrorMessage,
        											[ {
        												text : "OK",
        												handler : function() {
        													window.location.hash = '#billpayment';
        													WL.Logger
        															.debug("OK button pressed");
        												}
        											} ]);

        							return;
        						}
        					}
        				}
        			}
        		}

        		busyInd.hide();
        		if (invocationResult.Error) {
        			if (invocationResult.Error.BusinessException) {
        				if (invocationResult.Error.BusinessException.ErrorDetail) {
        					if (invocationResult.Error.BusinessException.ErrorDetail.ErrorDesc) {

        						WL.SimpleDialog
        								.show(
        										"Alert",
        										invocationResult.Error.BusinessException.ErrorDetail.ErrorDesc,
        										[ {
        											text : "OK",
        											handler : function() {
        												window.location.hash = '#billpayment';
        												WL.Logger
        														.debug("OK button pressed");
        											}
        										} ]);

        						return;

        						// navigator.notification.alert(invocationResult.Error.BusinessException.ErrorDetail.ErrorDesc);
        						// return;

        					}

        				}

        			}
        		}

        		navigator.notification
        				.alert("We apologize this facility is temporarily unavailable.Please try later. ");

        	};

        	self.goToPreviousPage = function() {
        		// alert("previousPage"+billPayCopy.previousPage);

        		if (billPayCopy.previousPage == "Home") {
        			window.location.hash = '#billpayment';
        		} else if (billPayCopy.previousPage == "AddBiller_Home") {

        			// alert("INININ");
        			// Go to First page of Addbiller
        			// Check for the length of categories. If it is greater than 0 then
        			// show error otherwise show add biller UI.
        			$("#contentData").load("Views/Biller/BillerHome.html",
        					null,
        					function(response, status, xhr) {
        						if (status != "error") {
        						}
        						$(".h_title").html("Add Biller - Utility Payments");
        						billPayCopy.previousPage = "Home";
        						if (self.categoryList().length == 0) {

        							$("#NoCategory").show();
        							$("#YesCategory").hide();
        						} else {
        							$("#NoCategory").hide();
        							$("#YesCategory").show();
        						}

        						ko.applyBindings(self, $(".dynamic-page-content")
        								.get(0));
        					});

        			// });

        		} else if (billPayCopy.previousPage == "AddBiller_Add") {
        			// Go to Add first page
        			//alert(authenticatorList.length);
        			if (authenticatorList.length > 0) {
        				$("#contentData").load("Views/Biller/AddBillerDetails.html",null,function(response, status, xhr) {

        							self.previousPage = "AddBiller_Home";
        							//alert(JSON.stringify(authenticatorList));
        							$(".payeeConfirmHeader").html(Addbiller_name);
        							$(".h_title").html("Add Biller - Utility Payments");
        							$("#biller_type").val(billersType);
        							$("#biller_short_name").val(billersShortName);
        							/*$("#fieldsetdetails1").css('display', 'none');
        							$("#fieldsetdetails2").css('display', 'none');
        							$("#fieldsetdetails3").css('display', 'none');
        							$("#fieldsetdetails4").css('display', 'none');
        							$("#fieldsetdetails5").css('display', 'none');*/
        							for(var z=1;z<=5;z++){
        								$("#fieldsetdetails"+z).css('display', 'none');
        								$("#fieldsetdetails"+z+"_drop").css('display', 'none');
        							}
        							for(var x=0;x<authenticatorList.length;x++){

        								var y=x+1;
        								var sel_val='';
        								if(y==1){sel_val=valuedetails1;}
        								else if(y==2){sel_val=valuedetails2;}
        								else if(y==3){sel_val=valuedetails3;}
        								else if(y==4){sel_val=valuedetails4;}
        								else if(y==5){sel_val=valuedetails5;}
        								//alert(sel_val);

        								//alert(authenticatorList[x].ValidationType);
        								if (authenticatorList[x].ValidationType == "1") {

        									$("#fieldsetdetails"+y+"_drop").css('display','block');
        									$("#inputdetails"+y+"_drop").css('color','#193080');

        									var strArray = authenticatorList[x].Auth_validation_mesg.split('~')
        									var s = "<option value=''>-Select-</option>";
        									for (var i = 0; i < strArray.length; i++) {
        										var selected='';
        										if(strArray[i]==sel_val){
        											selected="selected=true";
        										}
        										s += "<option value="
        											+ strArray[i] + " " +selected+ ">"
        											+ strArray[i]
        										+ "</option>";
        									}
        									$("#inputdetails"+y+"_drop").html(s);
        								}
        								else {
        									$("#inputdetails"+y).val(sel_val);
        									$("#fieldsetdetails"+y).css('display', 'block');
        								}


        							$(".labeldetails"+y).html(authenticatorList[x].Auth_validation);
        							}
        							/*switch (authenticatorList.length) {
        							case 1:

        								$("#fieldsetdetails1").css('display', 'block');
        								$(".labeldetails1").html(authenticatorList[0].Auth_validation);
        								$("#inputdetails1").val(valuedetails1);
        								break;
        							case 2:

        								$("#fieldsetdetails1").css('display', 'block');
        								$("#fieldsetdetails2").css('display', 'block');
        								$(".labeldetails1").html(authenticatorList[0].Auth_validation);
        								$(".labeldetails2").html(authenticatorList[1].Auth_validation);
        								$("#inputdetails1").val(valuedetails1);
        								$("#inputdetails2").val(valuedetails2);
        								break;
        							case 3:

        								$("#fieldsetdetails1").css('display', 'block');
        								$("#fieldsetdetails2").css('display', 'block');
        								$("#fieldsetdetails3").css('display', 'block');
        								$(".labeldetails1").html(authenticatorList[0].Auth_validation);
        								$(".labeldetails2").html(authenticatorList[1].Auth_validation);
        								$(".labeldetails3").html(authenticatorList[2].Auth_validation);
        								$("#inputdetails1").val(valuedetails1);
        								$("#inputdetails2").val(valuedetails2);
        								$("#inputdetails3").val(valuedetails3);
        								break;
        							case 4:

        								$("#fieldsetdetails1").css('display', 'block');
        								$("#fieldsetdetails2").css('display', 'block');
        								$("#fieldsetdetails3").css('display', 'block');
        								$("#fieldsetdetails4").css('display', 'block');
        								$(".labeldetails1").html(authenticatorList[0].Auth_validation);
        								$(".labeldetails2").html(authenticatorList[1].Auth_validation);
        								$(".labeldetails3").html(authenticatorList[2].Auth_validation);
        								$(".labeldetails4").html(authenticatorList[3].Auth_validation);
        								$("#inputdetails1").val(valuedetails1);
        								$("#inputdetails2").val(valuedetails2);
        								$("#inputdetails3").val(valuedetails3);
        								$("#inputdetails4").val(valuedetails4);
        								break;
        							case 5:

        								$("#fieldsetdetails1").css('display', 'block');
        								$("#fieldsetdetails2").css('display', 'block');
        								$("#fieldsetdetails3").css('display', 'block');
        								$("#fieldsetdetails4").css('display', 'block');
        								$("#fieldsetdetails5").css('display', 'block');
        								$(".labeldetails1").html(authenticatorList[0].Auth_validation);
        								$(".labeldetails2").html(authenticatorList[1].Auth_validation);
        								$(".labeldetails3").html(authenticatorList[2].Auth_validation);
        								$(".labeldetails4").html(authenticatorList[4].Auth_validation);
        								$(".labeldetails5").html(authenticatorList[5].Auth_validation);
        								$("#inputdetails1").val(valuedetails1);
        								$("#inputdetails2").val(valuedetails2);
        								$("#inputdetails3").val(valuedetails3);
        								$("#inputdetails4").val(valuedetails4);
        								$("#inputdetails5").val(valuedetails5);
        								break;
        							default:
        								break;

        							}*/

        							ko.applyBindings(self, $(".dynamic-page-content")
        									.get(0));

        						});
        			}
        		} else if (billPayCopy.previousPage == "BillerList") {
        			//alert(1);
        			//billPayCopy.previousPage = "BillerCategory";  ///--- commented as category page removed
        			billPayCopy.previousPage = "billpayment"; ///--- added as category page removed
        			billPayCopy.showBillerList();
        		} else if (billPayCopy.previousPage == "billpayment") { //BillerCategory

        			billPayCopy.previousPage = "Home";
        			window.location.hash = '#billpayment';
        			// Check for the length of categories. If it is greater than 0 then
        			// show error otherwise show add biller UI.

        			///---- This is commented on 21st dec 16 -As category page is removed
        			/*$("#contentData")
        					.load(
        							"Views/Biller/BillerListCategory.html",
        							null,
        							function(response, status, xhr) {
        								if (status != "error") {
        								}
        								$(".h_title").html("View/Delete Biller");
        								if (billPayCopy.categoryList().length == 0) {

        									$("#NoListCategory").show();
        									$("#YesListCategory").hide();
        								} else {

        									$("#NoListCategory").hide();
        									$("#YesListCategory").show();

        									$("#BillerListCategory").html('');
        									var list = $("#BillerListCategory");

        									for (i = 0; i < billPayCopy.categoryList().length; i++) {

        										var strname = billPayCopy.categoryList()[i].Category;

        										if (i % 2 == 1) {
        											list
        													.append(
        													// href='#Payee_Beneficiery_Details'
        													"<a id='BeneIndexCategory'  onclick='return Biller_ListCategory_Details("
        															+ i
        															+ ")'>"
        															+ "<div class='info_row even'>"
        															+ "<span class='info_row_menu_right page_title_carousel'>"
        															+ strname
        															+ "</span>"
        															+ "<span class='right_arrow'><img src='img/rarrow.png' /></span>"
        															+ "</div>"
        															+ "</a>"
        															+

        															"<div class='clearfix'></div>"
        															+ "<div class='linedivider'></div>"
        															+ "<div class='clearfix'></div>");
        										} else {
        											list.append(
        													// href='#Payee_Beneficiery_Details'
        													"<a id='BeneIndexCategory'  onclick='return Biller_ListCategory_Details("
        															+ i
        															+ ")'>"
        															+ "<div class='info_row odd'>"
        															+ "<span class='info_row_menu_right page_title_carousel'>"
        															+ strname
        															+ "</span>"
        															+ "<span class='right_arrow'><img src='img/rarrow.png' /></span>"
        															+ "</div>"
        															+ "</a>"
        															+

        															"<div class='clearfix'></div>"
        															+ "<div class='linedivider'></div>"
        															+ "<div class='clearfix'></div>");
        										}

        									}
        								}

        								ko.applyBindings(billPayCopy, $(
        										".dynamic-page-content").get(0));
        							});
        							//comment end
        							*/
        		}

        	};

        	// To get biller list
        	this.ViewTheBillerList = function() {
        //alert("ViewTheBillerList");
        		if (window.navigator.onLine) {

        			previousSession = CurrentSessionId;

        			var reqParams = {
        				"viewBillerAccount" : {
        					"UniqueRefNo" : randomStringNum(8),
        					"Channel" : "MOB",
        					"CustomerId" : LoggedInUserID,
        					"BillerAccountId" : ""
        				}
        			}

        			// console.log(JSON.stringify(reqParams));

        			busyInd.show();

        			if (lgnout == false) {
        				window.location.href = "#logout";
        			}
        			var invocationData = {
        				adapter : "API_Adapter",
        				procedure : "biller",
        				parameters : [ booksStore(JSON.stringify(reqParams)),
        						booksStore(LoggedInUserID) ],
        				compressResponse : true
        			};

        			WL.Client.invokeProcedure(invocationData, {
        				onSuccess : BillerListSuccess,
        				onFailure : AdapterFail,
        				timeout : timeout
        			});

        		} else {
        			navigator.notification
        					.confirm("Please ensure that you have network connectivity and try again!",onNetworkCheck, "Connection Error", "OK");
        		}

        	};

        	BillerListSuccess = function(result) {
        		if (previousSession != CurrentSessionId) {
        			return;
        		}
        		invocationResult = result.invocationResult;
        		//alert("ABC "+JSON.stringify(invocationResult));
        		//console.log(JSON.stringify(invocationResult));
        //invocationResult = '{"viewBillerResponse": {"viewBillerResponseReply": {  "UniqueRefNo": "123456",   "Channel": "MOB",  "CustomerId": "RATNAB1024893", "billers":    [      { "BillerId": "AECGAH",   "BillerAccountId": "RTBBL749", "BillerShortName": "Torren",     "Status": "ACTIVE",    "Authenticator":   {   "Service Number": "963636278",  "NA":   [ "NA",  "NA"   ]    },    "BillerType": "Presentment Only",   "BillerName": "Torrent Power - AECG", "Category": "Electricity",    "SubCategory": "NA" }, {"BillerId": "GSPCGAS","BillerAccountId": "RTBBL751","BillerShortName": "Gujgas", "Status": "ACTIVE",     "Authenticator":          {    "Customer ID": "2627737378", "NA":    [   "NA", "NA"] },"BillerType": "Presentment Only", "BillerName": "Gujrat Gas Limited",  "Category": "Gas", "SubCategory": "NA"},]}}}';
        		//alert("ABC "+JSON.stringify(invocationResult));
        		//console.log(JSON.stringify(invocationResult));
        //invocationResult.isSuccessful="true";
        		BillerViewList.length = 0;
        		var list1 = [];

        		if (invocationResult.isSuccessful) {
        			if (invocationResult.viewBillerResponse) {

        				if (invocationResult.viewBillerResponse.viewBillerResponseReply) {

        					toAccdata = invocationResult.viewBillerResponse.viewBillerResponseReply.billers;

        					$(toAccdata).each(function(index, obj) {

        						var BillerAccountId = $.trim(obj.BillerAccountId);
        						var BillerShortName = $.trim(obj.BillerShortName);
        						var BillerName = $.trim(obj.BillerName);
        						var BillerType = obj.BillerType;
        						var BillerId = obj.BillerId;
        						var Status = obj.Status;
        						var Authenticator = obj.Authenticator;
        						var Category = obj.Category;
        						var SubCategory = obj.SubCategory;
        //alert(SubCategory);
        						// if(Category == 'Utility')
        						// {
        						list1.push({
        							BillerAccountId : obj.BillerAccountId,
        							BillerShortName : obj.BillerShortName,
        							BillerName : obj.BillerName,
        							BillerType : obj.BillerType,
        							BillerId : obj.BillerId,
        							Status : obj.Status,
        							Authenticator : obj.Authenticator,
        							Category : obj.Category,
        							SubCategory : obj.SubCategory

        						});
        						// }

        					});

        					BillerViewList = list1.slice(0);

        					// Here we call function to filter the categoy list.
        					self.doFiltrationOfViewBillerList();

        				}
        			}

        			else {
        				busyInd.hide();
        				// navigator.notification.alert(invocationResult.Error.BusinessException.ErrorDetail.ErrorDesc);

        				 $("#contentData").load("Views/Biller/BillerList.html",null,function(response, status, xhr) {
        					 if (status != "error") {}
        					 $(".h_title").html("View/Delete Biller - Utility Payments");
        					 	$("#error_norecord").css("display","block");
        					 $("#BillerList").css("display","none");
        					 	ko.applyBindings(self, $(".dynamic-page-content").get(0));
        					 });

        //				$("#contentData").load("Views/Biller/BillerList.html",null,function(response, status, xhr) {
        //									if (status != "error") {}
        //									alert("2");
        //									$(".h_title").html("View/Delete Biller");
        ////									$("#BillerList").hide();
        ////									$("#error_norecord").show();
        //									alert("3");
        //
        //									$("#error_norecord").css("display","block");
        //									alert("4");
        //								 	$("#BillerList").css("display","none");
        //								 	alert("5");
        //
        //									//$('#error').html(invocationResult.Error.BusinessException.ErrorDetail.ErrorDesc);
        //
        //									ko.applyBindings(self, $(".dynamic-page-content").get(0));
        //								});

        				// window.location = '#billpayment'

        			}

        		} else {
        			navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        			busyInd.hide();
        			window.location.hash = '#billpayment';
        		}
        		busyInd.hide();

        	};

        	self.doFiltrationOfViewBillerList = function() {
        		// alert("Tapped");
        		//alert(2);
        		// Here we start the filtration of list according to sub categories
        		self.categoryList.removeAll();

        		var l = BillerViewList.length;
        		billPayCopy.previousPage="BillerList"; //added --to directly call biller short name list
        		billPayCopy.showBillerList(); //added --to directly call biller short name list
        		// alert(l);
        		///Start - comment to skip category page
        		/*
        		 *
        		var i = 0;
        		var j = 0;
        		for (i = 0; i < l; i++) {

        			if (self.categoryList().length == 0) {
        				self.categoryList.push(BillerViewList[i]);
        				continue;
        			}

        			var res = false;

        			for (j = 0; j < self.categoryList().length; j++) {
        				if (self.categoryList()[j].Category == BillerViewList[i].Category) {
        					res = true;
        					break;
        				}
        			}

        			if (res == false) {
        				self.categoryList.push(BillerViewList[i]);
        			}

        			//showBillerList
        		}


        		// Check for the length of categories. If it is greater than 0 then show
        		// error otherwise show add biller UI.
        		$("#contentData").load("Views/Biller/BillerListCategory.html",null,function(response, status, xhr) {
        							if (status != "error") {
        							}
        							$(".h_title").html("View/Delete Biller");
        							if (self.categoryList().length == 0) {

        								$("#NoListCategory").show();
        								$("#YesListCategory").hide();
        							} else {

        								$("#NoListCategory").hide();
        								$("#YesListCategory").show();

        								$("#BillerListCategory").html('');
        								var list = $("#BillerListCategory");

        								for (i = 0; i < self.categoryList().length; i++) {
        alert("strname "+strname);
        									var strname = self.categoryList()[i].Category;

        									if (i % 2 == 1) {
        										list
        												.append(
        												// href='#Payee_Beneficiery_Details'
        												"<a id='BeneIndexCategory'  onclick='return Biller_ListCategory_Details("
        														+ i
        														+ ")'>"
        														+ "<div class='info_row even'>"
        														+ "<span class='info_row_menu_right page_title_carousel'>"
        														+ strname
        														+ "</span>"
        														+ "<span class='right_arrow'><img src='img/rarrow.png' /></span>"
        														+ "</div>"
        														+ "</a>"
        														+

        														"<div class='clearfix'></div>"
        														+ "<div class='linedivider'></div>"
        														+ "<div class='clearfix'></div>");
        									} else {
        										list
        												.append(
        												// href='#Payee_Beneficiery_Details'
        												"<a id='BeneIndexCategory'  onclick='return Biller_ListCategory_Details("
        														+ i
        														+ ")'>"
        														+ "<div class='info_row odd'>"
        														+ "<span class='info_row_menu_right page_title_carousel'>"
        														+ strname
        														+ "</span>"
        														+ "<span class='right_arrow'><img src='img/rarrow.png' /></span>"
        														+ "</div>"
        														+ "</a>"
        														+

        														"<div class='clearfix'></div>"
        														+ "<div class='linedivider'></div>"
        														+ "<div class='clearfix'></div>");
        									}

        								}
        							}

        							ko.applyBindings(self, $(".dynamic-page-content")
        									.get(0));
        						});*/
        		///End -comment to skip category page
        		busyInd.hide();
        	};

        	self.showBillerList = function() {

        		BillerViewListFiltered.length = 0;

        		// Now here we will filter the record according to the categories
        		 //alert("showBillerList :: "+self.categoryList()[selectedCategoryBillerListIndex].SubCategory);
        		//BillerViewList.length=0;
        		//alert(BillerViewList.length);

        		///---- > condition commented for no biller
        		if(BillerViewList.length>0){
        			//$("#error_norecord").css("display","none");
        		 	//$("#BillerList").css("display","block");

        			//var strNCat = self.categoryList()[selectedCategoryBillerListIndex].Category; ///--- statement commented : cause category removd
        			for (i = 0; i < BillerViewList.length; i++) {

        				//if (BillerViewList[i].Category == strNCat) {  ///--- condition commented : cause category removd
        					BillerViewListFiltered.push(BillerViewList[i]);
        				//}
        			}

        			// Now we need to show the list of the billers
        			$(".h_title").html("View/Delete Biller - Utility Payments");

        			$("#contentData").load("Views/Biller/BillerList.html",null,	function(response, status, xhr) {

        							//	$(".viewBillerHeader").html("test");//.html(strNCat);

        								$("#BillerList").html('');
        								var list = $("#BillerList");
        								//alert("Views/Biller/BillerList.html"+BillerViewListFiltered.length);
        								//BillerViewListFiltered=BillerViewListFiltered.sort();
        								///---sort according to shortname
        								BillerViewListFiltered.sort(function(a,b)
        								{
        										var x = a.BillerShortName.toLowerCase();
        										var y = b.BillerShortName.toLowerCase();
        										return x < y ? -1 : x > y ? 1 : 0;
        								});

        								for (i = 0; i < BillerViewListFiltered.length; i++) {

        									var strname = "<div style='font-size:15px;font-weight:bold;clear:both;'>"+BillerViewListFiltered[i].BillerShortName+"</div><div style='clear:both;'>"+BillerViewListFiltered[i].BillerName+"</div>";

        									if (i % 2 == 1) {
        										list.append(
        												// href='#Payee_Beneficiery_Details'
        												"<a id='BeneIndex'  onclick='return Biller_List_Details("
        														+ i
        														+ ")'>"
        														+ "<div class='info_row even' >"
        														+ "<span class='info_row_menu_right page_title_carousel' >"
        														+ strname
        														+ "</span>"
        														+ "<span class='right_arrow'><img src='img/rarrow.png' /></span>"
        														+ "</div>"
        														+ "</a>"
        														+

        														"<div class='clearfix'></div>"
        														+ "<div class='linedivider'></div>"
        														+ "<div class='clearfix'></div>");
        									} else {
        										list
        												.append(
        												// href='#Payee_Beneficiery_Details'
        												"<a id='BeneIndex'  onclick='return Biller_List_Details("
        														+ i
        														+ ")'>"
        														+ "<div class='info_row odd'>"
        														+ "<span class='info_row_menu_right page_title_carousel'>"
        														+ strname
        														+ "</span>"
        														+ "<span class='right_arrow'><img src='img/rarrow.png' /></span>"
        														+ "</div>"
        														+ "</a>"
        														+

        														"<div class='clearfix'></div>"
        														+ "<div class='linedivider'></div>"
        														+ "<div class='clearfix'></div>");
        									}

        								}
        								ko.applyBindings(self, $(".dynamic-page-content")
        										.get(0));
        							});
        		}//if(BillerViewList.length>0){
        		else{
        			//navigator.notification.confirm("No Biller Record Found!",onRecord, "No records", "OK");

        			 $("#contentData").load("Views/Biller/BillerList.html",null,	function(response, status, xhr) {
        			 	$("#error_norecord").css("display","block");
        			 	$("#BillerList").css("display","none");
        			 	ko.applyBindings(self, $(".dynamic-page-content").get(0));
        			 });


        		}
        	}

        	// Delete biller binding on confirm page
        	self.Biller_List_Details = function() {
        		// alert("inside binding");
        		$("#contentData").load("Views/Biller/DeleteBillerDetails.html",null,function(response, status, xhr) {

        							$(".h_title").html("View/Delete Biller - Utility Payments");

        							var obj = BillerViewListFiltered[selectedBillerListIndex];
        							$('.BillerName').html(obj.BillerName);
        							// $('.BillerType').html(obj.BillerType);
        							$('.BillerShortName').html(obj.BillerShortName);
        							$('.BillerStatus').html(obj.Status);

        							$(".deleteBillerHeader").html(obj.Category);
        							var list = $("#AuthenticatorLists");

        							if (obj.Authenticator != null) {
        								var keyNames = Object.keys(obj.Authenticator);

        								for (i = 0; i < keyNames.length; i++) {
        									if (keyNames[i] == "NA") {
        										continue;
        									}
        									list
        											.append("<div class='info_row custfont row_aadhar'> <span class='info_row_left'>"
        													+ keyNames[i]
        													+ "</span> <span class='info_row_right f18'>"
        													+ obj.Authenticator[keyNames[i]]
        													+ "</span>"
        													+ "<div class='clearfix'></div>"
        													+ "</div>");
        								}

        							}

        							ko.applyBindings(self, $(".dynamic-page-content")
        									.get(0));

        						});
        	};

        	// Delete biller submit
        	self.DeleteBillerDetailsSubmit = function() {

        		if ($("#frmDeleteBillerDetails").valid()) {
        			var obj = BillerViewListFiltered[selectedBillerListIndex];

        			previousSession = CurrentSessionId;

        			var reqParams = {
        				"deleteBiller" : {
        					"UniqueRefNo" : randomStringNum(8),
        					"Channel" : "MOB",
        					"CustomerId" : LoggedInUserID,
        					"BillerName" : obj.BillerName,
        					"BillerAccountId" : obj.BillerAccountId
        				}
        			}

        			if (lgnout == false) {
        				window.location.href = "#logout";
        			}

        			busyInd.show();
        			var invocationData = {
        				adapter : "API_Adapter",
        				procedure : "biller",
        				parameters : [ booksStore(JSON.stringify(reqParams)),booksStore(LoggedInUserID) ],
        				compressResponse : true
        			};

        			WL.Client.invokeProcedure(invocationData, {
        				onSuccess : DeleteBillerDetailsSubmitSuccess,
        				onFailure : AdapterFail,
        				timeout : timeout
        			});

        		}
        	};

        	DeleteBillerDetailsSubmitSuccess = function(result) {

        		if (previousSession != CurrentSessionId) {
        			return;
        		}

        		invocationResult = result.invocationResult;

        		var obj = BillerViewListFiltered[selectedBillerListIndex];

        		if (invocationResult.isSuccessful) {
        			if (invocationResult.deleteBillerResponse) {
        				if (invocationResult.deleteBillerResponse.deleteBillerReply) {

        					if (invocationResult.deleteBillerResponse.deleteBillerReply.Valid == "Y") {
        						WL.SimpleDialog.show(
        										"Alert",
        										obj.BillerName+" "+invocationResult.deleteBillerResponse.deleteBillerReply.ErrorMessage,
        										[ {
        											text : "OK",
        											handler : function() {
        												window.location.hash = '#billpayment';
        												WL.Logger.debug("OK button pressed");
        											}
        										} ]);

        					}
        				}
        			} else {
        				busyInd.hide();
        				navigator.notification.alert(invocationResult.Error.BusinessException.ErrorDetail.ErrorDesc);
        				window.location.hash = '#billpayment';
        			}
        			busyInd.hide();
        		}

        		else {
        			busyInd.hide();
        			navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
        		}

        	};

        	//BBPS

		//BBPS
		self.getAccountsListForBBPS = function(){
                			if(window.navigator.onLine){

                				previousSession = CurrentSessionId;

                				fldjsessionid = '';
                					reqParams = {};
                					reqParams["RQLoginUserId"] = LoggedInUserID;
                					reqParams["RQDeviceFamily"] = Device_Platform;
                					reqParams["RQDeviceFormat"] = Device_Model;
                					reqParams["RQOperationId"] = "NFTFNDTFR";
                					reqParams["RQClientAPIVer"] = RQClientAPIVer;
                					reqParams["SessionId"] = CurrentSessionId;
                					reqParams["RQTransSeq"] = "01";

                					busyInd.show();
                					if(lgnout == false){ window.location.href = "#logout";}var invocationData = {
                						adapter : "API_Adapter",
                						procedure : "GetNeft",
                						parameters : [fldjsessionid,reqParams],
                						compressResponse : true
                					};
                					WL.Logger.debug(invocationData, '');

                					WL.Client.invokeProcedure(invocationData, {
                							onSuccess : getAccountsListForBBPSSuccess,
                							onFailure : AdapterFail,
                							timeout: timeout
                					});
                					}else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
                			}
                			};


        getAccountsListForBBPSSuccess = function(result){

//        alert(JSON.stringify(result));

                                    if(CurrentSessionId != previousSession)
                                    {
                                        busyInd.hide();
                                        return;
                                    }

                                    invocationResult = result.invocationResult;

                                    accountListFt.removeAll();
                                    rdAccountList.removeAll();
                                if(invocationResult.isSuccessful){
                                if(invocationResult.RBL){
                                    if(invocationResult.RBL.Response){
                                        if(invocationResult.RBL.STATUS.CODE == '0'){
                                            var responsesessionid = invocationResult.RBL.SessionEnv.SessionId;
                                            var currentuser = currentreguserid;
                                            var responseuser = invocationResult.RBL.RequestParams.RQLoginUserId;
                                            if(currentuser == responseuser && CurrentSessionId == responsesessionid){

                                                if(invocationResult.RBL.Response.CustDetails!=undefined){
                                                    if(invocationResult.RBL.Response.CustDetails.length!=0){
                                                        acctdtls = invocationResult.RBL.Response.validAcct.acctdetails;
                                                        custdtls = invocationResult.RBL.Response.CustDetails;
                                                        var idx = 1;
                                                        $(acctdtls).each(function(index, obj) {
                                                            strid = "item"+idx;

                                                            $(custdtls).each(function(j, obj1) {
                                                                custnames="";
                                                                if(obj.acctindex == obj1.acctindex){
                                                                    var actbal = "₹ "+ parseFloat(obj.acctbalance);
                                                                    displaytxt = $.trim(obj.acctno)+"-"+obj.branchname;
                                                                    //accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
                                                                    accountListFt.push({ codacctno: obj.acctno+"#"+obj1.custName+"#"+actbal, acctType: obj.acctType, acctbalance: actbal, acctbranch: obj.branchname, custnames: custnames, namccyshrt: obj.currency, displaytxt: displaytxt, strid:strid });
                                                                }
                                                            });
                                                            idx++;
                                                        });

                                                    }
                                                }

                                                $("#contentData").load("Views/BillPay/BBPSSelectAccount.html", null, function (response, status, xhr) {

                                                    if (status != "error") {}

                                                    $(".h_title").html("BBPS");

                                                    $("#nftfundtrns").show();

                                                        ko.applyBindings(self, $(".dynamic-page-content").get(0));

                                                });

                                                busyInd.hide();
                                            }
                                            else{
                                                    busyInd.hide();
                                                    navigator.notification.alert("Your session has timed out!");
                                                    window.location.hash = "#logout";
                                            }
                                        }
                                        else{
                                            busyInd.hide();
                                            invalidresponse();
                                        }
                                    }else{
                                                    busyInd.hide();
                                                    handleError(invocationResult.RBL.Response, invocationResult.RBL);
                                            }
                                }else{
                                     navigator.notification.alert("We apologize this facility is temporarily unavailable.Please try later. ");
                                    busyInd.hide();
                                }

                                }
                                busyInd.hide();
                            };




        self.BBPSSubmit = function(){
                        			if(window.navigator.onLine){
                        			if($("#frmBBPSSelectAccount").valid()){

                        				previousSession = CurrentSessionId;

//                                       var AccNo = booksStore("123456789");
//                                        var	IMEI = booksStore("5465465465");
//                                        var	CIFID = booksStore("6546555554");
//                                        var	MobileNo = booksStore("7799082941");
//                                        var	customerID = booksStore(LoggedInUserID);

                                       var accno_selected_bbps = self.selAccount();
                                       accno_selected_bbps = (accno_selected_bbps.split("#")[0]);

//                                       alert(accno_selected_bbps);
//                                       alert(deviceId);
//                                       alert(LoggedInUserID);

                                       var MobileNoBBPS="1111111111";

                                       	busyInd.show();


                        					if(lgnout == false){ window.location.href = "#logout";}


                        					var invocationData = {
                        						adapter : "API_Adapter",
                        						procedure : "BBPS",
                        						parameters : [booksStore(accno_selected_bbps),booksStore(deviceId),booksStore(LoggedInUserID),booksStore(MobileNoBBPS),booksStore(LoggedInUserID)],
                        						compressResponse : true
                        					};



                        					WL.Client.invokeProcedure(invocationData, {
                        							onSuccess : BBPSSubmitSuccess,
                        							onFailure : AdapterFail,
                        							timeout: timeout
                        					});


                        					}
                        					}
                        		else{ navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
                        			}
                        			};


        BBPSSubmitSuccess = function(result){

           //If session id is expired then dont show screen
           if(CurrentSessionId != previousSession)
           {
           	busyInd.hide();
           	return;
           }

           invocationResult = result.invocationResult;
           //console.log(JSON.stringify(invocationResult));
         // alert(JSON.stringify(invocationResult));

               if(invocationResult.isSuccessful){
                if(invocationResult.bbpstokencreationres){

                // var Passkey = invocationResult.bbpstokencreationres.Passkey;
                // var agentid=invocationResult.bbpstokencreationres.agentid;

                 //alert(Passkey);
                 //alert(agentid);


        // var urlBBPS="https://muat.rblbank.com/rblbbps/resource/html/RB01/tenantBillPayHTML?agentid="+agentid+"&passkey="+Passkey+"&paymentmode=Internet%20Banking&imeicode=";
        // alert(urlBBPS);
        // console.log(urlBBPS);

        var urlBBPS = invocationResult.bbpstokencreationres.udf1;
      var urlBBPSnew = urlBBPS.replace(/\*/g,"&");

    //   var urlBBPSnew= urlBBPS.replaceAll("*", "&");
     //   alert(urlBBPS);
     //   alert(urlBBPSnew);

          busyInd.hide();

         // console.log("###"+urlBBPSnew);

        // var testurl="https://muat.rblbank.com/rblbbps/resource/html/RB01/tenantBillPayHTML?agentid=RB01AB21000001123458&passkey=4E0038006B00720059006E0069006200440065006F00670030004700580044004700380068004C006F003200520036006D00760036004E004C006B0079006A004900740044006C00390038006B00340066003600740056007800730033005A007700740033006A006600500062002F0057007300740078006D00670074003500340035002B0069004D00320064003400320035003600710063003900650044005100310078004200330067003D003D00&paymentmode=Internet%20Banking&imeicode=";

        openLink(urlBBPSnew);


//        $("#contentData").load("Views/BillPay/BBPSWebView.html", null, function(response,status, xhr) {
//        alert("1");
//         if (status != "error") { }
//          alert("2");
//         $("#iframeBBPS").attr('src',testurl);
//          alert("3");
//         ko.applyBindings(model, $(".content").get(0));
//          alert("4");
////          busyInd.hide();
//
//            });

          }
                else{
                busyInd.hide();
               navigator.notification.alert(invocationResult.errorres.description);
                }
        }
         else{
                 busyInd.hide();
              	navigator.notification.confirm("Please ensure that you have network connectivity and try again!", onNetworkCheck, "Connection Error", "OK");
               }
           };

};

function randomStringNum(length) {
	var result = '';
	var d = new Date();
	var chars = d.getTime();
	chars = chars + '';
	for (var i = length; i > 0; --i) {
		result += chars[Math.round(Math.random() * (chars.length - 1))];
	}
	return result;
}


function onRecord()
{
	window.location.hash = '#billpayment';
}