# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\AndroidStudio/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keep class hexadots.in.rblmobank.models.GlobalResponse** { *; }
-dontwarn hexadots.in.rblmobank.models.GlobalResponse**

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }
# -keep class mypersonalclass.data.model.** { *; }
# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

##---------------End: proguard configuration for Gson  ----------


##---------------Begin: proguard configuration for Jackson  ----------
-keepattributes *Annotation*,EnclosingMethod,Signature
-keepnames class com.fasterxml.jackson.** { *; }
 -dontwarn com.fasterxml.jackson.databind.**
 -keep class org.codehaus.** { *; }
 -keepclassmembers public final enum org.codehaus.jackson.annotate.JsonAutoDetect$Visibility {
 public static final org.codehaus.jackson.annotate.JsonAutoDetect$Visibility *; }
-keep public class mydatapackage.** {
  public void set*(***);
  public *** get*();
  public boolean is*();
}

##---------------End: proguard configuration for Jackson  ----------

#For getFocus
-dontwarn com.google.android.gms.**
-dontwarn io.protostuff.**
-dontwarn sun.reflet.**
-dontwarn org.apache.**
-dontwarn jsat.**
-dontwarn java.awt.**
-dontwarn java.applet.**

-keep class jsat.**{
*;
}
-keep class sun.misc.Unsafe {
*;
}
-keep class com.focus.**{
*;
}
#protostuff specific
-keep class io.protostuff.**{
*;
}

-dontwarn hexadots.in.rblmobank.utils.**
#-keep class hexadots.in.rblmobank.utils.**
#-keepclassmembers class hexadots.in.** { *; }
#-keepclassmembers interface hexadots.in.** { *; }
#-keep public abstract class hexadots.in.** { *; }

-keep public class hexadots.in.rblmobank.utils.Constants
-keepclassmembers public class hexadots.in.rblmobank.utils.Constants { *;}

-keep public class com.focus.android.sdk.services.exceptions.FocusSdkInitializationException
-keepclassmembers public class com.focus.android.sdk.services.exceptions.FocusSdkInitializationException { *;}
