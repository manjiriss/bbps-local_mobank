package hexadots.in.rblmobank.common;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import hexadots.in.rblmobank.R;
import hexadots.in.rblmobank.utils.Utils;

/**
 * Created by Mani P on 10/26/2016.
 */
public class DeviceInfo extends AppCompatActivity {



//    public static String getUUID(Context context){
//        return  Settings.Secure.getString(context.getContentResolver(),
//                Settings.Secure.ANDROID_ID);
//
////        return "FD1DA61B-95CE-431C-8470-31CC83B112A8";
//    }

    public static String getTabletType(Context context) {
        String phoneType="";
        boolean tabletSize = context.getResources().getBoolean(R.bool.isTablet);
        if (tabletSize) {
            // do something
            //Start activity for tablet
            phoneType="Tablet";
        } else {
            // do something else
            //Start activity for phone
            phoneType="Phone";
        }

        return  phoneType;
    }

    public static String getUUID(Context context) {

        //return "5a53a08f-1c77-3335-aa38-13fa3a06da73";

        //return "e65290bd-bb47-3bb7-8f9e-36b4ebf9e805";
       //return "39ec8abe-de32-326e-981d-e6a252556d38";
        //return  "e67fd7e8-0a98-3c34-a4d1-31c6f057d289";
        return Utils.getDeviceID(context);
    }
}
