package hexadots.in.rblmobank.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import hexadots.in.rblmobank.beans.Account;
import hexadots.in.rblmobank.R;
import hexadots.in.rblmobank.interfaces.AccountCommunicator;
import hexadots.in.rblmobank.models.LoginAccountDetailsType;


public class AccountRecyclerAdapter extends RecyclerView.Adapter<AccountRecyclerAdapter.MyViewHolder> {

    private  Context context;
    private List<LoginAccountDetailsType> accountList;
    AccountCommunicator accountCommunicator;
    ImageButton changeAccount;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView accountNumber, accountType;
        public LinearLayout accountItem;

        public MyViewHolder(View view) {
            super(view);
            accountItem = (LinearLayout) view.findViewById(R.id.accountItem);
            accountNumber = (TextView) view.findViewById(R.id.account_number);
            accountType = (TextView) view.findViewById(R.id.account_type);
        }
    }

    public AccountRecyclerAdapter(Context context, List<LoginAccountDetailsType> accountList, ImageButton changeAccount) {
        this.context=context;
        this.accountList = accountList;
        this.accountCommunicator = (AccountCommunicator) context;
        this.changeAccount=changeAccount;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.account_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final LoginAccountDetailsType account = accountList.get(position);
        holder.accountNumber.setText(account.getAcctno());
        holder.accountType.setText(account.getAcctType());

        holder.accountItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAccount.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.button_state_not_pressed));
                accountCommunicator.callBack(account);
            }
        });
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }
}