package hexadots.in.rblmobank.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import hexadots.in.rblmobank.R;
import hexadots.in.rblmobank.models.TptAcctDetails;
import hexadots.in.rblmobank.interfaces.TptCommunicator;


public class OtherBanksListAdapter extends RecyclerView.Adapter<OtherBanksListAdapter.MyViewHolder> {

    private List<TptAcctDetails> payeeList;
    TptCommunicator payeeCommunicator;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView payeeName, payeeAccountNumber;
        public RelativeLayout payeeItem;

        public MyViewHolder(View view) {
            super(view);
            payeeItem = (RelativeLayout) view.findViewById(R.id.payeeItem);
            payeeName = (TextView) view.findViewById(R.id.payee_name);
            payeeAccountNumber = (TextView) view.findViewById(R.id.payee_account_number);
        }
    }

    public OtherBanksListAdapter(Context context, List<TptAcctDetails> payeeList) {
        this.payeeList = payeeList;
        this.payeeCommunicator = (TptCommunicator) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payee_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final TptAcctDetails payee = payeeList.get(position);
        holder.payeeName.setText(payee.getBenefname());
        holder.payeeAccountNumber.setText(payee.getAcctno());

        holder.payeeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                payeeCommunicator.callBack(payee);
            }
        });
    }

    @Override
    public int getItemCount() {
        return payeeList.size();
    }
}