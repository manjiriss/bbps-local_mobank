package hexadots.in.rblmobank.interfaces;


import hexadots.in.rblmobank.beans.Payee;
import hexadots.in.rblmobank.models.InterTptAcctDetails;
import hexadots.in.rblmobank.models.TptAcctDetails;

public interface PayeeCommunicator {

    void callBack(InterTptAcctDetails payee);


}