package hexadots.in.rblmobank.interfaces;

import hexadots.in.rblmobank.beans.Account;
import hexadots.in.rblmobank.models.LoginAccountDetailsType;


public interface AccountCommunicator {

    void callBack(LoginAccountDetailsType account);


}