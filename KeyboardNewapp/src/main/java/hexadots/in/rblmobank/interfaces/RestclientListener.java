package hexadots.in.rblmobank.interfaces;

public interface RestclientListener{

    void onRestfulResponse(int reqId, String response, boolean connectedState);
}