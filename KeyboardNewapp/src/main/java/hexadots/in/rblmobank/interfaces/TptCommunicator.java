package hexadots.in.rblmobank.interfaces;

import hexadots.in.rblmobank.models.TptAcctDetails;

/**
 * Created by Mani P on 10/27/2016.
 */
public interface TptCommunicator {

    void callBack(TptAcctDetails payee);


}