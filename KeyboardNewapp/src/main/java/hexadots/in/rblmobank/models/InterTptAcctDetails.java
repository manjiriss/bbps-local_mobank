package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 10/27/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class InterTptAcctDetails {

    private String benefname;
    private String acctno;

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    private boolean type;

    public InterTptAcctDetails() {
    }

    public InterTptAcctDetails(String name, String accnum) {
        this.benefname = name;
        this.acctno = accnum;


    }

    public String getBenefname() {
        return benefname;
    }

    public void setBenefname(String benefname) {
        this.benefname = benefname;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }
}
