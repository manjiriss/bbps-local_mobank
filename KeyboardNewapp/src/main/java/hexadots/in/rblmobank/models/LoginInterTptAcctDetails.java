package hexadots.in.rblmobank.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mani P on 10/27/2016.
 */
public class LoginInterTptAcctDetails extends ArrayList<InterTptAcctDetails> {


    List<InterTptAcctDetails> result = this;

    public List<InterTptAcctDetails> getResult() {
        return result;
    }

    public void setResult(List<InterTptAcctDetails> result) {
        if (result != null) {
            this.clear();
            this.addAll(result);
        }
        this.result = result;
    }

}