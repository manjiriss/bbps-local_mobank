package hexadots.in.rblmobank.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mani P on 10/27/2016.
 */
public class LoginTptAcctDetails extends ArrayList<TptAcctDetails> {


    List<TptAcctDetails> result = this;

    public List<TptAcctDetails> getResult() {
        return result;
    }

    public void setResult(List<TptAcctDetails> result) {
        if (result != null) {
            this.clear();
            this.addAll(result);
        }
        this.result = result;
    }
}
