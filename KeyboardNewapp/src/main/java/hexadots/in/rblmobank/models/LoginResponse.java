package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 10/26/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse {

    private String customerid;
    private String SessionKey;
//    private  LoginStatus STATUS;


    private String acctindex;
    private String jointAcct;
    private String custName;
    private String acctno;
    private String savingacctcount;
    private String currentacctcount;
    private String Intra_Bank_Beneficiaries;


    public String getSavingacctcount() {
        return savingacctcount;
    }

    public void setSavingacctcount(String savingacctcount) {
        this.savingacctcount = savingacctcount;
    }

    public String getCurrentacctcount() {
        return currentacctcount;
    }

    public void setCurrentacctcount(String currentacctcount) {
        this.currentacctcount = currentacctcount;
    }


    public String getAcctindex() {
        return acctindex;
    }

    public void setAcctindex(String acctindex) {
        this.acctindex = acctindex;
    }

    public String getJointAcct() {
        return jointAcct;
    }

    public void setJointAcct(String jointAcct) {
        this.jointAcct = jointAcct;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getImps_Beneficiaries() {
        return Imps_Beneficiaries;
    }

    public void setImps_Beneficiaries(String imps_Beneficiaries) {
        Imps_Beneficiaries = imps_Beneficiaries;
    }

    private String Imps_Beneficiaries;
    private String Account_Summary;

    public String getAccount_Summary() {
        return Account_Summary;
    }

    public void setAccount_Summary(String account_Summary) {
        Account_Summary = account_Summary;
    }
    //    private LoginAccountSummary Account_Summary;//array
//    private LoginImpsBeneficiaries Imps_Beneficiaries;
//    private LoginIntraBankBeneficiaries Intra_Bank_Beneficiaries;

    private String CODE;
    private String MESSAGE;
    private String SEVERITY;

    public String getIntra_Bank_Beneficiaries() {
        return Intra_Bank_Beneficiaries;
    }

    public void setIntra_Bank_Beneficiaries(String intra_Bank_Beneficiaries) {
        Intra_Bank_Beneficiaries = intra_Bank_Beneficiaries;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getSEVERITY() {
        return SEVERITY;
    }

    public void setSEVERITY(String SEVERITY) {
        this.SEVERITY = SEVERITY;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getSessionKey() {
        return SessionKey;
    }

    public void setSessionKey(String sessionKey) {
        SessionKey = sessionKey;
    }

//    public LoginStatus getSTATUS() {
//        return STATUS;
//    }
//
//    public void setSTATUS(LoginStatus STATUS) {
//        this.STATUS = STATUS;
//    }

//    public LoginAccountSummary getAccount_Summary() {
//        return Account_Summary;
//    }
//
//    public void setAccount_Summary(LoginAccountSummary account_Summary) {
//        Account_Summary = account_Summary;
//    }
//
//    public LoginImpsBeneficiaries getImps_Beneficiaries() {
//        return Imps_Beneficiaries;
//    }
//
//    public void setImps_Beneficiaries(LoginImpsBeneficiaries imps_Beneficiaries) {
//        Imps_Beneficiaries = imps_Beneficiaries;
//    }

//    public LoginIntraBankBeneficiaries getIntra_Bank_Beneficiaries() {
//        return Intra_Bank_Beneficiaries;
//    }
//
//    public void setIntra_Bank_Beneficiaries(LoginIntraBankBeneficiaries intra_Bank_Beneficiaries) {
//        Intra_Bank_Beneficiaries = intra_Bank_Beneficiaries;
//    }
}
