package hexadots.in.rblmobank.models;

/**
 * Created by sandeep on 10/28/2016.
 */

public class TransactionData {

    private String description;
    private String ifscCode;
    private String referenceNumber;
    private String toAccount;
    private String bankName;
    private String fromAccount;
    private String senderName;
    private String benyName;
    private String otp;
    private String otpRefNumber;
    private String amount;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getToAccount() {
        return toAccount;
    }

    public void setToAccount(String toAccount) {
        this.toAccount = toAccount;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(String fromAccount) {
        this.fromAccount = fromAccount;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getBenyName() {
        return benyName;
    }

    public void setBenyName(String benyName) {
        this.benyName = benyName;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getOtpRefNumber() {
        return otpRefNumber;
    }

    public void setOtpRefNumber(String otpRefNumber) {
        this.otpRefNumber = otpRefNumber;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
