package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 10/26/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginAccountSummary {

    private String savingacctcount;
    private String currentacctcount;
    private LoginAccountDetails acctdetails;
    private LoginCustDetails CustDetails;

    public LoginCustDetails getCustDetails() {
        return CustDetails;
    }

    public void setCustDetails(LoginCustDetails custDetails) {
        CustDetails = custDetails;
    }

    public String getSavingacctcount() {
        return savingacctcount;
    }

    public void setSavingacctcount(String savingacctcount) {
        this.savingacctcount = savingacctcount;
    }

    public String getCurrentacctcount() {
        return currentacctcount;
    }

    public void setCurrentacctcount(String currentacctcount) {
        this.currentacctcount = currentacctcount;
    }

    public LoginAccountDetails getAcctdetails() {
        return acctdetails;
    }

    public void setAcctdetails(LoginAccountDetails acctdetails) {
        this.acctdetails = acctdetails;
    }
}
