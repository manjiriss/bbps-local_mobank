package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

/**
 * Created by Mani P on 10/27/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginAccountDetails {

    private Object acctdetails;

    public Object getAcctdetails() {
        return acctdetails;
    }

    public void setAcctdetails(Object acctdetails) {
        this.acctdetails = acctdetails;
    }


    public List<LoginAccountDetailsType> getListType() {
        if (acctdetails instanceof List) {
            return (List<LoginAccountDetailsType>) acctdetails;
        } else
            return null;
    }

    public LoginAccountDetailsType getObjectType() {
        if (acctdetails instanceof LoginAccountDetailsType) {
            return (LoginAccountDetailsType) acctdetails;
        } else if (acctdetails instanceof Map) {
            Map<String, String> map = (Map<String, String>) acctdetails;
            LoginAccountDetailsType newType = new LoginAccountDetailsType(map.get("acctType"),
                    map.get("acctbalance"), map.get("clearbal"), map.get("acctindex"),
                    map.get("currency"), map.get("acctno"), map.get("branchname"));
            return newType;
        }
        return null;
    }


}
