package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 10/29/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginAccountDetailsType {


    private String acctType;
    private String acctbalance;
    private String clearbal;
    private String acctindex;
    private String currency;
    private String acctno;
    private String branchname;


    public LoginAccountDetailsType() {
    }

    public LoginAccountDetailsType(String acctType, String acctbalance, String clearbal, String acctindex,
                                   String currency, String acctno, String branchname) {
        this.acctType = acctType;
        this.acctbalance = acctbalance;
        this.clearbal = clearbal;
        this.acctindex = acctindex;
        this.currency = currency;
        this.acctno = acctno;
        this.branchname = branchname;

    }


    public String getAcctType() {
        return acctType;
    }

    public void setAcctType(String acctType) {
        this.acctType = acctType;
    }

    public String getAcctbalance() {
        return acctbalance;
    }

    public void setAcctbalance(String acctbalance) {
        this.acctbalance = acctbalance;
    }

    public String getClearbal() {
        return clearbal;
    }

    public void setClearbal(String clearbal) {
        this.clearbal = clearbal;
    }

    public String getAcctindex() {
        return acctindex;
    }

    public void setAcctindex(String acctindex) {
        this.acctindex = acctindex;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

}
