package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 10/26/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GlobalResponse {

    private String OperationID;
    private String OutString;
    private String Salt;
    private String Code;
    private String Message;
    private String Severity;


    public String getOperationID() {
        return OperationID;
    }

    public void setOperationID(String operationID) {
        OperationID = operationID;
    }

    public String getOutString() {
        return OutString;
    }

    public void setOutString(String outString) {
        OutString = outString;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getSeverity() {
        return Severity;
    }

    public void setSeverity(String severity) {
        Severity = severity;
    }
}
