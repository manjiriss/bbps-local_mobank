package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 11/18/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerAccountDetails {



    private String custName;
    private String acctno;
    private String acctindex;
    private String jointAcct;

    public CustomerAccountDetails() {
    }

    public CustomerAccountDetails(String acctIndex, String jointacct, String custname, String acctnum
                                   ) {
        this.acctindex = acctIndex;
        this.jointAcct = jointacct;
        this.custName = custname;
        this.acctindex = acctnum;


    }


    public String getAcctindex() {
        return acctindex;
    }

    public void setAcctindex(String acctindex) {
        this.acctindex = acctindex;
    }

    public String getJointAcct() {
        return jointAcct;
    }

    public void setJointAcct(String jointAcct) {
        this.jointAcct = jointAcct;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }


}
