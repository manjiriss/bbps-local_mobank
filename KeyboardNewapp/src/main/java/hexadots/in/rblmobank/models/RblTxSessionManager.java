package hexadots.in.rblmobank.models;

/**
 * Created by sandeep on 10/28/2016.
 */
public class RblTxSessionManager {
    private static RblTxSessionManager ourInstance = new RblTxSessionManager();
    TransactionData transactionData = new TransactionData();


    TransactionType transactionType;


    public TransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(TransactionData transactionData) {
        this.transactionData = transactionData;
    }


    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }


    public static RblTxSessionManager getInstance() {
        return ourInstance;
    }

    private RblTxSessionManager() {
    }

    public enum TransactionType {

        rblIMPSTransaction, rblIFTTransaction


    }
}
