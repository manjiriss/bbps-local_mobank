package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

/**
 * Created by Mani P on 10/27/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginIntraBankBeneficiaries {

    private Object tptacctdetails;


    public Object getAcctdetails() {
        return tptacctdetails;
    }

    public void setAcctdetails(Object acctdetails) {
        this.tptacctdetails = acctdetails;
    }


    public List<InterTptAcctDetails> getListType() {
        if (tptacctdetails instanceof List) {
            return (List<InterTptAcctDetails>) tptacctdetails;
        } else
            return null;
    }

    public String getStringType() {
        if (tptacctdetails instanceof String) {
            return "";
        } else
            return null;
    }

    public InterTptAcctDetails getObjectType() {
        if (tptacctdetails instanceof InterTptAcctDetails) {
            return (InterTptAcctDetails) tptacctdetails;
        } else if (tptacctdetails instanceof Map) {
            Map<String, String> map = (Map<String, String>) tptacctdetails;
            InterTptAcctDetails newType = new InterTptAcctDetails(map.get("benefname"),
                    map.get("acctno"));
            return newType;
        }
        return null;
    }


//    private LoginInterTptAcctDetails tptacctdetails;
//
//    public LoginInterTptAcctDetails getTptacctdetails() {
//        return tptacctdetails;
//    }
//
//    public void setTptacctdetails(LoginInterTptAcctDetails tptacctdetails) {
//        this.tptacctdetails = tptacctdetails;
//    }
}
