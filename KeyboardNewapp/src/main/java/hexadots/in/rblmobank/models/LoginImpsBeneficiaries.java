package hexadots.in.rblmobank.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Mani P on 10/27/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginImpsBeneficiaries {

    private LoginTptAcctDetails tptacctdetails;

    public LoginTptAcctDetails getTptacctdetails() {
        return tptacctdetails;
    }

    public void setTptacctdetails(LoginTptAcctDetails tptacctdetails) {
        this.tptacctdetails = tptacctdetails;
    }
}
