package hexadots.in.rblmobank.utils;

/**
 * Created by Mani P on 10/27/2016.
 */
public class URLS {

    public static String loginUrl = "SessionId=&RQDeviceId=%s&RQDeviceFormat=%s&RQOperationId=%s&RQDeviceFamily=%s&RQTransSeq=%s&RQClientAPIVer=%s&RQLoginPwd=%s&RQLoginUserId=";
    public static String generateOTPUrl = "SessionId=%s&RQLoginUserId=%s&RQDeviceFormat=%s&RQOperationId=%s&RQDeviceFamily=%s&RQTransSeq=%s&RQClientAPIVer=%s";
    public static String IMPSTransferUrl = "RQDesc=%s&RQIFSCCode=%s&SessionId=%s&RQRefNo=%s&RQToAcctNo=%s&RQOperationId=%s&RQBeneBank=%s&RQDeviceFamily=%s&RQFromAcctNo=%s&RQToAccType=%s&RQSndName=%s&RQBeneAddress=%s&RQLoginUserId=%s&RQDeviceFormat=%s&RQBeneName=%s&RQTransSeq=%s&RQOTP=%s&RQTxnAmount=%s&RQOTPReq=%s&RQClientAPIVer=%s";
    public static String TransferWithinBankUrl = "RQLoginUserId=%s&RQDeviceFamily=%s&RQDeviceFormat=%s&RQOperationId=%s&RQClientAPIVer=%s&SessionId=%s&RQFromAcctNo=%s&RQToAcctNo=%s&RQTxnAmount=%s&RQTransSeq=%s&RQDesc=%s&RQOTP=%s&RQRefNo=%s&RQOTPReq=%s&RQSndName=%s&RQBenename=%s";
    public static String BalanceEnquirykUrl = "SessionId=%s&RQLoginUserId=%s&RQDeviceFormat=%s&RQOperationId=%s&RQDeviceFamily=%s&RQTransSeq=%s&RQClientAPIVer=%s";

}
