package hexadots.in.rblmobank.utils;


import android.util.Base64;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class EncryptionUtil {

	public static void main(String[] args) throws Exception {
	}

	public static String RandomStringGen(int Len) {

		SecureRandom randomString = new SecureRandom();
		char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		StringBuilder SB = new StringBuilder();
		for(int i=0;i<Len;i++){
			char c = chars[randomString.nextInt(chars.length)];
			SB.append(c);
		}
		String output = SB.toString();
		return output;
	}

	private static SecretKeySpec secretKey ;
	private static byte[] key ;

	private static String encryptedString;
	private static String decryptedString;
	private static String KEY;
	public static void setKey(String myKey){
		KEY= myKey;
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
//			sha = MessageDigest.getInstance("SHA-256");
//			key = sha.digest(key);
			key = Arrays.copyOf(key, 16); // use only first 128 bit
			secretKey = new SecretKeySpec(key, "AES");

		}
		catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static String getEncryptedString() {
		return encryptedString;
	}
	public static void setEncryptedString(String encryptedString) {
		EncryptionUtil.encryptedString = encryptedString;
	}

	public static String encrypt(String strToEncrypt)
	{
		try
		{
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			setEncryptedString(Base64.encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")), Base64.DEFAULT));

		}
		catch (Exception e)
		{
			System.out.println("Error while encrypting: "+e.toString());
		}
		return EncryptionUtil.encryptedString;
	}

	public static String getEncryptedString(String key,String value)
	{
		EncryptionUtil.setKey(key);
		EncryptionUtil.encrypt(value.trim());
		return EncryptionUtil.getEncryptedString();
	}
	private static String getDecryptedString() {
		return decryptedString;
	}
	private static void setDecryptedString(String decryptedString) {
		EncryptionUtil.decryptedString = decryptedString;
	}

	private static void decrypt(String strToDecrypt) throws Exception
	{
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		setDecryptedString(new String(cipher.doFinal(Base64.decode(strToDecrypt,Base64.DEFAULT))));
	}

	public static String getDecryptedVal(String key,String encryptedVal) throws Exception
	{
		EncryptionUtil.setKey(key);
		EncryptionUtil.decrypt(encryptedVal.trim());
		return EncryptionUtil.getDecryptedString();
	}

	public static String getCheckSum(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest
					.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
	public static String getSalt() {
		return EncryptionUtil.encrypt(KEY);
	}
}
