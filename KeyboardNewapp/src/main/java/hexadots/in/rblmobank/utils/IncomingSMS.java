package hexadots.in.rblmobank.utils;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;


public class IncomingSMS extends BroadcastReceiver {


    private Bundle bundle;
    private SmsMessage currentSMS;
    private String message;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdu_Objects = (Object[]) bundle.get("pdus");
                if (pdu_Objects != null) {
                    for (Object aObject : pdu_Objects) {
                        currentSMS = getIncomingMessage(aObject, bundle);
                        String senderNo = currentSMS.getDisplayOriginatingAddress();
                        message = currentSMS.getDisplayMessageBody();
                       // String fullSMS = "senderNum: " + senderNo + " :\n message: " + message;
                        if(senderNo.contains("-RBLBNK")) {
                            sendLocalBroadcast(message);
                            break;
                        }
                    }
                    this.abortBroadcast();
                }
            }
        }

    }

    /**
     * This is used to send SMS localBroadCast to our mainActivity.
     *
     * @param message
     */
    private void sendLocalBroadcast(String message) {
        Intent intent = new Intent(Constants.KEY_LOCAL_SMS_RECEIVER);
        intent.putExtra(Constants.KEY_SMS, message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    /**
     * This is to process the incoming bundle and get the message out from it
     *
     * @param aObject
     * @param bundle
     * @return
     */
    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        // In above M devices we need to pass the format to createFromPdu
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }

}

