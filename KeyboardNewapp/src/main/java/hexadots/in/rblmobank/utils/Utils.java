package hexadots.in.rblmobank.utils;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.inputmethodservice.InputMethodService;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsMessage;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import android.content.SharedPreferences;

import hexadots.in.rblmobank.MainActivity;

public class Utils {

    public static String readFromAssets(String fileName,InputMethodService context) throws IOException {
        StringBuilder buf=new StringBuilder();
        InputStream json= context.getAssets().open(fileName);
        BufferedReader in=
                new BufferedReader(new InputStreamReader(json, "UTF-8"));
        String str;

        while ((str=in.readLine()) != null) {
            buf.append(str);
        }

        in.close();
        return buf.toString();

    }
    public static void switchKeyboard(InputMethodService context) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            final IBinder iBinder = getToken(context);
                inputMethodManager.switchToLastInputMethod(iBinder);

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private static IBinder getToken(InputMethodService context) {
        final Dialog dialog = context.getWindow();
        if (dialog == null) {
            return null;
        }
        final Window window = dialog.getWindow();
        if (window == null) {
            return null;
        }
        return window.getAttributes().token;
    }

    public static Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
        Map<String, Object> retMap = new HashMap<String, Object>();

        if(json != JSONObject.NULL) {
            retMap = toMap(json);
        }
        return retMap;
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
    public static Boolean isAppActived(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.hexadots.keyboardPayments.RBLMoBank", 0);
        String appActived = sharedPreferences.getString("AppActivatedFlag", "N");
        if (appActived.equalsIgnoreCase("Y"))
        {
            return true;
        }
        else {
            return false;
        }

    }
    public  static Boolean isUserLocked(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.hexadots.keyboardPayments.RBLMoBank", 0);
        String userLocked = sharedPreferences.getString("AppUserLockedFlag", "N");
        if (userLocked.equalsIgnoreCase("Y"))
        {
            return true;
        }
        else {
            return false;
        }

    }
    public  static void setUserLocked(Context context)
    {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.hexadots.keyboardPayments.RBLMoBank", 0).edit();
        edit.putString ("AppUserLockedFlag","Y"); // Y or N
        edit.commit();
    }
    public static String getDeviceID(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.hexadots.keyboardPayments.RBLMoBank", 0);
        String deviceID = sharedPreferences.getString("AppDeviceIdFlag", "DeviceID");
        if (deviceID.length() > 0){
            return deviceID;
        }
        return "";
    }

    public static Boolean checkForRootingDevice(Context context) {
        if (RootDetectionChecked.isDeviceRooted()) {
            return true;
//            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
//            alertDialogBuilder.setTitle("Security Alert");
//            alertDialogBuilder
//                    .setMessage("For Security purpose RBL Bank won't run on rooted device.!")
//                    .setCancelable(false)
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int id) {
//                            MainActivity.this.finish();
//                        }
//                    });
//            AlertDialog alertDialog = alertDialogBuilder.create();
//            alertDialog.show();
        }
        return false;
    }

    public static String getCurrentDate()
    {
        Date cDate = new Date();
        String fDate = new SimpleDateFormat("dd-MM-yyyy").format(cDate);
        return fDate;
    }



}
