package hexadots.in.rblmobank.utils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.json.XMLTokener;
import hexadots.in.rblmobank.utils.JSONEXTNS;

public class XMLEXTNS extends XML {

	public static JSONObject toNewJSONObject(String string, boolean keepStrings)
			throws JSONException {
		JSONObject jo = new JSONObject();
		XMLTokener x = new XMLTokener(string);
		while (x.more() && x.skipPast("<")) {
			parse(x, jo, null, keepStrings);
		}
		return jo;
	}

	public static String unescape(String string) { 
        StringBuilder sb = new StringBuilder(string.length()); 
        for (int i = 0, length = string.length(); i < length; i++) { 
            char c = string.charAt(i); 
            if (c == '&') { 
                final int semic = string.indexOf(';', i); 
                if (semic > i) { 
                    final String entity = string.substring(i + 1, semic); 
                    if (entity.charAt(0) == '#') { 
                        int cp; 
                        if (entity.charAt(1) == 'x') { 
                            // hex encoded unicode 
                            cp = Integer.parseInt(entity.substring(2), 16); 
                        } else { 
                            // decimal encoded unicode 
                            cp = Integer.parseInt(entity.substring(1)); 
                        } 
                        sb.appendCodePoint(cp); 
                    } else { 
                        if ("quot".equalsIgnoreCase(entity)) { 
                            sb.append('"'); 
                        } else if ("amp".equalsIgnoreCase(entity)) { 
                            sb.append('&'); 
                        } else if ("apos".equalsIgnoreCase(entity)) { 
                            sb.append('\''); 
                        } else if ("lt".equalsIgnoreCase(entity)) { 
                            sb.append('<'); 
                        } else if ("gt".equalsIgnoreCase(entity)) { 
                            sb.append('>'); 
                        } else { 
                            sb.append('&').append(entity).append(';'); 
                        } 
                    } 
                    // skip past the entity we just parsed. 
                    i += entity.length() + 1; 
                } else { 
                    // this shouldn't happen in most cases since the parser 
                    // errors on unclosed enties. 
                    sb.append(c); 
                } 
            } else { 
                // not part of an entity 
                sb.append(c); 
            } 
        } 
        return sb.toString(); 
    } 

	public static Object stringToValue(String string) { 
        Object ret = JSONEXTNS.stringToValue(string);
        if(ret instanceof String){ 
            return unescape((String)ret); 
        } 
        return ret; 
    } 

	
	private static boolean parse(XMLTokener x, JSONObject context, String name,
			boolean keepStrings) throws JSONException {
		char c;
		int i;
		JSONObject jsonobject = null;
		String string;
		String tagName;
		Object token;

		// Test for and skip past these forms:
		// <!-- ... -->
		// <! ... >
		// <![ ... ]]>
		// <? ... ?>
		// Report errors for these forms:
		// <>
		// <=
		// <<

		token = x.nextToken();

		// <!

		if (token == BANG) {
			c = x.next();
			if (c == '-') {
				if (x.next() == '-') {
					x.skipPast("-->");
					return false;
				}
				x.back();
			} else if (c == '[') {
				token = x.nextToken();
				if ("CDATA".equals(token)) {
					if (x.next() == '[') {
						string = x.nextCDATA();
						if (string.length() > 0) {
							context.accumulate("content", string);
						}
						return false;
					}
				}
				throw x.syntaxError("Expected 'CDATA['");
			}
			i = 1;
			do {
				token = x.nextMeta();
				if (token == null) {
					throw x.syntaxError("Missing '>' after '<!'.");
				} else if (token == LT) {
					i += 1;
				} else if (token == GT) {
					i -= 1;
				}
			} while (i > 0);
			return false;
		} else if (token == QUEST) {

			// <?
			x.skipPast("?>");
			return false;
		} else if (token == SLASH) {

			// Close tag </

			token = x.nextToken();
			if (name == null) {
				throw x.syntaxError("Mismatched close tag " + token);
			}
			if (!token.equals(name)) {
				throw x.syntaxError("Mismatched " + name + " and " + token);
			}
			if (x.nextToken() != GT) {
				throw x.syntaxError("Misshaped close tag");
			}
			return true;

		} else if (token instanceof Character) {
			throw x.syntaxError("Misshaped tag");

			// Open tag <

		} else {
			tagName = (String) token;
			token = null;
			jsonobject = new JSONObject();
			for (;;) {
				if (token == null) {
					token = x.nextToken();
				}
				// attribute = value
				if (token instanceof String) {
					string = (String) token;
					token = x.nextToken();
					if (token == EQ) {
						token = x.nextToken();
						if (!(token instanceof String)) {
							throw x.syntaxError("Missing value");
						}
						jsonobject.accumulate(string,
								keepStrings ? unescape((String) token)
										: stringToValue((String) token));
						token = null;
					} else {
						jsonobject.accumulate(string, "");
					}

				} else if (token == SLASH) {
					// Empty tag <.../>
					if (x.nextToken() != GT) {
						throw x.syntaxError("Misshaped tag");
					}
					if (jsonobject.length() > 0) {
						context.accumulate(tagName, jsonobject);
					} else {
						context.accumulate(tagName, "");
					}
					return false;

				} else if (token == GT) {
					// Content, between <...> and </...>
					for (;;) {
						token = x.nextContent();
						if (token == null) {
							if (tagName != null) {
								throw x.syntaxError("Unclosed tag " + tagName);
							}
							return false;
						} else if (token instanceof String) {
							string = (String) token;
							if (string.length() > 0) {
								jsonobject.accumulate("content",
										keepStrings ? unescape(string)
												: stringToValue(string));
							}

						} else if (token == LT) {
							// Nested element
							if (parse(x, jsonobject, tagName, keepStrings)) {
								if (jsonobject.length() == 0) {
									context.accumulate(tagName, "");
								} else if (jsonobject.length() == 1
										&& jsonobject.opt("content") != null) {
									context.accumulate(tagName,
											jsonobject.opt("content"));
								} else {
									context.accumulate(tagName, jsonobject);
								}
								return false;
							}
						}
					}
				} else {
					throw x.syntaxError("Misshaped tag");
				}
			}
		}
	}

}
