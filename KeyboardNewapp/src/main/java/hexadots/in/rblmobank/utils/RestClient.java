package hexadots.in.rblmobank.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import hexadots.in.rblmobank.interfaces.RestclientListener;



public class RestClient extends AsyncTask<String, String, String> {

    private static final String TAG = "MyKeyboard1";

	//private final String SERVICE_URL = "https://mobileapps.rblbank.com/KeypadUAT";
   // private final String SERVICE_URL = "https://mobileapps.rblbank.com/KeypadQA";
    private final String SERVICE_URL =  "https://mbconnect.rblbank.com/ChatPay";
    //private final String SERVICE_URL =  "https://180.179.137.193:443/ChatPay";

    JSONObject postData;
    RestclientListener listener;
    int reqID;
    private String bodycontent;
    public static boolean isLoading =false;
    boolean connectedState =false;
    Context cont;
//    ProgressDialog progressDialog;
    public RestClient(RestclientListener listener, int reqID, String request, Context context) {
    	this.listener = listener;
    	this.reqID = reqID;
        this.bodycontent = request;
        this.cont=context;
    }
    public RestClient(JSONObject postData)  throws Exception{
        this.postData= postData;
    }

    @Override
    protected void onPreExecute() {
//        progressDialog = ProgressDialog.show(cont,
//                "ProgressDialog",
//                "Please Wait..");

        super.onPreExecute();
       }


    @Override
    protected String doInBackground(String... params) {

        String urlToHit = SERVICE_URL; // URL to call
        String page="";
//        publishProgress("Processing...");
        URL url;
        HttpURLConnection connection = null;
        try {
            url = new URL(urlToHit);
            connection = (HttpURLConnection)url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Cache-Control", "no-cache");//            connection.setC
            connection.connect();

            //Send request
            DataOutputStream wr = new DataOutputStream(
                    connection.getOutputStream ());
            wr.writeBytes(bodycontent.toString());
            wr.flush();
            wr.close ();

            InputStream is;
            int response = connection.getResponseCode();
            Log.v(TAG, "------ Response Message ------" + connection.getResponseMessage());
            Log.v(TAG, "------ Response Code ------" + connection.getResponseCode());
            if (response == 200 ){
                connectedState = true;
                 is = connection.getInputStream();
                byte[] buffer = readFully(is);
                 page = new String(buffer, 0, buffer.length, "UTF-8");
            } else {
                connectedState = false;
                is = connection.getErrorStream();
                byte[] buffer = readFully(is);
                 page = new String(buffer, 0, buffer.length, "UTF-8");
            }
          
        } catch (Exception e){

        }
        finally {

            if(connection != null) {
                connection.disconnect();
            }
        }

     return page;

    }
    public static byte[] readFully(InputStream input) throws IOException
    {
        byte[] buffer = new byte[1024];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

    @Override
    protected void onPostExecute(String result) {
    	listener.onRestfulResponse(reqID,result, connectedState);
    	
    }
}