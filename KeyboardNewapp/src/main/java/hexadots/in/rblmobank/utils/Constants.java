package hexadots.in.rblmobank.utils;

/**
 * Created by Mani P on 10/26/2016.
 */
public class Constants {

    public static  String publicKey="KBPAY0611RBL2609";
    //public static String passwordTest="MTExMTEx";
 // public static String passwordTest="MTIzNDU2";
//    public static String passwordTest="111111";
    public static String operationId="KeyboardLogin";
    public static String otpOperationId="OTPGen";
    public static String iMPSOperationId="IMPSTransfer";
    public static String iMPSOperationId2=" IMPSP2AFT";
    public static String deviceFamily="Android";
    public static String sequence="01";
    public static String androidVersion="4.1";
    public static Object otpRequest="Y";
    public static Object iftTransferId="TPTFNDTFR";
    public static Object balanceEnuiryId="AccSumm";
    public static Object iftTransferId2="IFTTransfer";
    //floatingKeyboardConstants
    public static final String KEY_PREF_NAME = "KEY_FLOATING_PREF";
    public static final String KEY_PREF_BOOT = "KEY_FLOATING_BOOT";
    public static final String KEY_PREF_START = "KEY_FLOATING_START";
    public static String PACKAGE_NAME = "com.rblbank.mobank";

    public static final String KEY_LOCAL_SMS_RECEIVER = "LocalSMSReceiver";
    public static final String KEY_SMS = "SMS";
}
