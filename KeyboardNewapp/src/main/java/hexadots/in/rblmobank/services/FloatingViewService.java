package hexadots.in.rblmobank.services;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import java.util.Calendar;

import hexadots.in.rblmobank.R;
import hexadots.in.rblmobank.keyboardbanking.Service;

/**
 * Created by Hexadots on 05-12-2016.
 */

public class FloatingViewService extends Service {

    private WindowManager windowManager;
    WindowManager.LayoutParams params;
    private ImageView ivShow;
    private View mFloatingView;
    private boolean isOnClick;
    private int SCROLL_THRESHOLD = 20;
    private static final String TAG = FloatingViewService.class.getName();
    private long startClickTime;
    private float x1;
    private float y1;
    private float x2;
    private float y2;
    private float dx;
    private float dy;
    private final int MAX_CLICK_DURATION = 200;
    private final int MAX_CLICK_DISTANCE = 5;
    private DisplayMetrics displaymetrics;

    @Override
    public void onCreate() {
        super.onCreate();
        mFloatingView = LayoutInflater.from(this).inflate(R.layout.service_floating_keyboard, null);
        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        displaymetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 0;
        params.y = displaymetrics.heightPixels - (displaymetrics.heightPixels / 2);

        windowManager.addView(mFloatingView, params);


        mFloatingView.findViewById(R.id.tray_image).setOnTouchListener(new View.OnTouchListener() {
            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //remember the initial position.
                        initialX = params.x;
                        initialY = params.y;
                        //get the touch location
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();

                        x1 = event.getX();
                        y1 = event.getY();

                        startClickTime = Calendar.getInstance().getTimeInMillis();

//                        Log.v(TAG, "---- initialTouchX --- " + initialTouchX);
//                        Log.v(TAG, "---- initialTouchY --- " + initialTouchY);

                        isOnClick = true;
                        return true;
                    case MotionEvent.ACTION_MOVE:

                        try {

                            //Calculate the X and Y coordinates of the view.
//                        Log.v(TAG, "---- eventRawX --- " + event.getRawX());
//                        Log.v(TAG, "---- eventRawY --- " + event.getRawY());

                            params.x = initialX + (int) (event.getRawX() - initialTouchX);
                            params.y = initialY + (int) (event.getRawY() - initialTouchY);
                            //Update the layout with new X & Y coordinate

                            Log.v(TAG, "---- params.x --- " + params.x);
                            Log.v(TAG, "---- params.y --- " + params.y);
                            Log.v(TAG, "---- Width Half ---- " + (displaymetrics.widthPixels / 2));
                            Log.v(TAG, "---- Height Half ---- " + (displaymetrics.heightPixels / 2));


                            double Wx = (displaymetrics.widthPixels / 2);
                            double Wy = (displaymetrics.heightPixels / 2);

                            //TODO enable this to stick the view to left while dragging.

//                        if (params.x < Wx) {
//                            params.x = 0;
//                        }

                            windowManager.updateViewLayout(mFloatingView, params);
                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }
                        return true;
                    case MotionEvent.ACTION_UP:

                        try{

                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            x2 = event.getX();
                            y2 = event.getY();
                            dx = x2 - x1;
                            dy = y2 - y1;
                            if (clickDuration < MAX_CLICK_DURATION && dx < MAX_CLICK_DISTANCE && dy < MAX_CLICK_DISTANCE) {
                                Log.v(TAG, "Click performed " + isOnClick);
                                InputMethodManager im = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                                im.showInputMethodPicker();
                            }


                        }
                        catch (Exception e){
                            e.printStackTrace();
                        }

                        return true;
                }
                return false;
            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFloatingView != null) windowManager.removeView(mFloatingView);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}
