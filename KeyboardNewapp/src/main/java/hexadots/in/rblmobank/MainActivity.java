package hexadots.in.rblmobank;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.ToggleButton;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import hexadots.in.rblmobank.keyboardbanking.MyShakeMotion;
import hexadots.in.rblmobank.services.FloatingViewService;
import hexadots.in.rblmobank.utils.Constants;
import hexadots.in.rblmobank.utils.RootDetection;
import hexadots.in.rblmobank.utils.RootDetectionChecked;

import static hexadots.in.rblmobank.utils.EncryptionUtil.RandomStringGen;
import static hexadots.in.rblmobank.utils.EncryptionUtil.getDecryptedVal;
import static hexadots.in.rblmobank.utils.EncryptionUtil.getEncryptedString;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "MainActivity";
    String Salt = "E9HZSKfdu1X6AUotpIjbgmtFvUih3Ea4FLtISdHgwxc%3D";
    String publicKey = "KBPAY0611RBL2609";
    private ToggleButton tglBoot, tglStart;
    private static final int OVERLAY_PERMISSION_REQ_CODE = 1000;
    private SharedPreferences sharedpreferences;
    private RelativeLayout rlToggleBoot, rlToggleStart;
    private Context context;

    private static final int REQUEST_SMS = 1;

    private static String[] PERMISSIONS_RBL = {
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_SMS
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        tglBoot = (ToggleButton) findViewById(R.id.toggle_boot_complete);
        tglStart = (ToggleButton) findViewById(R.id.toggle_start);
        rlToggleBoot = (RelativeLayout) findViewById(R.id.rl_switch);
        rlToggleStart = (RelativeLayout) findViewById(R.id.rl_switch_start);
//        checkForRootingDevice();

/*
        askForOverLay();*/
        setSupportActionBar(toolbar);
        startService();
        setAllviews();
        initToggle();

        getUUID();

        String ramKey = RandomStringGen(16);
        Log.v(TAG, "ramKey:" + ramKey);

        Log.v(TAG, "encrypt:" + getEncryptedString(publicKey, "YQs44a8iGwa0U0Jm"));

//        caJNskt8E57Ctw0pkIqXmJU+LCt74fE3A0qxILOLPOo=
        try {
            Log.v(TAG, "decrypt::" + getDecryptedVal(publicKey, "cFIUE7sIsKfodGJoJgk79JU+LCt74fE3A0qxILOLPOo="));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        cFIUE7sIsKfodGJoJgk79JU+LCt74fE3A0qxILOLPOo=
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        verifySMSPermissions();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tglBoot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedpreferences.edit().putBoolean(Constants.KEY_PREF_BOOT, b).apply();
            }
        });

        tglStart.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedpreferences.edit().putBoolean(Constants.KEY_PREF_START, b).apply();
                if (b) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        askForOverLay();
                    } else {
                        startFloatingService();
                    }
                } else {
                    stopFloatingService();
                }
            }
        });
    }




    private void setAllviews() {

        Button keySeetings = (Button) findViewById(R.id.keyset);

        keySeetings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(android.provider.Settings.ACTION_INPUT_METHOD_SETTINGS);
                startActivity(intent);
            }
        });
    }

    public UUID getUUID() {

        UUID uuid;
        final String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        // Use the Android ID unless it's broken, in which case fallback on deviceId,
        // unless it's not available, then fallback on a random number which we store
        // to a prefs file
        try {
            if (!"9774d56d682e549c".equals(androidId)) {
                uuid = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
            } else {
                final String deviceId = ((TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                uuid = deviceId != null ? UUID.nameUUIDFromBytes(deviceId.getBytes("utf8")) : UUID.randomUUID();
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }

        Log.v("uuid", "uuid" + uuid);
        return uuid;
    }

    // Method to start the service
    public void startService() {
        startService(new Intent(MainActivity.this, MyShakeMotion.class));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void startFloatingService() {
        startService(new Intent(MainActivity.this, FloatingViewService.class));
    }

    private void stopFloatingService() {
        stopService(new Intent(getApplication(), FloatingViewService.class));
    }

    private void initToggle() {
        sharedpreferences = this.getSharedPreferences(Constants.KEY_PREF_NAME, Context.MODE_PRIVATE);
        tglBoot.setChecked(sharedpreferences.getBoolean(Constants.KEY_PREF_BOOT, false));
        tglStart.setChecked(sharedpreferences.getBoolean(Constants.KEY_PREF_START, false));
        if (sharedpreferences.getBoolean(Constants.KEY_PREF_START, false)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                askForOverLay();
            } else {
                startFloatingService();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void askForOverLay() {
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
        } else {
            startFloatingService();
        }
    }





    @TargetApi(Build.VERSION_CODES.M)
    private void verifySMSPermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(context, Manifest.permission.RECEIVE_SMS);
        int readSMSPermissions = ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS);
        if (permission != PackageManager.PERMISSION_GRANTED || readSMSPermissions != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(PERMISSIONS_RBL, REQUEST_SMS);
        }else{
            Log.v(TAG,"--- permissions already granted ----");
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (!Settings.canDrawOverlays(this)) {
                // SYSTEM_ALERT_WINDOW permission not granted...
                //HERE we need to hide the floating switch because user didn't give the permission.
                rlToggleBoot.setVisibility(View.GONE);
                rlToggleStart.setVisibility(View.GONE);
            } else {
                startFloatingService();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_SMS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG,"--- permissions  granted for SMS----");
                }else{
                    Log.v(TAG,"--- permissions not provided by user so its not possible to read SMS ----");
                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }


}
