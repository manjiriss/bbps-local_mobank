package hexadots.in.rblmobank.keyboardbanking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent shakeService = new Intent(context, MyShakeMotion.class);
        context.startService(shakeService);
    }
}