package hexadots.in.rblmobank.keyboardbanking;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.view.inputmethod.InputMethodManager;


public class MyShakeMotion extends Service {

    private long lastTiltTime;

    @Override
    public void onCreate() {
        super.onCreate();
        initialize();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void initialize() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        TiltListener tiltListener = new TiltListener(this);

        sensorManager.registerListener(tiltListener,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        tiltListener.setOnTiltListener(new TiltListener.onTiltListener(){

            int count = -1;
            @Override
            public void ontiltDetection() {
                if (System.currentTimeMillis() - lastTiltTime > 220 || count == -1) {
                    if (count == -1) {
                        count = 0;
                    }
                    count++;
                } else {
                    return;
                }
                if ((System.currentTimeMillis() - lastTiltTime > 900) && (count == 2 || count == 3)) {
                    count = 1;
                }

                if (count == 3) {
                    count = 0;
                    lastTiltTime = System.currentTimeMillis() + 1000;
                    if (isAppShown()) {
                        InputMethodManager im = (InputMethodManager) MyShakeMotion.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        im.showInputMethodPicker();
                    }
                } else {
                    lastTiltTime = System.currentTimeMillis();
                }
            }
        });
    }

    public boolean isAppShown() {
        PowerManager powerManager = (PowerManager)
                getSystemService(Context.POWER_SERVICE);
        return powerManager.isScreenOn();
    }

}
