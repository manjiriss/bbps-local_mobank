package hexadots.in.rblmobank.keyboardbanking;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.inputmethodservice.InputMethodService;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.SortedMap;
import java.util.TreeMap;

import hexadots.in.rblmobank.R;
import hexadots.in.rblmobank.adapters.AccountRecyclerAdapter;
import hexadots.in.rblmobank.adapters.OtherBanksListAdapter;
import hexadots.in.rblmobank.adapters.PayeeRecyclerAdapter;
import hexadots.in.rblmobank.beans.TransactionResponse;
import hexadots.in.rblmobank.common.DeviceInfo;
import hexadots.in.rblmobank.interfaces.AccountCommunicator;
import hexadots.in.rblmobank.interfaces.PayeeCommunicator;
import hexadots.in.rblmobank.interfaces.RestclientListener;
import hexadots.in.rblmobank.interfaces.TptCommunicator;
import hexadots.in.rblmobank.models.CustomerAccountDetails;
import hexadots.in.rblmobank.models.GlobalResponse;
import hexadots.in.rblmobank.models.InterTptAcctDetails;
import hexadots.in.rblmobank.models.LoginAccountDetailsType;
import hexadots.in.rblmobank.models.LoginResponse;
import hexadots.in.rblmobank.models.RblTxSessionManager;
import hexadots.in.rblmobank.models.TptAcctDetails;
import hexadots.in.rblmobank.models.TransactionData;
import hexadots.in.rblmobank.networks.DetectNwtworkConnection;
import hexadots.in.rblmobank.utils.Constants;
import hexadots.in.rblmobank.utils.EncryptionUtil;
import hexadots.in.rblmobank.utils.RestClient;
import hexadots.in.rblmobank.utils.URLS;
import hexadots.in.rblmobank.utils.Utils;

import static hexadots.in.rblmobank.models.RblTxSessionManager.TransactionType.rblIFTTransaction;
import static hexadots.in.rblmobank.models.RblTxSessionManager.TransactionType.rblIMPSTransaction;
import static hexadots.in.rblmobank.utils.EncryptionUtil.RandomStringGen;

public class MyKeyboard extends InputMethodService implements View.OnTouchListener, PayeeCommunicator,
        AccountCommunicator, RestclientListener, TptCommunicator {

    private static final String TAG = "MyKeyboard";
    /*Edit Text*/
    private EditText enterAmount;
    /*TextViews & Arrays*/
    private TextView loginButton, errorMessage, submitButton, decimal,
            fromAccount, fromBalance, sendingTo, confirmOTP, reSendOTP, LoadingTv,
            amounterrorMessage,emptyBenficerys;
    private TextView[] numberKeys = new TextView[10];
    private TextView[] Pins = new TextView[6];
    /*Layouts*/
    private RelativeLayout keyboardContentLayout, selectPayeeRelativeLayout, errorContentLayout;
    private RelativeLayout keyboardenterAmountLayout;
    private RelativeLayout keyboardotplayout;
    /*Strings*/
    private String pin = "", amount = "", mode = "", fromAccountNumber = "",
            toName = "", toAccountNumber = "", fromAccountBalance = "", referenceNumber = "";

    /*Array Lists*/
    private List<InterTptAcctDetails> interBankpayeeList = new ArrayList<>();
    private List<TptAcctDetails> otherBankpayeeList = new ArrayList<>();
    private List<LoginAccountDetailsType> accountList = new ArrayList<>();
    private List<CustomerAccountDetails> customerAccountDetailsArrayList = new ArrayList<>();
    /*ImageButton & ImageViews*/
    private ImageButton changeLanguage, backspace, changeAccount;
    private ImageView ReloadBtn;
    /*Int*/
    private int keyboardContentLayoutHeight = 0;
    private final int LOGIN = 1;
    private final int OTP = 2;
    private final int Transaction_IMPS = 3;
    private int IFT_Transaction = 4;
    private int BalaceEnquiry = 5;
    private final int TAB1 = 1;
    private final int TAB2 = 2;
    /*views & Adapters*/
    private Context context;
    private View currentView = null;
    private RecyclerView payeeRecyclerView, changeAccountRecyclerView;
    private PayeeRecyclerAdapter payeeRecyclerAdapter;
    private AccountRecyclerAdapter accountRecyclerAdapter;
    private OtherBanksListAdapter OtherBanksListAdapter;
    private CardView LoadingLayout;
    private ProgressBar Pbar;
    private String ERR_SESSION_EXPIRE = "61191";
    private final static String ERR_AUTHENTICATION_FAIL = "16000";
    private final static String ERR_NUMBER_OF_ATTEMPTS_EXCEED = "19000";
    private final static String ERR_ACCOUNT_LOCKED = "17000";
    /*Objects*/
    private InterTptAcctDetails interBankPayee = null;
    private TptAcctDetails impsBankPayee = null;
    private LoginAccountDetailsType currentAccount = null;
    private LoginResponse loginResponse;
    private TransactionResponse transactionResponse;
    private static final double MINIMUM_AMOUNT = 1;
    private CountDownTimer countDownTimer;
    private String previousAmount;
    private int invalidPinCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        Intent service = new Intent(context, MyShakeMotion.class);
        context.startService(service);
        mode = "LOGIN";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(incomingSMSReceiver);
        pin = "";
        clearPIN();
        interBankPayee = new InterTptAcctDetails();
        impsBankPayee = new TptAcctDetails();
        currentAccount = new LoginAccountDetailsType();
        loginResponse = new LoginResponse();
        transactionResponse = new TransactionResponse();
        interBankpayeeList.clear();
        otherBankpayeeList.clear();
        accountList.clear();
        currentView = null;
        mode = "LOGIN";
    }

    @Override
    public void onStartInputView(EditorInfo info,
                                 boolean restarting) {

        super.onStartInputView(info, true);
        if (currentView == null) {
            setInputView(onCreateInputView());
            return;
        }

        initialiseKeyboardLoginVariables(currentView);

    }

    @Override
    public void onFinishInput() {
        pin = "";
        clearPIN();
        interBankPayee = new InterTptAcctDetails();
        impsBankPayee = new TptAcctDetails();
        currentAccount = new LoginAccountDetailsType();
        loginResponse = new LoginResponse();
        transactionResponse = new TransactionResponse();
        interBankpayeeList.clear();
        otherBankpayeeList.clear();
        accountList.clear();
        currentView = null;
        mode = "LOGIN";
    }

    @Override
    public View onCreateInputView() {

        if (!Utils.checkForRootingDevice(context)) {
            if (Utils.isAppActived(context) && !Utils.isUserLocked(context)) {
                currentView = View.inflate(this, R.layout.keyboard_login, null);
                LoadingLayout = (CardView) currentView.findViewById(R.id.loading_layout);
                Pbar = (ProgressBar) currentView.findViewById(R.id.loading_pbar);
                visibilityType(View.GONE);
                initialiseKeyboardLoginVariables(currentView);
                return currentView;
            } else {
                if (!Utils.isAppActived(context)) {
                    return welcomeScreen("Activate");
                } else {
                    return welcomeScreen("Re-Activate");
                }
            }
        } else {

            return rootScreen();
        }
    }

    public void initialiseKeyboardLoginVariables(final View myView) {
        keyboardContentLayout = (RelativeLayout) myView.findViewById(R.id.keyboardContentLayout);
        loginButton = (TextView) myView.findViewById(R.id.confirmOTPButton);
        //Initially login button is in disable state
        disableLoginOrOTPButton(mode);
        LoadingTv = (TextView) myView.findViewById(R.id.loading_tv);
        errorMessage = (TextView) myView.findViewById(R.id.errorMessage);
        Pins[0] = (TextView) myView.findViewById(R.id.Pin1);
        Pins[1] = (TextView) myView.findViewById(R.id.Pin2);
        Pins[2] = (TextView) myView.findViewById(R.id.Pin3);
        Pins[3] = (TextView) myView.findViewById(R.id.Pin4);
        Pins[4] = (TextView) myView.findViewById(R.id.Pin5);
        Pins[5] = (TextView) myView.findViewById(R.id.Pin6);


        errorMessage.setText("");
        commonInitialisation(myView);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pin.length() == 6) {
                    if (DetectNwtworkConnection.checkInternetConnection(context)) {
                        errorMessage.setText("");
                        visibilityType(View.VISIBLE);
                        disable(keyboardContentLayout);
                        loginRequest(pin);
                    } else {
                        errorMessage.setText(R.string.nointernet);
                    }
                } else {
                    errorMessage.setText(R.string.errorMessage);
                    pin = "";
                    clearPIN();
                    errorMessage.setText(R.string.validerrorMessage);
                }
            }
        });
    }

    private void visibilityType(int visible) {
        try {
            LoadingLayout.setVisibility(visible);
            LoadingTv.setText("Loading...");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void visibilityAutoReadOTP(int visible) {
        try {
            LoadingLayout.setVisibility(visible);
            LoadingTv.setText(" Waiting for OTP to be auto read...");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }

    public void switchToPreviousKeyboard(View v) {
        Utils.switchKeyboard(this);
    }

    private void enable(ViewGroup layout) {
        layout.setEnabled(true);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                enable((ViewGroup) child);
            } else {
                child.setEnabled(true);
            }
        }
    }

    public void enterPin(String pin) {
     /*   Toast.makeText(getApplicationContext(),"login pin called",Toast.LENGTH_SHORT).show();*/

        //Here we first check for PIN length to eanble and disablel login button
        if (TextUtils.isEmpty(pin)) {
            disableLoginOrOTPButton(mode);
        }

        if (pin.length() != 6) {
            disableLoginOrOTPButton(mode);
        } else {
            enableLoginOrOTPButton(mode);
        }

        if (pin.length() == 1) {
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_not_pressed);
        } else if (pin.length() == 2) {
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_not_pressed);
        } else if (pin.length() == 3) {
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_not_pressed);
        } else if (pin.length() == 4) {
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_not_pressed);
        } else if (pin.length() == 5) {
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_not_pressed);
        } else if (pin.length() == 6) {
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_pressed);
            if (mode == "LOGIN") {
                if (DetectNwtworkConnection.checkInternetConnection(context)) {
                    errorMessage.setText("");
                    visibilityType(View.VISIBLE);
                    disable(keyboardContentLayout);
                    loginRequest(pin);
                } else {
                    errorMessage.setText(R.string.nointernet);
                }
            }
        } else {
            Pins[0].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_not_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_not_pressed);
        }

    }

    private void enableLoginOrOTPButton(String mode) {
        if (mode.equalsIgnoreCase("LOGIN")) {
            Log.v(TAG, "------ Enabled Login Button called ------");
            loginButton.setEnabled(true);
            loginButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_state));
        } else if (mode.equalsIgnoreCase("SEND OTP")) {
            confirmOTP.setEnabled(true);
            confirmOTP.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_state));
        }
    }

    private void disableLoginOrOTPButton(String mode) {
        if (mode.equalsIgnoreCase("LOGIN")) {
            loginButton.setEnabled(false);
            loginButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_disable));
        } else if (mode.equalsIgnoreCase("SEND OTP")) {
            confirmOTP.setEnabled(false);
            confirmOTP.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_disable));
        }
    }

    private void disableResendOTP() {
        reSendOTP.setEnabled(false);
        reSendOTP.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_disable));
    }

    private void enableResendOTP() {
        reSendOTP.setEnabled(true);
        reSendOTP.setBackgroundDrawable(getResources().getDrawable(R.drawable.login_button_state));
    }

    /**
     * This function is used to start count down timer for 60S
     */
    private void startResendOTPTimer() {
        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long l) {
            }

            @Override
            public void onFinish() {
                //After finish 60s enable resend OTP
                enableResendOTP();
            }
        };
        countDownTimer.start();
    }

    public void enterAmount(String amount) {
        enterAmount.setText(amount);
    }

    public void setPin(View v)
    {
//        switch (v.getId()) {
        if (v.getId() == R.id.numberKey0)
        {
            pin = pin + "0";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey1)
        {
            pin = pin + "1";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey2)
        {
            pin = pin + "2";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey3)
        {
            pin = pin + "3";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey4)
        {
            pin = pin + "4";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey5)
        {
            pin = pin + "5";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey6)
        {
            pin = pin + "6";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey7)
        {
            pin = pin + "7";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey8)
        {
            pin = pin + "8";
            enterPin(pin);
        } else if (v.getId() == R.id.numberKey9)
        {
            pin = pin + "9";
            enterPin(pin);
        } else if (v.getId() == R.id.backspace)
        {
            errorMessage.setText("");
            if (pin.length() != 0)
            {
                pin = pin.substring(0, (pin.length() - 1));
            }
            enterPin(pin);
        }

//        }
    }

    public void setAmount(View v)
    {
//        switch (v.getId()) {
        if (v.getId() == R.id.numberKey0)
        {
            if (amount.length() > 0)
                amount = amount + "0";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey1)
        {
            amount = amount + "1";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey2)
        {
            amount = amount + "2";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey3)
        {
            amount = amount + "3";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey4)
        {
            amount = amount + "4";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey5)
        {
            amount = amount + "5";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey6)
        {
            amount = amount + "6";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey7)
        {
            amount = amount + "7";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey8)
        {
            amount = amount + "8";
            enterAmount(amount);
        } else if (v.getId() == R.id.numberKey9)
        {
            amount = amount + "9";
            enterAmount(amount);
        } else if (v.getId() == R.id.backspace)
        {

            if (amount.length() > 0)
            {
                amount = amount.substring(0, (amount.length() - 1));
                enterAmount(amount);
            }
        } else
        {
            enterAmount.setText("");
        }
//        }
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            if (mode.equals("LOGIN") || mode.equals("SEND OTP")) {
                if (pin.length() < 6 || (v.getId() == R.id.backspace))
                    setPin(v);
            } else if (mode.equals("TRANSFER")) {
                setAmount(v);
            } else if (mode.equals("OTHERTRANSFER")) {
                setAmount(v);
            }
        }
        return false;
    }

    private int selectedTab = TAB1;

    public void openSelectPayee() {

        mode = "SELECT PAYEE";
        amount = "";
        errorMessage.setText("");
        LayoutInflater inflater = LayoutInflater.from(context);
        final View inflatedLayoutView = inflater.inflate(R.layout.keyboard_select_payee, null, false);
        final TextView otherBanksPayeeButton = (TextView) inflatedLayoutView.findViewById(R.id.otherPayee);
        final TextView PayeeButton = (TextView) inflatedLayoutView.findViewById(R.id.fundTransferLabel);
        selectTab(selectedTab, inflatedLayoutView);
        otherBanksPayeeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTab(TAB2, inflatedLayoutView);
            }
        });
        PayeeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTab(TAB1, inflatedLayoutView);
            }
        });
    }

    private void selectTab(int index, View inflatedLayoutView) {
        final TextView otherBanksPayeeButton = (TextView) inflatedLayoutView.findViewById(R.id.otherPayee);
        final TextView PayeeButton = (TextView) inflatedLayoutView.findViewById(R.id.fundTransferLabel);
        if (index == TAB1) {
            PayeeButton.setSelected(true);
            otherBanksPayeeButton.setSelected(false);
            selectedTab = TAB1;
        } else if (index == TAB2) {
            otherBanksPayeeButton.setSelected(true);
            PayeeButton.setSelected(false);
            selectedTab = TAB2;
        }

        if (keyboardContentLayout.getHeight() != 0)
            keyboardContentLayoutHeight = keyboardContentLayout.getHeight();
        keyboardContentLayout.removeAllViews();
        keyboardContentLayout.addView(inflatedLayoutView);
        selectPayee(inflatedLayoutView);
    }

    public void sendOTP() {
        visibilityType(View.GONE);
        mode = "SEND OTP";
        LayoutInflater inflater = LayoutInflater.from(context);
        View myView = inflater.inflate(R.layout.keyboard_send_otp, null, false);
        keyboardContentLayoutHeight = keyboardContentLayout.getHeight();
        RelativeLayout otpview = (RelativeLayout) myView.findViewById(R.id.keyboardotplayout);
        keyboardContentLayout.removeAllViews();
        keyboardContentLayout.addView(otpview);
        reSendOTP = (TextView) myView.findViewById(R.id.sendOTP);
        visibilityAutoReadOTP(View.VISIBLE);
        //Intially disable the resend OTP butoon
        disableResendOTP();
        // Start the timer for 60S
        startResendOTPTimer();
        LocalBroadcastManager.getInstance(this).registerReceiver(incomingSMSReceiver,
                new IntentFilter(Constants.KEY_LOCAL_SMS_RECEIVER));
        keyboardotplayout = (RelativeLayout) myView.findViewById(R.id.keyboardotplayout);
        confirmOTP = (TextView) myView.findViewById(R.id.confirmOTPButton);
        disableLoginOrOTPButton(mode);
        errorMessage = (TextView) myView.findViewById(R.id.errorMessage);
        Button otpBackButton = (Button) myView.findViewById(R.id.otp_back);
        Pins[0] = (TextView) myView.findViewById(R.id.Pin1);
        Pins[1] = (TextView) myView.findViewById(R.id.Pin2);
        Pins[2] = (TextView) myView.findViewById(R.id.Pin3);
        Pins[3] = (TextView) myView.findViewById(R.id.Pin4);
        Pins[4] = (TextView) myView.findViewById(R.id.Pin5);
        Pins[5] = (TextView) myView.findViewById(R.id.Pin6);
        errorMessage.setText("");
        commonInitialisation(myView);
        otpBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin = "";
                openSelectPayee();
            }
        });
        confirmOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (pin.length() == 6) {
                    if (DetectNwtworkConnection.checkInternetConnection(context)) {
                        errorMessage.setText("");
                        RblTxSessionManager.getInstance().getTransactionData().setOtp(pin);
                        visibilityType(View.VISIBLE);
                        disable(keyboardotplayout);
                        if (RblTxSessionManager.getInstance().getTransactionType().equals(rblIFTTransaction))
                            InterBankTransactionRequest();
                        else
                            impsTransactionRequest();
                    } else {
                        errorMessage.setText(getString(R.string.nointernet));
                    }
                } else {
                    pin = "";
                    enable(keyboardotplayout);
                    errorMessage.setText(getString(R.string.invalid_otp));
                    clearPIN();
                    errorMessage.setText(getString(R.string.valid_otp));

                }
            }
        });
        reSendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (DetectNwtworkConnection.checkInternetConnection(context)) {
                    RblTxSessionManager.getInstance().getTransactionData().setOtp(pin);
                    visibilityType(View.VISIBLE);
                    disable(keyboardotplayout);
                    clearPIN();
                    OTPRequest();
                } else {
                    errorMessage.setText(getString(R.string.nointernet));
                }
            }
        });
    }

    public void success(TransactionResponse response) {
        visibilityType(View.GONE);
        mode = "SUCCESS";
        LayoutInflater inflater = LayoutInflater.from(context);
        View myView = inflater.inflate(R.layout.keyboard_success, null, false);
        if (resetheight) {
            keyboardContentLayoutHeight = keyboardContentLayout.getHeight();
        }

        String accountNumberLastDigits = toAccountNumber.substring(toAccountNumber.length()-4);
        final String successMessage = "Hi, I have transferred INR " + Double.parseDouble(amount) + " to your account XXXX"+ accountNumberLastDigits+ " – " + toName + " on date " + Utils.getCurrentDate() +" through RBL ChatPay Reference Number - " + referenceNumber;
        RelativeLayout successLayout = (RelativeLayout) myView.findViewById(R.id.successLayout);
        keyboardContentLayout.removeAllViews();
        keyboardContentLayout.addView(myView);
        Button notify = (Button) myView.findViewById(R.id.notify);
        Button home = (Button) myView.findViewById(R.id.home);
        TextView referenceID = (TextView) myView.findViewById(R.id.referenceID);
        TextView paidTo = (TextView) myView.findViewById(R.id.paidTo);
        TextView paidAmount = (TextView) myView.findViewById(R.id.paidAmount);
        TextView fromAccount = (TextView) myView.findViewById(R.id.fromAccountval);
        paidTo.setText(toName);
        paidAmount.setText(getString(R.string.str_rupeesymbol) + amount);
        fromAccount.setText(response.getFromacctno());
        referenceID.setText(response.getTxnrefno());
        if (resetheight) {
            ViewGroup.LayoutParams params = successLayout.getLayoutParams();
            params.height = keyboardContentLayoutHeight;
            successLayout.setLayoutParams(params);
        }
        resetheight = true;
        notify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    InputConnection ic = getCurrentInputConnection();
                    if (ic != null) {
                        ic.deleteSurroundingText(successMessage.length(), successMessage.length());
                        ic.commitText(successMessage, successMessage.length());
                    }
                    Utils.switchKeyboard(MyKeyboard.this);
                    pin = "";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibilityType(View.VISIBLE);
                BalanceEnquiryRequest();
                pin = "";
                clearPIN();
            }
        });
    }

    public void selectPayee(View myView) {
        selectPayeeRelativeLayout = (RelativeLayout) myView.findViewById(R.id.select_payee_relative_layout);
        payeeRecyclerView = (RecyclerView) myView.findViewById(R.id.select_payee_recycler_view);
        emptyBenficerys=(TextView)myView.findViewById(R.id.emptyBenficers);
        if (selectedTab == TAB1) {

            if (interBankpayeeList.size() > 0 ) {
                payeeRecyclerView.setVisibility(View.VISIBLE);
                emptyBenficerys.setVisibility(View.INVISIBLE);
                payeeRecyclerAdapter = new PayeeRecyclerAdapter(context, interBankpayeeList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                payeeRecyclerView.setLayoutManager(mLayoutManager);
                payeeRecyclerView.setAdapter(payeeRecyclerAdapter);
            }
            else {
                payeeRecyclerView.setVisibility(View.INVISIBLE);
                emptyBenficerys.setVisibility(View.VISIBLE);
                emptyBenficerys.setText("No Payees found, Kindly register payee before doing Fund Transfer");
            }

        } else if (selectedTab == TAB2) {

            if (otherBankpayeeList.size() > 0)
            {
                emptyBenficerys.setVisibility(View.INVISIBLE);
                payeeRecyclerView.setVisibility(View.VISIBLE);
                OtherBanksListAdapter = new OtherBanksListAdapter(context, otherBankpayeeList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                payeeRecyclerView.setLayoutManager(mLayoutManager);
                payeeRecyclerView.setAdapter(OtherBanksListAdapter);
            }
            else {
                payeeRecyclerView.setVisibility(View.INVISIBLE);
                emptyBenficerys.setVisibility(View.VISIBLE);
                emptyBenficerys.setText("No Payees found, Kindly register payee before doing Fund Transfer");
            }


        }
        ViewGroup.LayoutParams params = selectPayeeRelativeLayout.getLayoutParams();
        params.height = keyboardContentLayoutHeight;
        selectPayeeRelativeLayout.setLayoutParams(params);
        // if (loginResponse.getAccount_Summary() != null) {
        String custName = loginResponse.getCustName();
        ((TextView) selectPayeeRelativeLayout.findViewById(R.id.confirmOTPButton)).setText(getResources().getString(R.string.greet_title) + " " + custName);
        // }
    }


    public void commonInitialisation(View myView) {
        numberKeys[0] = (TextView) myView.findViewById(R.id.numberKey0);
        numberKeys[1] = (TextView) myView.findViewById(R.id.numberKey1);
        numberKeys[2] = (TextView) myView.findViewById(R.id.numberKey2);
        numberKeys[3] = (TextView) myView.findViewById(R.id.numberKey3);
        numberKeys[4] = (TextView) myView.findViewById(R.id.numberKey4);
        numberKeys[5] = (TextView) myView.findViewById(R.id.numberKey5);
        numberKeys[6] = (TextView) myView.findViewById(R.id.numberKey6);
        numberKeys[7] = (TextView) myView.findViewById(R.id.numberKey7);
        numberKeys[8] = (TextView) myView.findViewById(R.id.numberKey8);
        numberKeys[9] = (TextView) myView.findViewById(R.id.numberKey9);
        changeLanguage = (ImageButton) myView.findViewById(R.id.changeLanguage);
        backspace = (ImageButton) myView.findViewById(R.id.backspace);
        for (int i = 0; i <= 9; i++) {
            numberKeys[i].setOnTouchListener(this);
        }
        backspace.setOnTouchListener(this);
    }

    public void initialiseEnterAmountVariables(View myView) {
        commonInitialisation(myView);
        sendingTo = (TextView) myView.findViewById(R.id.sendingTo);
        keyboardenterAmountLayout = (RelativeLayout) myView.findViewById(R.id.keyboardenterAmountLayout);
        enterAmount = (EditText) myView.findViewById(R.id.enterAmount);
        if (!TextUtils.isEmpty(previousAmount)) {
            enterAmount.setText(previousAmount);
        }
        submitButton = (TextView) myView.findViewById(R.id.submitButton);
        fromAccount = (TextView) myView.findViewById(R.id.fromAccount);
        fromBalance = (TextView) myView.findViewById(R.id.fromAccountBalance);
        amounterrorMessage = (TextView) myView.findViewById(R.id.amounterrorMessage);
        decimal = (TextView) myView.findViewById(R.id.decimal);
        changeAccount = (ImageButton) myView.findViewById(R.id.changeAccount);
        TextView backButotn = (TextView) myView.findViewById(R.id.back);
        changeAccountRecyclerView = (RecyclerView) myView.findViewById(R.id.change_account_recycler_view);
        accountRecyclerAdapter = new AccountRecyclerAdapter(context, accountList, changeAccount);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        changeAccountRecyclerView.setLayoutManager(mLayoutManager);
        changeAccountRecyclerView.setAdapter(accountRecyclerAdapter);
        changeAccountRecyclerView.setVisibility(View.INVISIBLE);
        decimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!amount.contains(".")) {
                    if (amount.length() == 0)
                        amount = "0.";
                    else
                        amount = amount + ".";
                }
                enterAmount.setText(amount);
            }
        });
        changeAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (changeAccountRecyclerView.getVisibility() == View.INVISIBLE) {
                    changeAccountRecyclerView.setVisibility(View.VISIBLE);
                    changeAccount.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.button_state_pressed));
                } else {
                    changeAccount.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.button_state_not_pressed));
                    changeAccountRecyclerView.setVisibility(View.INVISIBLE);
                }
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateEnteredAmount(amount,MINIMUM_AMOUNT,amounterrorMessage)){
                    if (amount.charAt(amount.length() - 1) == '.') {
                        amount = amount.replace(".", "");
                    }
                    RblTxSessionManager.getInstance().getTransactionData().setAmount(amount);
                    if (fromAccountNumber != null && !fromAccountNumber.equals("") && fromAccountBalance != null && !fromAccountBalance.equals("")) {
                        if (amountValidation(amount, fromAccountBalance)) {
                            if (DetectNwtworkConnection.checkInternetConnection(context)) {
                                amounterrorMessage.setText("");
                                RblTxSessionManager.getInstance().getTransactionData().setFromAccount(fromAccountNumber);
                                OTPRequest();
                            } else {
                                amounterrorMessage.setText(R.string.nointernet);
                            }
                        } else {
                            amounterrorMessage.setText(getString(R.string.amount_validation));
                        }
                    } else {
                        if (amountValidation(amount, getAccbalance(fromAccountNumber))) {
                            if (DetectNwtworkConnection.checkInternetConnection(context)) {
                                amounterrorMessage.setText("");
                                RblTxSessionManager.getInstance().getTransactionData().setFromAccount(getAccNumber());
                                OTPRequest();
                            } else {
                                amounterrorMessage.setText(R.string.nointernet);
                            }
                        } else {
                            amounterrorMessage.setText(getString(R.string.amount_validation));
                        }
                    }
                }

            }
        });
        backButotn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSelectPayee();
            }
        });
    }

    private boolean validateEnteredAmount(String amount, double minAmount,  TextView errorView) {
        //Check amount is empty or not
        if (TextUtils.isEmpty(amount)) {
            errorView.setText("Please enter valid Amount");
            return false;

        }
        //Check the enter amount is less than min amount.
        if (Double.valueOf(amount) < minAmount) {
            errorView.setText("Amount should not be less than Rs " + minAmount);
          /*  disableSubmit();*/
            return false ;
        }
        //Check the enter amount is grater than  grater amount.
      /*  if (Double.valueOf(amount) > maxAmount) {
            errorView.setText("Amount should not be exceed Rs " + maxAmount);
         *//*   disableSubmit();*//*
            return false;
        }*/
        //If all satisfy then we make the error text as empty.
        errorView.setText("");
        return true;
      /*  enableSubmit();*/

    }

    private void disableSubmit() {
        submitButton.setEnabled(false);
        submitButton.setBackgroundColor(Color.parseColor("#BDBDBD"));
    }

    private void enableSubmit() {
        submitButton.setEnabled(true);
        submitButton.setBackgroundColor(Color.parseColor("#1e2358"));
    }


    private boolean amountValidation(String amount, String fromAccountBalance) {
        boolean flag = false;
        if (Double.valueOf(amount) <= Double.valueOf(fromAccountBalance)) {
            flag = true;
        }
        return flag;
    }

    public void openSelectFromAccount(LoginAccountDetailsType account) {
        changeAccountRecyclerView.setVisibility(View.INVISIBLE);
        fromAccountNumber = account.getAcctno();
        fromAccountBalance = account.getAcctbalance();
        fromAccount.setText(getString(R.string.fromaccount) + " " + fromAccountNumber);
        NumberFormat fmt = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
        Double balanceFormate = Double.parseDouble(fromAccountBalance);
        String balance = fmt.format(balanceFormate);
        RblTxSessionManager.getInstance().getTransactionData().setSenderName(getSenderName(fromAccountNumber));
        fromBalance.setText(getString(R.string.balance) + " " + balance);
    }

    /*Call Backs Start*/
    @Override
    public void callBack(InterTptAcctDetails payee) {
        interBankPayee = payee;
        RblTxSessionManager.getInstance().setTransactionType(rblIFTTransaction);
        interBankPayeeDetails(interBankPayee);
    }

    @Override
    public void callBack(TptAcctDetails payee) {
        impsBankPayee = payee;
        RblTxSessionManager.getInstance().setTransactionType(rblIMPSTransaction);
        impsUserSelectedAccount(impsBankPayee);
    }

    @Override
    public void callBack(LoginAccountDetailsType account) {
        currentAccount = account;
        openSelectFromAccount(account);
    }

    /*Call Backs END*/

    private boolean resetheight = true;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mode.equals("LOGIN")) {
          /*  Toast.makeText(getApplicationContext(), "login pin called", Toast.LENGTH_SHORT).show();*/
            enterPin(pin);

        } else if (mode.equals("SELECT PAYEE")) {
            openSelectPayee();
        } else if (mode.equals("TRANSFER")) {
            if (enterAmount != null)
                previousAmount = enterAmount.getText().toString();
            openSelectPayee();
            interBankPayeeDetails(interBankPayee);
        } else if (mode.equals("OTHERTRANSFER")) {
            if (enterAmount != null)
                previousAmount = enterAmount.getText().toString();
            openSelectPayee();
            impsUserSelectedAccount(impsBankPayee);
        } else if (mode.equals("SEND OTP")) {
            sendOTP();
            enterPin(pin);
        } else if (mode.equals("SUCCESS")) {
          /*  enable(keyboardContentLayout);*/
            resetheight = false;
            success(transactionResponse);
        } else if (mode.equalsIgnoreCase("FAILURE")) {
            failureResponse("-1100", "We are having difficulties processing your request. Please try again.");
            disable(keyboardContentLayout);
        }
    }


    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (Integer.parseInt(Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {


            pin = "";
            interBankPayee = new InterTptAcctDetails();
            impsBankPayee = new TptAcctDetails();
            currentAccount = new LoginAccountDetailsType();
            loginResponse = new LoginResponse();
            transactionResponse = new TransactionResponse();
            switchToPreviousKeyboard(null);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
//    public  boolean isApplicationSentToBackground(final Context context) {
//        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
//        if (!tasks.isEmpty()) {
//            ComponentName topActivity = tasks.get(0).topActivity;
//            if (!topActivity.getPackageName().equals(context.getPackageName())) {
//
//                return true;
//            }
//        }
//
//        return false;
//    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }
//        Toast.makeText(context, "test", Toast.LENGTH_SHORT).show();
//        pin="";
        return isInBackground;
    }

    private void printForegroundTask() {
        String currentApp = "NULL";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager usm = (UsageStatsManager) this.getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                }
            }
        } else {
            ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
            currentApp = tasks.get(0).processName;
        }

    }

    @Override
    public void onRestfulResponse(int reqID, String response, boolean connected) {
        /*mani teja*/
        if (!connected) {
//            String response1 = "{\"OPERATIONID\": \"IFTTransfer\",\"OutString\": \"nUvnoPle1sru/Y/dqte+F3J7ZbFW3B6z1N9gMEPbd//NBHYvYKtqfaU+lkhCY2sVZPgg3EJ0cMNWX0eJ0iiJIR7EoSCVqHM1tUyxrb7NpfnIsMtq9BosW6v8UDlJmmX+EzZaKdAPFD1OzpQdTrOD6/RJsxsb6DE/mM8JXDb+gQBGf+ZJzzYG/kGOPbuxqXg0aMZfjrvcY+jcbv5gEjoGBY6oFJTbHUvHYENRoG/7txqvGLBIx8iFHQGp7vBeG5resA+EWfH6lS8M+iFtY7okGKgO+flaRD3NdBxPgEsi4s3Nou/If31XNaYZ9qGdHfUKB4LYXUawTY9GaJtbehxcv+fT9ImhG6tg2afa+OtErGlap68dBjxO6CXFdzGTL9lz65olAowJkFx4zXWEigjz9TwQb/McLN645gX7U7LYwcMzoqRfpPVz1klURvz2L97TtJn5612xN820cm+nxTbHIvYjCEHu4+jLG710jUxfUyQHJK4C9EFvFdYt60l3ToeFdiBooufRRi3Ejf5nfE/6QQOeBQV/NppVyN54yUffOzYiqoLuKffZZnY8h+Eboq1PV4wyuee8vViLcKdHcgNs+RSYVIe3yTeXYfh9wqmBQHYiXsOF9m6Se1WRBWwhCprhD5WXaFoGMC12LXi8Lwf42XAns2Tn8r+fGcN8qLoCFkqssdToliMOeSkdtrD/zF4wpoajLxI1pHo30LrKM5s5nEajT2friLPIFWNPPle4RtnD5sDAioqBmP72el5emv2WJUqk7mLREzLoYJIeu8fpvreXxoiSN81K3p7s8cbwmxbmXm+GZRNqrMX4/3Zxuek/8JpfzXANpqAcvDP6CoOyH6O6JY3HlhJneQjnFUzRGn68JCOCN8OHrklgRCj8wUAWasJ43tRBocZktroyAzEwiJSnhOey/VPRJhhKbDkF3efC/NaY6cXtPv+FbTyTCkNuFYNiMET+8OIan/mPOAXGQRY90rFcKArztZauuvctP1dLsD1WUldEGCZqtQJx5dQ7SqWTGW0cw0WkI2kIsmQazxK0A8ZfrdZdq+6DycB6/q99a1nDx5GPklnNWpQn5s+1KDaxdRQG5shrlxEPKtr1P1iBkRQVsfcJ+t0vGKnaTbhusODAIq1/V4Nuef0CVfD8zPkGmifm8qhN7ZmxFVufxGsrS/2DuRZy7yCG+y4x1UCRm93sZikJyab/QsdjQYC/Ru87n7eT0Nti0iLJfdEyZUzMVDjNFewyX2jFG1LuBCPpGNCtq5G5I4cO2xalOt3M\",\"Salt\": \"ArFWgQ3dGszcUvh12JuqxWtFvUih3Ea4FLtISdHgwxc=\",\"Code\": \"0\",\"Message\": \"SUCCESS\",\"Severity\": \"SUCCESS\"}";
//            getIFTResponseObject(response1);
            failureResponse("-1", "We are having difficulties processing your request. Please try again.");
            visibilityType(View.GONE);
            return;
        }
        try {
            if (reqID == LOGIN) {
                getLoginResponseObject(response);
                pin = "";
            } else if (reqID == OTP) {
                getOTPResponseObject(response);
            } else if (reqID == Transaction_IMPS) {
                getIMPSResponseObject(response);

            } else if (reqID == IFT_Transaction) {
                getIFTResponseObject(response);

            } else if (reqID == BalaceEnquiry) {
                getBalanceEnquiryResponse(response);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*Request Data*/

    /*LOGIN REQUEST*/
    private void loginRequest(String pwdPin) {
        String ranGenratedKey = RandomStringGen(16);
        String password = Base64.encodeToString(pwdPin.getBytes(), Base64.DEFAULT).trim();
        String urlString = String.format(URLS.loginUrl, DeviceInfo.getUUID(context),
                DeviceInfo.getTabletType(context),
                "USRLGNREQ", Constants.deviceFamily,
                Constants.sequence, Constants.androidVersion, password);
        String request = EncryptionUtil.getEncryptedString(ranGenratedKey, urlString);
        JSONObject loginObjRequest = new JSONObject();
        try {
            loginObjRequest.put("Checksum", EncryptionUtil.getCheckSum(urlString));
            loginObjRequest.put("OPERATIONID", Constants.operationId);
            loginObjRequest.put("Salt", EncryptionUtil.getEncryptedString(Constants.publicKey, ranGenratedKey));
            loginObjRequest.put("InputString", request);
            loginObjRequest.put("SessionKey", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RestClient client = new RestClient(this, LOGIN, loginObjRequest.toString(), context);
        client.execute();
    }

    /*OTP REQUEST*/
    public void OTPRequest() {
        visibilityType(View.VISIBLE);
        disable(keyboardenterAmountLayout);
        String ranGenratedKey = RandomStringGen(16);
        String urlString = String.format(URLS.generateOTPUrl, loginResponse.getSessionKey(),
                loginResponse.getCustomerid(), DeviceInfo.getTabletType(context),
                "OTPGENREQ", Constants.deviceFamily,
                Constants.sequence, Constants.androidVersion);
        try {
            String request = EncryptionUtil.getEncryptedString(ranGenratedKey, urlString);
            JSONObject loginObjRequest = new JSONObject();
            loginObjRequest.put("Checksum", EncryptionUtil.getCheckSum(urlString));
            loginObjRequest.put("OPERATIONID", Constants.otpOperationId);
            loginObjRequest.put("Salt", EncryptionUtil.getEncryptedString(Constants.publicKey, ranGenratedKey));
            loginObjRequest.put("InputString", request);
            loginObjRequest.put("SessionKey", EncryptionUtil.getEncryptedString(Constants.publicKey, loginResponse.getSessionKey()));
            RestClient client = new RestClient(this, OTP, loginObjRequest.toString(), context);
            client.execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /*IMPS-FUNDTRANSFER*/
    private void impsTransactionRequest() {
        String ranGenratedKey = RandomStringGen(16);
        TransactionData globalSelection = RblTxSessionManager.getInstance().getTransactionData();
        String urlString = String.format(URLS.IMPSTransferUrl, "IMPS-FUNDTRANSFER",
                globalSelection.getIfscCode(), loginResponse.getSessionKey(),
                globalSelection.getOtpRefNumber(), globalSelection.getToAccount(), "IMPSP2AFT", globalSelection.getBankName(),
                Constants.deviceFamily, globalSelection.getFromAccount(), "", globalSelection.getSenderName(), "", loginResponse.getCustomerid(),
                DeviceInfo.getTabletType(context), globalSelection.getBenyName(), Constants.sequence, globalSelection.getOtp(), globalSelection.getAmount(), Constants.otpRequest, Constants.androidVersion);
        String request = EncryptionUtil.getEncryptedString(ranGenratedKey, urlString);
        JSONObject impsReqObject = new JSONObject();
        try {
            impsReqObject.put("Checksum", EncryptionUtil.getCheckSum(urlString));
            impsReqObject.put("OPERATIONID", Constants.iMPSOperationId);
            impsReqObject.put("Salt", EncryptionUtil.getEncryptedString(Constants.publicKey, ranGenratedKey));
            impsReqObject.put("InputString", request);
            impsReqObject.put("SessionKey", EncryptionUtil.getEncryptedString(Constants.publicKey, loginResponse.getSessionKey()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RestClient client = new RestClient(this, Transaction_IMPS, impsReqObject.toString(), context);
        client.execute();

    }

    /*IFT-FUNDTRANSFER*/
    private void InterBankTransactionRequest() {
        String ranGenratedKey = RandomStringGen(16);
        String fromAccount = RblTxSessionManager.getInstance().getTransactionData().getFromAccount();
        String toAccount = RblTxSessionManager.getInstance().getTransactionData().getToAccount();
        String amount = RblTxSessionManager.getInstance().getTransactionData().getAmount();
        String otp = RblTxSessionManager.getInstance().getTransactionData().getOtp();
        String otpRef = RblTxSessionManager.getInstance().getTransactionData().getOtpRefNumber();
        String senderName = RblTxSessionManager.getInstance().getTransactionData().getSenderName();
        String receiverName = RblTxSessionManager.getInstance().getTransactionData().getBenyName();
        String urlString = String.format(URLS.TransferWithinBankUrl, loginResponse.getCustomerid(),
                Constants.deviceFamily, DeviceInfo.getTabletType(context),
                Constants.iftTransferId, Constants.androidVersion, loginResponse.getSessionKey(),
                fromAccount, toAccount, amount, Constants.sequence,
                "TPTFNDTFR", otp, otpRef, Constants.otpRequest, senderName, receiverName);
        String request = EncryptionUtil.getEncryptedString(ranGenratedKey, urlString);
        JSONObject interbankReqObject = new JSONObject();
        try {
            interbankReqObject.put("Checksum", EncryptionUtil.getCheckSum(urlString));
            interbankReqObject.put("OPERATIONID", Constants.iftTransferId2);
            interbankReqObject.put("Salt", EncryptionUtil.getEncryptedString(Constants.publicKey, ranGenratedKey));
            interbankReqObject.put("InputString", request);
            interbankReqObject.put("SessionKey", EncryptionUtil.getEncryptedString(Constants.publicKey, loginResponse.getSessionKey()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RestClient client = new RestClient(this, IFT_Transaction, interbankReqObject.toString(), context);
        client.execute();

    }

    /*Balance Enquiry*/
    private void BalanceEnquiryRequest() {
        String ranGenratedKey = RandomStringGen(16);

        String urlString = String.format(URLS.BalanceEnquirykUrl, loginResponse.getSessionKey(),
                loginResponse.getCustomerid(), DeviceInfo.getTabletType(context),
                "ACCSUMINQ", Constants.deviceFamily, Constants.sequence
                , Constants.androidVersion);
        String request = EncryptionUtil.getEncryptedString(ranGenratedKey, urlString);
        JSONObject balanceEnquiryReqObject = new JSONObject();
        try {
            balanceEnquiryReqObject.put("Checksum", EncryptionUtil.getCheckSum(urlString));
            balanceEnquiryReqObject.put("OPERATIONID", Constants.balanceEnuiryId);
            balanceEnquiryReqObject.put("Salt", EncryptionUtil.getEncryptedString(Constants.publicKey, ranGenratedKey));
            balanceEnquiryReqObject.put("InputString", request);
            balanceEnquiryReqObject.put("SessionKey", EncryptionUtil.getEncryptedString(Constants.publicKey, loginResponse.getSessionKey()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RestClient client = new RestClient(this, BalaceEnquiry, balanceEnquiryReqObject.toString(), context);
        client.execute();

    }

    /*Inter bank selected user Details*/
    public void interBankPayeeDetails(InterTptAcctDetails payee) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View inflatedLayoutView = inflater.inflate(R.layout.keyboard_enter_amount, null, false);
        mode = "TRANSFER";

        TransactionData transactionData = RblTxSessionManager.getInstance().getTransactionData();
        selectPayeeRelativeLayout.removeAllViews();
        selectPayeeRelativeLayout.addView(inflatedLayoutView);

        fromAccountNumber = getAccNumber();
        toName = payee.getBenefname();
        toAccountNumber = payee.getAcctno();
        initialiseEnterAmountVariables(inflatedLayoutView);
        sendingTo.setText("To " + payee.getBenefname());
        TextView fromAccountView = (TextView) inflatedLayoutView.findViewById(R.id.fromAccount);
        TextView fromAccountBal = (TextView) inflatedLayoutView.findViewById(R.id.fromAccountBalance);
        fromAccountView.setText(getString(R.string.fromaccount) + " " +fromAccountNumber);//customer acc number
        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
        Double balanceFormate = Double.parseDouble(getAccbalance(fromAccountNumber));
        String balance = format.format(balanceFormate);
        fromAccountBal.setText(getString(R.string.balance) + " " + balance);
        transactionData.setDescription("IFT-FUNDTRANSFER");
        transactionData.setToAccount(payee.getAcctno()); /*selected account number*/
        transactionData.setBenyName(payee.getBenefname());/*selected benifacry name*/
        transactionData.setSenderName(getSenderName(fromAccountNumber));
    }

    private String getSenderName(String accountNumber) {
        String senderName = "";
        if (accountList != null && accountList.size() > 0) {
            for (int i = 0; i < accountList.size(); i++) {
                if (accountNumber.equals(customerAccountDetailsArrayList.get(i).getAcctno())) {
                    senderName = customerAccountDetailsArrayList.get(i).getCustName();
                }
            }
        }
        return senderName;
    }

    private String getAccbalance(String accountNumber) {
        String Accbalance = "";
        if (accountList != null && accountList.size() > 0) {
            for (int i = 0; i < accountList.size(); i++) {
                if (accountList.get(i).getAcctno().equals(accountNumber)) {
                    Accbalance = accountList.get(i).getAcctbalance();
                }
            }
        }
        return Accbalance;
    }


    private String getAccNumber() {
        String Accnumber = "";
        if (accountList != null && accountList.size() > 0) {
            LoginAccountDetailsType defaultAccount = accountList.get(0);
            Accnumber = defaultAccount.getAcctno();
    }
        return Accnumber;
    }

    /*imps bank payees*/
    public void impsUserSelectedAccount(TptAcctDetails payee) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View inflatedLayoutView = inflater.inflate(R.layout.keyboard_enter_amount, null, false);
        mode = "OTHERTRANSFER";
        selectPayeeRelativeLayout.removeAllViews();
        selectPayeeRelativeLayout.addView(inflatedLayoutView);
        toName = payee.getBenefname();
        fromAccountNumber = getAccNumber();
        toAccountNumber = payee.getAcctno();
        initialiseEnterAmountVariables(inflatedLayoutView);
        sendingTo.setText("To " + payee.getBenefname());
        TransactionData transactionData = RblTxSessionManager.getInstance().getTransactionData();
        transactionData.setDescription("IMPS-FUNDTRANSFER");
        transactionData.setToAccount(payee.getAcctno()); /*selected account number*/
        transactionData.setBenyName(payee.getBenefname());/*selected benifacry name*/
        transactionData.setIfscCode(payee.getIfsccode());
        transactionData.setBankName(payee.getBankname());
        transactionData.setSenderName(getSenderName(fromAccountNumber));
        TextView fromAccount = (TextView) inflatedLayoutView.findViewById(R.id.fromAccount);
        TextView fromAccountBal = (TextView) inflatedLayoutView.findViewById(R.id.fromAccountBalance);
        fromAccount.setText(getString(R.string.fromaccount) + " " + fromAccountNumber);//customer acc number

        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
        Double balanceFormate = Double.parseDouble(getAccbalance(fromAccountNumber));
        String balance = format.format(balanceFormate);
        fromAccountBal.setText(getString(R.string.balance) + " " + balance);

    }
    /*Request Data End*/


    /*Response data*/
    private void getLoginResponseObject(String responseData) {
  if (responseData != null && !responseData.equalsIgnoreCase("")) {
            GlobalResponse loginresObj = new Gson().fromJson(responseData.toString(), GlobalResponse.class);
            if (loginresObj.getCode().equals("0")) {
                try {
                    String key = EncryptionUtil.getDecryptedVal(Constants.publicKey, loginresObj.getSalt());
                    String loginResponse = EncryptionUtil.getDecryptedVal(key, loginresObj.getOutString());
                    parseLoginResponse(loginResponse);
                    visibilityType(View.GONE);
                    openSelectPayee();
                    pin = "";
                    invalidPinCount = 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                visibilityType(View.GONE);
                enable(keyboardContentLayout);
                if (loginresObj.getCode().equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(loginresObj.getMessage().toString());
                    return;
                }
                else if (loginresObj.getCode().equals(ERR_AUTHENTICATION_FAIL)) {
                    interBankpayeeList.clear();
                    otherBankpayeeList.clear();
                    errorMessage.setText("Authentication Failure");
                    pin = "";
                    clearPIN();
                    return;
                }
                else  if (loginresObj.getCode().equals(ERR_NUMBER_OF_ATTEMPTS_EXCEED)) {
         /*  home.setTex t("Exit");*/
                    errorMessage.setText("Your account is locked. Please try tomorrow or Re-register.");
                    Utils.setUserLocked(context);
                    pin = "";
                    clearPIN();
                    return;
                }
                else  if (loginresObj.getCode().equals(ERR_ACCOUNT_LOCKED)) {
         /*  home.setTex t("Exit");*/
                    setInputView( welcomeScreen("Re-Activate"));
                    Utils.setUserLocked(context);
                    pin = "";
                    clearPIN();
                    return;
                }
                failureResponse(loginresObj.getCode(), loginresObj.getMessage().toString());
            }
        } else {
            failureResponse("-1", "Server Not Responding");
        }
    }


    private void parseLoginResponse(String response) {
        response = removeschema(response);
        try {
            Log.d(TAG,"LoginResponce"+response);
            JSONObject jsonObject = new JSONObject(response.toString());
            loginResponse = new LoginResponse();
            try {
                loginResponse.setCustomerid(jsonObject.getString("customerid"));
                loginResponse.setSessionKey(jsonObject.getString("SessionKey"));
                loginResponse.setCustName(jsonObject.getString("CustomerName"));
                loginResponse.setCODE(jsonObject.getJSONObject("STATUS").getString("CODE"));
                loginResponse.setMESSAGE(jsonObject.getJSONObject("STATUS").getString("MESSAGE"));
                loginResponse.setSEVERITY(jsonObject.getJSONObject("STATUS").getString("SEVERITY"));
                Object account_summaryObj = (jsonObject.getJSONObject("Account_Summary"));
                if (account_summaryObj instanceof JSONObject) {
                    JSONObject summarydetailj = (JSONObject) account_summaryObj;
                    loginResponse.setSavingacctcount("savingacctcount");
                    loginResponse.setCurrentacctcount("currentacctcount");

                    JSONArray account_detailArry = summarydetailj.getJSONArray("acctdetails");
                        LoginAccountDetailsType genEsttModel;
                        ArrayList<LoginAccountDetailsType> accountObjlist = new ArrayList<>();
                        JSONObject object;
                        for (int i = 0; i < account_detailArry.length(); i++) {
                            object = account_detailArry.getJSONObject(i);
                            genEsttModel = new LoginAccountDetailsType();
                            genEsttModel.setAcctType(object.getString("acctType"));
                            genEsttModel.setAcctbalance(object.getString("acctbalance"));
                            genEsttModel.setClearbal(object.getString("clearbal"));
                            genEsttModel.setAcctindex(object.getString("acctindex"));
                            genEsttModel.setCurrency(object.getString("currency"));
                            genEsttModel.setAcctno(object.getString("acctno"));
                            genEsttModel.setBranchname(object.getString("branchname"));
                            accountObjlist.add(genEsttModel);
                        }
                        accountList.addAll(accountObjlist);

                    JSONArray CustDetailsArry = summarydetailj.getJSONArray("CustDetails");
                        CustomerAccountDetails gencstDetailsModel;
                        ArrayList<CustomerAccountDetails> customerAccountDetailses = new ArrayList<>();
                        JSONObject customerdetail;
                        for (int i = 0; i < CustDetailsArry.length(); i++) {
                            customerdetail = CustDetailsArry.getJSONObject(i);
                            gencstDetailsModel = new CustomerAccountDetails();
                            gencstDetailsModel.setAcctindex(customerdetail.getString("acctindex"));
                            gencstDetailsModel.setJointAcct(customerdetail.getString("jointAcct"));
                            gencstDetailsModel.setCustName(customerdetail.getString("custName"));
                            gencstDetailsModel.setAcctno(customerdetail.getString("acctno"));
                            customerAccountDetailses.add(gencstDetailsModel);
                        }

                        customerAccountDetailsArrayList.addAll(customerAccountDetailses);
                } else {
                    ArrayList<LoginAccountDetailsType> accountObjlist = new ArrayList<>();
                    loginResponse.setAccount_Summary(jsonObject.getString("Account_Summary"));
                    accountList.addAll(accountObjlist);
                }
                Object Imps_BeneficiariesObj = (jsonObject.getJSONObject("Imps_Beneficiaries"));
                if (Imps_BeneficiariesObj instanceof JSONObject) {
                    JSONObject impsdetails = (JSONObject) Imps_BeneficiariesObj;
                    JSONArray impstptacctdetailsArry = impsdetails.getJSONArray("tptacctdetails");
                        TptAcctDetails genEsttModel;
                        ArrayList<TptAcctDetails> impsregObjlist = new ArrayList<>();
                        JSONObject impsobject;
                        for (int i = 0; i < impstptacctdetailsArry.length(); i++) {
                            impsobject = impstptacctdetailsArry.getJSONObject(i);
                            genEsttModel = new TptAcctDetails();
                            genEsttModel.setBenefname(impsobject.getString("benefname"));
                            genEsttModel.setBankname(impsobject.getString("bankname"));
                            genEsttModel.setIfsccode(impsobject.getString("ifsccode"));
                            genEsttModel.setAcctno(impsobject.getString("acctno"));
                            impsregObjlist.add(genEsttModel);
                        }
                        otherBankpayeeList.addAll(impsregObjlist);

                } else {
                    loginResponse.setImps_Beneficiaries(jsonObject.getString("Imps_Beneficiaries"));
                }

                Object Intra_Bank_BeneficiariesObj = (jsonObject.getJSONObject("Intra_Bank_Beneficiaries"));
                if (Intra_Bank_BeneficiariesObj instanceof JSONObject) {
                    JSONObject interbankdetails = (JSONObject) Intra_Bank_BeneficiariesObj;
                    JSONArray intertptacctdetailsArry = interbankdetails.getJSONArray("tptacctdetails");
                        InterTptAcctDetails genEsttModel;
                        ArrayList<InterTptAcctDetails> interregObjlist = new ArrayList<>();
                        JSONObject impsobject;
                        for (int i = 0; i < intertptacctdetailsArry.length(); i++) {
                            impsobject = intertptacctdetailsArry.getJSONObject(i);
                            genEsttModel = new InterTptAcctDetails();
                            genEsttModel.setBenefname(impsobject.getString("benefname"));
                            genEsttModel.setAcctno(impsobject.getString("acctno"));
                            genEsttModel.setType(false);
                            interregObjlist.add(genEsttModel);
                        }
                        interBankpayeeList.addAll(interregObjlist);
                } else {
                    loginResponse.setIntra_Bank_Beneficiaries(jsonObject.getString("Intra_Bank_Beneficiaries"));
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*OTP RESPONSE OBJECT*/

    private void getOTPResponseObject(String responseData) {
        if (responseData != null && !responseData.equalsIgnoreCase("")) {
            GlobalResponse otpresObj = new Gson().fromJson(responseData.toString(), GlobalResponse.class);
            if (otpresObj.getCode().equals("0")) {
                try {
                    String key = EncryptionUtil.getDecryptedVal(Constants.publicKey, otpresObj.getSalt());
                    String otpResponse = EncryptionUtil.getDecryptedVal(key, otpresObj.getOutString());
                    psrseOTPResponse(otpResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                visibilityType(View.GONE);
                enable(keyboardenterAmountLayout);
                if (otpresObj.getCode().equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(otpresObj.getMessage().toString());
                    return;
                }
                failureResponse(otpresObj.getCode(), otpresObj.getMessage().toString());

            }
        } else {
            failureResponse("-1", "Server Not Responding");
//            errorMessage.setText("Server Not Responding");
        }
    }

    private View welcomeScreen(final String msg) {

        View myView = View.inflate(this, R.layout.keyboard_welcome_layout, null);
        TextView header = (TextView) myView.findViewById(R.id.messageLabel);
        TextView message = (TextView) myView.findViewById(R.id.failureMessage);
        Button home = (Button) myView.findViewById(R.id.home);
        ImageButton switchtoNewKeybord = (ImageButton) myView.findViewById(R.id.changeLanguage);
        mode = "FAILURE";
        String failmsg = "";
        if (msg.equalsIgnoreCase("Activate")) {
            failmsg = "Please complete one time activation of MoBank to access this ChatPay";
            header.setVisibility(View.VISIBLE);
        } else {
            failmsg = "Your Account is locked. Please try tomorrow or Re-register MoBank";
            header.setVisibility(View.INVISIBLE);
        }
        header.setText("Welcome to RBL ChatPay");
        message.setText(failmsg);
        home.setText(msg);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchIntent = getPackageManager().getLaunchIntentForPackage(Constants.PACKAGE_NAME);
                if (launchIntent != null) {
                    // Bring user to the market or let them choose an app?
                    launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(launchIntent);
                }


            }
        });
        switchtoNewKeybord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin = "";
                clearPIN();
                switchToPreviousKeyboard(null);
            }
        });
        return myView;
    }

    private View rootScreen() {

        mode = "FAILURE";
        String failmsg = "";

        failmsg = "For Security purpose RBL Bank won't run on rooted device.!";
        LayoutInflater inflater = LayoutInflater.from(context);
        View myView = View.inflate(this, R.layout.keyboard_welcome_layout, null);
        TextView header = (TextView) myView.findViewById(R.id.messageLabel);
        header.setText("Security Alert");
        TextView message = (TextView) myView.findViewById(R.id.failureMessage);
        message.setText(failmsg);
        Button home = (Button) myView.findViewById(R.id.home);
        ImageButton switchtoNewKeybord = (ImageButton) myView.findViewById(R.id.changeLanguage);
        home.setText("");
        home.setVisibility(View.INVISIBLE);
        switchtoNewKeybord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin = "";
                clearPIN();
                switchToPreviousKeyboard(null);
            }
        });
        return myView;
    }

    private void errorMessageScreen(final String errormessage) {

        mode = "FAILURE";
        LayoutInflater inflater = LayoutInflater.from(context);
        View myView = inflater.inflate(R.layout.errormessage_layoutt, null, false);
        keyboardContentLayoutHeight = keyboardContentLayout.getHeight();
        keyboardContentLayout.addView(myView);
        String failmsg = "Something went wrong";
        if (errormessage != null) {
            failmsg = errormessage.toString();
        }
        RelativeLayout failureLayout = (RelativeLayout) myView.findViewById(R.id.failureLayout);
        TextView message = (TextView) myView.findViewById(R.id.failureMessage);
        message.setText(failmsg);
        Button home = (Button) myView.findViewById(R.id.home);
        ImageButton switchtoNewKeybord = (ImageButton) myView.findViewById(R.id.changeLanguage);
        home.setText("LOGOUT");
        ViewGroup.LayoutParams params = failureLayout.getLayoutParams();
        params.height = keyboardContentLayoutHeight;
        failureLayout.setLayoutParams(params);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin = "";
                clearPIN();
                interBankPayee = new InterTptAcctDetails();
                impsBankPayee = new TptAcctDetails();
                currentAccount = new LoginAccountDetailsType();
                loginResponse = new LoginResponse();
                transactionResponse = new TransactionResponse();
                interBankpayeeList.clear();
                otherBankpayeeList.clear();
                mode = "LOGIN";
                //onCreateInputView();
                setInputView(onCreateInputView());
             /*   enable(keyboardContentLayout);*/
            }
        });
        switchtoNewKeybord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pin = "";
                clearPIN();
                switchToPreviousKeyboard(null);
            }
        });
    }

    private void failureResponse(String errorCode, String failureResponse) {

        mode = "FAILURE";
        LayoutInflater inflater = LayoutInflater.from(context);
        View myView = inflater.inflate(R.layout.errormessage_layoutt, null, false);
        keyboardContentLayoutHeight = keyboardContentLayout.getHeight();
        keyboardContentLayout.addView(myView);
        String failmsg = "Something went wrong";
        if (failureResponse != null) {
            failmsg = failureResponse.toString();
        }
        RelativeLayout failureLayout = (RelativeLayout) myView.findViewById(R.id.failureLayout);
        TextView message = (TextView) myView.findViewById(R.id.failureMessage);
        message.setText(failmsg);
        Button home = (Button) myView.findViewById(R.id.home);
        if (interBankpayeeList != null && interBankpayeeList.size() > 0 && otherBankpayeeList != null && otherBankpayeeList.size() > 0) {
            home.setText("Home");

        }
        ViewGroup.LayoutParams params = failureLayout.getLayoutParams();
        params.height = keyboardContentLayoutHeight;
        failureLayout.setLayoutParams(params);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                visibilityType(View.GONE);
                pin = "";
                clearPIN();
                if (interBankpayeeList != null && interBankpayeeList.size() > 0 && otherBankpayeeList != null && otherBankpayeeList.size() > 0) {
                    openSelectPayee();
                } else {
                    mode = "LOGIN";
                    setInputView(onCreateInputView());
                  /*  initialiseKeyboardLoginVariables(currentView);*/
                }
            }
        });

    }

    private void psrseOTPResponse(String response) {
        response = removeschema(response);
        try {
            JSONObject jsonObject = new JSONObject(response.toString());
            RblTxSessionManager.getInstance().getTransactionData().setOtpRefNumber((jsonObject.getJSONObject("Response").getString("otprefno")));
            sendOTP();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIMPSResponseObject(String impsResponse) {
        if (impsResponse != null && !impsResponse.equalsIgnoreCase("")) {
            GlobalResponse IMPSresObj = new Gson().fromJson(impsResponse.toString(), GlobalResponse.class);
            if (IMPSresObj.getCode().equals("0")) {
                try {
                    String key = EncryptionUtil.getDecryptedVal(Constants.publicKey, IMPSresObj.getSalt());
                    String otpResponse = EncryptionUtil.getDecryptedVal(key, IMPSresObj.getOutString());
                    parseIMPSResponse(otpResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                visibilityType(View.GONE);
                enable(keyboardotplayout);
//                errorMessage.setText(IMPSresObj.getMessage().toString());
                if (IMPSresObj.getCode().equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(IMPSresObj.getMessage().toString());
                    return;
                }
                else if (IMPSresObj.getCode().equals("1500"))
                {
                    errorMessage.setText(getString(R.string.invalid_otp));
                    pin = "";
                    clearPIN();
                    RblTxSessionManager.getInstance().getTransactionData().setOtp(pin);
                    enableResendOTP();
                    disableLoginOrOTPButton("SEND OTP");
                    return;
                }
                failureResponse(IMPSresObj.getCode(), IMPSresObj.getMessage().toString());
            }
        } else {
            Toast.makeText(context, "Server Not Responding", Toast.LENGTH_SHORT).show();
        }
    }

    private void parseIMPSResponse(String impsResponse) {

        impsResponse = removeschema(impsResponse);
        try {
            JSONObject jsonObject = new JSONObject(impsResponse.toString());
            String status = jsonObject.getJSONObject("STATUS").getString("CODE");
            String message = jsonObject.getJSONObject("STATUS").getString("MESSAGE");
            if (status.equals("0")) {
                errorMessage.setText("");
                transactionResponse = new TransactionResponse();
                if (jsonObject.getJSONObject("Response").has("txnrefno")){
                    referenceNumber =  (jsonObject.getJSONObject("Response").getString("txnrefno"));
                    transactionResponse.setTxnrefno(jsonObject.getJSONObject("Response").getString("txnrefno"));
                }
                transactionResponse.setCustomername(jsonObject.getJSONObject("Response").getString("customername"));
                transactionResponse.setFromacctno(jsonObject.getJSONObject("Response").getString("fromacctno"));
                transactionResponse.setToacctno(jsonObject.getJSONObject("Response").getString("toacctno"));
                transactionResponse.setBenefname(jsonObject.getJSONObject("Response").getString("benefname"));
                transactionResponse.setBankname(jsonObject.getJSONObject("Response").getString("bankname"));
                transactionResponse.setIfsccode(jsonObject.getJSONObject("Response").getString("ifsccode"));
                transactionResponse.setTxndesc(jsonObject.getJSONObject("Response").getString("txndesc"));
                transactionResponse.setAmttxn(jsonObject.getJSONObject("Response").getString("amttxn"));
                success(transactionResponse);
            } else {
                visibilityType(View.GONE);
//                errorMessage.setText(message);
                if (status.equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(message);
                    return;
                }
                failureResponse(status, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIFTResponseObject(String iftresponse) {
        iftresponse = removeschema(iftresponse);
        if (iftresponse != null && !iftresponse.equalsIgnoreCase("")) {
            GlobalResponse iftresObj = new Gson().fromJson(iftresponse.toString(), GlobalResponse.class);
            if (iftresObj.getCode().equals("0")) {
                try {
                    String key = EncryptionUtil.getDecryptedVal(Constants.publicKey, iftresObj.getSalt());
                    String iftResponse = EncryptionUtil.getDecryptedVal(key, iftresObj.getOutString());
                    parseIFTResponse(iftResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                visibilityType(View.GONE);
                enable(keyboardotplayout);
//                errorMessage.setText(iftresObj.getMessage().toString());

                if (iftresObj.getCode().equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(iftresObj.getMessage().toString());
                    return;
                }
                else if (iftresObj.getCode().equals("1500"))
                {
                    errorMessage.setText(getString(R.string.invalid_otp));
                    pin = "";
                    clearPIN();
                    RblTxSessionManager.getInstance().getTransactionData().setOtp(pin);
                    enableResendOTP();
                    disableLoginOrOTPButton("SEND OTP");
                    return;

                }
                failureResponse(iftresObj.getCode(), iftresObj.getMessage().toString());
            }
        } else {
            failureResponse("-1", "Server Not Responding");
//            errorMessage.setText("Server Not Responding");
        }
    }

    private void parseIFTResponse(String iftResponse) {
        iftResponse = removeschema(iftResponse);
        try {
            JSONObject jsonObject = new JSONObject(iftResponse.toString());
            String status = jsonObject.getJSONObject("STATUS").getString("CODE");
            String message = jsonObject.getJSONObject("STATUS").getString("MESSAGE");
            if (status.equals("0")) {
                errorMessage.setText("");
                transactionResponse = new TransactionResponse();
                referenceNumber =  (jsonObject.getJSONObject("Response").getString("txnrefno"));
                transactionResponse.setTxnrefno(jsonObject.getJSONObject("Response").getString("txnrefno"));
                transactionResponse.setFromacctno(jsonObject.getJSONObject("Response").getString("fromacctno"));
                transactionResponse.setToacctno(jsonObject.getJSONObject("Response").getString("toacctno"));
                transactionResponse.setBenefname(jsonObject.getJSONObject("Response").getString("benefname"));
                transactionResponse.setBankname(jsonObject.getJSONObject("Response").getString("bankname"));
                transactionResponse.setIfsccode(jsonObject.getJSONObject("Response").getString("ifsccode"));
                transactionResponse.setTxndesc(jsonObject.getJSONObject("Response").getString("txndesc"));
                transactionResponse.setAmttxn(jsonObject.getJSONObject("Response").getString("amttxn"));
                success(transactionResponse);
            } else {
                visibilityType(View.GONE);
//                errorMessage.setText(message);
                if (status.equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(message);
                    return;
                }
                failureResponse(status, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getBalanceEnquiryResponse(String balanceEnquiryresponse) {
        balanceEnquiryresponse = removeschema(balanceEnquiryresponse);
        if (balanceEnquiryresponse != null && !balanceEnquiryresponse.equalsIgnoreCase("")) {
            GlobalResponse balanceEnquiryresObj = new Gson().fromJson(balanceEnquiryresponse.toString(), GlobalResponse.class);
            if (balanceEnquiryresObj.getCode().equals("0")) {
                try {
                    visibilityType(View.GONE);
                    String key = EncryptionUtil.getDecryptedVal(Constants.publicKey, balanceEnquiryresObj.getSalt());
                    String balanceEnquiryResponse = EncryptionUtil.getDecryptedVal(key, balanceEnquiryresObj.getOutString());
                    parseBalanceEnquiryResponse(balanceEnquiryResponse);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                visibilityType(View.GONE);
//                errorMessage.setText(balanceEnquiryresObj.getMessage().toString());
                if (balanceEnquiryresObj.getCode().equals(ERR_SESSION_EXPIRE)) {
                    errorMessageScreen(balanceEnquiryresObj.getMessage().toString());
                    return;
                }
                failureResponse(balanceEnquiryresObj.getCode(), balanceEnquiryresObj.getMessage().toString());
            }
        } else {
            failureResponse("-1", "Server Not Responding");
        }
    }

    private void parseBalanceEnquiryResponse(String balanceEnquiryResponse) {

        if (accountList != null && accountList.size() > 0) {
            accountList.clear();
        }
        balanceEnquiryResponse = removeschema(balanceEnquiryResponse);
        try {
            JSONObject jsonObject = new JSONObject(balanceEnquiryResponse.toString());
            try {
                Object account_summaryObj = (jsonObject.getJSONObject("Account_Summary"));
                if (account_summaryObj instanceof JSONObject) {
                    JSONObject summarydetailj = (JSONObject) account_summaryObj;
                    JSONArray account_detailArry = summarydetailj.getJSONArray("acctdetails");
                    LoginAccountDetailsType genEsttModel;
                    ArrayList<LoginAccountDetailsType> accountObjlist = new ArrayList<>();
                    JSONObject object;
                    for (int i = 0; i < account_detailArry.length(); i++) {
                        object = account_detailArry.getJSONObject(i);
                        genEsttModel = new LoginAccountDetailsType();
                        genEsttModel.setAcctType(object.getString("acctType"));
                        genEsttModel.setAcctbalance(object.getString("acctbalance"));
                        genEsttModel.setClearbal(object.getString("clearbal"));
                        genEsttModel.setAcctindex(object.getString("acctindex"));
                        genEsttModel.setCurrency(object.getString("currency"));
                        genEsttModel.setAcctno(object.getString("acctno"));
                        genEsttModel.setBranchname(object.getString("branchname"));
                        accountObjlist.add(genEsttModel);
                    }
                    accountList.addAll(accountObjlist);
                }
                openSelectPayee();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String removeschema(String response) {
        String schema = response.substring(0, response.indexOf(">") + 1);
        response = response.toString().replaceAll(schema, "");
        response = response.toString().replaceAll("</NS1:RBL>", "");
        response = response.toString().replaceAll("<NS1:", "<");
        response = response.toString().replaceAll("</NS1:", "</");
        return response;
    }

    //clearpin
    private void clearPIN() {
        pin = "";
        for (int i = 0; i < Pins.length; i++) {
            if (Pins[i] != null) {
                Pins[i].setBackgroundResource(R.drawable.key_state_not_pressed);
                Pins[i].setText("");
            }
        }
    }
    /**
     * This is local broadCast to receive SMS for SMSListener
     */
    private BroadcastReceiver incomingSMSReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            visibilityType(View.GONE);
            String smsData = intent.getStringExtra(Constants.KEY_SMS);
            String substring=smsData.substring(0,6);
//                        otpEdit.setText(substring);
            Pins[0].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[1].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[2].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[3].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[4].setBackgroundResource(R.drawable.key_state_pressed);
            Pins[5].setBackgroundResource(R.drawable.key_state_pressed);
            pin=substring;
            Log.v(TAG, " --- SMS Data ----- " + smsData);
            RblTxSessionManager.getInstance().getTransactionData().setOtp(pin);
            visibilityType(View.VISIBLE);
            disable(keyboardotplayout);

            if (RblTxSessionManager.getInstance().getTransactionType().equals(rblIFTTransaction))
                InterBankTransactionRequest();
            else
                impsTransactionRequest();
            LocalBroadcastManager.getInstance(context).unregisterReceiver(incomingSMSReceiver);
        }
    };
}
