package hexadots.in.rblmobank.keyboardbanking;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


/**
 * Listener that detects shake gesture.
 */
public class TiltListener implements SensorEventListener {

    private static final int MIN_SHAKE_ACCELERATION = 9;

    private static final int MIN_MOVEMENTS = 1;

    private static final int MAX_SHAKE_DURATION = 100;

    private float[] mGravity = {0.0f, 0.0f, 0.0f};
    private float[] mLinearAcceleration = {0.0f, 0.0f, 0.0f};

    private static final int X = 0;
    private static final int Y = 1;
    private static final int Z = 2;

    private onTiltListener mTiltlistener;

    long startTime = 0;

    int moveCount = 0;

    // Constructor that sets the shake listener
    public TiltListener(onTiltListener tiltListener) {
        mTiltlistener = tiltListener;
    }

    private Context context;

    public TiltListener(Context context) {
        this.context = context;
    }

    public void setOnTiltListener(onTiltListener tiltListener) {
        mTiltlistener = tiltListener;
    }


    private float mLastX, mLastY, mLastZ;
    private boolean mInitialized;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private final float NOISE = (float) 8.0;
    boolean accelerating = false;

    @Override
    // This method will be called when the accelerometer detects a change.
    public void onSensorChanged(SensorEvent event) {

        setCurrentAcceleration(event);
        float maxLinearAcceleration = getMaxCurrentLinearAcceleration();

        if (maxLinearAcceleration > MIN_SHAKE_ACCELERATION) {
            long now = System.currentTimeMillis();

            // Set the startTime if it was reset to zero
            if (startTime == 0) {
                startTime = now;
            }

            long elapsedTime = now - startTime;

            // Check if we're still in the shake window we defined
            if (elapsedTime > MAX_SHAKE_DURATION) {
                // Too much time has passed. Start over!
                resetTiltDetection();
            } else {
                // Keep track of all the movements
                moveCount++;

                // Check if enough movements have been made to qualify as a shake
                if (moveCount >= MIN_MOVEMENTS) {
                    // It's a shake! Notify the listener.

                    mTiltlistener.ontiltDetection();

                    // Reset for the next one!
                    resetTiltDetection();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Intentionally blank
    }

    private void setCurrentAcceleration(SensorEvent event) {

        final float alpha = 0.8f;

        // Gravity components of x, y, and z acceleration
        mGravity[X] = alpha * mGravity[X] + (1 - alpha) * event.values[X];
        mGravity[Y] = alpha * mGravity[Y] + (1 - alpha) * event.values[Y];
        mGravity[Z] = alpha * mGravity[Z] + (1 - alpha) * event.values[Z];

        // Linear acceleration along the x, y, and z axes (gravity effects removed)
        mLinearAcceleration[X] = event.values[X] - mGravity[X];
        mLinearAcceleration[Y] = event.values[Y] - mGravity[Y];
        mLinearAcceleration[Z] = event.values[Z] - mGravity[Z];

        /*
         *  END SECTION from Android developer site
         */
    }

    private float getMaxCurrentLinearAcceleration() {
        // Start by setting the value to the x value
        float maxLinearAcceleration = mLinearAcceleration[X];

        // Check if the y value is greater
        if (mLinearAcceleration[Y] > maxLinearAcceleration) {
            maxLinearAcceleration = mLinearAcceleration[Y];
        }

        // Check if the z value is greater
        if (mLinearAcceleration[Z] > maxLinearAcceleration) {
            maxLinearAcceleration = mLinearAcceleration[Z];
        }

        // Return the greatest value
        return maxLinearAcceleration;
    }

    private void resetTiltDetection() {
        startTime = 0;
        moveCount = 0;
    }

    public interface onTiltListener{
        void ontiltDetection();
    }
}
