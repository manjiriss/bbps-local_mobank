package hexadots.in.rblmobank.beans;


public class TransactionResponse {

    private String txnrefno;
    private String customername;
    private String customerid;
    private String fromacctno;
    private String toacctno;
    private String benefname;
    private String bankname;
    private String ifsccode;
    private String txndesc;
    private String amttxn;





    public String getTxnrefno() {
        return txnrefno;
    }

    public void setTxnrefno(String txnrefno) {
        this.txnrefno = txnrefno;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getFromacctno() {
        return fromacctno;
    }

    public void setFromacctno(String fromacctno) {
        this.fromacctno = fromacctno;
    }

    public String getToacctno() {
        return toacctno;
    }

    public void setToacctno(String toacctno) {
        this.toacctno = toacctno;
    }

    public String getBenefname() {
        return benefname;
    }

    public void setBenefname(String benefname) {
        this.benefname = benefname;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getIfsccode() {
        return ifsccode;
    }

    public void setIfsccode(String ifsccode) {
        this.ifsccode = ifsccode;
    }

    public String getTxndesc() {
        return txndesc;
    }

    public void setTxndesc(String txndesc) {
        this.txndesc = txndesc;
    }

    public String getAmttxn() {
        return amttxn;
    }

    public void setAmttxn(String amttxn) {
        this.amttxn = amttxn;
    }
}
