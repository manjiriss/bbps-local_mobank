package hexadots.in.rblmobank.beans;

import java.util.ArrayList;

public class LoginData {

    String customerid;
    String SessionKey;
    Status STATUS;

    class Status{
        String CODE;
        String MESSAGE;
        String SEVERITY;

    }

    public Accountsummary getAccount_Summary() {
        return Account_Summary;
    }

    private void setAccount_Summary(Accountsummary account_Summary) {
        Account_Summary = account_Summary;
    }

    private Accountsummary Account_Summary;

   private class Accountsummary{
        AccountDetails acctdetails;


         class AccountDetails {
            String acctdetails;
            String acctType;
            String acctbalance;
            String clearbal;
            String acctindex;
            String currency;
            String acctno;
            String branchname;

        }
        private CustomerDetails CustDetails;

        private class CustomerDetails{
           String acctindex;
            String jointAcct;

            String  custName;
            String  acctno;

        }
        String savingacctcount;
        String currentacctcount;
    }
    ImpsBeneList Imps_Beneficiaries;
    class ImpsBeneList{
       ArrayList<Payee> tptacctdetails;
    }

    public  ArrayList<Payee> getIMPSBeneList(){
        return Imps_Beneficiaries.tptacctdetails;
    }
    IntraBankBeneList Intra_Bank_Beneficiaries;
    class IntraBankBeneList{
        ArrayList<Payee> tptacctdetails;
    }

    public  ArrayList<Payee> getIntraBankBeneList(){
        return Intra_Bank_Beneficiaries.tptacctdetails;
    }

    public String getCustName() {
        return Account_Summary.CustDetails.custName;
    }
    public String getAccBal() {
        return Account_Summary.acctdetails.acctbalance;
    }
    public String getAccNo() {
        return Account_Summary.acctdetails.acctno;
    }



//    private class TPaccDetails{
//        String  benefname;
//
//        private String getBenefname() {
//            return benefname;
//        }
//
//        private void setBenefname(String benefname) {
//            this.benefname = benefname;
//        }
//
//        private String getBankname() {
//            return bankname;
//        }
//
//        private void setBankname(String bankname) {
//            this.bankname = bankname;
//        }
//
//        private String getIfsccode() {
//            return ifsccode;
//        }
//
//        private void setIfsccode(String ifsccode) {
//            this.ifsccode = ifsccode;
//        }
//
//        private String getAcctno() {
//            return acctno;
//        }
//
//        private void setAcctno(String acctno) {
//            this.acctno = acctno;
//        }
//
//        String  bankname;
//        String  ifsccode;
//        String  acctno;
//    }

}
