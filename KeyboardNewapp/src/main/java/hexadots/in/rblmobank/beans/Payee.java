package hexadots.in.rblmobank.beans;

public class Payee {
    private String benefname;
    private String acctno;
    private String bankName;

    private String ifsccode;

    public Payee() {
    }

    public Payee(String payeeName, String payeeAccountNumber,String bankName,String ifsccode) {
        this.benefname = payeeName;
        this.acctno = payeeAccountNumber;
        this.bankName=bankName;
        this.ifsccode=ifsccode;
    }

    public String getPayeeName() {
        return benefname;
    }

    public void setPayeeName(String payeeName) {
        this.benefname = payeeName;
    }

    public String getPayeeAccountNumber() {
        return acctno;
    }

    public void setPayeeAccountNumber(String payeeAccountNumber) {
        this.acctno = payeeAccountNumber;
    }
    public String getIfsccode() {
        return ifsccode;
    }

    public void setIfsccode(String ifsccode) {
        this.ifsccode = ifsccode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}