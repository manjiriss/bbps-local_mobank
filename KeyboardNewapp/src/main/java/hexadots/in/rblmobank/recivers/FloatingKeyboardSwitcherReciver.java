package hexadots.in.rblmobank.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import hexadots.in.rblmobank.utils.Constants;
import hexadots.in.rblmobank.utils.Utils;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Hexadots on 05-12-2016.
 */

public class FloatingKeyboardSwitcherReciver extends BroadcastReceiver {

    private static final String TAG = FloatingKeyboardSwitcherReciver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v(TAG, "---- Boot completed received ------");
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.KEY_PREF_NAME, context.MODE_PRIVATE);
        Log.v(TAG, "---- Boot completed received Preference value ------" + sharedPreferences.getBoolean(Constants.KEY_PREF_BOOT, false));
        if (sharedPreferences != null && sharedPreferences.getBoolean(Constants.KEY_PREF_BOOT, false)) {
            Intent in = new Intent(context, Constants.class);
            if (in != null) {
                Log.v(TAG, "---- Starting service  ------");
                context.startService(in);
            }
        }
    }
}
