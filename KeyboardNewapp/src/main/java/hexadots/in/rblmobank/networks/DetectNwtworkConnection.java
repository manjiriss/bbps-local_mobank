package hexadots.in.rblmobank.networks;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Mani P on 11/6/2016.
 */

public class DetectNwtworkConnection {


    public static boolean checkInternetConnection(Context context) {

        ConnectivityManager con_manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        return con_manager.getActiveNetworkInfo() != null
                && con_manager.getActiveNetworkInfo().isAvailable()
                && con_manager.getActiveNetworkInfo().isConnected();
    }
}

