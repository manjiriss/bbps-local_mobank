package hexadots.in.rblmobank.requestobjects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import hexadots.in.rblmobank.utils.Constants;
import hexadots.in.rblmobank.utils.EncryptionUtil;
import hexadots.in.rblmobank.utils.URLS;

/**
 * Created by Mani P on 10/27/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GlobalObjRequest {


    private String Checksum;
    private String OPERATIONID;
    private String Salt;
    private String InputString;
    private String SessionKey;

    public String getChecksum() {
        return Checksum;
    }

    public void setChecksum(String checksum) {
        Checksum = checksum;
    }

    public String getOPERATIONID() {
        return OPERATIONID;
    }

    public void setOPERATIONID(String OPERATIONID) {
        this.OPERATIONID = OPERATIONID;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }

    public String getInputString() {
        return InputString;
    }

    public void setInputString(String inputString) {
        InputString = inputString;
    }

    public String getSessionKey() {
        return SessionKey;
    }

    public void setSessionKey(String sessionKey) {
        SessionKey = sessionKey;
    }
}
